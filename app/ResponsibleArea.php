<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsibleArea extends Model
{
    protected $fillable = ['id', 'name', 'contacto'];

    public function subareas() {
        return $this->hasMany('App\SubArea', 'responsable_area_id');
    }
}
