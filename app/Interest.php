<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{
	protected $table = 'interests';
    protected $fillable = [
        'id', 'title', 'description', 'deleted'
    ];
    protected $hidden = ['pivot', 'deleted', 'created_at', 'updated_at'];
}
