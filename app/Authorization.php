<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    protected $table = 'authorizations_openkey';
    protected $fillable = ['user_id', 'IdPersona', 'code', 'valid_from', 'valid_to', 'used', 'log'];
    protected $dates = ['valid_from', 'valid_to'];

    public function uer() {
        return $this->hasMany('App\User', 'user_id');
    }
}