<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id', 'description' ,'amount', 'status', 'type', 'method', 'created_at', 'updated_at'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function getDescription() {
        switch ($this->type) {
            case 'creditos-gym':
                return 'Creditos Gimnasio Costa Esmeralda';
            case 'reserva-ecuestre':
                return 'Reserva Ecuestre Costa Esmeralda';
            case 'reserva-deportiva':
                return 'Reserva Deportiva Costa Esmeralda';
            default: 
                return 'Reserva Costa Esmeralda';
        }
    }

    public function getStatusAdmin() {
        switch ($this->status) {
            case 'pending':
                return 'Pendiente de Pago (via MP)';
            case 'approved':
                return 'Aprobado (via MP)';
            case 'canceled':
                return 'Cancelado/Rechazado (via MP)';
            case 'approved-manual':
                return 'Agregado Via Admin';
        }
    }
}
