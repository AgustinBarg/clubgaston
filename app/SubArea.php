<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubArea extends Model
{
    protected $fillable = ['responsable_area_id'];

    public function responsableArea()
    {
        return $this->belongsTo('App\ResponsibleArea');
    }

    public $timestamps = false;
}
