<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaValidation extends Model
{
    protected $table = 'reservas_validation';
    protected $fillable = ['date', 'enabled_validation'];
}