<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaTimes extends Model
{
    protected $table = 'reservas_times';
    protected $fillable = [
        'time'
    ];
}