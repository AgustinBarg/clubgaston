<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model {
    protected $table = 'audits_log';
    protected $fillable = ['user_id', 'ip', 'headers', 'data'];

    public function user() {
        return $this->hasOne('App\User');
    }
}