<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 6/18/17
 * Time: 4:12 PM
 */

namespace App\Providers;


use App\ticketsIncidente;
use Illuminate\Support\ServiceProvider;
use App\Repositories\IncidenteRepository;

class RepositoriesServicesProvider extends ServiceProvider
{
    public function register() {
        $this->app->bind('App\Repositories\IncidenteInterface', function($app) {
            return new IncidenteRepository(new ticketsIncidente());
        });
    }
}