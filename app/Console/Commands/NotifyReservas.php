<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ReservasNotificationsService;

class NotifyReservas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'costa:notify-reservas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify Resrvas: Push para reserva media hora antes ';

    protected $reservasNotificationService;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(ReservasNotificationsService $reservasNotificationService)
    {
        parent::__construct();
        $this->reservasNotificationService = $reservasNotificationService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Import...');
            $this->reservasNotificationService->notifyReservas($this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

