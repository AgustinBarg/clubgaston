<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ReservasGymService;

class CleanCredits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'costa:clean-credits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Credits: Limpiar Creditos Packages sin usar.';

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Clean Credits...');
            ReservasGymService::cleanCredits($this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

