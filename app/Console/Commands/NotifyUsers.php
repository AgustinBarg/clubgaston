<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Repositories\UserRepository;

use App\Services\NotificationService;


class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify-users {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify Users: test push notifications ';

    protected $userRepository;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $title = 'Reserva tu cancha!';
            $message = 'Entra y reserva tu cancha o lugar en el gimnasio';
            $info = array('type' => 'reservations', 'id' => '');
            $feedbackIOS = NotificationService::notifyIOs( $title,  $message, ['eca39b90fe545435a3dc2ea5a2c591326a65c475be7b3c4a359b0ff4835fd24f'], $info);
            $this->info(print_r($feedbackIOS, true));
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

