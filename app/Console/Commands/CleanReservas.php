<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\ReservasNotificationsService;

class CleanReservas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'costa:clean-reservas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Reservas: Limpiar Reservas pendientes de pago ';

    protected $reservasNotificationService;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(ReservasNotificationsService $reservasNotificationService)
    {
        parent::__construct();
        $this->reservasNotificationService = $reservasNotificationService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Clean...');
            $this->reservasNotificationService->cleanReservas($this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

