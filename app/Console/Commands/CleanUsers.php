<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserImportService;

use League\Flysystem\Exception;

class CleanUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Users: Remover invitados caducados ';

    protected $importService;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(UserImportService $importService)
    {
        parent::__construct();
        $this->importService = $importService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Clean...');
            $this->importService->cleanUpUsers($this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}