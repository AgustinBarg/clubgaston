<?php

namespace App\Console\Commands;

use App\Http\Validators\ReservaValidatedValidator;
use Illuminate\Console\Command;

use Carbon\Carbon;

class AutoValidateReservas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'costa:auto-validate-reservas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate Reservas: Validar Reservas del dia ';

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Validating Reservas...');
            ReservaValidatedValidator::autoValidateReservas(Carbon::now(), $this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

