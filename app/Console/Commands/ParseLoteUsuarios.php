<?php

namespace App\Console\Commands;

use App\User;
use App\Services\HelperService;
use Illuminate\Console\Command;


class ParseLoteUsuarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'costa:parse-lote-usuarios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse Lote Usuarios: parsear lote / sector ';

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Parse Lotes...');
            $users = User::all();
            foreach ($users as $key => $user) {
                $user->update([
                    'lote_sector' => HelperService::parseSectorLote(
                        trim(explode('-', $user->lote_ubicacion)[0])
                    )
                ]);
            }
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

