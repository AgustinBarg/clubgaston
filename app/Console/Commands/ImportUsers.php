<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserImportService;

use League\Flysystem\Exception;

class ImportUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-users {emprendimiento_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Users from Eidico System';

    protected $importService;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(UserImportService $importService)
    {
        parent::__construct();
        $this->importService = $importService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // 144, COSTA
            // 192, Al Golf 19
            // 224, CE Mar III

            $this->info('Start Import...');
            $this->importService->importUsers($this->argument('emprendimiento_id'), $this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}