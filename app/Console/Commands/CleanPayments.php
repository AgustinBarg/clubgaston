<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Services\PaymentsService;

class CleanPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'costa:clean-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Payments: Limpiar Pagos pendientes de pago pasados 15min.';

    protected $paymentsService;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(PaymentsService $paymentsService)
    {
        parent::__construct();
        $this->paymentsService = $paymentsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Clean...');
            $this->paymentsService->cleanPayments($this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}

