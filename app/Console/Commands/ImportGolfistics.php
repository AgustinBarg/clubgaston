<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\GolfisticsService;

class ImportGolfistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-golfistics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Matriculas Golfistics desde Archivo';

    protected $golfisticsService;

    /**
     * Create a new command instance.
     *
     * @param  DripEmailer  $drip
     * @return void
     */
    public function __construct(GolfisticsService $golfisticsService)
    {
        parent::__construct();
        $this->golfisticsService = $golfisticsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Start Import...');
            $this->golfisticsService->importUsers($this);
        } catch(\Exception $e) {
            $this->error($e->getMessage());
            $this->error($e->getTraceAsString());
        }
    }
}