<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollOption extends Model
{
    protected $fillable = [
        'poll_id', 'text', 'allow_text', 'votes', 'created_at', 'updated_at'
    ];

    public function poll() {
        return $this->belongsTo('App\Poll');
    }

    public function answers() {
        return $this->hasMany('App\PollAnswer');
    }

    public function getPercentage() {
        $totalVotes = $this->poll->getVotes();
        return $totalVotes > 0 ? round($this->votes * 100 / $totalVotes, 0) : 0;
    }
}
