<?php

namespace App\Http\Validators;

use App\ReservaConfig;
use App\ReservaGym;

use App\Services\ReservasGymService;
use Carbon\Carbon;

class ReservaGymValidator
{
    public static function validate($user, $params) {
        $config = ReservaConfig::find(1);
        if (!$config->gym_allow) {
            return ['error' => $config->gym_allow_text];
        }

        // Check Creditos Inquilino
        if ($user->type === 'inquilino') {
            if ($config->gym_allow_credits) {
                $userCredits = ReservasGymService::getAccountCredits($user->id);
                if ($userCredits <= 0) {
                    return ['error' => 'No posees créditos actualmente para poder realizar la reserva. Por favor dirigite a la compra de créditos para poder continuar!'];
                }
            }
        }

        // Check if Places Available
        $existingForDayHour = ReservaGym::where('fechaInicio', $params['date'] . ' ' . $params['time'])
                                        ->where('deleted', false)
                                        ->count();
        if ($existingForDayHour >= $config->gym_max_hour) {
            return ['error' => 'La fecha y hora seleccionada ya se encuentra completa! Por favor refrescá la pantalla y volvé a elegir un horario!'];
        }

        // Saltear Usuario Reservas
        if ($user->id == 1) {
            return;
        }

        // Chequear fecha reserva
        $now = Carbon::now();
        $reservaDate = Carbon::createFromFormat('Y-m-d', $params['date']);
        if ($now->diffInDays($reservaDate) > 7) {
            return ['error' => 'Ups!
                                No es posible realizar reservas con una anticipación mayor a 7 días.
                                Para asegurar mayor disponibilidad y evitar inasistencias, tu reserva deberá ser entre el día de hoy y los próximos 7 días.'];
        }

        // Check same day/hour User
        // Check if Places Available
        $existingForDayHourUser = ReservaGym::where('fechaInicio', $params['date'] . ' ' . $params['time'])
                                        ->where('user_id', $user->id)
                                        ->where('deleted', false)
                                        ->count();
        if ($existingForDayHourUser >= 1) {
            return ['error' => 'Ya tenés una reserva para la fecha y hora seleccionada! Por favor refrescá la pantalla y volvé a elegir otro horario!'];
        }

        // Check Day User
        $existingForDayUser = ReservaGym::whereRaw('DATE(fechaInicio) = ?', $params['date'])
                                        ->where('user_id', $user->id)
                                        ->where('deleted', false)
                                        ->count();
        if ($existingForDayUser >= ReservasGymService::$maxDayReservations) {
            return ['error' => 'Atención, no se pueden tener más de 2 (dos) reservas por día por usuario. Por favor seleccioná otro día para poder realizar la misma.'];
        }
    }
}