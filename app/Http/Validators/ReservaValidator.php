<?php

namespace App\Http\Validators;

use App\Espacio;
use App\Reserva;
use Carbon\Carbon;

class ReservaValidator extends \Illuminate\Validation\Validator
{
    public function validateNotSpaceCalendarConflict($attribute, $value, $parameters, $validator)
    {
        return ReservaValidator::notSpaceCalendarConflict($attribute, $value, $parameters, $validator);
    }

    public function validateDateNotBeforeNow($attribute, $value, $parameters, $validator)
    {
        return ReservaValidator::dateNotBeforeNow($attribute, $value, $parameters, $validator);
    }

    public function validateUserHasManyReservations($attribute, $value, $parameters, $validator){
        return ReservaValidator::userHasManyReservations($attribute, $value, $parameters, $validator);
    }

    public function validateChildrenOrParentIsReserved($attribute, $value, $parameters, $validator){
        return ReservaValidator::childrenOrParentIsReserved($attribute, $value, $parameters, $validator);
    }


    private static function notSpaceCalendarConflict($attribute, $value, $parameters)
    {
        $currentDate = $parameters[2];

        if($currentDate == null || !isset($parameters[0]) || $parameters[1] == null) {
            return false;
        }

        $dateTime = explode('-', $currentDate);


        $currentDay = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], 0, 0, 0,'America/Argentina/Buenos_Aires');
        $startingTime = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], $parameters[0], 0, 0,'America/Argentina/Buenos_Aires');
        $endingTime = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], $parameters[1], 0, 0,'America/Argentina/Buenos_Aires');
        $afterDay = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2] + 1, 0, 0, 0,'America/Argentina/Buenos_Aires');
        $spaceId = $parameters['3'];

        $espacio= Espacio::findOrFail($spaceId);
        if ($startingTime->diffInHours($endingTime) > $espacio->maxHoras) {
            return false;
        }

        $reservas = Reserva::where([
            ['espacio_id', '=', $spaceId], ['horaInicio', '>=', $currentDay ], ['horaFin', '<=', $afterDay], ['deleted', false]
        ])->get();

        if($espacio->horaInicio > $parameters[0] || $espacio->horaFin <= $parameters[0]) {
            return false;
        }

        foreach ($reservas as $reserva) {
            if ($reserva->horaInicio <= $startingTime && $reserva->horaFin > $startingTime) {
                return false;
            }

            if($startingTime < $reserva->horaInicio && $endingTime > $reserva->horaInicio) {
                return false;
            }
        }

        return true;
    }

    private static function dateNotBeforeNow($attribute, $value, $parameters)
    {
        if($parameters[0] == null || $parameters[1] == null) {
            return false;
        }

        $currentDate = $parameters[0];
        $dateTime = explode('-', $currentDate);
        $currentDay = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], $parameters[2], 0, 0,'America/Argentina/Buenos_Aires');
        $now = Carbon::now('America/Argentina/Buenos_Aires');

        if ($currentDay < $now) {
            return false;
        }

        return true;

    }

    private static function userHasManyReservations($attribute, $value, $parameters){
        $user_id = $parameters[0];

        // user_id = 1 es el usuario para reservas generales, puede tener mas de 2 reservas cargadas.
        if ($user_id == 1) {
            return true;
        }

        $reservas_user = Reserva::where('user_id', $user_id)
                        ->where('deleted', false)
                        ->where('status', '<>', 'canceled')
                        ->where('horaInicio', '>', Carbon::now())
                        ->count();

        if ($reservas_user >= 2) {
            return false;
        }
        
        return true;
    }

    private static function childrenOrParentIsReserved($attribute, $value, $parameters){
        $currentDate = $parameters[2];

        if($currentDate == null || !isset($parameters[0]) || $parameters[1] == null) {
            return false;
        }

        $dateTime = explode('-', $currentDate);

        $startingTime = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], $parameters[0], 0, 0,'America/Argentina/Buenos_Aires');
        $spaceId = $parameters['3'];

        $isSpaceChildren = in_array($spaceId, [2, 5, 6]);
        $isSpaceParent = $spaceId == 1;

        if ($isSpaceChildren) {
            $isParentReserved = Reserva::where([
            ['deleted', false], ['espacio_id', '=', 1], ['horaInicio', '=', $startingTime ]])->get();

            if (count($isParentReserved) > 0) return false;
        }

        if ($isSpaceParent) {
            $isChildrenReserved = Reserva::whereIn('espacio_id', [2, 5, 6])
                                            ->where('deleted', false)
                                            ->where('status', '<>', 'canceled')
                                            ->where('horaInicio', '=', $startingTime)->get();

            if (count($isChildrenReserved) > 0) return false;
        }

        return true;
    }
}