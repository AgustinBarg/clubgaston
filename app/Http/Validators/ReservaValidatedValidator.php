<?php

namespace App\Http\Validators;

use App\Espacio;
use App\Reserva;
use App\ReservaConfig;
use App\ReservaGym;
use App\ReservaValidation;
use Carbon\Carbon;

class ReservaValidatedValidator {

    public static function autoValidateReservas( $date, $logger ) {
        $reservasConfig = ReservaConfig::find(1);

        ReservaValidation::updateOrCreate(
            ['date' => Carbon::now()->toDateString()],
            ['enabled_validation' => $reservasConfig->reservas_validation]
        );

        if ($reservasConfig->reservas_validation) {
            return true;
        }

        $morning = Carbon::createFromDate($date->year, $date->month, $date->day)->hour(0)->minute(0);
        $night = Carbon::createFromDate($date->year, $date->month, $date->day)->hour(23)->minute(59);
        Reserva::where('horaInicio', '>', $morning)
                ->where('horaFin', '<', $night)
                ->where('deleted', false)
                ->where('validated', false)
                ->update(['validated' => true]);
        ReservaGym::where('fechaInicio', '>', $morning)
                ->where('fechaFin', '<', $night)
                ->where('deleted', false)
                ->where('validated', false)
                ->update(['validated' => true]);
    }

    public static function validateReserva( $qrCode, $userId ) {
        $espacio = Espacio::whereNotNull('qr_validation')->where('qr_validation', $qrCode)->first();
        if (!$espacio) {
            return ['error' => 'Ups! El código QR no corresponde a ningun espacio.'];
        }

        $nowMorning = Carbon::now()->hour(0)->minute(0);
        $nowNight = Carbon::now()->hour(23)->minute(59);

        if (!$espacio->categoria_id) {
            // GYM
            $reserva = ReservaGym::where('user_id', $userId)
                                ->where('fechaInicio', '>=', $nowMorning)
                                ->where('fechaFin', '<=', $nowNight)
                                ->where('deleted', false)
                                ->where('validated', false)
                                ->first();
            
        } else {
            // SPORTS
            $idEspacios = Espacio::where('qr_validation', $qrCode)->pluck('id')->toArray();
            $reserva = Reserva::where('user_id', $userId)
                                ->whereIn('espacio_id', $idEspacios)
                                ->where('horaInicio', '>=', $nowMorning)
                                ->where('horaFin', '<=', $nowNight)
                                ->where('status', '<>', 'canceled')
                                ->where('deleted', false)
                                ->where('validated', false)
                                ->first();
        }
        if (!$reserva) {
            return ['error' => 'Ups! No tenés ninguna reserva activa y no validada correspondiente a este espacio para el dia de hoy.'];
        }

        $reserva->update([
            'validated'     => true,
            'validated_by'  => $userId,
            'validated_at'  => Carbon::now()
        ]);
        return true;
    }

    public static function checkUserCanReserveUnvalidated( $userId ) {
        $reservasConfig = ReservaConfig::find(1);
        if (!$reservasConfig->reservas_validation) {
            return true;
        }

        $today = Carbon::today()->hour(0)->minute(0);
        $daysAgo = Carbon::today()->subDays(7)->hour(0)->minute(0);
        $reservas = Reserva::where('user_id', $userId)
                                ->where('horaInicio', '>=', $daysAgo)
                                ->where('horaInicio', '<', $today)
                                ->where('deleted', false)
                                ->where('status', '<>', 'canceled')
                                ->where('validated', false)
                                ->orderBy('horaInicio', 'desc')
                                ->get();
        $reservasGym = ReservaGym::where('user_id', $userId)
                                ->where('fechaInicio', '>=', $daysAgo)
                                ->where('fechaInicio', '<', $today)
                                ->where('deleted', false)
                                ->where('validated', false)
                                ->orderBy('fechaInicio', 'desc')
                                ->get();

        $countReservas = count($reservas) + count($reservasGym);

        // + de 1 no validada en los ultimos 7, NO puede reservar
        if ($countReservas > 1) {
            return false;
        }
        // ninguna no validada en los ultimos 7, SI puede reservar
        if ($countReservas == 0) {
            return true;
        }

        // 1 sola en los ultimos 7, Chequeamos previos
        $ultimaReserva = count($reservas) > 0 ? $reservas[0]->horaInicio : Carbon::create(1980, 1, 1);
        $ultimaReservaGym = count($reservasGym) > 0 ? $reservasGym[0]->fechaInicio : Carbon::create(1980, 1, 1);
        $ultimaReservaFecha = $ultimaReserva > $ultimaReservaGym ? $ultimaReserva : $ultimaReservaGym;
        $ultimaReservaFechaBefore = $ultimaReservaFecha->copy()->subDays(7)->hour(0)->minute(0);
        // chequear reservas 7 dias previos a esa
        $reservas = Reserva::where('user_id', $userId)
                                ->where('horaInicio', '>=', $ultimaReservaFechaBefore)
                                ->where('horaInicio', '<', $ultimaReservaFecha)
                                ->where('deleted', false)
                                ->where('status', '<>', 'canceled')
                                ->where('validated', false)
                                ->count();
        $reservasGym = ReservaGym::where('user_id', $userId)
                                ->where('fechaInicio', '>=', $ultimaReservaFechaBefore)
                                ->where('fechaInicio', '<', $ultimaReservaFecha)
                                ->where('deleted', false)
                                ->where('validated', false)
                                ->count();

        return ($reservas + $reservasGym) == 0;
    }
}