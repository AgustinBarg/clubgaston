<?php

namespace App\Http\Validators;

use Carbon\Carbon;
use DB;

use App\ReservaEcuestre;
use App\EspacioEcuestre;
use App\ReservaConfig;

class ReservaEcuestreValidator
{
    public static function validate($user, $params, $onlyVacants = false) {
        $config = ReservaConfig::find(1);
        if (!$config->ecuestre_allow) {
            return ['error' => $config->ecuestre_allow_text];
        }

        if (!$onlyVacants) {
            // FIND CORRAL FOR PERIOD
            $corralInfo = ReservaEcuestre::where('deleted', false)
                                ->where('user_id', $user->id)
                                ->where('status', '<>', 'canceled')
                                ->whereIn('espacio_id', [5,8])
                                ->where(function ($subQuery) use ($params) {
                                    $subQuery->whereBetween('fechaInicio', [$params['fechaInicio'],$params['fechaFin']])
                                            ->orWhereBetween('fechaFin', [$params['fechaInicio'],$params['fechaFin']])
                                            ->orWhere(function($subQuery_from) use ($params) {
                                                $subQuery_from->where('fechaInicio', '<=', $params['fechaInicio'])
                                                            ->where('fechaFin', '>=', $params['fechaInicio']);
                                            })
                                            ->orWhere(function($subQuery_to) use ($params) {
                                                $subQuery_to->where('fechaInicio', '<=', $params['fechaFin'])
                                                            ->where('fechaFin', '>=', $params['fechaFin']);
                                            });
                                })
                                ->first();

            if ($params['category_id'] == 1001) {
                if ($corralInfo) {
                    return ['error' => 'Tenés un corral reservado entre las fechas: <br><br><strong>' . $corralInfo->fechaInicio->format('d/m/Y') . ' al ' . $corralInfo->fechaFin->format('d/m/Y') . '</strong><br><br> Solo podés reservar hasta 1 corral dentro de este período. <br><br>Por favor, contactate con el coordinador del sector ecuestre para más información.'];
                }
                // SEGUIR CON VALIDACION
            } else {
                // LOCKER o CAMA PETIZERO
                if (!$corralInfo) {
                    $corralInfoOtherPeriod = ReservaEcuestre::where('deleted', false)
                                ->where('status', '<>', 'canceled')
                                ->where('user_id', $user->id)
                                ->where('fechaFin', '>=', Carbon::now()->format('Y-m-d'))
                                ->whereIn('espacio_id', [5,8])
                                ->first();

                    if ($corralInfoOtherPeriod) {
                        return [
                            'error' => 'Tenés un corral reservado entre las fechas: <br><br><strong>' . $corralInfoOtherPeriod->fechaInicio->format('d/m/Y') . ' al ' . $corralInfoOtherPeriod->fechaFin->format('d/m/Y') . '</strong><br><br> Solo podés reservar camas o lockers dentro del período de reserva del corral. <br><br>Por favor realizá la reserva dentro de esas fechas.'
                        ];
                    } else {
                        return [
                            'error' => 'No hemos encontrado reservas confirmadas de Corrales a tu nombre. Si todavía no tenes un corral no podrás reservar lockers ni camas para petizeros.'
                        ];
                    }
                }

                // SEGUIR CON VALIDACION
                // LOCKER/CAMAS PUEDE RESERVAR 1 SOLO
                $lockerCamaInfo = ReservaEcuestre::where('deleted', false)
                                ->where('user_id', $user->id)
                                ->where('status', '<>', 'canceled')
                                ->where('fechaInicio', '<=', $params['date'])
                                ->where('fechaFin', '>=', $params['date'])
                                ->join('espacios_ecuestre', 'espacios_ecuestre.id', '=', 'reservas_ecuestre.espacio_id')
                                ->where('espacios_ecuestre.category_id', $params['category_id'])
                                ->first();
                if ($lockerCamaInfo) {
                    $name = $params['category_id'] == 3003 ? 'Camas para petizeros' : 'Locker';
                    return [
                        'error' => 'Podés reservar hasta 1 <strong>' . $name . '</strong>. Por favor, contactate con el coordinador del sector ecuestre para más información.'
                    ];
                }
            }
        }


        $date = $params['date'];
        $spaceWithVacants = EspacioEcuestre::where('espacios_ecuestre.id', $params['espacio_id'])
                        ->leftJoin('reservas_ecuestre', function ($join) use ($date) {
                            $join->on('espacios_ecuestre.id', '=', 'reservas_ecuestre.espacio_id')
                                ->where('reservas_ecuestre.deleted', false)
                                ->where('reservas_ecuestre.status', '<>', 'canceled')
                                ->where('reservas_ecuestre.fechaInicio', '<=', $date)
                                ->where('reservas_ecuestre.fechaFin', '>=', $date);
                        })
                        ->select(DB::raw('espacios_ecuestre.id, espacios_ecuestre.shared, espacios_ecuestre.nombre, (vacants - count(reservas_ecuestre.id)) as vacantes, espacios_ecuestre.places'))
                        ->first();


        if ($spaceWithVacants['vacantes'] == 0 || 
            ($spaceWithVacants['shared'] == '1' && isset($params['places']) && $spaceWithVacants['vacantes'] < $params['places']) ||
            ($spaceWithVacants['shared'] == '0' && isset($params['places']) && $spaceWithVacants['places'] < $params['places'])
            )  {
            return ['error' => 'Ya no quedan vacantes/espacio disponible para el periodo, el espacio y la cantidad de caballos seleccionados.'];
        }
    }
}