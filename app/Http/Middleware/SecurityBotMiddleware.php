<?php

namespace App\Http\Middleware;

use App\AuditLog;
use Closure;
use Laravel\Passport\Token;

class SecurityBotMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        $userAgent = strtolower($request->headers->get('User-Agent'));

        if (strpos($userAgent, 'python') !== false) {
            try {
                AuditLog::create([
                    'user_id'   => $user->id,
                    'ip'        => $request->ip(), 
                    'headers'   => $request->headers, 
                    'data'      => 'MIDDLEWARE BLOQUEADO USER AGENT'
                ]);
                Token::where('user_id', $user->id)->delete();
            } catch(\Exception $e) {}
            return response()->json('Ups! Ocurrió un error inesperado! ', 401);
        }

        if (in_array($request->ip(), ['3.92.2.115'])) {
            try {
                AuditLog::create([
                    'user_id'   => $user->id,
                    'ip'        => $request->ip(), 
                    'headers'   => $request->headers, 
                    'data'      => 'MIDDLEWARE BLOQUEADO IP'
                ]);
                Token::where('user_id', $user->id)->delete();
            } catch(\Exception $e) {}
            return response()->json('Ups! Ocurrió un error inesperado! ', 401);
        }

        if (!$user) {
            Token::where('user_id', $user->id)->delete();
            return response()->json('Ups! Ocurrió un error inesperado! ', 401);
        }

        return $next($request);
    }
}
