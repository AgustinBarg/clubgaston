<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/6/17
 * Time: 3:47 PM
 */

namespace App\Http\Middleware;

use Closure;

class CheckRoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && ($request->user()->role->id > 1)) {
            return $next($request);
        }

        return response()->redirectTo('/login');
    }
}