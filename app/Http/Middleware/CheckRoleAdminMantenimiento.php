<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleAdminMantenimiento
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && ($request->user()->role->id == 3 || $request->user()->role->id == 5)) {
            return $next($request);
        }

        return response()->json('error, NO TIENES EL ACCESO PERMITIDO', 401);
    }
}
