<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleAdminDeportes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && ($request->user()->role->id == 2 || $request->user()->role->id == 5)) {
            return $next($request);
        }

        return response()->json('error, NO TIENES EL ACCESO PERMITIDO', 401);
    }
}
