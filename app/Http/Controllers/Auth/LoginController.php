<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Admin\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\User;


class LoginController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }

    public function testTimeout() {
        sleep(300);
        return view('auth.login');
    }

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //facebook login


    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->stateless()->user();
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return redirect()->route('home');

        // $user->token;
    }

    private function findOrCreateUser($user)
    {
        $authUser = User::where('email', $user->email)->
        first();

        if ($authUser){
            return $authUser;
        }
        return User::create([
            'facebook_id' => $user->getId(),
            'name' =>  $user->getName(),
            'email' => $user->getEmail(),
            'role' => 0,
            'password' => null
        ]);
    }

}
