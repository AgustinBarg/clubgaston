<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\Controller;

use Illuminate\Http\Request;

use App\Repositories\IncidenteInterface;
use App\IncidenteState;
use App\ticketsMessage;
use App\ticketsMessageImage;

class TicketsController extends Controller
{
    protected $incidentesRepository;

    public function __construct(IncidenteInterface $incidentesRepository)
    {
        $this->incidentesRepository = $incidentesRepository;
    }

    public function getIncidente( $code )
    {
        $incidente = $this->incidentesRepository->getPublicIncident($code);
        return view('incidentes.public-incident', [
            'incidente'             => $incidente,
            'states'                => IncidenteState::all('id', 'name')
        ]);
    }

    protected function postMessage(Request $request) {
        $incident = $this->incidentesRepository->getById($request->get('incident_id'));
        if (!$incident || $incident->auth_code != $request->get('incident_code')) {
            $request->session()->flash('error', 'Ups! El incidente no se encuentra disponible o dejó de ser público. Por favor refresque la página o contáctese con administración.');
            return back();
        }

        if ($incident->state_id == 3) {
            $request->session()->flash('error', 'Ups! El incidente se encuentra en estado CERRADO y por tal motivo no se pueden agregar mas mensajes. Por favor contáctese con administración si necesitara reabrir el caso.');
            return back();
        }

        $newTicketMessage = ticketsMessage::create([
            'user_id'               => $incident->user_id,
            'tickets_incidente_id'  => $incident->id,
            'message'               => $request->get('comment')
        ]);

        if ($request->file('image') != null) {
            $imageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/', $imageName
            );
            ticketsMessageImage::create([
                'ticket_message_id' => $newTicketMessage->id,
                'image_path'        => $imageName
            ]);
        }

        $request->session()->flash('success', 'El mensaje ha sido enviado correctamente!');
        return back();
    }
}
