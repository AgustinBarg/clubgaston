<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/4/17
 * Time: 9:28 PM
 */

namespace App\Http\Controllers\Api;

use App\AuditLog;
use App\ReservaConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Services\EspacioService;
use App\Services\PaymentsService;
use App\Services\ReservasService;
use Carbon\Carbon;
use Laravel\Passport\Token;

class ReservasController extends ApiController
{
    private $reservasService;
    private $espacioService;

    public function __construct(ReservasService $reservasService, EspacioService $espacioService)
    {
        $this->reservasService = $reservasService;
        $this->espacioService = $espacioService;
    }

    public function getConfig() {
        return response()->json($this->reservasService->getConfig());
    }

    public function getCategoriasBySede($id)
    {
        $categorias = $this->espacioService->getCategoriasBySede($id);
        return response()->json(['categorias' => $categorias]);
    }

    public function getCategoriasTimetables(Request $request, $sedeId, $categoryId)
    {
        $this->validate($request, [
            'date' => 'date'
        ], [
            'date.date' => 'No es una fecha valida'
        ]);

        $date = $request->input('date');

        $timetables = $this->reservasService->getCategoriaTimetableFromDate($sedeId, $categoryId, $date);
        if (isset($timetables['error'])) {
            return response()->json($timetables['error'], $timetables['code']);
        } else {
            return response()->json(['horarios' => $timetables]);
        }
    }

    public function getCategoriasTimetablesTest(Request $request, $sedeId, $categoryId)
    {
        $this->validate($request, [
            'date' => 'date'
        ], [
            'date.date' => 'No es una fecha valida'
        ]);

        $date = $request->input('date');

        $timetables = $this->reservasService->getCategoriaTimetableFromDateTest($sedeId, $categoryId, $date);
        if (isset($timetables['error'])) {
            return response()->json($timetables['error'], $timetables['code']);
        } else {
            return response()->json(['horarios' => $timetables]);
        }
    }

    public function postReserva(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();

        $now = Carbon::now();
        if ($now < Carbon::create(2020, 5, 10)) {
            return $this->respondWithError('La reserva no pudo ser realizada. Debido a las medidas de cuarentena obligatoria, las reservas de espacios deportivos se encuentran suspendidas hasta el 10 de Mayo.', 422);
        }

        if ($user->borrado) {
            Token::where('user_id', $user->id)->delete();
            return $this->respondWithError('Ups! El espacio ya ha sido reservado para ese horario o se encuentra cerrado.', 401);
        }
        
        if ($user->moroso) {
            return $this->respondWithError('Este es un mensaje para los morosos. Quiere decir que no podes realizar una reserva. Por favor contactáte con la administración para poder realizar reservas.', 422);
        }

        $data['user_id'] = $user->id;
        $data['espacio_id'] = $id;
        $validatorFields = $this->validateReserva($data);

        if ($validatorFields->fails()) {
            return $this->respondWithError($validatorFields->errors(), 422);
        }
 
        $userAgent = strtolower($request->headers->get('User-Agent'));
        if (strpos($userAgent, 'python') !== false) {
            try {
                AuditLog::create([
                    'user_id'   => $user->id,
                    'ip'        => $request->ip(), 
                    'headers'   => $request->headers, 
                    'data'      => 'BLOQUEADO USER AGENT'
                ]);
                Token::where('user_id', $user->id)->delete();
            } catch(\Exception $e) {}
            return $this->respondWithError('Ups! El espacio ya ha sido reservado para ese horario o se encuentra cerrado!.', 401);
        }

        if (in_array($request->ip(), ['3.92.2.115'])) {
            try {
                AuditLog::create([
                    'user_id'   => $user->id,
                    'ip'        => $request->ip(), 
                    'headers'   => $request->headers, 
                    'data'      => 'BLOQUEADO USER AGENT'
                ]);
                Token::where('user_id', $user->id)->delete();
            } catch(\Exception $e) {}
            return $this->respondWithError('Ups! El espacio ya ha sido reservado para ese horario o se encuentra cerrado!.', 401);
        }

        // CHECK AUDITS
        try {
            AuditLog::create([
                'user_id'   => $user->id,
                'ip'        => $request->ip(), 
                'headers'   => $request->headers, 
                'data'      => ''
            ]);
        } catch(\Exception $e) {}

        $validatorCalendarConflicts = $this->validateNotSpaceCalendarConflict($data);

        if ($validatorCalendarConflicts->fails()) {
            $error = $validatorCalendarConflicts->errors()->first('horaInicio');
            $error_msg = '';
            if ($error == 'validation.date_not_before_now') {
                $error_msg = 'El horario de reserva es menor a la fecha actual.';
            } elseif ($error == 'validation.user_has_many_reservations') {
                $error_msg = 'Atención, no se pueden tener más de 2 (dos) reservas abiertas por usuario. En cualquier momento podés cancelar alguna de tus reservas abiertas y realizar una nueva';
            } 
            else {
                $error_msg = 'El espacio ya ha sido reservado para ese horario o se encuentra cerrado.';
            }
            return $this->respondWithError($error_msg, 422);
        }

        try {
            

            $reserva = $this->reservasService->createReserva($data);
            if ($reserva->amount > 0) {
                $paymentData = PaymentsService::generateMercadoPagoPayment($user, [
                    'amount'	=> $reserva->amount,
                    'type'		=> 'reserva-deportiva',
                    'method'    => 'mercadopago'
                ]);
                $reserva->payment_id = $paymentData['payment']->id;
                $reserva->save();
                return response()->json($paymentData['preference']);
            } else {
                return response()->json(['reserva' => $reserva], 201);
            }
            return response()->json(['reserva' => $reserva], 201);
        } catch(\Exception $e) {
            return $this->respondWithError($e->getMessage(), 422);
        }
    }

    public function getReservasByUser()
    {
        $userId = Auth::id();
        $reservas = $this->reservasService->getUserReservas($userId);
        return response()->json(['reservas' => $reservas]);
    }

    public function validateReserva($data)
    {
        return Validator::make($data, [
            'horaFin' => 'required',
            'espacio_id' => 'required',
            'user_id' => 'required',
            'date' => 'required',
        ]);
    }

    public function validateNotSpaceCalendarConflict($data)
    {
        return Validator::make($data, [
            'horaInicio' => 'required|userHasManyReservations:' . $data["user_id"] . '|notSpaceCalendarConflict:' . $data['horaInicio'] . ',' . $data['horaFin'] . ',' . $data['date'] . ',' . $data['espacio_id'] . '|dateNotBeforeNow:' . $data['date'] . ',' . $data['horaInicio'] . ',' . $data['horaFin'],
        ], [
            'horaInicio.notSpaceCalendarConflict' => 'El espacio ya ha sido reservado para ese horario o se encuentra cerrado',
            'horaInicio.dateNotBeforeNow' => 'El horario de reserva es menor a la fecha actual.',
            'horaInicio.validation.date_not_before_now' => 'El horario de reserva es menor a la fecha actual.',
            'horaInicio.userHasManyReservations' => 'Atención, no se pueden tener más de 2 (dos) reservas abiertas por usuario. En cualquier momento podés cancelar alguna de tus reservas abiertas y realizar una nueva.'
        ]);
    }

    public function cancelReserva(Request $request, $id, $reservaId)
    {
        $result = $this->reservasService->delete($reservaId, Auth::id());
        if (isset($result['error'])) {
            return response()->json(['error' => $result['error']], 400);
        } else {
            return response()->json(['success' => true], 201);
        }
    }

    public function validateReservaCode( Request $request ) {
        $result = $this->reservasService->validateReserva($request->input('qrCode'), Auth::id());
        return response()->json($result, isset($result['error']) ? 400 : 200);
    }
}