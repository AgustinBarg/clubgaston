<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Services\PollsService;

class PollsController extends ApiController
{
    public function __construct()
    {
    }

    public function getActivePolls()
    {
        try {
            $userId = Auth::id();
            $polls = PollsService::getActivePolls( $userId );
            return response()->json(['polls' => $polls], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }

    public function votePoll( $id, Request $request) {
        try {
            $userId = Auth::id();
            $poll = PollsService::votePoll( $userId, $id, $request->all());
            return response()->json(['poll' => $poll], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }
}