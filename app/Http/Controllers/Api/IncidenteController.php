<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/4/17
 * Time: 11:51 AM
 */

namespace App\Http\Controllers\Api;

use App\Services\IncidenteService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IncidenteController extends ApiController
{
    private $incidenteService;

    public function __construct(IncidenteService $incidenteService)
    {
        $this->incidenteService = $incidenteService;
    }

    public function create(Request $request) {
        $userId = Auth::id();
        $data = $request->all();
        $data['user_id'] = $userId;
        $data['estado'] = 2;
        $validator = $this->validatorCreate($data);

        if ($validator->fails()) {
            return $this->respondWithError($validator->errors(), 422);
        }

        if ($request->file('image') != null) {
            $imageName = time() . '.' . $request->file('image')->getClientOriginalExtension();

            $request->file('image')->move(
                base_path() . '/public/images/', $imageName
            );

            $data['image'] = $imageName;
        }

        $incidente = $this->incidenteService->create($data);

        return response()->json(['incidente' => $incidente], 200);
    }

    public function updateState(Request $request, $id)
    {
        $this->validate($request, array('state_id' => 'max:2', 'id' => 'required'));
        $incidente = $this->incidenteService->updateState($id, $request->input('state_id'));
        return response()->json(['incidente' => $incidente], 200);
    }

    public function userIncidentes(){
        $userId = Auth::id();
        $incidentes = $this->incidenteService->getUserIncidente($userId);
        return response()->json(['incidente' => $incidentes]);
    }

    public function messages(Request $request, $id)
    {
        $messages = $this->incidenteService->getIncidenteMessages($id);
        return response()->json(['messages' => $messages]);
    }

    public function getCategorias()
    {
        $categorias = $this->incidenteService->getCategorias();
        return response()->json(['categorias' => $categorias], 200);
    }

    public function sendMessage(Request $request, $id)
    {
        $userId = Auth::id();
        $data = $request->all();
        $data['user_id'] = $userId;
        $data['id'] = $id;

        if ($request->file('image') != null) {
            $imageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/', $imageName
            );
            $data['image'] = $imageName;
        }

        $this->incidenteService->sendMessages($data);
        return response()->json(['messageSuccess', 'El mensaje se ha creado con exito']);
    }

    public function validatorCreate($data)
    {
        return \Illuminate\Support\Facades\Validator::make($data, [
            'title' => 'required',
            'detalle' => 'required',
            'tema_id' => 'required|numeric',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    }
}