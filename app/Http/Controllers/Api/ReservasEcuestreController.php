<?php

namespace App\Http\Controllers\Api;

use App\Http\Validators\ReservaEcuestreValidator;
use App\Services\PaymentsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Services\ReservasEcuestreService;
use Carbon\Carbon;

class ReservasEcuestreController extends ApiController
{
    public function __construct()
    {
    }

    public function getCategorias() {
        return ReservasEcuestreService::getCategorias();
    }

    public function getReservasByUser() {
        return ReservasEcuestreService::getUserReservas(Auth::id());
    }

    public function validateCorral(Request $request) {
        $params = $request->all();
        $corralInfo =  ReservasEcuestreService::getCorralForPeriod(Auth::user(), $params);

        if (isset($corralInfo['error'])) {
            return response()->json($corralInfo['error'], $corralInfo['code']);
        } else {
            return response()->json($corralInfo, 200);
        }
    }

    public function getPlacesDisponibles($category, Request $request) {
        $params = $request->all();
        $period = ReservasEcuestreService::calculatePeriod($params['period'], $params['date']);

        return response()->json([
            'places' => ReservasEcuestreService::getPlacesForPeriod($category, $period, $params['period']),
            'fechaInicio' => $period['from'],
            'fechaFin' => $period['to'],
            // MULTIPLIER ALREADY CALCULATED IN PLACES
            'multiplier' => 1
        ], 200);
    }

    public function postReserva(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $now = Carbon::now();
        if ($now < Carbon::create(2020, 5, 10)) {
            return $this->respondWithError('La reserva no pudo ser realizada. Debido a las medidas de cuarentena obligatoria, las reservas de espacios ecuestres se encuentran suspendidas hasta el 10 de Mayo.', 422);
        }
        
        if ($user->moroso) {
            return $this->respondWithError('Este es un mensaje para los morosos. Quiere decir que no podes realizar una reserva. Por favor contactáte con la administración para poder realizar reservas.', 422);
        }

        $validator = ReservaEcuestreValidator::validate($user, $data);

        if (isset($validator['error'])) {
            return $this->respondWithError($validator['error'], 400);
        }

        try {
            $reserva = ReservasEcuestreService::createReserva($user, $data);
            if ($reserva->amount > 0) {
                $paymentData = PaymentsService::generateMercadoPagoPayment($user, [
                    'amount'	=> $reserva->amount,
                    'type'		=> 'reserva-ecuestre',
                    'method'    => 'mercadopago'
                ]);

                $reserva->payment_id = $paymentData['payment']->id;
                $reserva->save();
                return response()->json($paymentData['preference']);
            } else {
                return response()->json(['reserva' => $reserva], 201);
            }
        } catch(\Exception $e) {
            return $this->respondWithError($e->getMessage(), 400);
        }
    }

    public function cancelReserva( $reservaId ) {
        ReservasEcuestreService::delete($reservaId, Auth::id());
        return response()->json(['success' => true], 201);
    }
}