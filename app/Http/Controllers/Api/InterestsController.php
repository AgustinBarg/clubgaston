<?php

namespace App\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Services\InterestsService;

class InterestsController extends ApiController
{
    private $interestsService;

    public function __construct(InterestsService $interestsService)
    {
        $this->interestsService = $interestsService;
    }

    public function getInterests()
    {
        return response()->json(['interests' => $this->interestsService->getInterests()], 200);
    }

    public function saveInterests( Request $request )
    {
        try {
            $userId = Auth::id();
            $interests = $request->input('interests');
            $this->interestsService->saveInterests($userId, $interests);
            return response()->json(['success' => true], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
            return response()->json(['error' => 'Ups! En este momento no podemos guardar tus intereses. Por favor intenta nuevamente más tarde.'], 400);
        }
        
    }
}