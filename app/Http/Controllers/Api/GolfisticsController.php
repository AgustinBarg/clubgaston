<?php

namespace App\Http\Controllers\Api;

use App\Services\GolfisticsService;
use Illuminate\Support\Facades\Auth;

class GolfisticsController extends ApiController
{
    public function __construct()
    {
    }

    public function getGolfisticsUrl( ) {
        $user = Auth::user();
        $response = GolfisticsService::getGolfisticsUrl($user);
        
        if (isset($response['url'])) {
            return response()->json($response, 200);
        } else {
            return response()->json($response, 400);
        }
    }
}