<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/3/17
 * Time: 1:21 PM
 */

namespace App\Http\Controllers\Api;

use App\Services\CuotasService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CuotasController extends ApiController
{
    private $cuotasService;

    public function __construct(CuotasService $cuotasService)
    {
        $this->cuotasService = $cuotasService;
    }

    public function getUserCuotas()
    {
        $userId = Auth::id();
        try {
            $cuotas = $this->cuotasService->getCuotasByUser($userId);
        } catch (\Exception $e) {
            $this->respondWithError(['error_message' => 'Ha habido un error accediendo a los datos.']);
        }

        return response()->json(['cuotas', $cuotas]);
    }

    public function informarPagoCuota(Request $request)
    {
        $data = $request->all();

        try {
            if($request->file('payment_ticket')) {
                $imageName = time() . '.' . $request->file('payment_ticket')->getClientOriginalExtension();
                $request->file('payment_ticket')->move(
                    base_path() . '/public/images/', $imageName
                );
                $data['payment_ticket'] = $imageName;
            }
        } catch(\Exception $e) {

        }

        $message = $this->cuotasService->informarPago($data);

        return response()->json([$message['type'], $message['text']]);
    }
}