<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/4/17
 * Time: 12:18 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Admin\Controller;

class ApiController extends Controller
{
    /**
     * Handles an error response formatting it according to our spec.
     *
     * @param array $error
     * @param array $headers
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function respondWithError($error, $headers = [])
    {
        return response()->json(['errors' => $error], 400);
    }


    /**
     * @param Model $item
     * @param TransformerAbstract $transformer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function respondWithItem(Model $item, TransformerAbstract $transformer)
    {
        $resource = new Item($item, $transformer);
        return response()->json($this->manager->createData($resource)->toArray())->setStatusCode($this->getStatusCode());
    }
}