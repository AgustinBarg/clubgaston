<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

use App\Services\AuthorizationsService;

class AuthorizationsController extends ApiController
{
    private $authorizationsService;

    public function __construct( AuthorizationsService $authorizationsService ) {
        $this->authorizationsService = $authorizationsService;
    }

    public function listAuthorizations() {
        try {
            $userId = Auth::id();
            $list = $this->authorizationsService->listAuthorizations( $userId );
            return response()->json($list, 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }

    public function validateUser() {
        try {
            $userId = Auth::id();
            $userData = $this->authorizationsService->validatePersona( $userId );
            return response()->json($userData);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }

    public function getCategories() {
        $userId = Auth::id();
        return response()->json($this->authorizationsService->getUserCategories($userId));
    }

    public function createAuthorization( Request $request ) {
        try {
            $userId = Auth::id();
            $params = $request->all();

            if ($params['category'] != 4) {
                if (isset($params['email']) && $params['email'] !== '') {
                    $params['email'] = strtolower(trim($params['email']));
                    if (filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
                    } else {
                        return response()->json(['error' => 'Ups! El Email ingresado no posee un formato válido! Por favor revisá los datos ingresados y volvé a intentar!'] , 400);
                    }
                } else {
                    return response()->json(['error' => 'Ups! El Campo Email es obligatorio! Por favor revisá los datos ingresados y volvé a intentar!'] , 400);
                }

                if (isset($params['document']) && $params['document'] != '') {
                    if (strpos($params['document'], '.') !== false || strpos($params['document'], ' ') !== false) {
                        return response()->json(['error' => 'Ups! Por favor escriba el número de documento sin puntos ni espacios. Ejemplo: 32888111'] , 400);
                    }
                }
    
                if (isset($params['patent']) && $params['patent'] != '') {
                    if (strlen($params['patent']) >= 10) {
                        return response()->json(['error' => 'Ups! La Patente especificada supera la longitud máxima de 10 caracteres! Por favor escriba la patente con el siguiente formato: AB123CD o ABC123.'] , 400);
                    }
                }
            }
 
            $userData = $this->authorizationsService->addAuthorization( $userId, [
                'valid_from'=> $params['valid_from'],
                'valid_to'  => $params['valid_to'],
                'IdPersona' => $params['idPersona'],
                'document'  => isset($params['document']) ? $params['document'] : '',
                'last_name' => isset($params['last_name']) ? $params['last_name'] : '',
                'first_name'=> isset($params['first_name']) ? $params['first_name'] : '',
                'email'     => isset($params['email']) ? $params['email'] : '',
                'patent'    => isset($params['patent']) ? $params['patent'] : '',
                'comment'   => isset($params['comments']) ? $params['comments'] : '',
                'category'  => $params['category']
            ]);
            return response()->json($userData);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }

    public function cancelAuthorization( $authorizationId ) {
        try {
            $this->authorizationsService->cancelAuthorization($authorizationId);
            return response()->json(true);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }
}