<?php

namespace App\Http\Controllers\Api;

use League\Flysystem\Exception;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\SendNotificationRequest;
use App\Services\NotificationService;

class NotificationsController extends ApiController
{
    private $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function getUserNotifications()
    {
        $userId = Auth::id();
        $notifications = $this->notificationService->getUserNotifications( $userId );
        return response()->json(['notifications' => $notifications]);
    }

    public function readNotification( Request $request )
    {
        try {
        } catch (Exception $e) {
        }
    }
}