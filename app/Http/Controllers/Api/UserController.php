<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Auth\LoginProxy;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;

use App\Exceptions\CostaValidationException;
use App\UserDeviceId;

use DB;
use Carbon\Carbon;

class UserController extends ApiController
{
    private $loginProxy;
    private $userService;

    public function __construct(LoginProxy $loginProxy, UserService $userService)
    {
        $this->loginProxy = $loginProxy;
        $this->userService = $userService;
    }

    public function register(Request $request)
    {
        $data = $request->all();

        $validator = $this->registryValidator($data);
        if ($validator->fails()) {
            return $this->respondWithError($validator->errors());
        }

        $data['role_id'] = 1;
        try {
            $user = $this->userService->createUser($data);
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error' => 'Hubo un error al registrarse.']);
        }

        return response()->json($user, 201);
    }

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        try {
            $data = $this->loginProxy->attemptLogin($email, $password);
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error'=> 'El código ingresado es incorrecto. Por favor verificá que sea el que te enviamos por mail.']);
        }

        $user_found = null;

        if ($data) {
            try {
                //Chequear si la invitacion es valida desde_hasta y si el usuario fue borrado o no
                $this->userService->checkInvitationValidity($email, $password);
                $user_found = $this->userService->activateUser($email);
            } catch (Exception $e) {
                return $this->respondWithError(['message_error'=> $e->getMessage()]);     
            }
        }

        if ($user_found->email_duplicated) {
            return $this->respondWithError(['message_error' => 'La dirección de email asociada a tu cuenta está siendo utilizada por otro usuario. Por favor comunicate con Atención al Propietario para actualizar tu dirección de email.']);
        }

        // Guardar device uuid si viene
        $deviceUUid = $request->input('device');
        if ($deviceUUid && $deviceUUid != 'none' && $user_found) {
            UserDeviceId::updateOrCreate(
                ['device_uuid'  => $deviceUUid],
                ['user_id'      => $user_found->id, 'updated_at' => Carbon::now()]
            );
        }

        return response()->json($data);
    }

    public function loginDevice( Request $request ) {
        try {
            $deviceUUid = $request->input('device');
            if ($deviceUUid && $deviceUUid != 'none') {
                $foundDevice = UserDeviceId::where('device_uuid', $deviceUUid)->first();
                if ($foundDevice) {
                    $foundDevice->update(['updated_at' => Carbon::now()]);
                    $user = User::find($foundDevice->user_id);
                    $data = $this->loginProxy->attemptLogin($user->email, 'adminCosta2021');
                    return [
                        'user'          => $user,
                        'access_token'  => $data['access_token']
                    ];
                }
            }

            return response('error', 400);
        } catch(\Exception $e) {
            return response('error', 400);
        }
    }

    public function apiLoginRefresh(Request $request)
    {
        return response($this->loginProxy->attemptRefresh());
    }

    public function apiLogout()
    {
        $this->loginProxy->logout();

        return $this->response(null, 204);
    }

    public function checkUserDocument(Request $request)
    {
        $document = $request->input('document');
        try {
            $data = $this->userService->checkDocument($document);
            return response()->json($data);
        } catch(CostaValidationException $e) {
            return $this->respondWithError(['message_error'=> $e->getMessage()]);
        } catch(\Exception $e) {
            $user = $this->userService->getUserByDocument($document);

            if ($user) {
                return $this->respondWithError(['message_error'=> 'Ocurrió un error al enviar el código a su email (' . $user->email . '). Por favor verifique que su email sea válido.']);
            } else {
                return $this->respondWithError(['message_error'=> 'Ocurrió un error al enviar el código a su email. Por favor verifique que su email sea válido.']);
            }
        }
    }

    private function registryValidator($data)
    {
        return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6|confirmed',
                'sede_id' => 'required'
            ]
        );
    }

    public function updateToken(Request $request)
    {
        $userId = Auth::id();
        $this->validate($request, ['device_token' => 'required']);
        $token = $request->get('device_token');

        $result = $this->userService->refreshDeviceToken($token, $userId);
        return response()->json($result);
    }

    public function getUserProfile() {
        try {
            $user = $this->userService->getUserById(Auth::id());
            return response()->json($user, 200);
        } catch(\Exception $e) {
            return response()->json(['message_error' => 'Hubo un error al obtener el usuario.'], 401);
        }
    }

    public function getUserRelateds(Request $request)
    {
        $user = Auth::user();
        try {
            $data = $this->userService->getRelatedUsers($user);
            return response()->json($data);
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error'=> $e->getMessage()]);
        }
    }

    public function addUserRelated(Request $request) 
    {
        $user = Auth::user();
        $data = $request->all();
        $data['email'] = strtolower(trim($data['email']));
        $data['dni'] = trim(str_replace('.', '', $data['dni']));

        try {
            if ($user->type == 'familiar' && $data['userType'] == 'inquilino') {
                return $this->respondWithError(['message_error'=> 'Atención, por ahora solamente el usuario propietario titular del lote está autorizado para añadir inquilinos']);
            }
            if (!is_numeric($data['dni'])) {
                return $this->respondWithError(['message_error'=> 'Ups! El DNI ingresado no parece tener un formato válido! Por favor revisá el DNI (no debe contener puntos, ni letras, ni espacios!']);
            }
            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            } else {
                return $this->respondWithError(['message_error'=> 'Ups! El Email ingresado no posee un formato válido! Por favor revisá los datos ingresados y volvé a intentar!']);
            }
            $data = $this->userService->addRelatedUser($user, $data);
            return response()->json($data);
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error'=> $e->getMessage()]);
        }
    }

    public function updateUserRelated( Request $request ) {
        $user = Auth::user();
        $data = $request->all();
        $data['email'] = strtolower(trim($data['email']));
        $data['dni'] = trim(str_replace('.', '', $data['dni']));

        try {
            if ($user->type == 'familiar' && $data['userType'] == 'inquilino') {
                return $this->respondWithError(['message_error'=> 'Atención, por ahora solamente el usuario propietario titular del lote está autorizado para añadir inquilinos']);
            }
            if (!is_numeric($data['dni'])) {
                return $this->respondWithError(['message_error'=> 'Ups! El DNI ingresado no parece tener un formato válido! Por favor revisá el DNI (no debe contener puntos, ni letras, ni espacios!']);
            }
            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            } else {
                return $this->respondWithError(['message_error'=> 'Ups! El Email ingresado no posee un formato válido! Por favor revisá los datos ingresados y volvé a intentar!']);
            }
            $data = $this->userService->saveRelatedUser($user, $data);
            return response()->json($data);
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error'=> $e->getMessage()]);
        }
    }

    public function deleteUser(Request $request) {
        $data = $request->all();

        try {
            $data = $this->userService->softDeleteUser($data['userId']);
            return response()->json($data);
        } catch(\Exception $e) {
            return $this->respondWithError(['message_error'=> $e->getMessage()]);
        }
    }

    public function search(Request $request) {
        $users = User::where('name','like', '%' . $request->get('query') . '%')
                        ->orWhere('dni', 'like', '%' . $request->get('query') . '%')
                        ->select('id', DB::raw('CONCAT(dni, " - ", name) as name'))
                        ->get();
        return response()->json($users);
    }
}