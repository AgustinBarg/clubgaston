<?php

namespace App\Http\Controllers\Api;

use App\Services\MetricsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MetricsController extends ApiController
{

    public function __construct() {
    }

    public function addView(Request $request) {
        MetricsService::addView($request->get('name'));
        return response()->json(['success' => true], 200);
    }
}