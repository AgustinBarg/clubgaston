<?php

namespace App\Http\Controllers\Api;

use App\Services\ExpensesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExpensesController extends ApiController
{
    public function __construct()
    {
    }

    public function getUserExpenses()
    {
        $userId = Auth::id();
        try {
            $expenses = ExpensesService::getUserExpenses($userId);
            return response()->json($expenses);
        } catch (\Exception $e) {
            return $e;
            // return response()->json(['error' => $e->getMessage()] , 400);
        }
    }
}