<?php
 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request;

use App\Services\PaymentsService;
 
class PaymentsController extends ApiController
{
    public function __construct(){
    }

    public function processPayment(Request $request) {
    	$notification_type = $request->input('type');
		$data = $request->input('data');
		
    	if ($notification_type == 'payment') {
            $notification_id = $data['id'];
    		$result = PaymentsService::processPayment($notification_id);
            return response()->json($result, 200);
    	} else {
    		return response()->json('success', 200);
    	}
    }
}