<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

use App\Services\AuthorizationsService;
use App\Services\EmergenciesService;

class EmergenciesController extends ApiController
{
    private $emergenciesService;

    public function __construct( EmergenciesService $emergenciesService ) {
        $this->emergenciesService = $emergenciesService;
    }

    public function callEmergency( Request $request ) {
        try {
            $userId = Auth::id();
            $params = $request->all();
            $response = $this->emergenciesService->callEmergency( $userId, $params );
            return response()->json($response, 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()] , 400);
        }
    }
}