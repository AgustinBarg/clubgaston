<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Validators\ReservaGymValidator;
use App\ReservaConfig;
use App\Services\PaymentsService;
use App\Services\ReservasGymService;

use App\User;
use App\UserCreditsPackage;
use Exception;

class ReservasGymController extends ApiController
{
    public function __construct() {
    }

    public function getCreditsAndPayments() {
        return response()->json(ReservasGymService::getCreditsAndPayments(Auth::id()), 200);
    }

    public function getCreditsMovements() {
        return response()->json(ReservasGymService::getCreditsMovements(Auth::id()), 200);
    }

    public function getPackages() {
        return response()->json(ReservasGymService::getCreditsPackages(), 200);
    }

    public function getReservasByUser() {
        return ReservasGymService::getUserReservas(Auth::id());
    }

    // TESTING PAYMENT
    public function generateCreditsPayment( Request $request ) {
        $user = User::fin(5);
        $params = $request->all();
        $payment = PaymentsService::generateMercadoPagoPayment($user, [
    		'amount'	=> $params['amount'],
    		'type'		=> 'creditos-gym',
            'method'    => 'mercadopago'
        ]);
        return $payment;
    }

    public function buyPackage( Request $request ) {
        try {
            $user = Auth::user();

            $config = ReservaConfig::find(1);
            if (!$config->gym_allow_credits) {
                return response($config->gym_allow_credits_text, 400);
            }

            $existingPayment = PaymentsService::getUserPayment($user->id, 'creditos-gym', 'pending');
            if ($existingPayment) {
                return response('Ups! Ya tenés un pago en estado pendiente para comprar créditos. <br>
                                Asegurate de pagarlo, o esperá 15 minutos hasta que el mismo cumpla su vencimiento y asi crear uno nuevo!', 400);
            }

            $existingPackage = UserCreditsPackage::where('user_id', $user->id)
                                                ->where('finished', false)
                                                ->first();
            if ($existingPackage) {
                return response('Ups! Ya tenés créditos disponibles para utilizar. <br>
                                Si no te aparecen, por favor refrescá la sección de reservas. <br>
                                Si el problema persiste, comunicate con soporte.', 400);
            }

            $params = $request->all();
            $packageData = ReservasGymService::getPackage( $params['package_id' ]);
            $payment = PaymentsService::generateMercadoPagoPayment($user, [
                'amount'	=> $packageData['amount'],
                'type'		=> 'creditos-gym',
                'method'    => 'mercadopago',
                'description'=> $packageData['credits'] . ' ' . $packageData['subtitle'] . ' (' . $packageData['description'] . ')'
            ]);
            return response()->json($payment['preference'], 200);
        } catch (Exception $e) {
            return response('Ups! No se pudo generar el link de pago. Por favor intentá nuevamente más tarde!', 400);
        }
    }

    public function getTimesAvailable( Request $request ) {
        $params = $request->all();
        $timesInfo =  ReservasGymService::getTimesAvailable(Auth::user()->type, $params['date']);
        if (isset($timesInfo['error'])) {
            return response()->json($timesInfo['error'], $timesInfo['code']);
        } else {
            return response()->json($timesInfo, 200);
        }
    }

    public function postReserva(Request $request) {
        $user = Auth::user();
        $data = $request->all();

        if ($user->moroso) {
            return $this->respondWithError('Este es un mensaje para los morosos. Quiere decir que no podes realizar una reserva. Por favor contactáte con la administración para poder realizar reservas.', 422);
        }

        $validator = ReservaGymValidator::validate($user, $data);

        if (isset($validator['error'])) {
            return $this->respondWithError($validator['error'], 400);
        }

        try {
            $reserva = ReservasGymService::createReserva($user, $data);
            return response()->json(['reserva' => $reserva], 201);
        } catch(\Exception $e) {
            return $this->respondWithError($e->getMessage(), 400);
        }
    }

    public function cancelReserva( $reservaId ) {
        $user = Auth::user();
        $result = ReservasGymService::delete($user, $reservaId);
        if (isset($result['error'])) {
            return response()->json(['error' => $result['error']], 400);
        } else {
            return response()->json(['success' => true], 201);
        }
    }
}