<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\ticketsIncidente;
use App\ticketsMessage;
use App\ticketsMessageImage;

use App\Services\IncidenteService;

use Mail;

class ticketMessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'message' => 'required|string|max:1000',
        ]);
    }

    public function sendMessage(Request $request) {
        $userId = Auth::id();
        $data = $request->all();

        $newTicketMessage = ticketsMessage::create([
            'user_id' => $userId,
            'tickets_incidente_id' => $data['id'],
            'message' => $data['message']
        ]);

        if ($request->file('image') != null) {
            $imageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/', $imageName
            );
            ticketsMessageImage::create([
                'ticket_message_id' => $newTicketMessage->id,
                'image_path' => $imageName
            ]);
        }

        $ticket_updated = ticketsIncidente::find($data['id']);
        $info = array('type' => 'gestion', 'id' => $ticket_updated->id);

        IncidenteService::sendNotification($ticket_updated->user->id, 'Mensaje Nuevo en Gestión', 'Tu Gestion: ' . $ticket_updated->title . ' recibió un mensaje.', $info);

        Mail::send('mails.mail-update-incident-user', ['incident' => $ticket_updated], function($message) use ($ticket_updated)
        {
            $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
            $message->to($ticket_updated->user->email)->subject('Nuevo Mensaje en Gestion');
        });

        return back();
    }

}