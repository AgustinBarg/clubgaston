<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Requests\CreateUser;
use App\ResponsibleArea;
use App\Services\AuthorizationsService;
use App\Services\UserService;

use Exception;
use Illuminate\Support\MessageBag;

class UserController extends Controller
{

    private $userService;
    private $authorizationsService;

    public function __construct(UserService $userService, AuthorizationsService $authorizationsService) {
        $this->userService = $userService;
        $this->authorizationsService = $authorizationsService;
    }

    /**
     * Show user list.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loggedIn = Auth::user();

        if ($loggedIn->role_id == 2) {
            return redirect('reservas/list');
        } else if ($loggedIn->role_id == 3) {
            return redirect('incidentes');
        }

        $users = $this->userService->listUsers($request->get('searchText'), $request->get('order_by'), $request->get('order_direction'));
        return view('user.userlist')->with([
                'users' => $users, 
                'searchText' => $request->get('searchText'),
                'status' => "El usuario se ha agregado con exito."]);
    }

    public function createAdminIndex() {
        $areas = ResponsibleArea::all();
        return view('user.create-admin')->with(['areas' => $areas]);
    }

    public function createAdmin( Request $request ) {
        $result = $this->userService->createUserAdmin($request->all());
        if (isset($result['error'])) {
            $messageBag = new MessageBag();
            $messageBag->add('error', $result['error']);
            return back()->withErrors($messageBag)->withInput();
        } else {
            $request->session()->flash('status', 'El usuario ha sido creado correctamente!');
            return back();
        }
    }

    public function newAction(CreateUser $request)
    {
        $this->userService->createUser($request->all());
        return redirect('usuarios')->with('status', 'El usuario ha sido creado con exito');
    }

    public function editUser(Request $request)
    {}

    public function updateUser()
    {}

    public function getProfile( $userId ) {
        $user = $this->userService->getUserById($userId);

        $openKey = [];
        $openKeyUsers = [];
        try {
            $openKey = $this->authorizationsService->getPersonData($user);
            $openKeyUsers = $this->authorizationsService->listAuthorizations($user->id);
        } catch(Exception $e) {}

        return view('user.profile')->with([
            'user'          => $this->userService->getUserById($userId),
            'relatedUsers'  => $user->type === 'owner' ? $this->userService->getRelatedUsers($user) : [],
            'openKey'       => $openKey,
            'openKeyUsers'  => $openKeyUsers
        ]);
    }

    public function editEmail( $userId, Request $request) {
        try {
            $email = $request->get('email');
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            } else {
                $request->session()->flash('error', 'Ups! El Email ingresado: (' . $email . ') no posee un formato válido! Por favor revisá los datos ingresados y volvé a intentar');
                return back();
            }
            $this->userService->updateUserEmail($userId, $email);
            $request->session()->flash('success', 'El email del usuario fue actualizado correctamente!');
            return back();
        } catch (Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return back();
        }
    }
}
