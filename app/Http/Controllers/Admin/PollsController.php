<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use League\Flysystem\Exception;

use App\Services\PollsService;

class PollsController extends Controller
{
    private $notificationService;

    public function __construct()
    {
    }

    public function index()
    {
        $polls = PollsService::getAll();
        return view('polls.list')->with(['polls' => $polls]);
    }

    public function createPoll() {
        return view('polls.create');
    }

    public function savePoll( Request $request ) {
        $this->validate($request, [
            'question' => 'required',
            'valid_from' => 'date|required',
            'valid_to' => 'date|required'
        ], [
            "Por favor verifique que la encuesta tenga una pregunta, una fecha inicio y fecha fin."
        ]);

        $newPoll = PollsService::createPoll( $request->all() );
        return redirect('encuestas/list')->with('status', 'Encuesta Agregada');
    }

    public function viewPoll( $pollId, Request $request )
    {
        $params = $request->all();
        $poll = PollsService::getPollStats( $pollId, $params );
        return view('polls.details')->with(['poll' => $poll, 'params' => $params]);
    }

    public function deletePoll( $pollId ) {
        PollsService::deletePoll( $pollId );
        return redirect('encuestas/list')->with('status', 'Encuesta Eliminada');
    }

    public function getAllPollsReports() {
        $polls = PollsService::getAll();
        return view('polls.list-report')->with(['polls' => $polls]);
    }
}