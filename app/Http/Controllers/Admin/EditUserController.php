<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\User;

class EditUserController extends Controller
{
    public function showUserId($id){
        $userId = $id;
        $user = User::where('id', $userId)->get();
        return response()->json(
            [
                'user' => $user
            ]);
    }

    protected function editUser(Request $request, User $user) {
      $data =  $request->except(['password']);

        User::where('id', $data['id'])
            ->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'role' => $data['role'],
            ]);
        return view('home')->with('status', 'Usuario Editado!');
    }

}
