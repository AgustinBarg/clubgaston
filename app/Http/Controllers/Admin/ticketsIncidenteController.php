<?php

namespace App\Http\Controllers\Admin;

use App\Categorias;
use App\IncidenteState;
use App\Repositories\IncidenteRepository;
use App\ResponsibleArea;
use App\SubArea;
use App\ticketsMessage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\ticketsIncidente;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Repositories\IncidenteInterface;
use Illuminate\Support\MessageBag;

class ticketsIncidenteController extends Controller
{
    protected $incidentesRepository;

    public function __construct(IncidenteInterface $incidentesRepository)
    {
        $this->incidentesRepository = $incidentesRepository;
    }

    public function getIncidente(Request $request)
    {
        $temasResponableArea = SubArea::with('responsableArea')->get()->sortBy(function($item) {
            return $item->responsableArea->name . ' - ' . $item->name;
        });
        $incidente = $this->incidentesRepository->getWithMessages($request->get('id'));
        return view('incidentes.mensajesincidente', [
            'incidente'             => $incidente,
            'states'                => IncidenteState::all('id', 'name'),
            'temasResponableArea'   => $temasResponableArea
        ]);
    }

    public function indexTickets(Request $request)
    {
        $userId = Auth::id();
        $tickets = ticketsIncidente::where('user_id', $userId)->get()->toJson();
        return view('incidentes.incidentesindex')->with('tickets', json_decode($tickets, true));
    }

    public function create()
    {
        $user = Auth::user();
        $categories = Categorias::all('id', 'name');
        $subAreas = SubArea::with('responsableArea')->get();
        return view('incidentes.reportarincidente', ['user' => $user, 'subAreas' => $subAreas, 'categories' => $categories]);
    }

    protected function postIncidente(Request $request) {
        $this->validate($request, [
            'tema_responsable_area_id' => 'required',
            'detalle' => 'required',
            'title' => 'required',
            'location' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ], [
            "title.required" => "Se necesita un titulo.",
            "detalle.required" => "Se debe escribir un detalle del incidente",
            "location.required" => "Se debe colocar una Ubicación (Lote, sector, lugar)",
            "image.mimes" => "Formato de imagen no válida, solo usar JPEG, JPG, GIF, PNG."
        ]);

        if ($request->file('image') != null) {
            $imageName = time() . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/', $imageName
            );
            $data['image'] = $imageName;
        }

        $data = $request->all();
        $validation = $this->incidentesRepository->createIncidente($data, Auth::id());

        if (isset($validation['error'])) {
            $messageBag = new MessageBag();
            $messageBag->add('error', $validation['error']);
    
            return back()->withErrors($messageBag)->withInput();
        }

        return Redirect::to('incidentes');
    }

    public function index(Request $request)
    {
        $query = $request->get('query');
        $temaId = $request->get('tema_id');
        $areaId = $request->get('area_id');
        $stateId = $request->get('state_id');
        $incidenteId = $request->get('incidente_id');
        $userName = $request->get('user_name');
        $parentuserName = $request->get('parentuser_name');
        $orderBy = $request->get('order_by');
        $orderDirection = $request->get('order_direction');

        $incidentes = $this->incidentesRepository->getAll($query, $temaId, $areaId, $stateId, $incidenteId, $userName, $parentuserName, $orderBy, $orderDirection);
        $temasResponableArea = SubArea::with('responsableArea')->get()->sortBy(function($item) {
            return $item->responsableArea->name . ' - ' . $item->name;
        });
        $responsableAreas = ResponsibleArea::all('id', 'name');
        $states = IncidenteState::all('id', 'name');
        return view('incidentes.listadoincidentes', [
            'incidentes' => $incidentes['incidentes'],
            'totals' => $incidentes['totals'],
            'responsableAreas' => $responsableAreas, 
            'temasResponableArea' => $temasResponableArea, 
            'states' => $states,
            'orderBy' => $orderBy, 
            'orderDirection' => $orderDirection,
            'temaId' => $temaId,
            'areaId' => $areaId,
            'userName' => $userName,
            'query' => $query,
            'parentuserName' => $parentuserName,
            'incidenteId' => $incidenteId,
            'stateId' => $stateId

        ]);
    }

    protected function edit(Request $request) {
        $this->validate($request, array('id' => 'required'));
        $params = $request->all();
        $this->incidentesRepository->update($params);

        if (isset($params['noredirect'])) {
            return redirect('incidente?id=' . $params['id'])->with('status', 'El incidente se ha editado correctamente');
        } else {
            return redirect('incidentes')->with('status', 'El incidente se ha editado correctamente');
        }
    }
}
