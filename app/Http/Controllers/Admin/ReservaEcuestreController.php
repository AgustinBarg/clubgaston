<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Excel;
use App\Http\Validators\ReservaEcuestreValidator;

use App\Services\ReservasEcuestreService;
use App\ReservaEcuestre;
use App\User;
use App\EspacioEcuestre;
use App\Services\PaymentsService;

class ReservaEcuestreController extends Controller
{
    private $reservasEcuestreService;

    public function __construct(ReservasEcuestreService $reservasEcuestreService)
    {
        $this->reservasEcuestreService = $reservasEcuestreService;
    }

    public function getReservas(Request $request){
        $query = $request->get('query');
        $espacioId = $request->get('espacio_id');
        $orderBy = $request->get('order_by');
        $orderDirection = $request->get('order_direction');
        $date = $request->get('date');
        $reservas = $this->reservasEcuestreService->getAll($query, $espacioId, $orderBy, $orderDirection, $date);

        $espacios = EspacioEcuestre::orderBy(DB::raw('LENGTH(nombre), nombre'))->get();

        return view('reservas-ecuestre.list')->with([
            'reservas' => $reservas,
            'espacios' => $espacios,
            'orderBy' => $orderBy,
            'orderDirection' => $orderDirection,
            'espacioId' => $espacioId,
            'date' => $date
        ]);
    }


    public function exportExcel(){

      $reservas = ReservaEcuestre::where('deleted', false)
                            ->where('fechaInicio', '>', date('Y-m-d 00:00:00'))
                            ->where('fechaInicio', '<', date('Y-m-d 23:59:59'))
                            ->with('payment')
                            ->get();

      Excel::create('Reservas '.date("d-m-Y"), function($excel) use($reservas) {
          $excel->sheet('Reservas', function($sheet) use($reservas) {
              $reservasExcel = [];

            foreach ($reservas as $r)
                $reservasExcel[] = array('Socio' => $r->user->name,
                                    'Espacio' => $r->espacio->nombre,
                                    'Fecha Inicio' => $r->fechaInicio,
                                    'Fecha Fin' => $r->fechaFin,
                                    'Notas' => $r->notas,
                                    'Monto' => $r->amount ? $r->amount : '-',
                                    'Forma Pago' => $r->payment ? $r->payment->method : '-',
                                    'Estado Pago' => $r->payment ? $r->payment->getStatusAdmin() : '-'
              );


            $sheet->fromArray($reservasExcel);

          });

      })->export('xls');

    }

    public function delete(Request $request) {
        $reservaId = $request->get('id');
        $this->reservasEcuestreService->delete($reservaId, Auth::id());
        return redirect('/reservas-ecuestre/list')->with('status', 'Reserva Eliminada');
    }

    public function create() {
        $espacios = EspacioEcuestre::orderBy(DB::raw('LENGTH(nombre), nombre'))->get();
        $users = User::all();
        return view('reservas-ecuestre.create')
                ->with(['espacios' => $espacios, 
                        'users' => $users]);
    }

    public function checkPlaces(Request $request) {
        $params = $request->all();
        $calculatedPeriod = $this->reservasEcuestreService->calculatePeriod($params['period'], $params['date']);
        $espacio = EspacioEcuestre::find($params['espacio_id']);

        return view('reservas-ecuestre.check-places')
                ->with(['period' => $calculatedPeriod,
                        'espacio' => $espacio,
                        // MULTIPLIER ALREADY CALCULATED IN PLACES
                        'multiplier' => 1,
                        'places' => $this->reservasEcuestreService->getPlacesForPeriod($espacio->category_id, $calculatedPeriod, $params['period'])]);
    }

    public function newReserva(Request $request) {
        $data = $request->all();
        $user = User::find($data['user_id']);

        $this->validate($request, [
            'user_id' => 'required',
            'date' => 'date|required',
            'espacio_id' => 'required'
        ], [
            'user_id.required' => 'No se esta enviando un usuario valido',
            'date.required' => 'Se necesita agregar una fecha de ingreso'
        ]);

        $validator = ReservaEcuestreValidator::validate($user, $data, true);

        if (isset($validator['error'])) {
            return redirect('reservas-ecuestre/create')
                        ->withErrors(['validation' => $validator['error']])
                        ->withInput();
        }

        try {
            $calculatedPeriod = ReservasEcuestreService::calculatePeriod($data['period'], $data['date']);
            $data['fechaInicio'] = $calculatedPeriod['from'];
            $data['fechaFin'] = $calculatedPeriod['to'];
            $reserva = ReservasEcuestreService::createReserva($user, $data);
            if ($reserva->amount > 0) {
                $paymentData = PaymentsService::createPayment([
                    'user_id' => $reserva->user_id,
                    'amount'  => $reserva->amount,
                    'type'    => 'reserva-ecuestre',
                    'method'  => $data['payment_method'],
                    'status'  => 'approved-manual'
                ]);
                $reserva->status = 'confirmed';
                $reserva->payment_id = $paymentData->id;
                $reserva->save();
            } else {
                $reserva->status = 'confirmed';
                $reserva->save();
            }
            return redirect('/reservas-ecuestre/list')->with('status', 'La reserva se ha guardado exitosamente');
        } catch(\Exception $e) {
            return redirect('reservas-ecuestre/create')
                        ->withErrors(['validation' => $e->getMessage()])
                        ->withInput();
        }
    }
}
