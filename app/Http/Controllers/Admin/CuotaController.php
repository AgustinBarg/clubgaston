<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GetCuotasRequest;
use App\Http\Requests\PostCuotaRequest;
use App\Services\CuotasService;
use App\Services\CuotasStatus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Cuota;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CuotaController extends Controller
{
    private $cuotaService;

    public function __construct(CuotasService $cuotaService)
    {
        $this->cuotaService = $cuotaService;
    }

    public function create(Request $request)
    {
        $users = null;
        $user = null;
        if (($userId = $request->get('id')) != null) {
            $user = User::find($userId);
        } else {
            $users = User::select('id','name')->get();
        }

        return view('cuotas.crear')->with(['users' => $users, 'user' => $user]);
    }

    protected function post(PostCuotaRequest $request)
    {
        $this->cuotaService->create($request->all());
        return redirect('cuotas/list')->with('status', 'Cuota Agregada');
    }

    public function getCuotas()
    {
        $cuotas = $this->cuotaService->getCuotasWithUser();
        foreach ($cuotas as $cuota) {
            switch ($cuota->status){
                case CuotasStatus::PAYED:
                    $cuota->paymentStatusMessage = "El pago se encuentra aprobado";
                    break;
                case CuotasStatus::PENDING:
                    $cuota->paymentStatusMessage = "El pago esta pendiente";
                    break;
                case CuotasStatus::INFORMED:
                    $cuota->paymentStatusMessage = "El pago ha sido informado por el usuario";
                    break;
                case CuotasStatus::REJECTED;
                    $cuota->paymentStatusMessage = "El pago se encuentra rechazado por el administrador";
                default:
                    $cuota->paymentStatusMessage = 'Estado de pago no valido';
            }
        }


        return view('cuotas.last_cuotas_list')->with('cuotas', $cuotas);
    }

    public function informarPago(Request $request)
    {
        $this->cuotaService->informarPago($request->all());
        return redirect('cuotas/list')->with('status', 'Pago de cuota Informado');
    }

    public function getPaymentData(Request $request)
    {
        $cuota = $this->cuotaService->getCuota($request->get('id'));
        return view('cuotas.payment_data')->with('cuota', $cuota);
    }

    public function approvePayment(Request $request)
    {
        $this->cuotaService->approvePayment($request->get('cuota_id'));
        return redirect('cuotas/list')->with('status', 'El pago ha sido aprobado');
    }

    public function rejectPayment(Request $request)
    {
        $this->cuotaService->rejectPayment($request->get('cuota_id'));
        return redirect('cuotas/list')->with('status', 'El pago ha sido rechazado');
    }

    public function getCuotasByUser(Request $request)
    {
        $userId = $request->get('id');
        $userCuotas = $this->cuotaService->getCuotasByUser($userId);
        $user = User::findOrFail($request->get('id'));
        return view('cuotas.user_cuotas')->with(['cuotas' => $userCuotas, 'user' => $user]);
    }

    public function edit(Request $request)
    {
        $cuota = $this->cuotaService
            ->getCuota($request->get('id'));
        return view('cuotas.edit')->with('cuota', $cuota);
    }

    public function update(PostCuotaRequest $request)
    {
        $this->cuotaService->update($request->all());
        return redirect('cuotas/list')->with('status', 'Cuota Editada!');
    }

}
