<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Services\MetricsService;

class MetricsController extends Controller
{
    public function __construct() {
    }

    // ANALYTICS
    public function getViewsTop(Request $request) {
        return MetricsService::getAllViews($request->input('dateRange'), 10);
    }

    public function getViewsTable(Request $request) {
        $dateRange = $request->input('dateRange');
        return view('reports.table-views')->with(['results' => MetricsService::getAllViews($dateRange)]);
    }

    public function getActiveUsersByOS(Request $request) {
        return MetricsService::getActiveUsersByOS($request->input('dateRange'));
    }

    public function analytics() {
        return view('reports.analytics');
    }

}
