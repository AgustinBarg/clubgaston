<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/10/17
 * Time: 3:33 PM
 */

namespace App\Http\Controllers\Admin;

use League\Flysystem\Exception;

use Illuminate\Http\Request;
use App\Http\Requests\SendNotificationRequest;
use App\Interest;
use App\NotificationSchedule;
use App\Services\InterestsService;
use App\User;
use App\Services\NotificationService;

class NotificationsController extends Controller
{
    private $interestsService;
    private $notificationService;

    public function __construct(InterestsService $interestsService, NotificationService $notificationService)
    {
        $this->interestsService = $interestsService;
        $this->notificationService = $notificationService;
    }

    public function index()
    {
        $notifications = $this->notificationService->getLastNotifications();
        return view('notifications.list')->with(['notifications' => $notifications]);
    }

    public function indexScheduled()
    {
        $notifications = $this->notificationService->getLastNotificationsScheduled();
        return view('notifications.list-programed')->with(['notifications' => $notifications]);
    }

    public function deleteScheduled( Request $request)
    {
        $notificationId = $request->get('id');
        NotificationSchedule::find($notificationId)->update(['deleted' => true]);
        return back()->with('status', 'La notificación fue eliminada correctamente!');
    }

    public function saveNotification( Request $request ) {
        $type = $request->input('notification_time');
        return $type == 'schedule' ? $this->scheduleNotification($request) : $this->sendNotification($request);
    }

    public function scheduleNotification( Request $request ) {
        try {
            $schedule = $this->notificationService->scheduleNotification($request->all());
            if (isset($schedule['error'])) {
                return back()->with('error', $schedule['error'])->withInput();
            } else {
                return redirect('notifications/list')->with('status', 'La notificación se enviará dentro de un rango de 15 minutos posteriores a la fecha seleccionada!');
            }
        } catch (Exception $e) {
            return redirect('notifications/list')->with('status', 'La notificación se enviará dentro de un rango de 15 minutos posteriores a la fecha seleccionada!');
        }
    }

    public function sendNotification(Request $request)
    {
        try {
            $receiverDevices = $this->notificationService->sendNotificationsToUsers($request->all());
            return redirect('notifications/list')->with('status', 'La notificación se enviará durante los proximos 5 minutos a todos los destinatarios.');
        } catch (Exception $e) {
            return redirect('notifications/list')->with('status', 'La notificación se enviará durante los proximos 5 minutos a todos los destinatarios.');
        }
    }

    public function createNotification()
    {
        $times = [];
        for ($i= 9; $i <= 23; $i++) { 
            array_push($times, $i . ':00');
            array_push($times, $i . ':30');
        }
        return view('notifications.create')->with([
            'times'     => $times,
            'intereses' => $this->interestsService->getInterestsWithUsers(),
            'sectores'  => User::where('lote_sector', '!=', null)
                                    ->select('lote_sector')
                                    ->distinct()
                                    ->pluck('lote_sector')
        ]);
    }
}