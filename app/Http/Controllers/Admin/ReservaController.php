<?php

namespace App\Http\Controllers\Admin;

use App\Categorias;
use App\Services\ReservasService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Reserva;
use App\User;
use App\Espacio;
use App\ReservaConfig;
use App\ReservaTimes;
use App\Services\PaymentsService;
use Carbon\Carbon;

use Excel;


use League\Flysystem\Exception;

class ReservaController extends Controller
{
    private $reservaService;

    public function __construct(ReservasService $reservasService)
    {
        $this->reservaService = $reservasService;
    }

    public function getReservasConfig() {
        return view('reservas.config')->with([
            'config'    => ReservaConfig::find(1),
            'times'     => ReservaTimes::all()
        ]);
    }

    public function saveReservasConfig( Request $request ) {
        $params = $request->all();

        if ($params['gym_owner_inicio'] >= $params['gym_owner_fin']) {
            $request->session()->flash('error', 'Ups! La hora de inicio no puede ser mayor a la hora de fin para el horario de propietarios!');
            return redirect('reservas/config');
        }
        
        if ($params['gym_inquilino_inicio'] >= $params['gym_inquilino_fin']) {
            $request->session()->flash('error', 'Ups! La hora de inicio no puede ser mayor a la hora de fin para el horario de inquilinos!');
            return redirect('reservas/config');
        }

        ReservaConfig::find(1)->update($params);
        $request->session()->flash('status', 'La configuración ha sido guardada correctamente!');
        return redirect('reservas/config');
    }

    public function getReservas(Request $request){
        $query = $request->get('query');
        $espacioId = $request->get('espacio_id');
        $orderBy = $request->get('order_by');
        $orderDirection = $request->get('order_direction');
        $validated = $request->get('validated');
        $date = $request->get('date');
        $reservas = $this->reservaService->getAll($query, $espacioId, $orderBy, $orderDirection, $date, $validated);

        $espacios = Espacio::whereNotNull('categoria_id')->orderBy(DB::raw('LENGTH(nombre), nombre'))->get();

        return view('reservas.list')->with([
            'reservas' => $reservas,
            'espacios' => $espacios,
            'orderBy' => $orderBy,
            'orderDirection' => $orderDirection,
            'espacioId' => $espacioId,
            'date' => $date,
            'validated' => $validated
        ]);
    }


    public function exportExcel(){

      $reservas = Reserva::where('deleted', false)
                            ->where('horaInicio', '>', date('Y-m-d 00:00:00'))
                            ->where('horaInicio', '<', date('Y-m-d 23:59:59'))
                            ->with('payment')
                            ->get();

      Excel::create('Reservas '.date("d-m-Y"), function($excel) use($reservas) {
          
          $excel->sheet('Reservas', function($sheet) use($reservas) {
              $reservasExcel = [];

            foreach ($reservas as $r)
                $reservasExcel[] = array('Socio' => $r->user->name,
                                    'Espacio' => $r->espacio->nombre,
                                    'Hora Inicio' => $r->horaInicio,
                                    'Hora Fin' => $r->horaFin,
                                    'Notas' => $r->notas,
                                    'Monto' => $r->amount ? $r->amount : '-',
                                    'Forma Pago' => $r->payment ? $r->payment->method : '-',
                                    'Estado Pago' => $r->payment ? $r->payment->getStatusAdmin() : '-'
              );


            $sheet->fromArray($reservasExcel);

          });

      })->export('xls');

    }

    public function delete(Request $request) {
        $reservaId = $request->get('id');
        $this->reservaService->delete($reservaId, Auth::id());
        return redirect('/reservas/list')->with('status', 'Reserva Eliminada');
    }

    public function validateReserva(Request $request) {
        $reservaId = $request->get('id');
        $this->reservaService->validateReservaAdmin($reservaId, Auth::id());
        return redirect('/reservas/list')->with('status', 'Reserva Validada Correctamente!');
    }

    public function create() {
        $espacios = Espacio::whereNotNull('categoria_id')->with('categoria')->orderBy(DB::raw('LENGTH(nombre), nombre'))->get();
        $users = User::all();
        $freeHours = range(8, 23);

        return view('reservas.create')->with(['espacios' => $espacios, 'users' => $users, 'freeHours' => $freeHours]);
    }

    public function newReserva(Request $request) {
        $userId = Auth::id();
        $data = $request->all();
        $data["horaFin"] = $request->get("horario_reserva") + 1;

        $this->validate($request, [
            'user_id' => 'required',
            'fecha_reserva' => 'date|required',
            'espacio_id' => 'required',
            'horario_reserva' => 'required|userHasManyReservations:' . $data['user_id'] . '|notSpaceCalendarConflict:' . $data['horario_reserva'] . ',' . $data['horaFin'] . ',' . $data['fecha_reserva'] . ',' . $data['espacio_id'] . '|dateNotBeforeNow:' . $data['fecha_reserva'] . ',' . $data['horario_reserva'] . ',' . $data['horaFin'] .
            '|childrenOrParentIsReserved:' . $data['horario_reserva'] . ',' . $data['horaFin'] . ',' . $data['fecha_reserva'] . ',' . $data['espacio_id']
        ], [
            "user_id.required" => "No se esta enviando un usuario valido",
            "fecha_reserva.required" => "Se necesita agregar una fecha",
            "horario_reserva.required" => "Se necesita agregar un horario",
            'espacio_id' => 'Es necesario seleccionar un espacio.',
            'fecha_reserva.date' => 'Se debe agregar una fecha valida',
            'horario_reserva.date_not_before_now' => 'La fecha es anterior a la actual',
            'horario_reserva.not_space_calendar_conflict' => 'El espacio se encuentra reservado o no está disponible en ese horario.',
            'horario_reserva.user_has_many_reservations' => 'Atención, no se pueden tener más de 2 (dos) reservas abiertas por usuario. En cualquier momento podés cancelar alguna de tus reservas abiertas y realizar una nueva.',
            'horario_reserva.children_or_parent_is_reserved' => 'Alguno de los espacios dependientes ya ha sido reservado para ese mismo horario.'
        ]);

        $data['horaInicio'] = $data['horario_reserva'];
        $data['horaFin'] = $data['horaInicio'] + 1;
        $data['date'] = $request->get('fecha_reserva');
        $data['user_id'] = $request->get('user_id');
        try {
            $reserva = $this->reservaService->createReserva($data);
            if ($reserva->amount > 0) {
                $paymentData = PaymentsService::createPayment([
                    'user_id' => $reserva->user_id,
                    'amount'  => $reserva->amount,
                    'type'    => 'reserva-deportiva',
                    'method'  => $data['payment_method'],
                    'status'  => 'approved-manual'
                ]);
                $reserva->status = 'confirmed';
                $reserva->payment_id = $paymentData->id;
                $reserva->save();
            } else {
                $reserva->status = 'confirmed';
                $reserva->save();
            }
        } catch(Exception $e) {
            return redirect('/reservas/list')->with('status', $e->getMessage());
        }

        return redirect('/reservas/list')->with('status', 'La reserva se ha guardado exitosamente');
    }

    public function tvList(Request $request) {
        $fechaReservas = $request->get('fecha_reservas');
        if (!$fechaReservas) {
            $fechaReservas = 'today';
        }

        $espacios = Espacio::whereNotNull('categoria_id')->with('reservas')->get();
        $dayFirstHour = (new Carbon($fechaReservas, 'America/Argentina/Buenos_Aires'))->addHours(8);
        $dayLastHour = (new Carbon($fechaReservas, 'America/Argentina/Buenos_Aires'))->addHours(24);
        $countedHours = $dayFirstHour->diffInHours($dayLastHour);

        for($i = 0; $i < $countedHours; $i++) {
            $totalHours[] = $dayFirstHour->hour + $i;
        }

        $espaciosCategoriasGridView = new \stdClass();
        $categorias = Categorias::all();
        $espaciosCategoriasGridView->categorias = array();

        foreach ($categorias as $categoria) {
            $categoriaData = new \stdClass();
            $categoriaData->id = $categoria->id;
            $categoriaData->name = $categoria->name;
            $categoriaData->espacios = array();
            $espaciosCategoriasGridView->categorias[] = $categoriaData;
        }

        $parentSpaceReservationsHours = array();
        $childrenSpaceReservationsHours = array();

        foreach ($espacios as $espacio) {
            $espacioGridView = new \stdClass();
            $espacioGridView->id = $espacio->id;
            $espacioGridView->name = $espacio->nombre;
            $espacioGridView->reservas = array();
            $espacioGridView->horaInicio = $espacio->horaInicio;
            $espacioGridView->horaFin = $espacio->horaFin;
            $espacioGridView->isSpaceChildren = in_array($espacio->id, [2, 4, 5]);
            $espacioGridView->isSpaceParent = $espacio->id == 1;

            $reservas = $this->reservaService->getReservasByTimestamp($dayFirstHour, $dayLastHour, $espacio->id);
            foreach ($totalHours as $hour) {
                $isHourFree = true;

                if ($reservas != null) {
                    foreach($reservas as $reserva) {
                        $horaInicio = Carbon::parse($reserva->horaInicio,'America/Argentina/Buenos_Aires');
                        if ($hour == $horaInicio->hour) {
                            $isHourFree = false;
                            $reservaGridView = new \stdClass();
                            $reservaGridView->userName = $reserva->userName;
                            $reservaGridView->hour = $reserva->horaInicio;
                            $espacioGridView->reservas[] = $reservaGridView;

                            // Si el espacio es cancha de 11
                            if($espacioGridView->isSpaceParent) {
                                $parentSpaceReservationsHours[] = $hour;
                            }

                            // Si el espacio es cancha de 7
                            if($espacioGridView->isSpaceChildren) {
                                $childrenSpaceReservationsHours[] = $hour;
                            }
                        }
                    }
                }

                if($isHourFree){

                    if ($espacio->horaInicio <= $hour && $espacio->horaFin  > $hour) {
                        $espacioGridView->reservas[] = "free";
                    } else {
                        $espacioGridView->reservas[] = "close";
                    }
                }
            }

            foreach ($espaciosCategoriasGridView->categorias as $categoria) {
                if ($espacio->categoria_id == $categoria->id) {
                    $categoria->espacios[] = $espacioGridView;
                }
            }
        }

        foreach ($espaciosCategoriasGridView->categorias as $categoria) {
            foreach($categoria->espacios as $espacio) {
                foreach ($espacio->reservas as $reservaIndex => $reservaValue) {
                    if($espacio->isSpaceChildren) {
                        foreach ($parentSpaceReservationsHours as $hour) {
                            if (($reservaIndex + 8) == $hour) {
                                if(!$reservaValue != "close") {
                                    $espacio->reservas[$reservaIndex] = "parentReserved";
                                }
                            }
                        }
                    } elseif($espacio->isSpaceParent) {
                        foreach ($childrenSpaceReservationsHours as $hour) {
                            if (($reservaIndex + 8) == $hour) {
                                if($reservaValue != "close") {
                                    $espacio->reservas[$reservaIndex] = "childrenReserved";
                                }
                            }
                        }
                    }


                }

            }
        }

        return view('reservas.tv_list')->with(['reservas_date' => $request->get('fecha_reservas'),  'espaciosCategoriasGridView' => $espaciosCategoriasGridView, 'totalHours' => $totalHours]);
    }
}
