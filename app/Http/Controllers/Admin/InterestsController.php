<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Services\InterestsService;

class InterestsController extends Controller
{
    private $interestsService;

    public function __construct(InterestsService $interestsService)
    {
        $this->interestsService = $interestsService;
    }

    public function index()
    {
        return view('interests.list')->with([
            'interests'    => $this->interestsService->getInterestsWithUsers()
        ]);
    }

    public function deleteInterest( $interestId, Request $request )
    {
        try {
            $this->interestsService->deleteInterest($interestId);
            $request->session()->flash('success', 'El Interés fue eliminado correctamente!');
            return back();
        } catch (Exception $e) {
            $request->session()->flash('error', 'Ups! ' . $e->getMessage());
            return back();
        }
    }

    public function saveInterest( Request $request )
    {
        try {
            $id = $request->input('interest_id');
            $title = $request->input('interest_title');
            $this->interestsService->saveInterest($id, $title);
            $request->session()->flash('success', 'El Interés fue creado/actualizado correctamente!');
            return back();
        } catch (Exception $e) {
            $request->session()->flash('error', 'Ups! ' . $e->getMessage());
            return back();
        }
    }
}