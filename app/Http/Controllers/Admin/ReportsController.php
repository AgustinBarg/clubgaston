<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Services\ReportsService;
use App\Services\PollsService;

class ReportsController extends Controller
{
    public function __construct() {
    }

    public function getUsers() {
        return response()->json(
            [
                'userTypes' => ReportsService::getUserTypes(),
                'userAdded' => ReportsService::getAVGUserAdded()
            ], 200);
    }

    public function getTicketsCategories(Request $request) {
        $dateRange = $request->input('dateRange');
        return view('reports.table-tickets')->with(['results' => ReportsService::getTicketsByType($dateRange)]);
    }

    public function getTicketsTotals(Request $request) {
        $dateRange = $request->input('dateRange');
        return response()->json(
            [
                'responseTime' => ReportsService::getAVGTicketsAnswer($dateRange),
                'averageCount' => ReportsService::getAVGTicketsCreated($dateRange),
                'ticketsByUserType' => ReportsService::getTicketsByUserTypeCreated($dateRange),
            ], 200);
    }

    public function getPollAnswers( $pollId = null ) {
        return response()->json([
            'report'  => ReportsService::getPollAnswersByUserType($pollId),
            'content' => $pollId ? view('polls.results')->with(['poll' => PollsService::getPollStats($pollId, null)])->render() : ''
        ], 200);
    }

    public function getReservas(Request $request) {
        $dateRange = $request->input('dateRange');
        return response()->json(
            [
                'deporte' => ReportsService::getReservasByCategory($dateRange),
                'usuarios' => ReportsService::getReservasByUserType($dateRange),
                'tiempo' => ReportsService::getReservasByTime($dateRange)
            ], 200);
    }

    public function home(Request $request)
    {
        return view('reports.home')->with(
            [
                'encuestas' => PollsService::getAll(),
                'deportesEspacios' => ReportsService::getDeportesEspacios()
            ]);
    }

}
