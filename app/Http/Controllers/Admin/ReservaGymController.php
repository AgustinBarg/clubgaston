<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Excel;
use Carbon\Carbon;
use Exception;

use App\Http\Validators\ReservaGymValidator;
use App\Services\ReservasGymService;

use App\User;
use App\Payment;
use App\ReservaGym;
use App\ReservaTimes;
use App\UserCredits;
use App\UserCreditsPackage;

class ReservaGymController extends Controller {

    public function __construct() { }

    public function getUsers() {
        return view('reservas-gym.user-status')->with([]);
    }

    public function showUser(Request $request) {
        $user = User::find($request->get('user_id'));
        return view('reservas-gym.user-status')->with([
            'user'      => $user,
            'payments'  => Payment::where('user_id', $user->id)
                                    ->where('type', 'creditos-gym')
                                    ->orderBy('id', 'desc')
                                    ->get(),
            'packages'  => UserCreditsPackage::where('user_id', $user->id)->orderBy('id', 'desc')->get(),
            'movements' => UserCredits::where('user_id', $user->id)->orderBy('id', 'desc')->get()
        ]);
    }

    public function getReservas(Request $request){
        $query = $request->get('query');
        $orderBy = $request->get('order_by');
        $orderDirection = $request->get('order_direction');
        $validated = $request->get('validated');
        $date = $request->get('date');
        $reservas =  ReservasGymService::getAll($query, $orderBy, $orderDirection, $date, $validated);
        return view('reservas-gym.list')->with([
            'reservas'      => $reservas,
            'orderBy'       => $orderBy,
            'orderDirection'=> $orderDirection,
            'date'          => $date,
            'validated'     => $validated
        ]);
    }

    public function exportExcel(){
      $reservas = ReservaGym::where('deleted', false)
                            ->where('fechaInicio', '>', date('Y-m-d 00:00:00'))
                            ->where('fechaInicio', '<', date('Y-m-d 23:59:59'))
                            ->orderBy('fechaInicio', 'ASC')
                            ->get();

        Excel::create('ReservasGym' . date("d-m-Y"), function($excel) use($reservas) {
            $excel->sheet('ReservasGym', function($sheet) use($reservas) {
                $reservasExcel = [];
                foreach ($reservas as $r)
                    $reservasExcel[] = array('Socio' => $r->user->name,
                                    'Fecha Inicio' => $r->fechaInicio,
                                    'Fecha Fin' => $r->fechaFin,
                                    'Notas' => $r->notas
                );
                $sheet->fromArray($reservasExcel);
            });
        })->export('xls');
    }

    public function create() {
        return view('reservas-gym.create')->with(['times' => ReservaTimes::all()]);
    }

    public function newReserva(Request $request) {
        try {
            $data = $request->all();
            $params = [
                'date'  => $data['fecha_reserva'],
                'time'  => $data['horario_reserva'],
                'notas' => $data['notas']
            ];
            $user = User::find($data['user_id']);

            if (!$user) {
                throw new Exception('Por favor asegurate de elegir a un usuario válido!');
                return;
            }

            $validator = ReservaGymValidator::validate($user, $params);
            if (isset($validator['error'])) {
                throw new Exception($validator['error']);
                return;
            }

            ReservasGymService::createReserva($user, $params);
            return redirect('/reservas-gym/list')->with('status', 'La reserva se ha guardado exitosamente');
        } catch(Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return redirect('/reservas-gym/create')->withInput();
        }
    }

    public function delete(Request $request) {
        $reservaId = $request->get('id');
        $reserva = ReservaGym::find($reservaId);
        ReservasGymService::delete($reserva->user, $reservaId);
        return redirect('/reservas-gym/list')->with('status', 'Reserva Eliminada');
    }

    public function validateReserva(Request $request) {
        $reservaId = $request->get('id');
        ReservasGymService::validateReservaAdmin($reservaId, Auth::id());
        return redirect('/reservas-gym/list')->with('status', 'Reserva Validada Correctamente!');
    }

    public function tvList(Request $request) {
        $fechaReservas = $request->get('fecha_reservas');

        if (!$fechaReservas) {
            $dates = [
                        Carbon::now()->toDateString(),
                        Carbon::now()->addDays(1)->toDateString(),
                        Carbon::now()->addDays(2)->toDateString(),
                        Carbon::now()->addDays(3)->toDateString(),
                        Carbon::now()->addDays(4)->toDateString(),
                        Carbon::now()->addDays(5)->toDateString(),
                        Carbon::now()->addDays(6)->toDateString(),
                        Carbon::now()->addDays(7)->toDateString()
            ];
        } else {
            $dates = [$fechaReservas];
        }

        $times = ReservaTimes::all();
        $availables = [];

        foreach ($dates as $key => $date) {
            array_push($availables, [
                'date'      => $date,
                'places'    => ReservasGymService::getTimesAvailable('owner', $date, true)
            ]);
        }

        return view('reservas-gym.tv_list')->with([
                    'reservas_date' => $request->get('fecha_reservas'),  
                    'availables'    => $availables, 
                    'dates'         => $dates,
                    'times'         => $times
                ]);
    }
}
