<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\EspaciosRepository;
use Illuminate\Http\Request;

class EspacioController extends Controller
{
    private $espacioRepository;

    public function __construct(EspaciosRepository $espaciosRepository)
    {
        $this->espacioRepository = $espaciosRepository;
    }

    public function getEspaciosBySede()
    {
        $espacios = $this->espacioRepository->getAllBySede();
        return response()->json(['espacios', $espacios]);
    }
}
