<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

use App\Services\AuthorizationsService;
use Carbon\Carbon;

class AuthorizationsController extends Controller
{
    private $authorizationsService;

    public function __construct( AuthorizationsService $authorizationsService ) {
        $this->authorizationsService = $authorizationsService;
    }

    public function validateAuthorization( $code ) {
        $foundAuthorization = $this->authorizationsService->getAuthorizationByCode( $code );
        $now = Carbon::now();
        if (!$foundAuthorization || $foundAuthorization->used  || $now->diffInHours($foundAuthorization->created_at) > 72) {
            return view('authorizations.form-error');
        }
        return view('authorizations.form')->with(['code' => $foundAuthorization->code]);
    }

    public function saveAuthorization( $code, Request $request ) {
        try {
            $foundAuthorization = $this->authorizationsService->getAuthorizationByCode( $code );
            $now = Carbon::now();
            if (!$foundAuthorization || $foundAuthorization->used  || $now->diffInHours($foundAuthorization->created_at) > 5) {
                return view('authorizations.form-error');
            }
    
            $params = $request->all();
            $params['email'] = strtolower(trim($params['email']));
            if (filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            } else {
                $request->session()->flash('error', 'Ups! El Email ingresado no posee un formato válido! Por favor revisá los datos ingresados y volvé a intentar!');
                return redirect('autorizaciones/' . $code)->withInput();
            }
            $authData = $this->authorizationsService->callAuthorizationOpenKey( $foundAuthorization->user_id, [
                'valid_from'=> $foundAuthorization->valid_from->toDateString(),
                'valid_to'  => $foundAuthorization->valid_to->toDateString(),
                'IdPersona' => $foundAuthorization->IdPersona,
                'document'  => $params['document'],
                'last_name' => $params['last_name'],
                'first_name'=> $params['first_name'],
                'email'     => $params['email'],
                'patent'    => isset($params['patent']) ? $params['patent'] : '',
                'comment'   => '',
                'category'  => 4
            ]);
            $foundAuthorization->update(['used'  => true]);
            try { $foundAuthorization->update(['log' => json_encode($authData)]); } catch(Exception $e) {}
            return view('authorizations.form-success')->with(['authorization' => $foundAuthorization]);
        } catch (Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return redirect('autorizaciones/' . $code)->withInput();
        }
    }
}