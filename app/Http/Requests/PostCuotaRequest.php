<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCuotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vencimiento' => 'required|date',
            'detalle' => 'max:200',
            'saldo' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'saldo.required' => 'La cuota debe tener un saldo',
            'saldo.numeric' => 'El saldo debe ser un numero',
            'vencimiento.date' => 'La fecha no es valida',
            'detalle.max' => 'El detalle debe tener un maximo de 200 caracteres.'
        ];
    }
}
