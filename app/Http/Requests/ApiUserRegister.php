<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiUserRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
            'sede_id' => 'required'
        ];
    }

//    public function messages()
//    {
//        return [
//            'name.required' => 'El usuario tiene que tener un nombre',
//            'email.email' => 'El mail es invalido',
//            'password.required' => 'El password es obligatorio',
//            'password.max' => 'El password debe ser menor a 20 caracteres',
//            'sede_id.required' => 'Se tiene que enviar un identificador de sede'
//        ];
//    }
}
