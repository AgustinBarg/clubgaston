<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:30',
            'email' => 'required|email',
            'password' => 'required|min:5|max:20',
            'sede_id' => 'required',
            'role_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El usuario tiene que tener un nombre',
            'email.required' => 'El mail es obligatorio',
            'email.email' => 'El mail es invalido',
            'password.required' => 'El password es obligatorio',
            'password.min' => 'El password debe tener al menos 4 caracteres',
            'password.max' => 'El password debe ser menor a 20 caracteres'
        ];
    }
}
