<?php 

namespace App\Metrics;

use Illuminate\Database\Eloquent\Model;

class MetricView extends Model
{
    protected $table = 'metrics_view';
    protected $fillable = ['name', 'date', 'count'];
}
