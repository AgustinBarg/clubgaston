<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use PushNotification;
use App\Services\NotificationService;

class ProcessNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $notification;
    protected $tokens;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification,  $tokens)
    {
        $this->notification = $notification;
        $this->tokens = $tokens;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $androidTokens = [];
        $iosTokens = [];

        foreach ($this->tokens as $key => $token) {
            if (strlen($token) > 70) {
                array_push($androidTokens, $token);
            } else {
                array_push($iosTokens, $token);
            }
        }

        $info = array('type' =>  $this->notification['type'], 'id' =>  $this->notification['type_id']);
        $feedbackIOS = NotificationService::notifyIOs( $this->notification['title'],  $this->notification['message'], $iosTokens, $info);
        $feedbackAndroid = NotificationService::notifyAndroid( $this->notification['title'],  $this->notification['message'], $androidTokens, $info);
    }
}
