<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espacio extends Model
{
    protected $fillable = ['nombre', 'categoria_id', 'imageFile', 'has_light', 'horaInicio', 'horaFin', 'price_owner', 'price_inquilino', 'qr_validation'];
    protected $casts = [
        'price_owner' => 'float',
        'price_inquilino' => 'float'
    ];

    public function reservas() {
        return $this->hasMany('App\Reserva');
    }

    public function categoria() {
        return $this->belongsTo('App\Categorias');
    }
}
