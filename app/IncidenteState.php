<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidenteState extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
}
