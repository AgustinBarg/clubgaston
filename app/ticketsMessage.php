<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\ticketsIncidente;

class ticketsMessage extends Model
{
    protected $fillable = ['user_id', 'tickets_incidente_id', 'message', 'created_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function images() {
        return $this->hasMany('App\ticketsMessageImage', 'ticket_message_id');
    }
}
