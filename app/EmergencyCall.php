<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmergencyCall extends Model
{
    protected $table = 'emergency_calls';
    protected $fillable = ['user_id', 'type', 'location', 'log'];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
