<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDeviceId extends Model
{
	protected $table = 'user_devices_id';
    protected $fillable = ['user_id', 'device_uuid', 'updated_at'];
}
