<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaEcuestre extends Model
{
    protected $table = 'reservas_ecuestre';
    protected $fillable = [
        'user_id', 'espacio_id', 'amount', 'places', 'fechaIngreso', 'fechaInicio', 'fechaFin', 
        'status', 'notas', 'deleted', 'updated_by', 'payment_id'
    ];
    protected $dates = ['fechaIngreso', 'fechaInicio', 'fechaFin'];
    protected $appends = ['statusString'];

    public function espacio() {
        return $this->belongsTo('App\EspacioEcuestre', 'espacio_id');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function payment() {
        return $this->belongsTo('App\Payment');
    }

    public function getStatus() {
        switch ($this->status) {
            case 'pending':
                return 'pendiente de pago';
            case 'confirmed':
                return 'confirmada';
            case 'canceled':
                return 'cancelada';
        }
    }

    public function getStatusStringAttribute() {
        return $this->getStatus();
    }
}