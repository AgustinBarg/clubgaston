<?php

namespace App\Repositories;


use App\Reserva;
use Illuminate\Support\Facades\DB;

class ReservaRepository
{
    public function getAllReservasBetween($startingTime, $endingTime, $categoryId = null, $sedeId = null, $espacioId = null)
    {

        $reservas = DB::table('reservas')
            ->leftJoin('espacios as s', 'reservas.espacio_id', 's.id')
            ->leftJoin('users as u', 'reservas.user_id', 'u.id')
            ->where('reservas.deleted', false)
            ->where('reservas.status', 'confirmed')
            ->where('reservas.horaInicio', '>=', $startingTime)
            ->where('reservas.horaFin', '<=', $endingTime);

        if($categoryId) {
            $reservas->where('s.categoria_id', '=', $categoryId);
        }

        if($sedeId) {
            $reservas->where('s.sede_id', '=', $sedeId);
        }

        if($espacioId) {
            $reservas->where('s.id', '=', $espacioId);
        }

        return $reservas->select('reservas.id', 'reservas.espacio_id', 'reservas.horaInicio', 'reservas.horaFin' , 'u.name as userName')->get();
    }

    public function getAllReservasByCategory()
    {

    }
}