<?php

namespace App\Repositories;

interface IncidenteInterface
{
    public function getAll();

    public function getById($id);

    public function update($data);

    public function getWithMessages($id);

    public function createIncidente($data);

    public function getPublicIncident($code);
}