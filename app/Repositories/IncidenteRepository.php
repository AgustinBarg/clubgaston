<?php

namespace App\Repositories;

use App\User;
use App\ticketsIncidente;
use App\ticketsMessage;

use App\Services\IncidenteService;
use Exception;
use Mail;

class IncidenteRepository implements IncidenteInterface
{
    protected $incidente;

    public function __construct(ticketsIncidente $incidente)
    {
        $this->incidente = $incidente;
    }

    private function createIncidentAuthCode() {
        return bin2hex(openssl_random_pseudo_bytes(4)) . '-' . bin2hex(openssl_random_pseudo_bytes(4)) . '-' . bin2hex(openssl_random_pseudo_bytes(4)) . '-' . bin2hex(openssl_random_pseudo_bytes(4));
    }

    public function getAll($query = null, $temaId = null, $responsableAreaId = null, $stateId = null, $incidenteId = null, $userName = null, $parentuserName = null, $orderBy = null, $orderDirection = 'asc')
    {
        $qb = $this->incidente->with(['user', 'tema', 'tema.responsableArea', 'state']);

        if($responsableAreaId) {
            $qb->join('sub_areas', 'sub_areas.id', '=', 'tickets_incidentes.tema_id')
               ->where('sub_areas.responsable_area_id', '=', $responsableAreaId);
        }

        if ($query) {
            $qb->where('title', 'like', '%' . $query . '%');
        }

        if ($temaId) {
            $qb->where('tema_id', '=' , $temaId);
        }

        if ($stateId) {
            $qb->where('state_id', '=', $stateId);
        }

        if ($incidenteId) {
            $qb->where('id', '=', $incidenteId);
        }

        if ($userName) {
            $qb->join('users', 'users.id', '=', 'tickets_incidentes.user_id')
               ->where('users.name', 'like', '%' . $userName . '%');
        }

        if ($parentuserName) {
            $qb->join('users', 'users.id', '=', 'tickets_incidentes.user_id')
                ->join('users as u', 'u.id', '=', 'users.invited_by')
                ->where('u.name', 'like', '%' . $parentuserName . '%');
        }

        if ($orderBy && $orderDirection) {
            $qb->orderBy($orderBy, $orderDirection);        
        } else {
            $qb->orderBy('state_id', 'asc')->orderBy('tickets_incidentes.created_at', 'desc');
        }

        $query_open = clone $qb;
        $total_open = $query_open->where('state_id', 1)->count();
        $query_closed = clone $qb;
        $total_closed = $query_closed->whereIn('state_id', [2,3])->count();

        $incidentes = $qb->select('tickets_incidentes.*')->paginate(20);

        return array('incidentes' => $incidentes, 'totals' => array('open' => $total_open, 'closed' => $total_closed));
    }

    public function getById($id)
    {
        return $this->incidente->find($id);
    }

    public function update($data)
    {
        $incidente = $this->getById($data['id']);

        if(isset($data['tema_responsable_area_id'])) {
            $incidente->update(['tema_id' => $data['tema_responsable_area_id']]);
        }

        if(isset($data['state_id'])) {
            $old_state = $incidente->state_id;

            $incidente->update(['state_id' => $data['state_id']]);

            $info = array('type' => 'gestion', 'id' => $incidente->id);

            IncidenteService::sendNotification($incidente->user->id, 'Cambio Estado Gestión', 'Tu Gestion: ' . $incidente->title . ' cambió de estado.', $info);

            if ($incidente->user_id == 5) {
                return;
            }

            Mail::send('mails.mail-update-incident-user-status', ['incident' => $incidente], function($message) use ($incidente)
            {
                $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                $message->to($incidente->user->email)->subject('Cambio Estado Gestión');
            });
        }
    }

    public function getWithMessages($id)
    {
        $incidente = ticketsIncidente::with(['messages', 'messages.images', 'user'])->find($id);

        return $incidente;
    }

    public function createIncidente($data)
    {
        $imageName = isset($data['image']) ? $data['image'] : null;

        // check usuario externo / interno
        if ($data['user_type'] == 'external_user') {
            $existingUser = User::where('email', $data['user_email'])
                                ->orWhere('ccdocumento', $data['user_dni'])
                                ->orWhere('dni', $data['user_dni'])
                                ->first();
            if ($existingUser && !$existingUser->borrado) {
                return ['error' => 'El Email o DNI ingresados corresponde al usuario: ' . $existingUser->name . ' (' . $existingUser->email . ').
                        Si de todas maneras quiere generar el incidente para este usuario, por favor busquelo por su nombre seleccionando Usuario Existente'];
            } else if ($existingUser && $existingUser->borrado) {
                // Esta borrado pero existe, asiq no lo damos de alta, solo le actualizamos el mail
                $data['user_type'] = 'existing_user';
                $existingUser->update(['email' => $data['user_email']]);
            }
        }

        $incidente = new ticketsIncidente();
        $incidente->tema_id = $data['tema_responsable_area_id'];
        $incidente->title = $data['title'];
        $incidente->detalle = $data['detalle'];
        $incidente->state_id = 1;
        $incidente->image_path = $imageName;
        $incidente->location = $data['location'];
        $incidente->latlong_location = isset($data['latlong_location']) ? $data['latlong_location'] : '';
        $incidente->auth_code = $this->createIncidentAuthCode();

        if ($data['user_type'] == 'existing_user') {
            $incidente->user_id = $data['user_id'];
            $user = User::find($incidente->user_id);
            if (!$user) {
                return ['error' => 'Por favor asegurate de seleccionar un usuario existente del campo que autocompleta!'];
            }
        } else {
            $user = $this->createUserExternal($data['user_name'], $data['user_email'], $data['user_dni']);
            $incidente->user_id = $user->id;
        }

        $incidente->save();

        // Notificar / Enviar Mail
        try {
            // ENVIAR NOTIFICACION
            $info = array('type' => 'gestion', 'id' => $incidente->id);
            IncidenteService::sendNotification($incidente->user->id, 'Nueva Gestión Creada', 'Se creó la gestión: ' . $incidente->title, $info);

            if ($incidente->user_id == 5) {
                return;
            }
            
            //ENVIAR MAIL
            Mail::send('mails.mail-new-incident', ['incident' => $incidente], function($message) use ($incidente) {
                $contacts = explode( ';', $incidente->tema->responsableArea->contacto ) ;
                $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                foreach ($contacts as $key => $contactMail) {
                    $message->to($contactMail)->subject('Nueva Gestión');
                }
            });
            Mail::send('mails.mail-new-incident-user', ['incident' => $incidente], function($message) use ($incidente)
            {
                $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                $message->to($incidente->user->email)->subject('Nueva Gestión');
            });
        } catch(Exception $e) {}
    }

    public function createUserExternal( $name, $email, $dni ) {
        $userCode = strtoupper(bin2hex(openssl_random_pseudo_bytes(4)));
        $props = array(
            'invited_by'    => 'external',
            'name'          => $name, 
            'email'         => $email, 
            'password'      => bcrypt($userCode),
            'codigo'        => $userCode,
            'ccdocumento'   => $dni,
            'dni'           => $dni,
            'lote'          => '',
            'lote_ubicacion'=> '',
            'type'          => 'external',
            'sede_id'       => 1,
            'role_id'       => 0 // EXTERNAL
        );
        return User::create($props);
    }

    public function getIncidentesByUser($userId)
    {
        return ticketsIncidente::where('user_id', '=', $userId)
                    ->orderBy('created_at', 'desc')
                    ->with('user', 'tema.responsableArea', 'state')->get();
    }

    public function getMessages($incidenteId)
    {
        return ticketsMessage::where('tickets_incidente_id', '=', $incidenteId)
                    ->with(['images'])->get();
    }

    public function getPublicIncident( $code ) {
        return ticketsIncidente::where('auth_code', $code)->with(['messages', 'messages.images', 'user'])->first();
    }

    public function getLastIncidentes() {
        return ticketsIncidente::where('id', '>', 0)
                    ->with(['user', 'tema', 'tema.responsableArea'])
                    ->orderBy('id', 'desc')
                    ->limit(10)
                    ->get();
    }
}