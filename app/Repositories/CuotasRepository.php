<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 6/30/17
 * Time: 4:51 PM
 */

namespace App\Repositories;


use App\Cuota;
use Illuminate\Support\Facades\DB;

class CuotasRepository
{
    private $cuotaModel;

    public function __construct(Cuota $cuotaModel)
    {
        $this->cuotaModel = $cuotaModel;
    }

    public function getCuotas($userId)
    {
        $cuotas = $this->cuotaModel->where('user_id', '=', $userId)->get();
        return $cuotas;
    }

    public function getCuotasWithUser()
    {
        return DB::table('cuotas')
            ->join('users', 'users.id', '=', 'cuotas.user_id')
            ->select('cuotas.id', 'cuotas.user_id as userId', 'status', 'pagoinformado', 'vencimiento', 'saldo', 'detalle', 'users.name as userName')
            ->get();
    }
}