<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 6/26/17
 * Time: 10:32 PM
 */

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

use App\User;

class UserRepository
{
    protected $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    public function getUsers($searchText, $orderBy = null, $orderDirection = 'asc')
    {
        if ($searchText != null) {
            $query = $this->userModel
                            ->where('name', 'like', '%' . $searchText . '%')
                            ->orWhere('ccdocumento', 'like', '%' . $searchText . '%')
                            ->orWhere('dni', 'like', '%' . $searchText . '%')
                            ->orWhere('email', 'like', '%' . $searchText . '%')
                            ->orWhere('lote', 'like', '%' . $searchText . '%')
                            ->orWhere('lote_ubicacion', 'like', '%' . $searchText . '%');

            if ($orderBy && $orderDirection) {
                $query->orderBy($orderBy, $orderDirection);
            }

            $users = $query->paginate(10);

            return $users;
        }

        if ($orderBy && $orderDirection) {
            $users = $this->userModel->orderBy($orderBy, $orderDirection)->paginate(10);
            return $users;
        }

        return $this->userModel->paginate(10);
    }

    public function getUserById($id)
    {
        return $this->userModel->findOrFail($id);
    }

    public function getUserTokens( $filters = null)
    {
        if (!$filters || (isset($filters['filter_type']) && $filters['filter_type'] == 'all_users')) {
            return DB::table('user_devices')->whereNotNull('token')->pluck('token')->toArray();
        } else {
            $userIds = [0];

            if (isset($filters['filter_type'])) {
                // all_users / single_user / owner / inquilino / morosos / sector / interes
                switch ($filters['filter_type']) {
                    case 'single_user':
                        $query = DB::table('users');
                        $userIds = $query->where('id', $filters['user_id'])->pluck('id')->toArray();
                        break;
                    case 'owner':
                    case 'inquilino':
                        $query = DB::table('users');
                        $userIds = $query->where('type', $filters['filter_type'])->pluck('id')->toArray();
                        break;
                    case 'morosos':
                        $query = DB::table('users');
                        $userIds = $query->where('moroso', true)->pluck('id')->toArray();
                        break;
                    case 'sector':
                        $query = DB::table('users');
                        $userIds = $query->where('lote_sector', $filters['lote_sector'])->pluck('id')->toArray();
                        break;
                    case 'interes':
                        $userIds = DB::table('user_interests')->where('interest_id', $filters['tema_interes_id'])->pluck('user_id')->toArray();
                        break;
                }
            }

            return DB::table('user_devices')
                        ->whereIn('user_id', $userIds)
                        ->whereNotNull('token')
                        ->pluck('token')
                        ->toArray();
        }
    }

    public function updateUserTokens()
    {

    }

    public function removeUserTokens()
    {

    }
}