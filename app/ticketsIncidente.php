<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\ticketsMessage;
use Laravel\Passport\HasApiTokens;



class ticketsIncidente extends Model

{
    use HasApiTokens;

    protected $fillable = [
        'id', 'user_id', 'title', 'state_id', 'tema_id', 'detalle', 'created_at', 'updated_at', 'image_path', 'latlong_location', 'location', 'auth_code'
    ];

    public function tema() {
        return $this->belongsTo('App\SubArea', 'tema_id');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function categoria() {
        return $this->hasOne('App\Categorias');
    }

    public function messages() {
        return $this->hasMany('App\ticketsMessage');
    }

    public function state() {
        return $this->belongsTo('App\IncidenteState');
    }

    public function images() {
        return $this->hasMany('App\ticketsIncidenteImage');
    }

    public function getLink() {
        return 'https://costa-esmeralda.covecinos.com/gestiones/' . $this->auth_code;
    }
}
