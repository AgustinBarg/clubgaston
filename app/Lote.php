<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
	protected $table = 'user_lotes';
	public $timestamps  = false;
	
    protected $fillable = [
        'user_id','id_lote','name', 'emprendimiento_id'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
