<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaGym extends Model
{
    protected $table = 'reservas_gym';
    protected $fillable = [
        'user_id', 'fechaInicio', 'fechaFin', 'notas', 'deleted', 'updated_by',
        'validated', 'validated_by', 'validated_at'
    ];
    protected $dates = ['fechaInicio', 'fechaFin', 'validated_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function validator() {
        return $this->belongsTo('App\User', 'validated_by');
    }

    public function getValidated() {
        return $this->validated ? 'Validada' : 'No Validada';
    }
}