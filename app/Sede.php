<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Sede extends Model
{
    protected $fillable = ['name'];
}
