<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationSchedule extends Model
{
    protected $table = 'notifications_scheduled';
    protected $fillable = ['title', 'message', 'type', 'type_id', 'filter_type', 'schedule_for', 'sent',
                            'user_id', 'tema_interes_id', 'lote_sector',
                            'deleted'];
    public $dates = ['created_at', 'updated_at', 'schedule_for'];

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function getType() {
        switch ($this->type) {
            case 'news':
                return 'Noticias';
            case 'news-single':
                return 'Noticia Single';
            case 'informes':
                return 'Costa Informes';
            case 'informes-single':
                return 'Costa Informes Single';
            case 'lanzamientos':
                return 'Lanzamientos';
            case 'lazamiento-single':
                return 'Lanzamientos Single';
            case 'servicios':
                return 'Guia de Servicios';
            case 'eventos':
                return 'Eventos';    
            case 'galerias':
                return 'Galerias';
            case 'authorizations':
                return 'Autorizaciones';
            case 'add-users':
                return 'Añadir Usuarios';
            case 'camera':
                return 'Camara en Vivo';
            case 'reservations':
                return 'Reservas';
            case 'expenses':
                return 'Expensas';
            case 'utilities':
                return 'Datos Utiles';
            case 'map':
                return 'Mapa';
            case 'polls':
                return 'Encuestas';
            case 'polls-single':
                return 'Encuesta Single';
            case 'general':
            default:
                return 'Texto Simple';
        }
    }

    public function getFilterType() { 
        switch ($this->filter_type) {
            case 'all_users':
                return 'Todos los Usuarios';
            case 'single_user':
                return 'Solo un Usuario';
            case 'owner':
                return 'Solo los Propietarios';
            case 'inquilino':
                return 'Solo los Inquilinos';
            case 'morosos':
                return 'Solo los Morosos';
            default:
                return 'Todos los Usuarios';
        }
    }
}