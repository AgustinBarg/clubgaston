<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Poll extends Model
{
    protected $dates = ['valid_from', 'valid_to', 'created_at', 'updated_at'];
    protected $fillable = [
        'question', 'valid_from', 'valid_to', 'created_at', 'updated_at'
    ];
    protected $appends = ['status'];

    public function options() {
        return $this->hasMany('App\PollOption');
    }

    public function getVotes() {
        $totalVotes = 0;

        foreach ($this->options as $key => $option) {
            $totalVotes += $option->votes;
        }

        return $totalVotes;
    }

    public function getStatusAttribute() {
        return $this->valid_to < Carbon::now() ? 'closed' : 'open';
    }
}
