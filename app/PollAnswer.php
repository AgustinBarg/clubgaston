<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollAnswer extends Model
{
    protected $fillable = [
        'poll_option_id', 'user_id', 'text', 'created_at', 'updated_at'
    ];

    public function pollOption() {
        return $this->belongsTo('App\PollOption');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
