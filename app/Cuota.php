<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuota extends Model
{
    protected $fillable = ['user_id', 'pagoinformado', 'status', 'vencimiento', 'saldo', 'detalle'];

    public function cuota() {
        return $this->hasOne('App\User');
    }

    public $timestamps  = false;
}
