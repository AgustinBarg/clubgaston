<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $fillable = ['name', 'icon'];

    public function espacios() {
        return $this->hasMany('App\Espacio', 'categoria_id');
    }
}
