<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/4/17
 * Time: 9:29 PM
 */

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Espacio;
use App\Http\Validators\ReservaValidatedValidator;
use App\Repositories\ReservaRepository;
use App\Reserva;
use App\ReservaConfig;
use Carbon\Carbon;
use Exception;

class ReservasService
{
    private $reservaRepository;

    public function __construct(ReservaRepository $reservaRepository)
    {
        $this->reservaRepository = $reservaRepository;
    }

    public function getConfig() {
        try {
            $authUser = Auth::user();
        } catch(Exception $e) {}
        $config = ReservaConfig::find(1);
        return [
            'ecuestre_allow'        => $config->ecuestre_allow,
            'gym_allow'             => $config->gym_allow,
            'gym_allow_credits'     => $config->gym_allow_credits,
            'gym_max_hour'          => $config->gym_max_hour,
            'gym_owner_inicio'      => $config->gym_owner_inicio,
            'gym_owner_fin'         => $config->gym_owner_fin,
            'gym_inquilino_inicio'  => $config->gym_inquilino_inicio,
            'gym_inquilino_fin'     => $config->gym_inquilino_fin,
            'ecuestre_allow_text'   => $config->ecuestre_allow_text, 
            'gym_allow_text'        => $config->gym_allow_text,
            'gym_allow_credits_text'=> $config->gym_allow_credits_text,
            'reservas_payment_allow'=> $config->reservas_payment_allow,
            'reservas_max_days'     => $config->reservas_max_days,
            'reservas_validation'   => $config->reservas_validation,
            'user_can_reserve'      => isset($authUser) ? ReservaValidatedValidator::checkUserCanReserveUnvalidated($authUser->id) : false
        ];
    }

    public function getAll($userName = null, $espacioId = null, $orderBy = null, $orderDirection = null, $date = null, $validated = null)
    {
        $reserva = Reserva::where('deleted', false)->with('espacio', 'user');

        if($userName) {
            $reserva->join('users', 'users.id', '=', 'reservas.user_id')
               ->where('users.name', 'like', '%' . $userName . '%');
        }

        if($espacioId) {
            $reserva->where('espacio_id', '=', $espacioId);
        }

        if ($validated != null) {
            $reserva->where('validated', '=', $validated);
        }

        if($date) {
            $dayStartingTime = Carbon::parse($date);
            $dayEndingTime = Carbon::parse($date);
            $dayEndingTime->addDay();

            $reserva->whereBetween('horaInicio', array($dayStartingTime, $dayEndingTime));
        } else {
            $weekAgo = Carbon::now()->addDays(-7);
            $reserva->where('horaInicio', '>=', $weekAgo);
        }

        if ($orderBy && $orderDirection) {
            $reserva->orderBy($orderBy, $orderDirection);        
        } else {
            $reserva->orderBy('horaInicio', 'desc');
        }

        $reserva->select('reservas.*')->orderBy('espacio_id');
            
        return $reserva->paginate(20);
    }

    public function delete($reservaId, $userId)
    {
        $reserva = Reserva::find($reservaId);
        if ($reserva->validated) {
            return ['error' => 'Ups! La reserva que estás intentando cancelar ya ha sido validada!'];
        }
        $reserva->deleted = true;
        $reserva->updated_by = $userId;
        $reserva->save();
    }

    public function validateReservaAdmin($reservaId, $userId) {
        $reserva = Reserva::find($reservaId);
        $reserva->validated = true;
        $reserva->validated_by = $userId;
        $reserva->validated_at = Carbon::now();
        $reserva->save();
    }

    public function getCategoriaTimetableFromDate($sedeId, $categoryId, $date = null)
    {
        // $now = Carbon::now();
        // $reservaDate = Carbon::createFromFormat('Y-m-d', $date);
        // if ($now->diffInDays($reservaDate) > 7) {
        //     return ['error' => 'Ups!
        //                         No es posible realizar reservas con una anticipación mayor a 7 días.
        //                         Para asegurar mayor disponibilidad y evitar inasistencias, tu reserva deberá ser entre el día de hoy y los próximos 7 días.', 'code' => 400];
        // }

        $spaces = Espacio::where([
            'categoria_id' => $categoryId,
            'sede_id' => $sedeId
        ])->get();

        $minStartingTime = 24;
        $maxEndingtime = 0;

        //Arma la grilla desde el menor y mayor horario entre todos los espacios posibles
        $config = ReservaConfig::find(1);
        $totalSpacesByCategory = array();
        foreach ($spaces as $space) {
            if ($space->horaInicio < $minStartingTime) {
                $minStartingTime = $space->horaInicio;
            }
            if ($space->horaFin > $maxEndingtime) {
                $maxEndingtime = $space->horaFin;
            }

            if (!$config->reservas_payment_allow) {
                $space->price_owner = 0;
                $space->price_inquilino = 0;
            }

            array_push($totalSpacesByCategory, $space);
        }

        if ($date) {
            $startingTime = (new Carbon($date, 'America/Argentina/Buenos_Aires'))->addHours($minStartingTime);
            $endingTime = (new Carbon($date, 'America/Argentina/Buenos_Aires'))->addHours($maxEndingtime);
        } else {
            $startingTime = (new Carbon('today', 'America/Argentina/Buenos_Aires'))->addHours($minStartingTime);
            $endingTime = (new Carbon('today', 'America/Argentina/Buenos_Aires'))->addHours($maxEndingtime);
        }

        $reservas = $this->reservaRepository->getAllReservasBetween(
            $startingTime,
            $endingTime,
            $categoryId,
            $sedeId
        );

        // 
        $timetableArray = array();
        for ($i = $minStartingTime; $i < $maxEndingtime; $i++) {
            
            $freeSpaces = $totalSpacesByCategory;
            $field_11_used = false;
            $field_7_used = false;

            foreach ($reservas as $reserva) {
                $dateReserva = new Carbon($reserva->horaInicio);
                if ($dateReserva->format('H') == $i) {
                    $keyIndex = -1;

                    for ($j = 0; $j < count($freeSpaces); $j++) {
                        if ($reserva->espacio_id == $freeSpaces[$j]->id) {
                            $keyIndex = $j;
                            $j = count($freeSpaces) + 1;

                            if ($reserva->espacio_id == 1) {
                                $field_11_used = true;
                            } elseif ($reserva->espacio_id == 2 || $reserva->espacio_id == 4 || $reserva->espacio_id == 5) {
                                $field_7_used = true;
                            }
                        }
                    }

                    if ($keyIndex != -1) {
                        array_splice($freeSpaces, $keyIndex, 1);
                    }
                }
            }

            $freeSpaces_filtered = array();

            foreach ($freeSpaces as $index => $space) {
                
                if ($i >= $space->horaInicio && $space->horaFin > $i) {
                    
                    if ($field_11_used) {
                        if ($space->id != 2 && $space->id != 4 && $space->id != 5) {
                            array_push($freeSpaces_filtered, $space);
                        }
                    } elseif ($field_7_used) {
                        if ($space->id != 1) {
                            array_push($freeSpaces_filtered, $space);
                        }
                    } else {
                        array_push($freeSpaces_filtered, $space);
                    }
                    
                }

                if ($space->horaFin <= $i) {
                    array_splice($freeSpaces, $index, 1);
                }

                if ($space->horaInicio >= $i) {
                    array_splice($freeSpaces, $index, 1);
                }
            }


            $timetableArray[] = [
                'hora' => $i,
                'free_spaces' => $freeSpaces_filtered
            ];
        }

        return $timetableArray;
    }

    public function getCategoriaTimetableFromDateTest($sedeId, $categoryId, $date = null)
    {
        $spaces = Espacio::where([
            'categoria_id' => $categoryId,
            'sede_id' => $sedeId
        ])->get();

        
        $minStartingTime = 24;
        $maxEndingtime = 0;

        //Arma la grilla desde el menor y mayor horario entre todos los espacios posibles
        $totalSpacesByCategory = array();
        foreach ($spaces as $space) {
            if ($space->horaInicio < $minStartingTime) {
                $minStartingTime = $space->horaInicio;
            }
            if ($space->horaFin > $maxEndingtime) {
                $maxEndingtime = $space->horaFin;
            }
            array_push($totalSpacesByCategory, $space);
        }

        if ($date) {
            $startingTime = (new Carbon($date, 'America/Argentina/Buenos_Aires'))->addHours($minStartingTime);
            $endingTime = (new Carbon($date, 'America/Argentina/Buenos_Aires'))->addHours($maxEndingtime);
        } else {
            $startingTime = (new Carbon('today', 'America/Argentina/Buenos_Aires'))->addHours($minStartingTime);
            $endingTime = (new Carbon('today', 'America/Argentina/Buenos_Aires'))->addHours($maxEndingtime);
        }

        $reservas = $this->reservaRepository->getAllReservasBetween(
            $startingTime,
            $endingTime,
            $categoryId,
            $sedeId
        );

        // 
        $timetableArray = array();
        for ($i = $minStartingTime; $i < $maxEndingtime; $i++) {
            
            $freeSpaces = $totalSpacesByCategory;
            $field_11_used = false;
            $field_7_used = false;

            foreach ($reservas as $reserva) {
                $dateReserva = new Carbon($reserva->horaInicio);
                if ($dateReserva->format('H') == $i) {
                    $keyIndex = -1;

                    for ($j = 0; $j < count($freeSpaces); $j++) {
                        if ($reserva->espacio_id == $freeSpaces[$j]->id) {
                            $keyIndex = $j;
                            $j = count($freeSpaces) + 1;

                            if ($reserva->espacio_id == 1) {
                                $field_11_used = true;
                            } elseif ($reserva->espacio_id == 2 || $reserva->espacio_id == 4 || $reserva->espacio_id == 5) {
                                $field_7_used = true;
                            }
                        }
                    }

                    if ($keyIndex != -1) {
                        array_splice($freeSpaces, $keyIndex, 1);
                    }
                }
            }

            $freeSpaces_filtered = array();

            foreach ($freeSpaces as $index => $space) {
                
                if ($i >= $space->horaInicio && $space->horaFin > $i) {
                    array_push($freeSpaces_filtered, $space);
                }

                if ($space->horaFin <= $i) {
                    array_splice($freeSpaces, $index, 1);
                }

                if ($space->horaInicio >= $i) {
                    array_splice($freeSpaces, $index, 1);
                }
            }


            $timetableArray[] = [
                'reservas' => $reservas,
                'startingTime' => $startingTime,
                'endingTime' => $endingTime,
                'hora' => $i,
                'free_spaces' => $freeSpaces,
                'free_spaces_filtered' => $freeSpaces_filtered
            ];
        }

        return $timetableArray;
    }

    public function getReservasByTimestamp($startingTime, $endingTime, $espacioId) {
        return $this->reservaRepository->getAllReservasBetween(
            $startingTime,
            $endingTime,
            null,
            null,
            $espacioId
        );
    }

    public function createReserva($data)
    {
        $dateTime = explode('-',$data['date']);
        $currentDay = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], 0, 0, 0,'America/Argentina/Buenos_Aires');
        $currentDay2 = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], 0, 0, 0,'America/Argentina/Buenos_Aires');
        $startingTime = $currentDay->addHours($data['horaInicio']);
        $endingTime = $currentDay2->addHours($data['horaFin']);

        if ($data['user_id'] != 1) {
            $now = Carbon::now();
            if ($now->diffInDays($currentDay) > 7) {
                throw new Exception('Ups!
                                    No es posible realizar reservas con una anticipación mayor a 7 días.
                                    Para asegurar mayor disponibilidad y evitar inasistencias, tu reserva deberá ser entre el día de hoy y los próximos 7 días.');
                return;
            }
        }

        $reservasConfig = ReservaConfig::find(1);
        if ($reservasConfig->reservas_validation && $data['user_id'] != 1) {
            $userCanReserve = ReservaValidatedValidator::checkUserCanReserveUnvalidated($data['user_id']);
            if (!$userCanReserve) {
                throw new Exception('Ups!
                                    No podés realizar reservas!
                                    Acumulaste 2 reservas sin validar en la última semana.');
                return;
            }
        }

        // $reservas_user = Reserva::where('user_id', $data['user_id'])
        //                         ->whereDate('horaInicio', '>', Carbon::now())
        //                         ->count();

        // if ($reservas_user >= 2) {
        //     throw new Exception("Atención, no se pueden tener más de 2 (dos) reservas abiertas por usuario. En cualquier momento podés cancelar alguna de tus reservas abiertas y realizar una nueva.", 1);
        //     return;
        // }

        return Reserva::create([
            'user_id' =>  $data['user_id'],
            'espacio_id' => $data['espacio_id'],
            'horaInicio' => $startingTime,
            'horaFin' => $endingTime,
            'notas' => isset($data['notas']) ? $data['notas'] : '',
            'amount' => isset($data['amount']) ? $data['amount'] : null,
            'status' => isset($data['amount']) && $data['amount'] > 0 ? 'pending' : 'confirmed',
            'updated_by' => $data['user_id'],
            'validated' => false
        ]);
    }

    public function getUserReservas($userId)
    {
        $reservas = Reserva::with('espacio')
            ->with('espacio.categoria')
            ->where('deleted', false)
            ->where('user_id', $userId)
            ->whereDate('horaInicio', '>', Carbon::now()->subMonth())
            ->orderBy('horaInicio', 'DESC')
            ->get();
        return $reservas;
    }

    public static function confirmPaymentReserva( $paymentId ) {
        $reserva = Reserva::where('payment_id', $paymentId)->first();
        if ($reserva) {
            $reserva->status = 'confirmed';
            $reserva->save();
        }
    }

    public function validateReserva( $qrCode, $userId ) {
        return ReservaValidatedValidator::validateReserva($qrCode, $userId);
    }
}