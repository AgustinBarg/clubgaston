<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/10/17
 * Time: 3:35 PM
 */

namespace App\Services;

use League\Flysystem\Exception;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Ecdsa\Sha256;

use PushNotification;
use Log;

use App\Notification;
use App\Repositories\UserRepository;
use App\UserDevice;
use App\User;

use App\Jobs\ProcessNotifications;
use App\NotificationSchedule;
use Carbon\Carbon;

class NotificationService
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function sendScheduled( $logger ) {
        $now = Carbon::now('America/Argentina/Buenos_Aires')->toDateTimeString();
        $notificationsToSend = NotificationSchedule::where('deleted', false)
                                                    ->where('sent', false)
                                                    ->where('schedule_for', '<=', $now)
                                                    ->get();
        foreach ($notificationsToSend as $notification) {
            $logger->info($notification->id . ' - ' . $notification->title);
            $logger->info(print_r(
                $this->sendNotificationsToUsers([
                    'filter_type'   => $notification->filter_type,
                    'title'         => $notification->title,
                    'body'          => $notification->message,
                    'type'          => $notification->type,
                    'type_id'       => $notification->type_id,
                    'user_id'       => $notification->user_id,
                    'tema_interes_id'=> $notification->tema_interes_id,
                    'lote_sector'   => $notification->lote_sector
                ])
            , true));
            $notification->update(['sent' => true]);
        }
    }

    public function scheduleNotification( $data ) {
        try {
            $time = $data['schedule_horario'];
            $date = $data['schedule_date'];
            $hoursTime = explode(':', $time);
            $dateTime = explode('-', $date);
            $currentDay = Carbon::create($dateTime[0], $dateTime[1], $dateTime[2], 0, 0, 0,'America/Argentina/Buenos_Aires');
            $currentDay->addHours($hoursTime[0])->addMinutes($hoursTime[1]);

            if (Carbon::now()->addMinutes(15)->diffInMinutes($currentDay) < 30) {
                return ['error' => 'Ups! No se puede Programar una notificación para una fecha/hora menor a 30 minutos.'];
            }

            $filterType = isset($data['filter_type']) ? $data['filter_type'] : null;

            NotificationSchedule::create([
                'title'         => $data['title'],
                'message'       => $data['body'],
                'type'          => $data['type'],
                'type_id'       => $data['type_id'],
                'filter_type'   => $filterType,

                'user_id'       => $filterType == 'single_user' && isset($data['user_id']) ? $data['user_id'] : null,
                'tema_interes_id'=> $filterType == 'interes' && isset($data['tema_interes_id']) ? $data['tema_interes_id'] : null,
                'lote_sector'   => $filterType == 'sector' && isset($data['lote_sector']) ? $data['lote_sector'] : null,

                'schedule_for'  => $currentDay->toDateTimeString(),
                'sent'          => false
            ]);
            return true;
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function sendNotificationsToUsers($data) {
        try {
            unset($data['_token']);

            if (!isset($data['tokens'])) {
                $tokens = $this->userRepository->getUserTokens($data);
            } else {
                $tokens = $data['tokens'];
            }

            if (count($tokens) == 0) {
                return true;
            }

            $filterType = isset($data['filter_type']) ? $data['filter_type'] : null;
            $notification = Notification::create([
                'title' => $data['title'],
                'message' => $data['body'],
                'type' => $data['type'],
                'type_id' => $data['type_id'],
                'filter_type'   => $filterType,

                'user_id'       => $filterType == 'single_user' && isset($data['user_id']) ? $data['user_id'] : null,
                'tema_interes_id'=> $filterType == 'interes' && isset($data['tema_interes_id']) ? $data['tema_interes_id'] : null,
                'lote_sector'   => $filterType == 'sector' && isset($data['lote_sector']) ? $data['lote_sector'] : null,

                'user_id' => isset($data['user_id']) ? $data['user_id'] : null
            ]);

            $tokensDivided = array_chunk($tokens, 500);

            foreach ($tokensDivided as $key => $chunk) {
                ProcessNotifications::dispatch(array(
                    'title' => $notification->title,
                    'message' => $notification->message,
                    'type' => $notification->type,
                    'type_id' => $notification->type_id
                ), $chunk);
            }

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    public function sendNotificationsToUsersOld($data)
    {
        try {
            unset($data['_token']);

            if (!isset($data['tokens'])) {
                $tokens = $this->userRepository->getUserTokens();
            } else {
                $tokens = $data['tokens'];
            }

            $androidTokens = [];
            $iosTokens = [];

            foreach ($tokens as $key => $token) {
                if (strlen($token) > 70) {
                    array_push($androidTokens, $token);
                } else {
                    array_push($iosTokens, $token);
                }
            }

            $info = array('type' => $data['type'], 'id' => $data['type_id']);

            if (count($iosTokens) > 0) {
                try {
                    $feedbackIOS = NotificationService::notifyIOs($data['title'], $data['body'], $iosTokens, $info);
                } catch (Exception $e) { }
            }
            if (count($androidTokens) > 0) {
                try {
                    $feedbackAndroid = NotificationService::notifyAndroid($data['title'], $data['body'], $androidTokens, $info);
                } catch (Exception $e) { }
            }

            return Notification::create([
                'title' => $data['title'],
                'message' => $data['body'],
                'type' => $data['type'],
                'type_id' => $data['type_id']
            ]);

        } catch (Exception $e) {
            return false;
        }

        // return array('ios' => $feedbackIOS, 'android' => $feedbackAndroid);
    }

    public static function notifyIOs($title, $body, $tokens, $info) {
        try {
            $apns_topic = config('pushnotification.apns.topic');
            $key_id = config('pushnotification.apns.key');
            $team_id = config('pushnotification.apns.team');
            $p8file = '/home/237708.cloudwaysapps.com/gdkxbzqaja/public_html/config/AuthKey_W4A92ZD4YY.p8';
            // $p8file = "/Users/matiasrodriguez/projects/bulldev/MCDG/clubgaston/config/AuthKey_W4A92ZD4YY.p8";
            $signer = new Sha256();
            $privateKey = new Key('file://' . $p8file);

            $tokenBearer = (string) (new Builder())
                            ->issuedBy($team_id) // (iss claim) // teamId
                            ->withHeader('kid', $key_id)
                            ->issuedAt(time()) // time the token was issuedAt
                            ->sign($signer, $privateKey)
                            ->getToken(); // get the generated token

            $payloadArray['aps'] = [
                'alert' => [
                    'title' => $title,
                    'body' => $body
                ],
                'sound' => 'default',
                'badge' => 1,
                'content-available' => 1
            ];
            $payloadArray['type'] = $info ? $info['type'] : '';
            $payloadArray['id'] = $info ? $info['id'] : '';
            $payloadJSON = json_encode($payloadArray);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payloadJSON);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer $tokenBearer","apns-topic: $apns_topic"]);

            foreach ($tokens as $token) {
                $url = "https://api.push.apple.com/3/device/" . $token;
                curl_setopt($ch, CURLOPT_URL, $url);
                $result = curl_exec($ch);
            }
            curl_close($ch);
            return;
        } catch(Exception $e) {}
    }

    public static function notifyAndroid($title, $body, $tokens, $info) {
        PushNotification::setService('fcm')
            ->setMessage([
                'data' => [
                    'title'=> $title,
                    'body'=> $body,
                    'type' => $info ? $info['type'] : '',
                    'id' => $info ? $info['id'] : ''
                ]
            ])
            ->setDevicesToken($tokens)
            ->send();
    }

    public function getLastNotifications()
    {
        return Notification::where('user_id', null)
                            ->orWhere('filter_type', 'single_user')
                            ->orderBy('created_at', 'desc')->get();
    }

    public function getLastNotificationsScheduled() {
        return NotificationSchedule::where('sent', false)
                            ->where('deleted', false)
                            ->orderBy('schedule_for', 'asc')->get();
    }

    public function deleteTokens($tokens)
    {
        UserDevice::whereIn('token', $tokens)->update(['token' => null]);
    }

    public function modifyTokens($tokens)
    {
        foreach ($tokens as $oldToken => $newToken) {
            UserDevice::where('token', $oldToken)->update(['token' => $newToken]);
        }
    }

    public function toRetryTokens($tokens, $data)
    {
        $data['tokens'] = $tokens;
        $this->sendNotificationsToUsers($data);
    }

    public function getUserNotifications( $userID ) 
    {
        $user = User::find($userID);
        $monthAgo = Carbon::now()->subDays(30);
        $query = Notification::where('created_at', '>', $monthAgo);

        $query->where(function($subQuery) use ($user, $userID) {
            $subQuery->where('user_id', $userID);

            if ($user) {
                $subQuery->orWhere('filter_type', 'all_users');
                $subQuery->orWhere('filter_type', $user->type);
                if ($user->moroso) {
                    $subQuery->orWhere('filter_type', 'morosos');
                }
            }
    
            $subQuery->orWhere(function ($subSubQuery) {
                $subSubQuery->where('type', 'general')
                      ->where('filter_type', null);
            });
        });

        return $query->orderBy('created_at', 'desc')->get();
    }
}