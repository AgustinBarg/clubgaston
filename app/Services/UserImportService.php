<?php

namespace App\Services;

use nusoap_client;

use SimpleXMLElement;

use App\Repositories\UserRepository;
use App\User;
use App\Lote;
use Laravel\Passport\Token;

use Carbon\Carbon;
use Exception;

use App\Services\HelperService;

use Mail;

class UserImportService
{
    protected $userRepository;

    public function __construct(UserRepository  $userRepository, User $userModel, Lote $loteModel)
    {
        $this->userRepository = $userRepository;
        $this->userModel = $userModel;
        $this->loteModel = $loteModel;
    }

    public function cleanUpUsers($logger) {
        $logger->info('Starting Clean Up');

        $users_to_invalidate = User::where('role_id', 1)
                  ->where('type', '<>', 'owner')
                  ->where('borrado', false)
                  ->whereDate('invited_valid_to', '<', Carbon::now())
                  ->get();

        foreach ($users_to_invalidate as $key => $user) {
            try {
                $logger->info('User To Invalidate: (' . $user->owner->client_id . ' - ' . trim($user->owner->name) . ') - ' . trim($user->name));

                Token::where('user_id', $user->id)->delete();
                $user->borrado = true;
                $user->save();
            } catch (Exception $e) {
                $logger->error('error invalidando usuario');
                $logger->error($e->getMessage());
            }
        }
    }

    public function importUsers($emprendimientoId, $logger) {
        $soap_client = new \nusoap_client('http://181.174.193.74:8081/foxacceso.asmx?WSDL', true, false, true, false, false, 0, 300, '');
        $parameters = array('tidEntidadCab' => $emprendimientoId);
        // 144, COSTA
        // 192, Al Golf 19
        // 224, CE Mar III

        $resultString = $soap_client->call('ControlDeAcceso', $parameters);
        $result = new SimpleXMLElement($resultString['ControlDeAccesoResult']);
        $array_ids = array();
        $userLotes = array();

        foreach ($result as $key => $person) {
            array_push($array_ids, $person->idcliente);

            
            $person->cnumerodocumento = str_replace('.', '', trim($person->cnumerodocumento));

            $logger->info('Emprendimiento: ' . $emprendimientoId . ' - (' . $person->idcliente . ') ' . trim($person->cnombre) . ' ' . trim($person->capellido));
            
            if (strlen($person->cnumerodocumento) > 8) {
                // ES CUIT/CUIL
                $dni = substr($person->cnumerodocumento, 2);
                $dni = substr($dni, 0, strlen($dni) - 1);

                if (strpos($dni, '0') === 0) {
                    $dni = substr($dni, 1);
                }
            } else {
                $dni = trim($person->cnumerodocumento);
            }

            $user_found = $this->userModel->where('ccdocumento', $person->cnumerodocumento)
                                          ->orWhere('dni', $dni)->first();
            $userNewEmail = trim($person->cemail);

            if (!$user_found) {
                $userCode = bin2hex(openssl_random_pseudo_bytes(3));
                $userCode = strtoupper($userCode);

                // Chequear si existe usuario con el mail
                try {
                    // Chequear si existe usuario con el mail
                    $existingUser = $this->userModel->where('email', $userNewEmail)->first();
                    if ($existingUser) {
                        $existingUser->update([
                            'email_duplicated'  => true,
                            'email'             => 'DUPLICATED_' . time() . '_' . $existingUser->email
                        ]);
                    }
                } catch (Exception $e) {
                    $logger->error('error agregando usuario sin Email 1: ' . $existingUser->id);
                    $logger->error($e->getMessage());
                }

                $props = array(
                    'id_cliente' => $person->idcliente,
                    'name' => trim(trim($person->cnombre) . ' ' . trim($person->capellido)), 
                    'email' => $userNewEmail, 
                    'telefono_movil' => trim($person->ctelefonomovil),
                    'password' => bcrypt($userCode),
                    'codigo' => $userCode,
                    'ccdocumento' => trim($person->cnumerodocumento), 
                    'dni' => $dni,
                    'lote' => trim($person->ilote),
                    'lote_ubicacion' => trim($person->cubicacion),
                    'lote_sector' => HelperService::parseSectorLote(
                        trim(explode('-', trim($person->cubicacion))[0])
                    ),
                    'sede_id' => 1,
                    'role_id' => 1,
                    'emprendimiento_id' => $emprendimientoId,
                    'identidad_det' => trim($person->identidaddet),
                    'moroso' => false//$person->cesmoroso == '*'
                );

                try {
                    $createdUser = $this->userModel->create($props);
                    $logger->info('New User: (' . $person->idcliente . ') ' . trim($person->cnombre) . ' ' . trim($person->capellido));
                    $this->loteModel->create(array(
                            'user_id' => $createdUser->id,
                            'id_lote' => $person->ilote,
                            'name' => trim($person->cubicacion),
                            'emprendimiento_id' => $emprendimientoId
                        ));
                } catch(Exception $e) {
                    $logger->error('error intentando agregar usuario');
                    $logger->error($e->getMessage());
                }
            } else {
                $user_found->id_cliente = $person->idcliente;
                $user_found->name = trim(trim($person->cnombre) . ' ' . trim($person->capellido)); 
                $user_found->telefono_movil = $person->ctelefonomovil;
                $user_found->ccdocumento = $person->cnumerodocumento; 
                $user_found->dni = $dni;
                $user_found->emprendimiento_id = $emprendimientoId;
                $user_found->identidad_det = trim($person->identidaddet);
                $user_found->lote = trim($person->ilote);
                $user_found->lote_ubicacion = trim($person->cubicacion);
                $user_found->lote_sector = HelperService::parseSectorLote(
                    trim(explode('-', trim($person->cubicacion))[0])
                );
                $user_found->moroso = false; //$person->cesmoroso == '*';
                $user_found->type = 'owner';
                $user_found->role_id = 1;
                $user_found->invited_by = null; // Sacar el campo invitado si es owner
                $user_found->borrado = false;


                // RELACIONADOS
                $usersRelated = User::where('invited_by', $user_found->id)->get();
                foreach ($usersRelated as $key => $userRelated) {
                    $userRelated->update([
                        'lote' => $user_found->lote,
                        'lote_ubicacion' => $user_found->lote_ubicacion
                    ]);
                }

                // CHEQUEAR SI EL USUARIO VIENE CON MAIL DISTINTO, SI EXISTE OTRO USUARIO CON ESE MAIL, MARCARLO COMO DUPLICADO
                if ($user_found->email != $userNewEmail) {
                    // Chequear si existe usuario con el mail
                    try {
                        $existingUserSecond = $this->userModel->where('email', $userNewEmail)->first();
                        if ($existingUserSecond) {
                            $existingUserSecond->update([
                                'email_duplicated'  => true,
                                'email'             => 'DUPLICATED_' . time() . '_' . $existingUserSecond->email
                            ]);
                        }
                    } catch (Exception $e) {
                        $logger->error('error agregando usuario sin Email 2: ' . $existingUserSecond->id);
                        $logger->error($e->getMessage());
                    }
                }

                $user_found->email = $userNewEmail; 

                //ADD NUEVO LOTE
                try {
                    $user_found->save();
                    $logger->info('Existing User: (' . $person->idcliente . ') ' . trim($person->cnombre) . ' ' . trim($person->capellido));
                    if (isset($userLotes[$user_found->id])) {
                        array_push($userLotes[$user_found->id], $person->ilote);
                    } else {
                        $userLotes[$user_found->id] = [$person->ilote];
                    }
                    $this->loteModel->updateOrCreate(
                        [
                            'user_id' => $user_found->id,
                            'id_lote' => $person->ilote,
                        ],
                        [
                            'name' => trim($person->cubicacion),
                            'emprendimiento_id' => $emprendimientoId
                        ]);
                } catch(Exception $e) {
                    $logger->error('error agregando lote usuario');
                    $logger->error($e->getMessage());
                }
            }
        }

        if (count($array_ids) < 5) {
            return;
        }

        //Add 0 so we don't get users with id_cliente = 0
        array_push($array_ids, 0);

        try {
            $users_to_delete = $this->userModel
                                ->whereNotIn('id_cliente', $array_ids)
                                ->where('type', 'owner')
                                ->where('emprendimiento_id', $emprendimientoId)
                                ->where('role_id', 1)
                                ->with('lotes')
                                ->get();
        } catch (Exception $e) {
            $logger->error('error consultando usuarios');
            $logger->error($e->getMessage());
        }
        

        foreach ($users_to_delete as $key => $user) {
            try {
                $logger->info('User To Delete: (' . $user->id_cliente . ') - ' . trim($user->name));
                
                // CHEQUEAR SI TIENE LOTES DE OTROS EMPRENDIMIENTOS
                $lotesEmprendimientos = $user->lotes->pluck('emprendimiento_id')->toArray();
                
                if (count($lotesEmprendimientos) == 0 || (count($lotesEmprendimientos) == 1 && $lotesEmprendimientos[0] == $emprendimientoId)) {
                    // LO BORRO PQ TIENE SOLO LOTES DE ESTE EMPRENDIMIENTO
                    Token::where('user_id', $user->id)->delete();
                    $user->borrado = true;
                    $user->save();

                    User::where('invited_by', $user->id)->update(['borrado' => true]);

                    $user_ids = User::select('id')->where('invited_by', $user->id)->get();
                    Token::whereIn('user_id', $user_ids)->delete();
                } else {
                    $logger->error('Tiene Lote en Otro Emprendimiento');
                }

                // BORRAMOS LOTES DE ESTE EMPRENDIMIENTO
                $this->loteModel->where('user_id', $user->id)
                                    ->where('emprendimiento_id', $emprendimientoId)
                                    ->delete();
                
            } catch(Exception $e) {
                $logger->error('error borrando usuarios');
                $logger->error($e->getMessage());
            }
        }

        foreach ($userLotes as $userId => $lotes) {
            // Borramos Lotes de Este Emprendimiento para este usuario que no sean los que llegaron
            $this->loteModel->where('user_id', $userId)
                    ->where('emprendimiento_id', $emprendimientoId)
                    ->whereNotIn('id_lote', $lotes)
                    ->delete();
        }
    }
}