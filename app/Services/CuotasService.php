<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 6/30/17
 * Time: 4:50 PM
 */

namespace App\Services;


use App\Cuota;
use App\Repositories\CuotasRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CuotasService
{
    private $cuotasRepository;
    private $cuotaModel;

    public function __construct(CuotasRepository $cuotasRepository, Cuota $cuotaModel)
    {
        $this->cuotasRepository = $cuotasRepository;
        $this->cuotaModel = $cuotaModel;
    }

    public function create($data)
    {
        return $this->cuotaModel->create([
            'user_id' => $data['user_id'],
            'pagoinformado' => $data['pagoinformado'],
            'status' => CuotasStatus::PENDING,
            'vencimiento' => $data['vencimiento'],
            'saldo' => $data['saldo'],
            'detalle' => $data['detalle']
        ]);
    }

    public function update($data)
    {
        return $this->cuotaModel->where('id', $data['id'])
            ->update([
                'pagoinformado' => $data['pagoinformado'],
                'vencimiento' => $data['vencimiento'],
                'saldo' => $data['saldo'],
                'detalle' => $data['detalle']

            ]);
    }

    public function informarPago($data)
    {
        $payment_ticket = isset($data['payment_ticket']) ? $data['payment_ticket'] : null;
        try {
            $this->cuotaModel->findOrFail($data['id'])->update([
                'pagoinformado' => date('Y-m-d'),
                'payment_ticket' => $payment_ticket,
                'status' => CuotasStatus::INFORMED
            ]);
        } catch (ModelNotFoundException $exception) {
            return array('type' => 'error', 'text' => 'no se ha encontrado la cuota');
        }

        return array('type' => 'success', 'text' => 'La cuota ha sido guardada exitosamente');

    }

    public function approvePayment($cuotaId)
    {
        $this->cuotaModel->find($cuotaId)->update([
           'status' => CuotasStatus::PAYED
        ]);
    }

    public function rejectPayment($cuotaId)
    {
        $this->cuotaModel->find($cuotaId)->update([
            'status' => CuotasStatus::REJECTED
        ]);
    }

    public function paginatedList()
    {}

    public function getCuotasByUser($userId)
    {
        $cuotas = $this->cuotaModel->where('user_id', '=', $userId)->get();
        return $cuotas;
    }

    public function getCuota($cuotaId)
    {
        return $this->cuotaModel->find($cuotaId);
    }

    public function getCuotasWithUser()
    {
        return $this->cuotasRepository->getCuotasWithUser();
    }

}