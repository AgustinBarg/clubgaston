<?php

namespace App\Services;

use Carbon\Carbon;
use DB;

use App\EspacioEcuestre;
use App\ReservaEcuestre;

class ReservasEcuestreService
{
    public function __construct()
    {
    }

    public static function getMultiplier( $period, $category ) {
        if ($period == 'semana') {
            return ['owner' => 1, 'inquilino' => 1];
        }

        if ($category == 1002 || $category == 1003) {
            if ($period == 'quincena') {
                return ['owner' => 1.136364, 'inquilino' => 1.125];
            }
        } else if ($category == 2002) {
            if ($period == 'quincena') {
                return ['owner' => 2, 'inquilino' => 0.5];
            }
        } else if ($category == 3003) {
            if ($period == 'quincena') {
                return ['owner' => 1.363636, 'inquilino' => 0.66667];
            }
        }

        if ($category == 1002 || $category == 1003) {
            if ($period == 'mes') {
                return ['owner' => 2.215909, 'inquilino' => 2.25];
            }
        } else if ($category == 2002) {
            if ($period == 'mes') {
                return ['owner' => 3.5, 'inquilino' => 1.333333];
            }
        } else if ($category == 3003) {
            if ($period == 'mes') {
                return ['owner' => 1.2307692308, 'inquilino' => 1.3333333333];
            }
        }

        return ['owner' => 1, 'inquilino' => 1];
    }

    public static function getAll($userName = null, $espacioId = null, $orderBy = null, $orderDirection = null, $date = null)
    {
        $reserva = ReservaEcuestre::where('deleted', false)->with('espacio', 'user');

        if($userName) {
            $reserva->join('users', 'users.id', '=', 'reservas_ecuestre.user_id')
               ->where('users.name', 'like', '%' . $userName . '%');
        }

        if($espacioId) {
            $reserva->where('espacio_id', '=', $espacioId);
        }

        if($date) {
            $dayStartingTime = Carbon::parse($date);
            $dayEndingTime = Carbon::parse($date);
            $dayEndingTime->addDay();

            $reserva->whereBetween('fechaIngreso', array($dayStartingTime, $dayEndingTime));
        } else {
            $weekAgo = Carbon::now()->addDays(-7);
            $reserva->where('fechaIngreso', '>=', $weekAgo);
        }

        if ($orderBy && $orderDirection) {
            $reserva->orderBy($orderBy, $orderDirection);        
        } else {
            $reserva->orderBy('fechaIngreso', 'desc');
        }

        $reserva->select('reservas_ecuestre.*')->orderBy('espacio_id');
            
        return $reserva->paginate(20);
    }

    public static function getCategorias() {
        return array(
            [
                "id" => 1001,
                "name" => "Corrales",
                "icon" => "icon-corral.png",
                "iconActive" => "icon-corral-active.png",
                "subcategories" => [
                    array(
                        "id" => 1002, 
                        "name" => "Corrales Sector Ecuestre",
                        "description" => "La reserva en este sector es por corral completo. La disponibilidad es de 1 corral por propietario o inquilino y hasta un máximo de 6 caballos.",
                        "imageFile" => "http://costa-esmeralda.com.ar/wp-content/uploads/2020/12/corrales.jpeg"
                    ),
                    array(
                        "id" => 1003,
                        "name" => "Corrales Sector Playa",
                        "description" => "La reserva en este sector es por caballo. La disponibilidad es de 1 corral por propietario o inquilino y hasta un máximo de 4 caballos.",
                        "imageFile" => "http://costa-esmeralda.com.ar/wp-content/uploads/2020/12/corrales.jpeg"
                    )
                ]
            ],
            [
                "id" => 2002,
                "name" => "Lockers",
                "icon" => "icon-corral.png",
                "iconActive" => "icon-corral-active.png",
            ],
            [
                "id" => 3003,
                "name" => "Camas para petiseros",
                "icon" => "icon-cama.png",
                "iconActive" => "icon-cama-active.png",
            ]
        );
    }

    public static function calculatePeriod($period, $date) {
        $date = new Carbon($date);

        if ($period == 'semana') {
            return [
                'from' => $date->startOfWeek()->format('Y-m-d'),
                'to' => $date->endOfWeek()->format('Y-m-d')
            ];
        } else if($period == 'quincena') {
            return [
                'from' => $date->day >= 15 ? $date->firstOfMonth()->addDays(14)->format('Y-m-d') : $date->firstOfMonth()->format('Y-m-d'),
                'to' => $date->day >= 15 ? $date->lastOfMonth()->format('Y-m-d') : $date->firstOfMonth()->addDays(14)->format('Y-m-d')
            ];
        } else { //mes
            return [
                'from' => $date->firstOfMonth()->format('Y-m-d'),
                'to' => $date->lastOfMonth()->format('Y-m-d')
            ];
        }
    }

    public static function getUserReservas($userId) {
        return ReservaEcuestre::with('espacio')
            ->where('deleted', false)
            ->where('status', '<>', 'canceled')
            ->where('user_id', $userId)
            ->whereDate('fechaInicio', '>', Carbon::now()->subMonth())
            ->orderBy('fechaInicio', 'DESC')
            ->get();
    }

    public static function getCorralForPeriod( $user, $params ) {
        $corralInfo = ReservaEcuestre::where('deleted', false)
                            ->where('user_id', $user->id)
                            ->where('status', '<>', 'canceled')
                            ->where('fechaInicio', '<=', $params['date'])
                            ->where('fechaFin', '>=', $params['date'])
                            ->whereIn('espacio_id', [5,8])
                            ->first();
        if ($corralInfo) {
            $diffDays = $corralInfo->fechaFin->diffInDays($corralInfo->fechaInicio);
            $period = $diffDays < 8 
                        ? 'semana' 
                        : ($diffDays < 16
                                ? 'quincena'
                                : 'mes'
                        );
            return [
                        'success' => true, 
                        'corral' => $corralInfo,
                        'multiplier' => 1,
                        'places' => ReservasEcuestreService::getPlacesForPeriod($params['category'], [
                            'from' => $corralInfo->fechaInicio, 'to' => $corralInfo->fechaFin
                        ], $period)
                ];
        } else {
            $corralInfoOtherPeriod = ReservaEcuestre::where('deleted', false)
                            ->where('status', '<>', 'canceled')
                            ->where('user_id', $user->id)
                            ->where('fechaFin', '>=', Carbon::now()->format('Y-m-d'))
                            ->whereIn('espacio_id', [5,8])
                            ->first();
            if ($corralInfoOtherPeriod) {
                return [
                    'code' => 402,
                    'error' => 'Tenés un corral reservado entre las fechas: <br><br><strong>' . $corralInfoOtherPeriod->fechaInicio->format('d/m/Y') . ' al ' . $corralInfoOtherPeriod->fechaFin->format('d/m/Y') . '</strong><br><br> Solo podés reservar camas o lockers dentro del período de reserva del corral. <br><br>Por favor realizá la reserva dentro de esas fechas.'
                ];
            } else {
                return [
                    'code' => 400,
                    'error' => 'No hemos encontrado reservas confirmadas de Corrales a tu nombre. Si todavía no tenes un corral no podrás reservar lockers ni camas para petizeros.'
                ];
            }
        }
    }

    public static function getPlacesForPeriod( $category, $period, $periodString ) {
        $multiplier = ReservasEcuestreService::getMultiplier($periodString, $category);
        return EspacioEcuestre::where('category_id', $category)
                        ->leftJoin('reservas_ecuestre', function ($join) use ($period) {
                            $join->on('espacios_ecuestre.id', '=', 'reservas_ecuestre.espacio_id')
                                ->where('deleted', false)
                                ->where('status', '<>', 'canceled')
                                ->where(function ($subQuery) use ($period) {
                                    $subQuery->whereBetween('reservas_ecuestre.fechaInicio', [$period['from'],$period['to']])
                                            ->orWhereBetween('reservas_ecuestre.fechaFin', [$period['from'],$period['to']])
                                            ->orWhere(function($subQuery_from) use ($period) {
                                                $subQuery_from->where('reservas_ecuestre.fechaInicio', '<=', $period['from'])
                                                              ->where('reservas_ecuestre.fechaFin', '>=', $period['from']);
                                            })
                                            ->orWhere(function($subQuery_to) use ($period) {
                                                $subQuery_to->where('reservas_ecuestre.fechaInicio', '<=', $period['to'])
                                                              ->where('reservas_ecuestre.fechaFin', '>=', $period['to']);
                                            });
                                });
                                
                        })
                        ->select(DB::raw('espacios_ecuestre.id, espacios_ecuestre.nombre, espacios_ecuestre.descripcion, 
                                        (vacants - count(reservas_ecuestre.id)) as vacantes, vacants,
                                        (vacants - COALESCE(SUM(reservas_ecuestre.places),0)) as lugares, 
                                        espacios_ecuestre.places, shared, ROUND(price_owner * ' . $multiplier['owner'] . ') as price_owner, ROUND(price_inquilino * ' . $multiplier['inquilino'] . ') as price_inquilino'))
                        ->groupBy('espacios_ecuestre.id')
                        ->get();
    }

    public static function createReserva( $user, $params ) {
        return ReservaEcuestre::create([
            'user_id' =>  $user->id,
            'espacio_id' => $params['espacio_id'],
            'fechaIngreso' => $params['date'],
            'fechaInicio' => $params['fechaInicio'],
            'fechaFin' => $params['fechaFin'],
            'amount' => isset($params['amount']) ? $params['amount'] : null,
            'status' => isset($params['amount']) && $params['amount'] > 0 ? 'pending' : 'confirmed',
            'notas' => isset($params['notas']) ? $params['notas'] : '',
            'places' => isset($params['places']) ? $params['places'] : 1,
            'updated_by' => $user->id
        ]);
    }

    public static function delete($reservaId, $userId) {
        $reserva = ReservaEcuestre::find($reservaId);
        $reserva->deleted = true;
        $reserva->updated_by = $userId;
        $reserva->save();

        // CANCELAR CORRALES / LOCKERS PARA ESE PERIODO
        if ($reserva->espacio->category_id == 1002 || $reserva->espacio->category_id == 1003) {
            $reservas = ReservaEcuestre::where('deleted', false)
                            ->where('user_id', $userId)
                            ->where('status', '<>', 'canceled')
                            ->where('fechaInicio', $reserva->fechaInicio)
                            ->where('fechaFin', $reserva->fechaFin)
                            ->whereIn('espacio_id', [1,2])
                            ->get();
            foreach ($reservas as $key => $reserv) {
                $reserv->deleted = true;
                $reserv->updated_by = $userId;
                $reserv->save();
            }
        }

    }

    public static function confirmPaymentReserva( $paymentId ) {
        $reserva = ReservaEcuestre::where('payment_id', $paymentId)->first();
        if ($reserva) {
            $reserva->status = 'confirmed';
            $reserva->save();
        }
    }
}