<?php

namespace App\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\Flysystem\Exception;

use Carbon\Carbon;

use stdClass;

use App\Poll;
use App\PollOption;
use App\PollAnswer;

class PollsService
{

    public function __construct() {
    }

    public static function getAll()
    {
        return Poll::where('deleted', false)
                    ->with(['options', 'options.answers'])
                    ->orderBy('created_at', 'desc')
                    ->get();
    }

    public static function getPoll( $pollId ) {
        return Poll::where('id', $pollId)->with(['options', 'options.answers'])->first();
    }

    public static function getPollStats( $pollId, $params ) {
        $pollDb = Poll::find($pollId);

        $poll = new stdClass();
        $poll->id = $pollDb->id;
        $poll->question = $pollDb->question;
        $poll->created_at = $pollDb->created_at;
        $poll->valid_from = $pollDb->valid_from;
        $poll->valid_to = $pollDb->valid_to;
        $poll->totalVotes = 0;

        $optionsDb = PollOption::where('poll_id', $pollId)->get();
        $options = array();
        foreach ($optionsDb as $key => $optionDb) {
            $option = new stdClass();
            $option->text = $optionDb->text;
            $votes = 0;

            if ($params) {
                if ($params['type'] == 'all') {
                    $query = PollAnswer::where('poll_option_id', $optionDb->id);
                } else {
                    if ($params['type'] == 'propietarios') {
                        $query = PollAnswer::where('poll_option_id', $optionDb->id)
                                            ->join('users', 'users.id', '=', 'poll_answers.user_id')
                                            ->where(function($q) {
                                                $q->where('users.type', 'owner')
                                                ->orWhere('users.type', 'familiar');
                                            });
                    } else {
                        $query = PollAnswer::where('poll_option_id', $optionDb->id)
                                            ->join('users', 'users.id', '=', 'poll_answers.user_id')
                                            ->where('users.type', 'inquilino');
                    }
                }

                if ($params['month']) {
                    $query->whereMonth('poll_answers.created_at', '=', $params['month']);
                }
                
                if ($params['year']) {
                    $query->whereYear('poll_answers.created_at', '=', $params['year']);
                }
                
                $votes = $query->count();
            } else {
                $votes = PollAnswer::where('poll_option_id', $optionDb->id)->count();
            }

            $option->votes = $votes;
            $poll->totalVotes += $votes;
            array_push($options, $option);
        }

        $poll->options = $options;
        return $poll;
    }

    public static function deletePoll( $pollId ) {
        $poll = Poll::find($pollId);
        $poll->deleted = true;
        $poll->save();
    }

    public static function getActivePolls( $userId )
    {
        $twoMonthsAgo = Carbon::now()->subMonths(2);
        $polls = Poll::where('deleted', false)
                    ->where('valid_to', '>', $twoMonthsAgo)
                    ->where('valid_from', '<', Carbon::now())
                    ->with(['options', 'options.answers'])
                    ->orderBy('created_at', 'desc')
                    ->get();

        foreach ($polls as $key => $poll) {
            $pollAnswerForUser = PollAnswer::where('user_id', $userId)
                                        ->join('poll_options', 'poll_options.id', '=', 'poll_answers.poll_option_id')
                                        ->where('poll_options.poll_id', $poll->id)
                                        ->first();

            $poll->answer = $pollAnswerForUser ? $pollAnswerForUser->poll_option_id : 0;
        }

        return $polls;
    }

    public static function createPoll ( $params ) {
        $poll = Poll::create([
            'question' => $params['question'],
            'valid_from' => $params['valid_from'],
            'valid_to' => $params['valid_to']
        ]);

        foreach ($params['answers'] as $key => $answer) {
            PollOption::create([
                'poll_id' => $poll->id,
                'text' => $answer
            ]);
        }
    }

    public static function votePoll( $userId, $pollId, $params ) {
        $poll = Poll::find($pollId);

        if ($poll->deleted) {
            throw new Exception('Esta encuesta ha sido eliminada y ya no se encuentra disponible para votar.');
            return;
        }

        $pollOption = PollOption::find($params['option']);
        $pollAnswerForUser = PollAnswer::where('user_id', $userId)
                                        ->join('poll_options', 'poll_options.id', '=', 'poll_answers.poll_option_id')
                                        ->where('poll_options.poll_id', $pollId)
                                        ->first();

        if ($pollAnswerForUser) {
            throw new Exception('Su voto ya ha sido registrado en esta encuesta. Muchas Gracias por participar!');
            return;
        }

        PollAnswer::create([
            'user_id' => $userId,
            'poll_option_id' => $params['option'],
            'text'=> $params['text']
        ]);

        $pollOption->votes = $pollOption->votes + 1;
        $pollOption->save();

        return true;
    }
}