<?php

namespace App\Services;

use App\Authorization;
use App\Exceptions\CostaValidationException;
use GuzzleHttp\Client as HttpClient;

use Log;
use Carbon\Carbon;
use Exception;

use App\User;

class AuthorizationsService {

    private $domain = 'http://179.40.95.122:8090/v1/';
    private $headers = [
        'Authorization' => 'Basic bWNkZzphZmoyM3IwdTk='
    ];
    private $accountId = 2;

    public function __construct() {
    }

    public function getTypes( $userId ) {
        return [
            [ 'id'  => 1, 'name' => 'SERVICIO DOMESTICO'],
            [ 'id'  => 3, 'name' => 'SERVICIO DE JARDINERIA / PILETA'],
            [ 'id'  => 5, 'name' => 'OTROS PROVEEDORES / SERVICIOS'],
            [ 'id'  => 6, 'name' => 'INVITADO (tengo los datos)'],
            [ 'id'  => 4, 'name' => 'INVITADO (no tengo sus datos)']
        ];
    }

    public function getAuthorizationByCode( $code ) {
        return Authorization::where('code', $code)->first();
    }

    public function getTipoDocParams( $ccdocumento, $dni, $reintent = false ) {
        if (strlen($ccdocumento) > 8) {
            if (strpos($ccdocumento, '30') === 0 || strpos($ccdocumento, '33') === 0 || strpos($ccdocumento, '34') === 0) {
                return [
                    'idTipoDoc' => $reintent ? 6 : 7,
                    'nroDoc'    => $ccdocumento
                ];
            }
        }
        return [
            'idTipoDoc' => 1,
            'nroDoc'    => $dni
        ];
    }

    public function getPersonGrupos($idPersona) {
        try {
            $client = new HttpClient();
            $params = 'idPersona=' . $idPersona . '&accountId=' . $this->accountId;
            $response = $client->request('GET', $this->domain . 'Grupo/GetUnidadesFuncionales?' . $params, ['headers' => $this->headers, 'verify' => false, 'query' => $params]);
            $userData = json_decode($response->getBody(),true);
            return $userData;
        } catch(Exception $e) {
            Log::error('AuthService:getPersonGrupos', array('userId' => $idPersona, 'error' => $e->getMessage()));
            return ['ReturnVal' => -1];
        }
    }

    public function getPersonData($user, $reintent = false) {
        try {
            $client = new HttpClient();
            $params = $this->getTipoDocParams($user->ccdocumento, $user->dni, $reintent);
            $response = $client->request('GET', $this->domain . 'Persona/GetByNroDoc', ['headers' => $this->headers, 'verify' => false, 'query' => $params]);
            $userData = json_decode($response->getBody(), true);
            return $userData;
        } catch(Exception $e) {
            Log::error('AuthService:getPersonData', array('userId' => $user->id, 'error' => $e->getMessage()));
            return ['ReturnVal' => -1];
        }
    }

    public function validatePersona( $userId ) {
        try {
            $user = User::find($userId);
            $userData = $this->getPersonData($user);
            if ($userData['ReturnVal'] == 0) {
                if ($userData['PuedeAutorizar'] == true) {
                    return $userData;
                }
            }
            if (strlen($user->ccdocumento) > 8) {
                $userData = $this->getPersonData($user, true);
                if ($userData['ReturnVal'] == 0) {
                    if ($userData['PuedeAutorizar'] == true) {
                        return $userData;
                    }
                }
            }
            Log::error('AuthService:validatePersona', array('userId' => $userId, 'response' => $userData));
            throw new Exception('');
        } catch (Exception $e) {
            Log::error('AuthService:validatePersona', array('userId' => $userId, 'error' => $e->getMessage()));
            throw new Exception('Ups! El documento registrado en la plataforma no coincide con un usuario activo para realizar autorizaciones. En caso de ser inquilino o familiar, comuníquese con el propietario para verificar su cuenta. Si usted es propietario, comuníquese con la administración o soporte@costa-esmeralda.com.ar');
        }
    }

    public function listAuthorizations( $userId ) {
        try {
            $user = User::find($userId);
            $client = new HttpClient();
            $paramsTipoDoc = $this->getTipoDocParams($user->ccdocumento, $user->dni);
            $params = [ 'nroDoc' => $paramsTipoDoc['nroDoc'] ];
            $response = $client->request('GET', $this->domain . 'Autorizacion/List', ['headers' => $this->headers, 'verify' => false, 'query' => $params]);
            $authorizations = json_decode($response->getBody(),true);
            if ($authorizations['ReturnVal'] == 0) {
                return $authorizations['List'];
            } else {
                return [];
            }
        } catch (Exception $e) {
            Log::error('AuthService:listAuthorizations', array('userId' => $userId, 'error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no se pudo obtener la lista de autorizaciones. Por favor intentá más tarde!');
        }
    }

    public function getUserCategories( $userId ) {
        try {
            $user = User::find($userId);

            // FAMILIARES NO PUEDEN AUTORIZAR INQUILINOS
            if ($user->type == 'familiar') {
                return $this->getTypes($userId);
            }

            $userData = $this->getPersonData($user);

            if ($userData['ReturnVal'] == 0) {
            } else {
                if (strlen($user->ccdocumento) > 8) {
                    $userData = $this->getPersonData($user, true);
                }
            }


            if ($userData['ReturnVal'] == 0) {
                if ($userData['PuedeAutorizar'] == true) {
                    $userGroups = $this->getPersonGrupos($userData['IdPersona']);
                    $staticTypes = $this->getTypes($userId);
                    if ($userGroups['ReturnVal'] == 0) {
                        if (count($userGroups['ListaKeyValue']) > 1) {
                            foreach ($userGroups['ListaKeyValue'] as $lista) {
                                array_push($staticTypes, [
                                    'id'    => 'INQ_' . $lista['Key'] . '_1',
                                    'name'  => $lista['Value'] . ' - INQUILINO (puede autorizar)'
                                ]);
                                array_push($staticTypes, [
                                    'id'    => 'INQ_' . $lista['Key'] . '_0',
                                    'name'  => $lista['Value'] . ' - INQUILINO (NO puede autorizar)'
                                ]);
                            }
                        } else if (count($userGroups['ListaKeyValue']) == 1) {
                            array_push($staticTypes, [
                                'id'    => 'INQ_' . $userGroups['ListaKeyValue'][0]['Key'] . '_1',
                                'name'  => 'INQUILINO (puede autorizar)'
                            ]);
                            array_push($staticTypes, [
                                'id'    => 'INQ_' . $userGroups['ListaKeyValue'][0]['Key'] . '_0',
                                'name'  => 'INQUILINO (NO puede autorizar)'
                            ]);
                        }
                    }
                    return $staticTypes;
                }
            }
            return $this->getTypes($userId);
        } catch (Exception $e) {
            Log::error('AuthService:listAuthorizations', array('userId' => $userId, 'error' => $e->getMessage()));
            return $this->getTypes($userId);
        }
    }

    public function callAuthorizationOpenKey( $userId, $params ) {
        try {
            $client = new HttpClient();
            $params = [
                'AutorizacionFechaDesde'=> $params['valid_from'],
                'AutorizacionFechaHasta'=> $params['valid_to'],
                'IdAutorizante'         => $params['IdPersona'],
                'VisitaIdTipoDocumento' => '1',
                'VisitaDocumentoNro'    => $params['document'],
                'VisitaApellido'        => $params['last_name'],
                'VisitaNombre'          => $params['first_name'],
                'VisitaEmail'           => $params['email'],
                'VisitaPatente'         => $params['patent'],
                'AutorizacionComentario'=> $params['comment'],
                'CategoriaServicio'     => $params['category']
            ];
            $response = $client->request('POST', $this->domain . 'Autorizacion/Add', ['headers' => $this->headers, 'verify' => false, 'json' => $params]);
            $userData = json_decode($response->getBody(),true);
            if ($userData['ReturnVal'] == 0) {
                return $userData;
            } else {
                if ($userData['ReturnVal'] == -12) {
                    throw new CostaValidationException('Ups! El documento con el que estás intentando crear la autorización no tiene permisos para hacerlo.');
                    return;
                }
                if ($userData['ReturnVal'] == -13) {
                    throw new CostaValidationException('Ups! El documento con el que estás intentando crear la autorización no tiene permisos para hacerlo en el rango de fechas seleccionado.');
                    return;
                }
                Log::error('AuthService:callAuthorizationOpenKey', array('userId' => $userId, 'response' => $userData));
                throw new CostaValidationException('Ups! Comunicate con la administración de Costa para verificar tus permisos de Open Key. Código de Error: ' . $userData['ReturnVal']);
            }
        } catch (CostaValidationException $e) {
            throw new CostaValidationException($e->getMessage());
        } catch (Exception $e) {
            Log::error('AuthService:callAuthorizationOpenKey', array('userId' => $userId, 'error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no podés agregar esta autorización. Por favor intentá más tarde!');
        }
    }

    public function addInquilinoOpenKey( $userId, $params ) {
        try {
            $client = new HttpClient();
            $params = [
                'FechaDesde'            => $params['valid_from'],
                'FechaHasta'            => $params['valid_to'],
                'IdAutorizante'         => $params['IdPersona'],
                'IdGrupoAlquiler'       => $params['IdGrupo'],
                'DocumentoNro'          => $params['document'],
                'IdTipoDocumento'       => '1',
                'ApellidoInquilino'     => $params['last_name'],
                'NombreInquilino'       => $params['first_name'],
                'InquilinoOtorgaAutorizacion'   => $params['Autoriza']
            ];
            $response = $client->request('POST', $this->domain . 'Autorizacion/Inquilino/Add', ['headers' => $this->headers, 'verify' => false, 'json' => $params]);
            $userData = json_decode($response->getBody(),true);
            if ($userData['ReturnVal'] == 0) {
                return $userData;
            } else {
                Log::error('AuthService:addInquilinoOpenKey', array('userId' => $userId, 'response' => $userData));
                throw new CostaValidationException('Ups! Comunicate con la administración de Costa para verificar tus permisos de Open Key. Inquilino Código de Error: ' . $userData['ReturnVal']);
            }
        } catch (CostaValidationException $e) {
            throw new CostaValidationException($e->getMessage());
        } catch (Exception $e) {
            Log::error('AuthService:addInquilinoOpenKey', array('userId' => $userId, 'error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no podés agregar esta autorización. Por favor intentá más tarde!');
        }
    }

    public function addAuthorization( $userId, $params ) {
        try {
            $user = User::find($userId);

            $fechaIni = Carbon::createFromFormat('d-m-Y', $params['valid_from']);
            $fechaFin = Carbon::createFromFormat('d-m-Y', $params['valid_to']);
            if ($fechaFin->lessThan($fechaIni)) {
                throw new CostaValidationException('Ups! La fecha de fin debe ser posterior a la fecha de inicio de la autorización!');
                return;
            }

            if ($params['category'] == 4) {
                $code = uniqid() . '-' . uniqid() . '-' . uniqid();
                Authorization::create([
                    'user_id'   => $user->id, 
                    'IdPersona' => $params['IdPersona'], 
                    'code'      => $code, 
                    'valid_from'=> Carbon::createFromFormat('d-m-Y', $params['valid_from'])->toDateString(), 
                    'valid_to'  => Carbon::createFromFormat('d-m-Y', $params['valid_to'])->toDateString()
                ]);
                return [
                    'title' => 'Invitación Costa Esmeralda',
                    'text'  => 'Ingresá y completá tus datos!',
                    'image' => 'http://costa-esmeralda.com.ar/wp-content/uploads/2020/09/Bandera-Costa-Esmeralda-Septiembre-2020.jpg',
                    'link'  => 'http://costa-esmeralda.covecinos.com/autorizaciones/' . $code
                ];
            } 

            // Si es invitado = 6 -> pongo como 4
            $params['category'] = $params['category'] == 6 ? 4 : $params['category'];
            $params['valid_from'] = Carbon::createFromFormat('d-m-Y', $params['valid_from'])->toDateString();
            $params['valid_to'] = Carbon::createFromFormat('d-m-Y', $params['valid_to'])->toDateString();

            if (strpos($params['category'], 'INQ_') !== false) {
                $parsedCategory = explode('_', $params['category']);
                $params['IdGrupo'] = $parsedCategory[1];
                $params['Autoriza'] = $parsedCategory[2] == 1 ? true : false;
                return $this->addInquilinoOpenKey($userId, $params);
            } else {
                return $this->callAuthorizationOpenKey($userId, $params);
            }
        } catch (CostaValidationException $e) {
            throw new Exception($e->getMessage());
        } catch (Exception $e) {
            Log::error('AuthService:addAuthorization', array('userId' => $userId, 'error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no podés agregar esta autorización. Por favor intentá más tarde!');
        }
    }

    public function cancelAuthorization( $authorizationId ) {
        try {
            $client = new HttpClient();
            $params = [
                'id'    => $authorizationId
            ];
            $response = $client->request('POST', $this->domain . 'Autorizacion/Anular', ['headers' => $this->headers, 'verify' => false, 'query' => $params]);
            $userData = json_decode($response->getBody(),true);
            if ($userData['ReturnVal'] == 0) {
                return ['success' => true, 'userData' => $userData];
            } else {
                Log::error('AuthService:cancelAuthorization', array('authId' => $authorizationId, 'response' => $userData));
                throw new Exception('');
            }
        } catch (Exception $e) {
            Log::error('AuthService:cancelAuthorization', array('authId' => $authorizationId, 'error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no podés cancelar esta autorización. Por favor intentá más tarde!');
        }
    }
}