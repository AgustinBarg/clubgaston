<?php namespace App\Services;

class HelperService {
	public static $mapSectores = [
		'DEPO' 		=> 'Deportivo I',
		'SEND II' 	=> 'Senderos II',
		'GOLF' 		=> 'Golf',
		'Barrio Ecuestre' => 'Ecuestre',
		'LOCAL' 	=> 'Comercial',
		'Senderos III'=> 'Senderos III',
		'DEPO II' 	=> 'Deportivo II',
		'RESID' 	=> 'Residencial I',
		'Senderos IV'=> 'Senderos IV',
		'BOSQ' 		=> 'Bosques',
		'SEND' 		=> 'Senderos I',
		'RES II' 	=> 'Residencial II',
		'MAR II' 	=> 'Marítimo II',
		'MAR III' 	=> 'Marítimo III',
		'MAR IV' 	=> 'Marítimo IV',
		'MAR' 		=> 'Marítimo I',
		'MI' 		=> 'Marítimo I',
		'MII' 		=> 'Marítimo II',
		'DEP' 		=> 'Deportivo I',
		'MAR I'		=> 'Marítimo I',
		'MIII' 		=> 'Marítimo III',
		'Oficina' 	=> 'Comercial',
		'GONDOLA' 	=> 'Comercial',
		'AL GOLF 19'=> 'Al Golf 19',
		'MIII' 		=> 'Marítimo III',
		'Albatros' 	=> 'Al Golf 19',
		'Par' 		=> 'Al Golf 19',
		'Aguila' 	=> 'Al Golf 19',
		'Birdie' 	=> 'Al Golf 19'
	];
	public static function parseSectorLote( $loteUbicacion ) {
        if (strpos($loteUbicacion, 'Albatros') === 0) {
            $loteUbicacion = 'Albatros';
        } else if (strpos($loteUbicacion, 'Aguila') === 0) {
            $loteUbicacion = 'Aguila';
        } else if (strpos($loteUbicacion, 'Par') === 0) {
            $loteUbicacion = 'Par';
        } else if (strpos($loteUbicacion, 'Birdie') === 0) {
            $loteUbicacion = 'Birdie';
        } else if (strpos($loteUbicacion, 'LOCAL') === 0) {
            $loteUbicacion = 'LOCAL';
        } else if (strpos($loteUbicacion, 'Oficina') === 0) {
            $loteUbicacion = 'Oficina';
        } else if (strpos($loteUbicacion, 'GONDOLA') === 0) {
            $loteUbicacion = 'GONDOLA';
        } else if (strpos($loteUbicacion, 'MIII') === 0) {
            $loteUbicacion = 'MIII';
        } else if (strpos($loteUbicacion, 'MII') === 0) {
            $loteUbicacion = 'MII';
        }
		return (array_key_exists($loteUbicacion, HelperService::$mapSectores)) 
				? HelperService::$mapSectores[$loteUbicacion]
				: null;
	}
}