<?php

namespace App\Services;

use App\Exceptions\CostaValidationException;

use Log;

use App\Repositories\UserRepository;
use App\ResponsibleArea;
use App\User;

use App\UserDevice;
use League\Flysystem\Exception;
use Mail;
use Carbon\Carbon;

use Laravel\Passport\Token;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository  $userRepository, User $userModel)
    {
        $this->userRepository = $userRepository;
        $this->userModel = $userModel;
    }

    public function softDeleteUser($userId) {
        $user_found = $this->userModel->find($userId);
        if (!$user_found) {
            throw new Exception('El usuario q esta intentando eliminar no existe o ya fue eliminado.', 1);
        } else {
            Token::where('user_id', $userId)->delete();
            $user_found->borrado = true;
            $user_found->save();
        }
    }

    public function listUsers($searchText = null, $orderBy = null, $orderDirection = null)
    {
        $users = $this->userRepository->getUsers($searchText, $orderBy, $orderDirection);
        return $users;
    }

    public function createUser($data)
    {
        $user = $this->userModel->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'sede_id' => $data['sede_id'],
            'role_id' => $data['role_id']
        ]);

        return $user;
    }

    public function createUserAdmin($data)
    {
        $userExisting = $this->userModel->where('email', $data['email'])
                                        ->orWhere('dni', $data['document'])
                                        ->orWhere('ccdocumento', $data['document'])
                                        ->first();
        if ($userExisting) {
            return ['error' => 'Ups! Ya existe un usuario con el Email o DNI ingresados!'];
        }

        $user = $this->userModel->create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
            'sede_id'   => 1,
            'role_id'   => $data['role_id']
        ]);
        if (isset($data['responsible_area'])) {
            foreach ($data['responsible_area'] as $key => $value) {
                $responsibleArea = ResponsibleArea::find($value);
                if ($responsibleArea) {
                    $emails = explode(';', $responsibleArea->contacto);
                    array_push($emails, $user->email);
                    $responsibleArea->update([
                        'contacto'  => implode(';', $emails)
                    ]);
                }
            }
        }
        return true;
    }

    public function getUserById($id)
    {
        return $this->userModel->with(['lotes', 'interests'])->findOrFail($id);
    }

    public function getUserByDocument($document)
    {
        return User::where('ccdocumento', $document)
                    ->orWhere('dni', $document)->first();
    }

    public function checkDocument($document)
    {
        $user_found = $this->userModel->where('ccdocumento', $document)
                                      ->with('lotes')
                                      ->orWhere('dni', $document)
                                      ->first();

        if (!$user_found) {
            throw new CostaValidationException('No existe un usuario con el DNI ingresado. En caso de ser inquilino o familiar, comuníquese con el propietario para activar su cuenta. Si usted es propietario, comuníquese con la administración o soporte@costa-esmeralda.com.ar', 1);
        } else {

            if ($user_found->borrado == 1) {
                if ($user_found->type == 'owner') {
                    throw new CostaValidationException('Su Usuario ha sido borrado. Si usted piensa que fue borrado incorrectamente, por favor comuniquese con administración para revalidar su identidad y volver a activar su cuenta.', 1);
                } else {
                    throw new CostaValidationException('Su Usuario ha sido borrado o caducó su invitación. Usted es un usuario invitado del propietario: ' . $user_found->owner->name . '. Por favor comuniquese con el propietario para que vuelva a activar su cuenta desde la sección Añadir Usuarios.', 1);
                }
            }

            if ($user_found->email_duplicated) {
                throw new CostaValidationException('La dirección de email asociada a tu cuenta está siendo utilizada por otro usuario. Por favor comunicate con Atención al Propietario para actualizar tu dirección de email.');
            }

            //ENVIAR MAIL CON CODIGO
            Mail::send('mails.mail-user-activation-code', ['user_found' => $user_found], function($message) use ($user_found)
            {
                $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                $message->to($user_found->email)->subject('Código de Activación');
            });
            return $user_found;
        }
    }

    public function activateUser($email) {
        $user_found = $this->userModel->where('email', $email)->first();
        if ($user_found) {
            $user_found->activado = true;
            $user_found->save();
        }
        return $user_found;
    }

    public function updateUserEmail( $userId, $email ) {
        $user_found = $this->userModel->where('email', $email)->where('id', '<>', $userId)->first();
        if ($user_found) {
            throw new Exception('El usuario: ' . $user_found->name . ' ya existe con el email: ' . $user_found->email . '', 1);
            return;
        }
        $this->userModel->find($userId)->update(['email' => $email]);
    }

    public function checkInvitationValidity($email, $codePassword){
        $user_found = $this->userModel->where('email', $email)->first();

        if ($user_found->role_id == 0) {
            throw new Exception('Su usuario fue generado para acceder a incidentes pero no tiene permisos para ingresar a la aplicación. Por favor comuniquese con la administración.', 1);
            return;
        }

        if ($user_found->borrado) {
            throw new Exception('Su usuario fue borrado o temporalmente desactivado. Por favor comuniquese con la administración.', 1);
            return;
        }

        if ($user_found->type != 'owner') {
            $invited_from = new Carbon($user_found->invited_valid_from, 'America/Argentina/Buenos_Aires');
            $invited_to = new Carbon($user_found->invited_valid_to, 'America/Argentina/Buenos_Aires');

            if (!Carbon::now()->between($invited_from, $invited_to, false)) {
                $message = 'El código sólo es válido desde: ' . $invited_from->format('d-m-Y') . ' hasta: ' . $invited_to->format('d-m-Y');
                if (Carbon::now()->gt($invited_to)) {
                    $user_found->update(['borrado' => true]);
                }
                throw new Exception($message, 1);
            }
        }
    }

    public function getRelatedUsers($user) {
        $invitedBy = $user->type === 'owner' ? $user->id : $user->invited_by;
        return $this->userModel
                            ->where('invited_by', $invitedBy)
                            ->where('borrado', false)
                            ->get();
    }

    public function addRelatedUser($user, $userData) {
        $userMailFound = $this->userModel->where('email', $userData['email'])->first();
        if ($userMailFound) {
            if ($userMailFound->dni != $userData['dni']) {
                throw new Exception('El email ingresado ya corresponde a otro usuario de la plataforma con diferente DNI', 1);
                return;
            }
        }

        $user_found = $this->userModel->where('ccdocumento', $userData['dni'])
                                      ->orWhere('dni', $userData['dni'])
                                      ->orWhere('email', $userData['email'])
                                      ->first();

        if ($user_found) {
            // Me fijo si es inquilino y caduco la invitacion
            if ($user_found->type === 'inquilino') {
                $invited_to = new Carbon($user_found->invited_valid_to, 'America/Argentina/Buenos_Aires');
                if (Carbon::now()->gt($invited_to)) {
                    $user_found->borrado = true;
                }
            }

            if ($user_found->type === 'external') {
                $user_found->borrado = true;
                $user_found->role_id = 1;
            }

            //Si estaba borrado, lo vuelvo a activar.
            if ($user_found->borrado) {
                $user_found->invited_by = $user->type === 'owner' || $user->invited_by === 'external' ? $user->id : $user->invited_by;
                $user_found->borrado = false;
                $user_found->email = $userData['email'];
                $user_found->lote = $user->lote;
                $user_found->lote_ubicacion = $user->lote_ubicacion;
                $user_found->invited_valid_from = Carbon::createFromFormat('d-m-Y', $userData['valid_from']);
                $user_found->invited_valid_to = Carbon::createFromFormat('d-m-Y', $userData['valid_to']);
                $user_found->type = $userData['userType'];
                $user_found->save();

                try {
                    //ENVIAR MAIL CON INVITACIÓN
                    Mail::send('mails.mail-user-invite', ['new_user' => $user_found, 'owner' => $user], function($message) use ($user_found)
                    {
                        $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                        $message->to($user_found->email)->subject('Invitación App Costa Esmeralda');
                    });
                } catch(Exception $e) {}

                return $user_found;
            } else {
                throw new Exception('El usuario q esta intentando invitar ya se encuentra registrado y activo en el sistema.', 1);
            }
        } else {

            // CHECK EMAIL
            if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", $userData['email'])) {
                throw new Exception('El email ingresado no tiene un formato válido. Por favor asegúrese de escribirlo correctamente');
                return;
            }

            $userData['email'] = preg_replace('/\s/', '', $userData['email']);
            $userData['email'] = strtolower($userData['email']);
            
            $userCode = bin2hex(openssl_random_pseudo_bytes(4));
            $userCode = strtoupper($userCode);

            $props = array(
                'invited_by' => $user->type === 'owner' ? $user->id : $user->invited_by,
                'name' => $userData['firstname'] . ' ' . $userData['lastname'], 
                'email' => $userData['email'], 
                'password' => bcrypt($userCode),
                'codigo' => $userCode,
                'ccdocumento' => str_replace('.', '', $userData['dni']),
                'dni' => str_replace('.', '', $userData['dni']),
                'lote' => $user->lote,
                'lote_ubicacion' => $user->lote_ubicacion,
                'lote_sector' => $user->lote_sector,
                'type' => $userData['userType'],
                'sede_id' => 1,
                'role_id' => 1,
                'invited_valid_from' => Carbon::createFromFormat('d-m-Y', $userData['valid_from']),
                'invited_valid_to' => Carbon::createFromFormat('d-m-Y', $userData['valid_to'])
            );
            $new_user = $this->userModel->create($props);

            try {
                //ENVIAR MAIL CON INVITACIÓN
                Mail::send('mails.mail-user-invite', ['new_user' => $new_user, 'owner' => $user], function($message) use ($new_user)
                {
                    $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda');
                    $message->to($new_user->email)->subject('Invitación App Costa Esmeralda');
                });
            } catch(Exception $e) {}

            return $new_user;
        }
    }

    public function saveRelatedUser($user, $userData) {
        $userMailFound = $this->userModel->where('email', $userData['email'])->where('id', '<>', $userData['user_id'])->first();
        if ($userMailFound) {
            throw new Exception('El email ingresado ya corresponde a otro usuario de la plataforma', 1);
            return;
        }

        $userDniFound = $this->userModel->where('id', '<>', $userData['user_id'])
                                        ->where(function ($subQuery) use ($userData) {
                                            $subQuery->where('ccdocumento', $userData['dni'])
                                                    ->orWhere('dni', $userData['dni']);
                                        })
                                        ->first();
        if ($userDniFound) {
            throw new Exception('El DNI ingresado ya corresponde a otro usuario de la plataforma', 1);
            return;
        }

        try {
            $user_found = $this->userModel->find($userData['user_id']);
            $user_found->name = $userData['firstname'] . ' ' . $userData['lastname'];
            $user_found->email = $userData['email'];
            $user_found->invited_valid_from = Carbon::createFromFormat('d-m-Y', $userData['valid_from']);
            $user_found->invited_valid_to = Carbon::createFromFormat('d-m-Y', $userData['valid_to']);
            $user_found->dni = $userData['dni'];
            $user_found->ccdocumento = $userData['dni'];
            $user_found->save();
            return $user_found;
        } catch(Exception $e) {
            throw new Exception('Ups! Ocurrió un error al intentar actualizar el usuario. Por favor revise que los datos ingresados sean válidos', 1);
        }
    }

    public function refreshDeviceToken($token, $userId)
    {
        $userDevice = UserDevice::where('user_id', $userId)->get();
        if (count($userDevice) > 0) {
            return UserDevice::where('user_id', $userId)->update([
                'token' => $token
            ]);
        }

        return UserDevice::create([
            'token' => $token,
            'deviceOs' => 'android',
            'user_id' => $userId
        ]);
    }
}