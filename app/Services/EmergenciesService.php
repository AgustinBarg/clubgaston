<?php

namespace App\Services;

use App\EmergencyCall;
use GuzzleHttp\Client as HttpClient;

use Log;
use Exception;

use App\User;

class EmergenciesService {

    private $domain = 'http://179.40.95.122:8090/v1/';
    private $headers = [
        'Authorization' => 'Basic bWNkZzphZmoyM3IwdTk='
    ];
    private $type_Security = 'SECURITY';
    private $type_Medical = 'MEDICAL';
    private $type_Fire = 'FIRE';
    private $type_911 = '911';
    private $type_GenderViolence = 'GENDER_VIOLENCE';
    
    private $authorizationsService;

    public function __construct( AuthorizationsService $authorizationsService ) {
        $this->authorizationsService = $authorizationsService;
    }

    private function getType( $type ) {
        switch ($type) {
            case 'incendio':
                return $this->type_Fire;
            case 'seguridad':
                return $this->type_Security;
            case 'medica':
                return $this->type_Medical;
        }
    }

    public function callEmergency( $userId, $params ) {
        try {
            $user = User::find($userId);
            $personData = $this->authorizationsService->getPersonData($user);
            $emergencyType = $this->getType($params['type']);
            if ($personData['ReturnVal'] == 0) {
                $response = $this->callService([
                    'IdPersona'     => $personData['IdPersona'],
                    'URL'           => 'https://www.google.com.ar/maps/place/' . (isset($params['latlong_location']) ? $params['latlong_location'] : ''),
                    'Type'          => $emergencyType,
                    'Accuracy'      => '0'
                ]);
                $emergencyData = json_decode($response->getBody(),true);
                if ($emergencyData['ReturnVal'] == 0) {
                    EmergencyCall::create([
                        'user_id'   => $userId,
                        'type'      => $emergencyType,
                        'location'  => isset($params['latlong_location']) ? $params['latlong_location'] : ''
                    ]);
                    return ['success' => true, 'emergencyData' => $emergencyData];
                } else {
                    EmergencyCall::create([
                        'user_id'   => $userId,
                        'type'      => $emergencyType,
                        'location'  => isset($params['latlong_location']) ? $params['latlong_location'] : '',
                        'log'       => json_encode($emergencyData)
                    ]);
                    Log::error('EmergenciesService:callEmergency', array('response' => $emergencyData));
                    throw new Exception('');
                }
            } else {
                EmergencyCall::create([
                    'user_id'   => $userId,
                    'type'      => $emergencyType,
                    'location'  => isset($params['latlong_location']) ? $params['latlong_location'] : '',
                    'log'       => json_encode($personData)
                ]);
                throw new Exception('');
            }
        } catch (Exception $e) {
            Log::error('EmergenciesService:callEmergency', array('error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no se pudo realizar el aviso. Por favor intentá más tarde!');
        }
    }

    public function callService( $params ) {
        try {
            $client = new HttpClient();
            return $client->request('POST', $this->domain . 'Persona/Emergencia', ['headers' => $this->headers, 'verify' => false, 'json' => $params]);
        } catch (Exception $e) {
            Log::error('EmergenciesService:callEmergency', array('error' => $e->getMessage()));
            throw new Exception('Ups! En este momento no podés cancelar esta autorización. Por favor intentá más tarde!');
        }
    }
}