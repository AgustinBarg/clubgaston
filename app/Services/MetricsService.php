<?php

namespace App\Services;

use App\Metrics\MetricView;
use Carbon\Carbon;

use DB;

class MetricsService
{

    public function __construct() {
    }

    public static function getActiveUsersByOS($dateRange) {
        $query = DB::table('user_devices')
                        ->select(DB::raw('IF(LENGTH(token) > 70, "android", "ios") as os, count(*) as count'));
        
        if ($dateRange && isset($dateRange['start'])) {
            $query->where('updated_at', '>=' , $dateRange['start']);
            $query->where('updated_at', '<=' , $dateRange['end']);
        }

        return $query->groupBy('os')->get();
    }

    public static function addView($name) {
        $now = Carbon::now();
        $now->tz('America/Argentina/Buenos_Aires');
        $view = MetricView::where('name', $name)
                            ->where('date', $now->toDateString())
                            ->first();
        if (!$view) {
            $view = MetricView::create([
                                'name' => $name,
                                'date' => $now->toDateString(),
                                'count' => 1
                            ]);
        } else {
            $view->count = $view->count + 1;
            $view->save();
        }
    }

    public static function getAllViews($dateRange, $limit = 100) {
        $query = DB::table('metrics_view')
                    ->select(DB::raw('distinct name, sum(count) as count'));

        if ($dateRange && isset($dateRange['start'])) {
            $query->where('date', '>=' , $dateRange['start']);
            $query->where('date', '<=' , $dateRange['end']);
        }
        
        return $query->groupBy('name')
                    ->orderBy('count', 'DESC')
                    ->limit($limit)
                    ->get();
    }
}