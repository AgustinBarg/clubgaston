<?php

namespace App\Services;

use nusoap_client;

use SimpleXMLElement;

use App\User;
use Laravel\Passport\Token;

use Carbon\Carbon;

class ExpensesService
{
    public function __construct()
    {
    }

    public static function getUserExpenses( $userId ) {
        $user = User::find($userId);

        if (!$user || !$user->id_cliente) {
            return array('expenses' => []);
        }

        try {
            $fechaDesde = Carbon::now()->subMonths(6)->format('Ymd');
            $fechaHasta = Carbon::now()->day(19)->format('Ymd');;
            $expense_movements = array();


            $lotesEmprendimientos = $user->lotes->pluck('emprendimiento_id')->toArray();
            array_push($lotesEmprendimientos, $user->emprendimiento_id);
            $lotesEmprendimientos = array_unique($lotesEmprendimientos);
            foreach ($lotesEmprendimientos as $key => $emprendimientoId) {
                if ($emprendimientoId) {
                    try {
                        $soap_client = new nusoap_client('http://181.174.193.74/foxserver.asmx?WSDL', true, false, true, false, false, 0, 300, '');
                        $parameters = array(
                                        'tidCliente' => $user->id_cliente,
                                        'tIdEmprendimiento' => $emprendimientoId,
                                        'tcDesde' => $fechaDesde,
                                        'tcHasta' => $fechaHasta,
                                        'tcCobranza' => $fechaDesde
                                    );
                        $result = $soap_client->call('EstadoDeCuenta', $parameters);
                        if ($result) {
                            $result = new SimpleXMLElement($result['EstadoDeCuentaResult']);
                            foreach ($result as $key => $record) {
                                if ($record->lote) {
                                    $object = array(
                                        'lote' => trim($record->lote),
                                        'saldo' => -1 * floatval(trim($record->nsaldo)),
                                        'concepto' => trim($record->concepto),
                                        'identidaddet' =>  trim($record->identidaddet),
                                        'grupo_concepto' =>  trim($record->cgrupoconcepto),
                                        'fecha' =>  trim($record->fecha),
                                        'debito' => floatval(trim($record->debito)),
                                        'credito' => floatval(trim($record->credito)),
                                        'numero' =>  trim($record->nnumero), 
                                        'comprobante' =>  trim($record->idcomprobante)
                                    );
                                    array_push($expense_movements, $object);
                                }
                            }
                        }
                    } catch (\Exception $e) {
                    }
                }
            }

            return array('expenses' => array_reverse($expense_movements));
        } catch(\Exception $e) {
            return array('expenses' => []);
        }
    }
}