<?php

namespace App\Services;

use DB;

use App\Categorias;

class ReportsService
{
    public static function getDeportesEspacios() {
        return Categorias::with('espacios')->get();
    }
    public static function getUserTypes() {
        return DB::table('users')
                    ->select(DB::raw('type, count(*) as count'))
                    ->where('type', '<>', '')
                    ->groupBy('type')
                    ->get();
    }

    public static function getAVGUserAdded() {
        return DB::select("SELECT FORMAT(AVG(cantidad), 2) as average
                            FROM (select invited_by, count(*) as cantidad
                            from users
                            where 
                            type = 'inquilino' and invited_by is not null
                            group by invited_by) as Invitados")[0];
    }

    public static function getTicketsByType($dateRange) {
        $query = "SELECT sa.name AS name, count(*) as count FROM tickets_incidentes t
                            INNER JOIN sub_areas sa ON sa.id = t.tema_id ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " AND t.created_at > '" . $dateRange['start'] . "'";
            $query .= " AND t.created_at < '" . $dateRange['end'] . "'";
        }
        $query .= " GROUP BY sa.name
                    ORDER BY count DESC ";
        return DB::select($query);
    }

    public static function getAVGTicketsAnswer($dateRange) {
        $query = "SELECT AVG(tiempo) AS average FROM
                            (SELECT t.id, t.title, min(TIMESTAMPDIFF(HOUR, t.created_at, tm.created_at)) AS tiempo
                            FROM tickets_incidentes t
                            LEFT OUTER JOIN tickets_messages tm ON tm.tickets_incidente_id = t.id
                            WHERE tm.tickets_incidente_id IS NOT NULL ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " AND t.created_at > '" . $dateRange['start'] . "'";
            $query .= " AND t.created_at < '" . $dateRange['end'] . "'";
        }
        $query .= " GROUP BY t.id, t.title) AS Respuestas ";
        return DB::select($query)[0];
    }

    public static function getAVGTicketsCreated($dateRange) {
        return [];
    }

    public static function getTicketsByUserTypeCreated($dateRange) {
        $query = "SELECT u.type AS type, count(*) AS count
                            FROM tickets_incidentes t 
                            INNER JOIN users u ON u.id = t.user_id ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " WHERE t.created_at > '" . $dateRange['start'] . "'";
            $query .= " AND t.created_at < '" . $dateRange['end'] . "'";
        }
        $query .= " GROUP BY 1 ";
        return DB::select($query);
    }

    public static function getPollAnswersByUserType( $pollId ) {
        $query = "SELECT u.type AS type, count(*) AS count
                    FROM poll_answers p
                    INNER JOIN users u ON u.id = p.user_id 
                    INNER JOIN poll_options po ON po.id = p.poll_option_id ";
        if ($pollId != 0) {
            $query .= " WHERE po.poll_id = " . $pollId;
        }
        $query .= " GROUP BY 1";
        return DB::select($query);
    }

    public static function getReservasByCategory( $dateRange ) {
        $query = "SELECT c.name as name, count(*) as count FROM reservas r
                    INNER JOIN espacios e on e.id = r.espacio_id
                    INNER JOIN categorias c on c.id = e.categoria_id ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " AND r.horaInicio > '" . $dateRange['start'] . "'";
            $query .= " AND r.horaInicio < '" . $dateRange['end'] . "'";
        }
        $query .= " GROUP BY 1 ";
        return DB::select($query);
    }

    public static function getReservasByUserType( $dateRange ) {
        $query = "SELECT u.type as type, count(*) as count FROM reservas r
                    INNER JOIN users u on u.id = r.user_id
                    where r.user_id != 1 ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " AND r.horaInicio > '" . $dateRange['start'] . "'";
            $query .= " AND r.horaInicio < '" . $dateRange['end'] . "'";
        }
        $query .= " GROUP BY 1 
                    UNION 
                    SELECT 'Reservas' as type, count(*) as count
                    FROM reservas WHERE user_id = 1 ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " AND horaInicio > '" . $dateRange['start'] . "'";
            $query .= " AND horaInicio < '" . $dateRange['end'] . "'";
        }
        return DB::select($query);
    }

    public static function getReservasByTime( $dateRange ) {
        $query = "SELECT DATE_FORMAT(horaInicio, '%H:%i') as hora, count(*) as count
                    FROM reservas ";
        if ($dateRange && isset($dateRange['start'])) {
            $query .= " WHERE horaInicio > '" . $dateRange['start'] . "'";
            $query .= " AND horaInicio < '" . $dateRange['end'] . "'";
        }
        $query .= " GROUP BY 1 ";
        return DB::select($query);
    }
}