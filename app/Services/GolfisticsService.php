<?php

namespace App\Services;

use SplFileObject;

use App\User;

class GolfisticsService
{
    private static $key = ' ';

    public function __construct()
    {
    }

    public function importUsers($logger) {
        $logger->info('');
        $file = new SplFileObject('storage/golf/golf-25122019.txt');
        while (!$file->eof()) {
            $userLine = $file->fgets();
            $userArray = explode(',', $userLine);
            $document = $userArray[5];

            $userFound = User::where('ccdocumento', $document)
                                ->orWhere('dni', $document)->first();


            if ($userFound) {
                $logger->info('Encontrado: DNI: ' . $document . ' - Usuario: ' . $userFound->id . ': ' . $userFound->name);
                try {
                    $userFound->matricula_golfistics = $userArray[0];
                    $userFound->save();
                } catch(\Exception $e) {
                    $logger->error($e->getMessage());
                    $logger->error($userLine);
                }
            } else {
                $logger->error('Usuario No Encontrado: ' . $document . ': ' . $userArray[1]);
            }
        }
    }

    public static function cryptMessage($message){
        return base64_encode($message);
    }

    public static function getGolfisticsUrl( $user ) {
        if (!$user->matricula_golfistics) {
            return ['url' => 'http://www.costaesmeraldagolf.com/golf/loginCE.php'];
        }
        $matriculaCrypted = GolfisticsService::cryptMessage($user->matricula_golfistics);
        return ['url' => 'http://www.costaesmeraldagolf.com/golf/loginCE.php?matricula=' . urlencode($matriculaCrypted)];
    }
}