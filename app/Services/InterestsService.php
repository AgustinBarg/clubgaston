<?php

namespace App\Services;

use DB;

use App\Interest;
use App\InterestPivot;

class InterestsService {

    public function getInterestsWithUsers() {
        return Interest::where('deleted', false)
                        ->leftJoin('user_interests', function ($join) {
                            return $join->on('user_interests.interest_id', '=', 'interests.id');
                        })
                        ->select(DB::raw('interests.id, interests.title, count(distinct(user_interests.user_id)) as usuarios'))
                        ->orderBy('usuarios', 'desc')
                        ->groupBy('interests.id')
                        ->get();
    }

    public function getInterests() {
        return Interest::where('deleted', false)->get();
    }

    public function saveInterest( $id, $title ) {
        if ($id) {
            Interest::find($id)->update(['title' => $title]);
        } else {
            Interest::create(['title' => $title]);
        }
    }

    public function deleteInterest( $id ) {
        Interest::find($id)->update(['deleted' => true]);
    }

    public function saveInterests( $userId, $interests ) {
        InterestPivot::where('user_id', $userId)->delete();
        foreach ($interests as $interestId) {
            InterestPivot::create([
                'user_id'       => $userId,
                'interest_id'   => $interestId
            ]);
        }
    }
}