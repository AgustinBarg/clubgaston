<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/11/17
 * Time: 12:16 PM
 */

namespace App\Services;


class CuotasStatus
{
    const INFORMED = 0;
    const PENDING = 1;
    const PAYED = 2;
    const REJECTED = 3;
}