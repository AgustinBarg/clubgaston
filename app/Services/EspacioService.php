<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/11/17
 * Time: 3:53 PM
 */

namespace app\Services;

use App\Espacio;
use Illuminate\Support\Facades\DB;

class EspacioService
{
    private $model;

    public function __construct(Espacio $model)
    {
        $this->model = $model;
    }

    public function getCategoriasBySede($sedeId)
    {
        $categorias = DB::table('categorias')
            ->distinct('categorias.id')
            ->join('espacios', 'categorias.id', 'espacios.categoria_id')
            ->where('espacios.sede_id', '=', $sedeId)
            ->select('categorias.id as category_id', 'categorias.name as name', 'categorias.icon as icon')
            ->get();

        return $categorias;
    }
}