<?php

namespace App\Services;

use App\Http\Validators\ReservaValidatedValidator;
use Carbon\Carbon;
use Exception;
use DB;

use App\Payment;
use App\ReservaConfig;
use App\ReservaGym;
use App\ReservaTimes;
use App\UserCredits;
use App\UserCreditsPackage;

abstract class UserCreditsPackages {
    const DAY = 'day';
    const WEEK = 'WEEK';
    const FORTNIGHT = 'fortnight';
}

class ReservasGymService
{
    static $maxGymPlaces = 10;
    static $maxDayReservations = 2;

    public function __construct() {}

    public static function cleanCredits($logger) {
        $now = Carbon::now();
        $now->tz('America/Argentina/Buenos_Aires');
        
        $packages = UserCreditsPackage::where('finished', 'false')
                            ->where('valid_to', '<', $now)
                            ->get();
        foreach ($packages as $key => $package) {
            $package->update(['finished' => 'true']);
            $userCredits = ReservasGymService::getAccountCredits($package->user_id);
            if ($userCredits > 0) {
                ReservasGymService::addCredits($package->user_id, UserCredits::$TYPE_VENCIMIENTO, -$userCredits, $package->id, null);
            }
        }
    }

    public static function getPackage( $packageId ) {
        $packages = ReservasGymService::getCreditsPackages();
        return array_first($packages, function($pack) use ($packageId) {
            return $pack['id'] == $packageId;
        });
    }

    public static function getPackageByPrice( $amount ) {
        $packages = ReservasGymService::getCreditsPackages();
        return array_first($packages, function($pack) use ($amount) {
            return $pack['amount'] == $amount;
        });
    }

    public static function getCreditsPackages() {
        return [
            [   
                'id'        => UserCreditsPackages::DAY, 
                'amount'    => 200,
                'credits'   => 1,
                'subtitle'  => 'crédito',
                'description'=> '1 crédito = 1 hora',
                'valid_to'  => Carbon::now()->addDays(7)->toDateString()
            ],
            [
                'id'        => UserCreditsPackages::WEEK, 
                'amount'    => 1200,
                'credits'   => 7,
                'subtitle'  => 'créditos',
                'description'=> '7 créditos = 7 horas',
                'valid_to'  => Carbon::now()->addDays(15)->toDateString()
            ],
            [   
                'id'        => UserCreditsPackages::FORTNIGHT, 
                'amount'    => 1750,
                'credits'   => 15,
                'subtitle'  => 'créditos',
                'description'=> '15 créditos = 15 horas',
                'valid_to'  => Carbon::now()->addDays(30)->toDateString()
            ]
        ];
    }

    public static function getAll($userName = null, $orderBy = null, $orderDirection = null, $date = null, $validated = null) {
        $reserva = ReservaGym::where('deleted', false);

        if($userName) {
            $reserva->join('users', 'users.id', '=', 'reservas_gym.user_id')
               ->where('users.name', 'like', '%' . $userName . '%');
        }

        if ($validated != null) {
            $reserva->where('validated', '=', $validated);
        }

        if($date) {
            $reserva->where('fechaInicio', '<', $date . ' 23:59:00');
            $reserva->where('fechaInicio', '>', $date . ' 00:00:00');
        } else {
            $weekAgo = Carbon::now()->addDays(-7);
            $reserva->where('fechaInicio', '>=', $weekAgo);
        }

        if ($orderBy && $orderDirection) {
            $reserva->orderBy($orderBy, $orderDirection);        
        } else {
            $reserva->orderBy('fechaInicio', 'desc')->orderBy('id', 'asc');
        }

        return $reserva->select('reservas_gym.*')->paginate(20);
    }

    public static function getAccountCredits( $userId ) {
        $userCredits = UserCredits::where('user_id', $userId)
                                    ->orderBy('id', 'desc')
                                    ->first();
        return $userCredits ? $userCredits->subtotal : 0;
    }
    
    public static function addCredits( $userId, $type, $credits, $packageId = null, $reservaId = null ) {
        $userCredits = ReservasGymService::getAccountCredits($userId);
        UserCredits::create([
            'user_id'           => $userId,
            'type'              => $type,
            'reserva_id'       => $reservaId,
            'credits_package'   => $packageId,
            'amount'            => $credits,
            'subtotal'          => floatval($userCredits) + $credits
        ]);
    }

    public static function getCreditsAndPayments( $userId ) {
        return [
            'credits'   => ReservasGymService::getAccountCredits($userId),
            'payment'   => Payment::where('user_id', $userId)
                                    ->where('type', 'creditos-gym')
                                    ->where('status', 'pending')
                                    ->orderBy('id', 'desc')
                                    ->first()
        ];
    }

    public static function getCreditsMovements( $userId ) {
        return UserCredits::where('user_id', $userId)
                            ->orderBy('id', 'desc')
                            ->get();
    }

    public static function getUserReservas($userId) {
        return ReservaGym::where('deleted', false)
                            ->where('user_id', $userId)
                            ->whereDate('fechaInicio', '>', Carbon::now()->subMonth())
                            ->orderBy('fechaInicio', 'DESC')
                            ->get();
    }

    // Capacidad máxima por turno (10 personas)
    public static function getTimesAvailable( $userType, $date, $isAdmin = false ) {
        $config = ReservaConfig::find(1);
        $maxHour = $config->gym_max_hour;
        $timeFrom = $userType === 'inquilino' 
                                ? $config->gym_inquilino_inicio 
                                : $config->gym_owner_inicio;
        $timeTo = $userType === 'inquilino' 
                                ? $config->gym_inquilino_fin 
                                : $config->gym_owner_fin;
                            
        if (!$isAdmin && Carbon::now()->isSameDay(Carbon::parse($date))) {
            $timeFromParsed = Carbon::createFromFormat('Y-m-d H:i:s', $date . ' ' . $timeFrom);
            $timeToParsed = Carbon::createFromFormat('Y-m-d H:i:s', $date . ' ' . $timeTo);
            $timeNow = Carbon::now();
            if ($timeNow->greaterThan($timeFromParsed) && $timeNow->lessThan($timeToParsed)) {
                $timeFrom = $timeNow->hour . ':00:00';
            }
        }

        return ReservaTimes::where('time', '>=', $timeFrom)
                            ->where('time', '<=', $timeTo)
                            ->leftJoin('reservas_gym', function ($join) use ($date) {
                                $join->on(DB::raw('TIME(reservas_gym.fechaInicio)'), '=', 'time')
                                        ->where('deleted', false)
                                        ->whereRaw('DATE(fechaInicio) = ?', $date);
                            })
                            ->select(DB::raw('time, 
                                        (' . $maxHour . ' - count(reservas_gym.id)) as vacantes,
                                        ' . $maxHour . ' as vacants'))
                            ->groupBy('time')
                            ->get();
    }

    public static function createReserva( $user, $params ) {
        $reservasConfig = ReservaConfig::find(1);
        if ($reservasConfig->reservas_validation && $user->id != 1) {
            $userCanReserve = ReservaValidatedValidator::checkUserCanReserveUnvalidated($user->id);
            if (!$userCanReserve) {
                throw new Exception('Ups!
                                    No podés realizar reservas!
                                    Acumulaste 2 reservas sin validar en la última semana.');
                return;
            }
        }

        $dateInicio = Carbon::createFromFormat('Y-m-d H:i:s', $params['date'] . ' ' . $params['time']);
        $reserva = ReservaGym::create([
            'user_id'       => $user->id,
            'fechaInicio'   => $dateInicio->toDateTimeString(),
            'fechaFin'      => $dateInicio->addHour(1)->toDateTimeString(),
            'notas'         => isset($params['notas']) ? $params['notas'] : '',
            'updated_by'    => $user->id,
            'validated'     => false
        ]);
        if ($user->type === 'inquilino') {
            $config = ReservaConfig::find(1);
            if ($config->gym_allow_credits) {
                ReservasGymService::addCredits($user->id, UserCredits::$TYPE_RESERVA, -1, null, $reserva->id);
                // IF CREDITS = 0 , REMOVE LAST PACKAGE
                $userCredits = ReservasGymService::getAccountCredits($user->id);
                if ($userCredits == 0) {
                    $lastPackage = UserCreditsPackage::where('user_id', $user->id)
                                                    ->where('finished', false)
                                                    ->first();
                    if ($lastPackage) {
                        $lastPackage->update(['finished' => true]);
                    }
                }
            }
        }
        return $reserva;
    }

    public static function delete($user, $reservaId) {
        // TODO: CHEQUEAR ROLLBACK CREDITOS
        $reserva = ReservaGym::find($reservaId);
        
        if ($reserva->validated) {
            return ['error' => 'Ups! La reserva que estás intentando cancelar ya ha sido validada!'];
        }

        $reserva->deleted = true;
        $reserva->updated_by = $user->id;
        $reserva->save();

        if ($user->type === 'inquilino') {
            $config = ReservaConfig::find(1);
            $checkNewPackages = UserCreditsPackage::where('user_id', $user->id)
                                                    ->where('created_at', '>', $reserva->created_at)
                                                    ->where('finished', false)
                                                    ->first();
            if ($checkNewPackages || $config->gym_allow_credits) {
                //DO NOTHING / NO SE ROLLBACKEA
            } else {
                ReservasGymService::addCredits($user->id, UserCredits::$TYPE_CANCELACION, 1, null, $reserva->id);
            }
        }
    }

    public static function validateReservaAdmin($reservaId, $userId) {
        $reserva = ReservaGym::find($reservaId);
        $reserva->validated = true;
        $reserva->validated_by = $userId;
        $reserva->validated_at = Carbon::now();
        $reserva->save();
    }

    public static function confirmPaymentReserva( $paymentId ) {
        $payment = Payment::find($paymentId);
        if ($paymentId) {
            $packageData = ReservasGymService::getPackageByPrice($payment->amount);
            $pack = UserCreditsPackage::create([
                'user_id'   => $payment->user_id,
                'payment_id'=> $payment->id,
                'amount'    => $packageData['credits'],
                'valid_to'  => $packageData['valid_to']
            ]);
            ReservasGymService::addCredits($payment->user_id, UserCredits::$TYPE_COMPRA, $pack->amount, $pack->id, null);
        }
    }
}