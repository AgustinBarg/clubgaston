<?php
/**
 * Created by PhpStorm.
 * User: marcelo
 * Date: 7/4/17
 * Time: 11:51 AM
 */

namespace App\Services;


use App\Repositories\IncidenteRepository;
use App\ResponsibleArea;
use App\ticketsIncidente;
use App\ticketsMessage;
use App\ticketsMessageImage;
use App\UserDevice;
use App\Notification;

use App\Services\NotificationService;
use Mail;

class IncidenteService
{
    private $incidenteRepository;
    private $incidenteModel;

    public function __construct(IncidenteRepository $incidenteRepository, ticketsIncidente $incidenteModel)
    {
        $this->incidenteRepository = $incidenteRepository;
        $this->incidenteModel = $incidenteModel;
    }

    public function create($data)
    {
        $image = isset($data['image']) ? $data['image'] : null;
        $new_ticket = $this->incidenteModel->create([
                'user_id'=> $data['user_id'],
                'tema_id' => $data['tema_id'],
                'title' => $data['title'],
                'detalle'=> $data['detalle'],
                'image_path' => $image,
                'state_id' =>  1,
                'location'=> isset($data['location']) ? $data['location'] : '',
                'latlong_location'=> isset($data['latlong_location']) ? $data['latlong_location'] : ''
        ]);

        if ($new_ticket->user_id == 5) {
            return;
        }

        try {
            Mail::send('mails.mail-new-incident', ['incident' => $new_ticket], function($message) use ($new_ticket) {
                $contacts = explode( ';', $new_ticket->tema->responsableArea->contacto ) ;
                $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                foreach ($contacts as $key => $contactMail) {
                    if ($contactMail != '') {
                        $message->to($contactMail)->subject('Nueva Gestión');
                    }
                }
            });
        } catch (\Exception $e) {}
        try {
            Mail::send('mails.mail-new-incident-user', ['incident' => $new_ticket], function($message) use ($new_ticket) {
                $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
                $message->to($new_ticket->user->email)->subject('Nueva Gestión');
            });
        } catch (\Exception $e) { }
        return $new_ticket;
    }

    public function updateState($id, $stateId)
    {
        $ticket_updated = $this->incidenteModel->findOrFail($id);

        $ticket_updated->update(['state_id' => $stateId]);

        if ($ticket_updated->user_id == 5) {
            return $ticket_updated;
        }

        Mail::send('mails.mail-update-incident', ['incident' => $ticket_updated], function($message) use ($ticket_updated)
        {
            $contacts = explode( ';', $ticket_updated->tema->responsableArea->contacto ) ;
            $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');

            foreach ($contacts as $key => $contactMail) {
                $message->to($contactMail)->subject('Nueva Gestión');
            }
        });

        return $ticket_updated;
    }

    public function getUserIncidente($userId)
    {
        return $this->incidenteRepository->getIncidentesByUser($userId);
    }

    public function getIncidenteMessages($incidenteId)
    {
        return $this->incidenteRepository->getMessages($incidenteId);
    }

    public function getCategorias()
    {
        return ResponsibleArea::with('subareas')->get();
    }

    public function sendMessages($data)
    {
        $new_message = ticketsMessage::create([
            'user_id' => $data['user_id'],
            'tickets_incidente_id' => $data['id'],
            'message' => $data['message']
        ]);

        if (isset($data['image'])) {
            ticketsMessageImage::create([
                'ticket_message_id' => $new_message->id,
                'image_path' => $data['image']
            ]);
        }

        $reOpen = false;
        $ticket_updated = $this->incidenteModel->findOrFail($data['id']);
        if ($ticket_updated->state_id != 1) {
            $ticket_updated->state_id = 1;
            $ticket_updated->save();
            $reOpen = true;
        }

        if ($ticket_updated->user_id == 5) {
            return $ticket_updated;
        }

        Mail::send('mails.mail-update-incident', ['incident' => $ticket_updated, 'reOpen' => $reOpen], function($message) use ($ticket_updated, $reOpen)
        {
            $contacts = explode( ';', $ticket_updated->tema->responsableArea->contacto ) ;
            $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');

            $subject = $reOpen ? 'Gestión Re-Abierta' : 'Nuevo Mensaje en Gestión';
            foreach ($contacts as $key => $contactMail) {
                $message->to($contactMail)->subject($subject);
            }
        });
    }

    public static function sendNotification( $userId, $title, $message, $info ) {
        $userDevice = UserDevice::where('user_id', $userId)->first();

        Notification::create([
            'title' => $title,
            'message' => $message,
            'user_id' => $userId,
            'type' => 'gestion',
            'type_id' => $info['id']
        ]);

        if ($userDevice && $userDevice->token) {
            if (strlen($userDevice->token) > 70) {
                return NotificationService::notifyAndroid( $title, $message, array($userDevice->token), $info);
            } else {
                return NotificationService::notifyIOs( $title, $message, array($userDevice->token), $info);
            }
        }
    }
    
}