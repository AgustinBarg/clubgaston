<?php

namespace App\Services;

use MP;
use Mail;
use Carbon\Carbon;

use App\Payment;

class PaymentsService
{
    public function __construct() {
	}

	public function cleanPayments($logger) {
        $twentyMinutesAgo = Carbon::now()->subMinutes(25);
        $twentyMinutesAgo->tz('America/Argentina/Buenos_Aires');
        
        $logger->info('Horario Limite: ' . $twentyMinutesAgo);
        $payments = Payment::where('status', 'pending')
                            ->where('created_at', '<', $twentyMinutesAgo)
                            ->get();
        foreach ($payments as $key => $payment) {
			$payment->update(['status' => 'canceled']);
        }
    }
	
	public static function getUserPayment( $userId, $type, $status ) {
		return Payment::where('user_id', $userId)
						->where('type', $type)
						->where('status', $status)
						->first();
	}

    public static function createPayment($params) {
        $payment = new Payment();
        $payment->user_id = $params['user_id'];
        $payment->amount = $params['amount'];
        $payment->type = $params['type'];
        $payment->method = $params['method'];
        $payment->status = isset($params['status']) ? $params['status'] : 'pending';
        $payment->save();
        return $payment;
    }

    public static function generateMercadoPagoPayment($user, $params) {
    	$params = [
    		'user_id'	=> $user->id,
    		'amount'	=> $params['amount'],
    		'type'		=> $params['type'],
            'method'    => $params['method']
        ];
		$payment = PaymentsService::createPayment($params);
		$expireIn = Carbon::now();
		$expireIn->tz('America/Argentina/Buenos_Aires');
		$expireIn->addMinutes(15);
        $preferenceData = [
	  		'items' => [
	  			[
	  				'id' => 1,
	  				'title' => 'Pago ' . $payment->getDescription(),
	  				'description' => isset($params['description']) ? $params['description'] : '',
	  				'quantity' => 1,
	  				'currency_id' => 'ARS',
	  				'unit_price' => $payment->amount
	  			]
	  		],
	  		'payer' => [
	  			'email'	=> $payment->user->email
			],
			'payment_methods' => [
				'excluded_payment_types' => [
					['id' => 'ticket'],
					['id' => 'atm']
				]
			],
			'expires' => true,
			'expiration_date_to' => $expireIn->format('Y-m-d\TH:i:s.000-03:00'),
	  		'external_reference' => $payment->id,
	  		'notification_url' => 'http://costa-esmeralda.covecinos.com/api/payments/process'
	  	];
		$preference = MP::create_preference($preferenceData);
		if ($params['type'] == 'reserva-ecuestre' || $params['type'] == 'reserva-deportiva' || $params['type'] == 'creditos-gym') {
			Mail::send('mails.mail-payment-reserva', 
						array('user' => $payment->user, 'paymentLink' => $preference['response']['init_point']), function($message) use ($payment) {
				$message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
				$message->to($payment->user->email)->subject('Pago reserva Costa Esmeralda');
			});
		}
	  	return [
              'preference' => $preference['response'],
              'payment' => $payment
        ];
    }

    public static function processPayment( $notification_id ) {
		try {
    		$payment = MP::get_payment($notification_id);

    		$payment_response = $payment['response']['collection'];

			$payment_internal_id = $payment_response['external_reference'];
			$paymentInternal = Payment::find($payment_internal_id);
			$paymentInternal->status =  $payment_response['status'];
			$paymentInternal->save();

			if ($paymentInternal->status == 'approved') {
				if ($paymentInternal->type == 'reserva-deportiva') {
					ReservasService::confirmPaymentReserva($paymentInternal->id);
				} else if ($paymentInternal->type == 'reserva-ecuestre') {
					ReservasEcuestreService::confirmPaymentReserva($paymentInternal->id);
				} else if ($paymentInternal->type == 'creditos-gym') {
					ReservasGymService::confirmPaymentReserva($paymentInternal->id);
				}
			}
		} catch (\Exception $e) {
			$error = $e->getMessage();
			// Mail::send('mails.error-payment', array('error' => $error), function($message) {
			// $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
			// 	$message->to('m.gaston.rodriguez@gmail.com')->subject('Error en Pago');
			// });
			return $e->getMessage();
		} catch(\SantiGraviano\LaravelMercadoPago\MercadoPagoException $e) {
			$error = $e->getMessage();
			// Mail::send('mails.error-payment', array('error' => $error), function($message) {
			// $message->from('no-reply@costa-esmeralda.covecinos.com','Costa Esmeralda App');
			// 	$message->to('m.gaston.rodriguez@gmail.com')->subject('Error MP en Pago');
			// });
			return $e->getMessage();
		}
	}
}