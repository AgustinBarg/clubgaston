<?php

namespace App\Services;

use Carbon\Carbon;

use App\Notification;
use App\Reserva;
use App\ReservaEcuestre;
use App\UserDevice;

class ReservasNotificationsService
{
    public function __construct()
    {
    }

    public function cleanReservas($logger) {
        $thirtyMinutesAgo = Carbon::now()->subMinutes(15);
        $thirtyMinutesAgo->tz('America/Argentina/Buenos_Aires');
        
        $logger->info('Horario Inicio: ' . $thirtyMinutesAgo);
        $reservas = Reserva::where('deleted', false)
                            ->where('status', 'pending')
                            ->where('created_at', '<', $thirtyMinutesAgo)
                            ->get();
        foreach ($reservas as $key => $reserva) {
            $reserva->status = 'canceled';
            $reserva->deleted = true;
            $reserva->save();
        }

        $logger->info('Buscando Reservas Ecuestres');
        $reservas = ReservaEcuestre::where('deleted', false)
                            ->where('status', 'pending')
                            ->where('created_at', '<', $thirtyMinutesAgo)
                            ->get();
        foreach ($reservas as $key => $reserva) {
            $reserva->status = 'canceled';
            $reserva->deleted = true;
            $reserva->save();
        }
    }

    public function notifyReservas($logger) {
        $logger->info('Buscando Reservas');
        
        $twoHoursFromNow = Carbon::now()->minute(0)->second(0)->addHours(2);
        $twoHoursFromNow->tz('America/Argentina/Buenos_Aires');
        $logger->info('Horario Inicio: ' . $twoHoursFromNow);
        $reservas = Reserva::where('deleted', false)
                            ->where('status', '<>', 'canceled')
                            ->where('horaInicio', $twoHoursFromNow)
                            ->get();
        foreach ($reservas as $key => $reserva) {
            $this->sendNotification($reserva->user_id, 'Recordatorio Reserva Deportiva', 'No te olvides que a las ' . $reserva->horaInicio->format('H:i')  . ' tenés una reserva:: ' . $reserva->espacio->nombre);
        }

        $logger->info('Buscando Reservas Ecuestres');
        if ($twoHoursFromNow->hour == 23) {
            $tomorrow = Carbon::tomorrow('America/Argentina/Buenos_Aires');
            $reservas = ReservaEcuestre::where('deleted', false)
                                ->where('status', '<>', 'canceled')
                                ->where('fechaIngreso', $tomorrow->toDateString())
                                ->get();
            foreach ($reservas as $key => $reserva) {
                $this->sendNotification($reserva->user_id, 'Recordatorio Reserva Ecuestre', 'No te olvides que mañana comienza tu reserva en el sector Ecuestre: ' . $reserva->espacio->nombre);
            }
        }
    }

    public function sendNotification( $userId, $title, $message ) {
        $userDevice = UserDevice::where('user_id', $userId)->first();

        Notification::create([
            'title' => $title,
            'message' => $message,
            'user_id' => $userId,
            'type' => 'reservations'
        ]);

        if ($userDevice && $userDevice->token) {
            if (strlen($userDevice->token) > 70) {
                return NotificationService::notifyAndroid($title, $message, array($userDevice->token), ['id' => '', 'type' => 'reservations']);
            } else {
                return NotificationService::notifyIOs($title, $message, array($userDevice->token), ['id' => '', 'type' => 'reservations']);
            }
        }
    }
}