<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaConfig extends Model
{
    protected $table = 'reservas_config';
    protected $fillable = ['ecuestre_allow', 'gym_allow', 'gym_allow_credits', 'gym_max_hour', 
        'gym_owner_inicio', 'gym_owner_fin',
        'gym_inquilino_inicio', 'gym_inquilino_fin',
        'ecuestre_allow_text', 'gym_allow_text', 'gym_allow_credits_text',
        'reservas_payment_allow', 'reservas_max_days', 'reservas_validation'];

    protected $casts = [
        'ecuestre_allow'    => 'boolean',
        'gym_allow'         => 'boolean',
        'gym_allow_credits' => 'boolean',
        'reservas_payment_allow'=> 'boolean',
        'reservas_validation'=> 'boolean'
    ];
}