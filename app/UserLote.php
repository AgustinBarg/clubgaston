<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserLote extends Model
{
	protected $table = 'user_lotes';
    protected $fillable = ['id_lote', 'name', 'user_id', 'emprendimiento_id'];
    public $timestamps = false;

    public function user() {
        return $this->belongsTo('App\User');
    }
}
