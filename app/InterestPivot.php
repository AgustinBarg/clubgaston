<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestPivot extends Model
{
	protected $table = 'user_interests';
    public $timestamps = false;
    protected $fillable = [
        'user_id', 'interest_id'
    ];
}
