<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspacioEcuestre extends Model
{
    protected $table = 'espacios_ecuestre';
    protected $fillable = ['nombre', 'descripcion', 'vacants', 'places', 'category_id', 'imageFile', 'price_owner', 'price_inquilino'];
    protected $casts = [
        'price_owner' => 'float',
        'price_inquilino' => 'float'
    ];

    public function reservas() {
        return $this->hasMany('App\ReservaEcuestre');
    }
}
