<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCreditsPackage extends Model
{
	protected $table = 'user_credits_packages';
    protected $fillable = ['user_id', 'payment_id', 'amount', 'valid_to', 'finished'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function payment() {
        return $this->belongsTo('App\Payment');
    }
}