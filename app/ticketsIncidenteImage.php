<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticketsIncidenteImage extends Model
{
    protected $table = 'tickets_incidentes_images';
    protected $fillable = [
        'id', 'ticket_incidente_id', 'image_path', 'created_at', 'updated_at'
    ];

    public function ticket() {
        return $this->belongsTo('App\ticketsIncidente', 'ticket_incidente_id');
    }
}
