<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCredits extends Model
{
	protected $table = 'user_credits';
    protected $fillable = ['user_id', 'reserva_id', 'credits_package', 'type', 'amount', 'subtotal'];

    static $TYPE_COMPRA = 'compra';
    static $TYPE_RESERVA = 'reserva';
    static $TYPE_VENCIMIENTO = 'vencimiento';
    static $TYPE_CANCELACION = 'cancelacion';

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function reserva() {
        return $this->belongsTo('App\ReservaGym');
    }
}
