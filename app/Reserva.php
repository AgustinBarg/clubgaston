<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $fillable = [
        'user_id','espacio_id' ,'horaInicio', 'horaFin', 'amount', 'status', 'notas', 
        'deleted', 'updated_by', 'payment_id', 'created_at', 'updated_at',
        'validated', 'validated_by', 'validated_at'
    ];
    protected $dates = ['horaInicio', 'horaFin', 'validated_at'];
    protected $appends = ['statusString'];
    
    public function espacio() {
        return $this->belongsTo('App\Espacio');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function validator() {
        return $this->belongsTo('App\User', 'validated_by');
    }

    public function payment() {
        return $this->belongsTo('App\Payment');
    }

    public function getStatus() {
        switch ($this->status) {
            case 'pending':
                return 'pendiente de pago';
            case 'confirmed':
                return 'confirmada';
            case 'canceled':
                return 'cancelada';
        }
    }

    public function getStatusIcon() {
        switch ($this->status) {
            case 'pending':
                return 'time';
            case 'confirmed':
                return 'check';
            case 'canceled':
                return 'remove';
        }
    }

    public function getStatusClass() {
        switch ($this->status) {
            case 'pending':
                return 'text-warning';
            case 'confirmed':
                return 'text-success';
            case 'canceled':
                return 'text-danger';
        }
    }

    public function getValidated() {
        return $this->validated ? 'Validada' : 'No Validada';
    }

    public function getStatusStringAttribute() {
        return $this->getStatus();
    }
}
