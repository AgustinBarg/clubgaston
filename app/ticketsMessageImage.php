<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticketsMessageImage extends Model
{   
    protected $table = 'tickets_messages_images';
    protected $fillable = [
        'id', 'ticket_message_id', 'image_path', 'created_at', 'updated_at'
    ];

    public function ticket() {
        return $this->belongsTo('App\ticketsMessage', 'ticket_message_id');
    }
}
