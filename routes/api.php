<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Login
Route::post('payments/process', 'Api\PaymentsController@processPayment');

Route::middleware('guest')->post('/register', 'Api\UserController@register');
Route::middleware('guest')->post('/login', 'Api\UserController@login');
Route::middleware('guest')->post('/login-device', 'Api\UserController@loginDevice');
Route::middleware('guest')->get('/user/search','Api\UserController@search');

// Chequear Usuario
Route::middleware('guest')->post('/validate', 'Api\UserController@checkUserDocument');

Route::middleware(['auth:api', 'securitybot'])->namespace('Api')->group(function () {
    Route::post('/logout', 'UserController@logout');
    Route::post('/profile', 'UserController@getUserProfile');

    //Users
    Route::get('/user/related', 'UserController@getUserRelateds');
    Route::post('/user/related', 'UserController@addUserRelated');
    Route::post('/user/related/update', 'UserController@updateUserRelated');
    Route::post('/user/delete', 'UserController@deleteUser');

    // Cuotas
    Route::get('/user/cuotas','CuotasController@getUserCuotas');
    Route::post('/user/cuotas/informarpago', 'CuotasController@informarPagoCuota');
    Route::post('/user/token', 'UserController@updateToken');

    // Incidente
    Route::post('/incidente', 'IncidenteController@create');
    Route::get('/incidente/user', 'IncidenteController@userIncidentes');
    Route::post('/incidente/{id}/change_state', 'IncidenteController@updateState');
    Route::get('/incidente/{id}/messages', 'IncidenteController@messages');
    Route::post('/incidente/{id}/message', 'IncidenteController@sendMessage');
    Route::get('/incidente/categorias', 'IncidenteController@getCategorias');


    // Reservas Config
    Route::get('/reservas/config', 'ReservasController@getConfig');

    // Reservas Ecuestre
    Route::get('/reservas-ecuestre/usuario', 'ReservasEcuestreController@getReservasByUser');
    Route::post('/reservas-ecuestre/cancel/{reservaId}', 'ReservasEcuestreController@cancelReserva');
    Route::get('/espacios/categorias-ecuestre', 'ReservasEcuestreController@getCategorias');
    Route::post('/espacios/ecuestre/validar-corral', 'ReservasEcuestreController@validateCorral');
    Route::post('/espacios/ecuestre/reserva', 'ReservasEcuestreController@postReserva');
    Route::post('/espacios/ecuestre/{category}', 'ReservasEcuestreController@getPlacesDisponibles');

    // Reservas Ecuestre
    Route::get('/reservas-gym/usuario', 'ReservasGymController@getReservasByUser');
    Route::get('/reservas-gym/packages', 'ReservasGymController@getPackages');
    Route::post('/reservas-gym/packages', 'ReservasGymController@buyPackage');
    Route::post('/reservas-gym/credits', 'ReservasGymController@getCreditsAndPayments');
    Route::post('/reservas-gym/user-credits', 'ReservasGymController@getCreditsMovements');
    Route::post('/reservas-gym/times', 'ReservasGymController@getTimesAvailable');
    Route::post('/reservas-gym', 'ReservasGymController@postReserva');
    Route::post('/reservas-gym/cancel/{reservaId}', 'ReservasGymController@cancelReserva');

    // Reservas
    Route::get('/sedes/{id}/espacios/categorias', 'ReservasController@getCategoriasBySede');
    Route::post('/espacios/{id}/reserva', 'ReservasController@postReserva');
    Route::post('/espacios/{id}/reserva/cancel/{reservaId}', 'ReservasController@cancelReserva');
    Route::post('/sedes/{sede_id}/categorias/{categoryId}/espacios/horarios', 'ReservasController@getCategoriasTimetables');

    Route::post('/sedes/{sede_id}/categorias/{categoryId}/espacios/horarios-test', 'ReservasController@getCategoriasTimetablesTest');
    
    Route::get('/reservas/usuario', 'ReservasController@getReservasByUser');
    Route::post('/reservas/validar', 'ReservasController@validateReservaCode');

    // Expensas
    Route::get('/user/expenses','ExpensesController@getUserExpenses');

    // Notificaciones
    Route::get('/user/notifications','NotificationsController@getUserNotifications');

    // Expensas
    Route::get('/polls','PollsController@getActivePolls');
    Route::post('/polls/{id}/vote','PollsController@votePoll');

    //GOLF
    Route::get('/golfistics-url', 'GolfisticsController@getGolfisticsUrl');

    //METRICS
    Route::post('/metrics/view', 'MetricsController@addView');

    // Authorizations
    Route::post('/authorizations/list', 'AuthorizationsController@listAuthorizations');
    Route::post('/authorizations/validate', 'AuthorizationsController@validateUser');
    Route::post('/authorizations/categories', 'AuthorizationsController@getCategories');
    Route::post('/authorizations/create', 'AuthorizationsController@createAuthorization');
    Route::post('/authorizations/cancel/{authorizationId}', 'AuthorizationsController@cancelAuthorization');

    // Interests
    Route::get('/interests', 'InterestsController@getInterests');
    Route::post('/interests/save', 'InterestsController@saveInterests');


    Route::post('/emergencies', 'EmergenciesController@callEmergency');
});





