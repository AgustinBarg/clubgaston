<?php

Auth::routes();
Route::get('/', 'Auth\LoginController@index')->name('login');
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::get('/register_backend', 'Auth\RegisterController@register')->name('register');

Route::get('/autorizaciones/{code}', 'Admin\AuthorizationsController@validateAuthorization');
Route::post('/autorizaciones/{code}', 'Admin\AuthorizationsController@saveAuthorization')->name('authorizations-save');


Route::get('/gestiones/{code}', 'TicketsController@getIncidente');
Route::post('/gestiones/mensaje', 'TicketsController@postMessage')->name('gestion-send-message');

Route::middleware(['checkroleadmin'])->namespace('Admin')->group(function () {

    Route::get('/', 'ReportsController@home')->name('home');
    Route::get('/home', 'ReportsController@home')->name('home');

    Route::get('admin-api/reports/users', 'ReportsController@getUsers');
    Route::post('admin-api/reports/tickets-categories', 'ReportsController@getTicketsCategories');
    Route::post('admin-api/reports/tickets-totals', 'ReportsController@getTicketsTotals');
    Route::get('admin-api/reports/poll-answers', 'ReportsController@getPollAnswers');
    Route::get('admin-api/reports/poll-answers/{pollId}', 'ReportsController@getPollAnswers');
    Route::post('admin-api/reports/reservas', 'ReportsController@getReservas');

    // Analytics
    Route::get('/analytics', 'MetricsController@analytics')->name('analytics');
    Route::post('admin-api/analytics/active-users', 'MetricsController@getActiveUsersByOS');
    Route::post('admin-api/analytics/top-views', 'MetricsController@getViewsTop');
    Route::post('admin-api/analytics/all-views', 'MetricsController@getViewsTable');


    // Users
    Route::get('/usuarios', 'UserController@index')->name('user.list');
    Route::get('/usuarios/nuevo-admin', 'UserController@createAdminIndex')->name('user.create-admin')->middleware('checkrolesuperadmin');
    Route::post('/usuarios/nuevo-admin', 'UserController@createAdmin')->name('user.save-admin')->middleware('checkrolesuperadmin');
    Route::get('/usuarios/{userId}', 'UserController@getProfile')->name('user.profile')->middleware('checkrolesuperadmin');
    Route::post('/usuarios/{userId}/edit-email', 'UserController@editEmail')->name('user.profile-edit-email')->middleware('checkrolesuperadmin');

    // Intereses Users
    Route::get('/intereses', 'InterestsController@index')->name('interests.list');
    Route::post('/intereses', 'InterestsController@saveInterest')->name('interests.save');
    Route::post('/intereses/delete/{interestId}', 'InterestsController@deleteInterest')->name('interests.delete');

    // Cuota
    Route::get('/cuotasindex', 'CuotaController@index')->name('cuotas.index');
    Route::get('/cuotas/list', 'CuotaController@getCuotas')->name('cuotas.list');
    Route::get('/cuotas/user_cuotas', 'CuotaController@getCuotasByUser')->name('cuotas.user');
    Route::get('/cuotas/crear', 'CuotaController@create')->name('cuotas.crear');
    Route::post('/cuotas/postcuota', 'CuotaController@post')->name('cuotas.post');
    Route::get('/cuotas/edit', 'CuotaController@edit')->name('cuotas.edit');
    Route::post('/cuotas/update', 'CuotaController@update')->name('cuotas.update');

    Route::get('/cuotas/delete', 'CuotaController@deleteCuota')->name('deletecuota')->middleware('checkrolesuperadmin');
    Route::post('/cuotas/pagoinformado', 'CuotaController@informarPago')->name('cuotas.informarpago');
    Route::get('/cuotas/payment', 'CuotaController@getPaymentData')->name('cuotas.payment_data');
    Route::get('/cuotas/payment_approve', 'CuotaController@approvePayment')->name('cuotas.payment_approve');
    Route::get('/cuotas/payment_reject', 'CuotaController@rejectPayment')->name('cuotas.payment_reject');

    // Incidentes
    Route::middleware('checkroleadminmantenimiento')->get('incidentes', 'ticketsIncidenteController@index')->name('incidente.index');
    Route::middleware('checkroleadminmantenimiento')->get('incidente', 'ticketsIncidenteController@getIncidente')->name('incidente.id');
    Route::middleware('checkroleadminmantenimiento')->get('incidente/create', 'ticketsIncidenteController@create')->name('incidente.create');
    Route::middleware('checkroleadminmantenimiento')->post('incidente/new', 'ticketsIncidenteController@postIncidente')->name('incidente.new');
    Route::middleware('checkroleadminmantenimiento')->post('incidente/edit', 'ticketsIncidenteController@edit')->name('incidente.edit');
    Route::middleware('checkroleadminmantenimiento')->post('sendmessage', 'ticketMessageController@sendMessage')->name('sendmessage');

    // Espacios
    Route::middleware('checkroleadmindeportes')->get('/reservasindex', 'ReservaController@indexReservas')->name('reservasindex');
    Route::middleware('checkroleadmindeportes')->get('/reservas/create', 'ReservaController@create')->name('reservas.create');
    Route::middleware('checkroleadmindeportes')->post('/reservas/new', 'ReservaController@newReserva')->name('reservas.new');
    Route::middleware('checkroleadmindeportes')->get('/espacio', 'ReservaController@indexEspacios')->name('espacio');
    Route::middleware('checkroleadmindeportes')->get('/reservar', 'ReservaController@selectEspacioId')->name('reserva');

    // Reservas Config
    Route::middleware('checkroleadmindeportes')->get('/reservas/config', 'ReservaController@getReservasConfig')->name('reservas.config');
    Route::middleware('checkroleadmindeportes')->post('/reservas/config', 'ReservaController@saveReservasConfig')->name('reservas.config-save');


    // Reserva
    Route::middleware('checkroleadmindeportes')->get('/reservas/list', 'ReservaController@getReservas')->name('reservas.list');
    Route::middleware('checkroleadmindeportes')->get('/reservas/export', 'ReservaController@exportExcel')->name('reservas.export');
    Route::middleware('checkroleadmindeportes')->post('/reservas/delete', 'ReservaController@delete')->name('reservas.delete');
    Route::middleware('checkroleadmindeportes')->post('/reservas/validate', 'ReservaController@validateReserva')->name('reservas.validate');
    Route::middleware('checkroleadmindeportes')->get('/reservas/tvList', 'ReservaController@tvList')->name('reservas.tv_list');

    // Reserva Ecuestre
    Route::middleware('checkroleadmindeportes')->get('/reservas-ecuestre/list', 'ReservaEcuestreController@getReservas')->name('reservas-ecuestre.list');
    Route::middleware('checkroleadmindeportes')->get('/reservas-ecuestre/create', 'ReservaEcuestreController@create')->name('reservas-ecuestre.create');
    Route::middleware('checkroleadmindeportes')->post('/reservas-ecuestre/check-places', 'ReservaEcuestreController@checkPlaces')->name('reservas-ecuestre.check-places');
    Route::middleware('checkroleadmindeportes')->post('/reservas-ecuestre/new', 'ReservaEcuestreController@newReserva')->name('reservas-ecuestre.new');

    // Reserva GYM
    Route::middleware('checkroleadmindeportes')->get('/reservas-gym/list', 'ReservaGymController@getReservas')->name('reservas-gym.list');
    Route::middleware('checkroleadmindeportes')->get('/reservas-gym/export', 'ReservaGymController@exportExcel')->name('reservas-gym.export');
    Route::middleware('checkroleadmindeportes')->get('/reservas-gym/create', 'ReservaGymController@create')->name('reservas-gym.create');
    Route::middleware('checkroleadmindeportes')->post('/reservas-gym/new', 'ReservaGymController@newReserva')->name('reservas-gym.new');
    Route::middleware('checkroleadmindeportes')->get('/reservas-gym/tvList', 'ReservaGymController@tvList')->name('reservas-gym.tv_list');
    Route::middleware('checkroleadmindeportes')->get('/reservas-gym/usuario', 'ReservaGymController@getUsers')->name('reservas-gym.users');
    Route::middleware('checkroleadmindeportes')->post('/reservas-gym/usuario', 'ReservaGymController@showUser')->name('reservas-gym.users-status');
    Route::middleware('checkroleadmindeportes')->post('/reservas-gym/delete', 'ReservaGymController@delete')->name('reservas-gym.delete');
    Route::middleware('checkroleadmindeportes')->post('/reservas-gym/validate', 'ReservaGymController@validateReserva')->name('reservas-gym.validate');


    // Notificaciones
    Route::get('notifications/list', 'NotificationsController@index')->name('notifications.index');
    Route::get('notifications/programadas', 'NotificationsController@indexScheduled')->name('notifications.programed');
    Route::get('notifications/create', 'NotificationsController@createNotification')->name('notifications.create');
    Route::post('notifications/send', 'NotificationsController@saveNotification')->name('notifications.send');
    Route::post('notifications/programadas/delete', 'NotificationsController@deleteScheduled')->name('notifications.programed.delete');

    Route::get('encuestas/list', 'PollsController@index')->name('polls.index');
    Route::get('encuestas/reportes', 'PollsController@getAllPollsReports')->name('polls.reports');
    Route::get('encuestas/create', 'PollsController@createPoll')->name('polls.create');
    Route::post('encuestas/create', 'PollsController@savePoll')->name('polls.save');
    Route::get('encuestas/{pollId}', 'PollsController@viewPoll')->name('polls.details');
    Route::post('encuestas/{pollId}/delete', 'PollsController@deletePoll')->name('polls.delete');

});

// facebook LOG IN and create user (backend)

Route::get('auth/facebook',
    [
        'as' => 'auth/facebook',
        'uses' => 'Auth\LoginController@redirectToProvider']);

Route::get('auth/facebook/callback',
    [
        'as' => 'auth/facebook/callback',
        'uses' => 'Auth\LoginController@handleProviderCallback']);
