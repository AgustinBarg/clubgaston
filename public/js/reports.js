(function() {
    var chartUsers,
        chartEncuestas,
        chartReservasDeporte,
        chartReservasUsuario,
        chartReservasTiempo,
        dateParamsReservas,
        colors = ['#F66D44', '#FEAE65', '#AADEA7', '#64C2A6', '#2D87BB', '#476088', '#FFC562', '#FF6D74', '#4FDDC3', '#61A8E8', '#559821', '#8EAF2D', '#CFD861', '#EB5F59', '#2B425A', '#192938'];

    function callWS(url, method, data) {
        return new Promise(function(resolve, reject) {
            $.ajax({
                url: url,
                method: method,
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                }
            }).done(function(resp) {
                resolve(resp);
                })
                .fail(function(err) {
                    resolve(err)
                });
        });
    }

    function parseUserType(type) {
        switch (type) {
            case 'familiar': return 'Familiar';
            case 'inquilino': return 'Inquilino';
            case 'owner': return 'Propietario';
            default: return type;
        }
    }

    function updateTicketsByCategory(params) {
        callWS('/admin-api/reports/tickets-categories', 'POST', params)
            .then(function(table) {
                $('#panel_gestiones .panel-loading').addClass('hide');
                $('#table_tickets').html(table);
            }).catch(function(error) {
            });
    }

    function updateTicketsTotals(params) {
        callWS('/admin-api/reports/tickets-totals', 'POST', params)
            .then(function(response) {
                if (response.responseTime && response.responseTime.average) {
                    $('#avg_ticket_answer').numerator({ toValue: parseFloat(response.responseTime.average), rounding: 2 });
                } else {
                    $('#avg_ticket_answer').html('--');
                }
                if (response.ticketsByUserType) {
                    $('#count_tickets_user').html('');
                    for (var i = 0; i < response.ticketsByUserType.length; i++) {
                        const element = response.ticketsByUserType[i];
                        $('#count_tickets_user').append(`
                            <div class="d-inline padding-sides">
                                <p class="text-uppercase small m-0">${parseUserType(element.type)}</p>
                                <div class="text-center h1 m-0 text-costa text-bold">${element.count}</div>
                            </div>`);
                    }
                }
            }).catch(function(error) {

            });

    }

    function updatePollAnswers( pollId ) {
        callWS('/admin-api/reports/poll-answers/' + pollId, 'GET', {})
            .then(function(results) {
                $('#panel_encuestas .panel-loading').addClass('hide');
                var configEncuesta = {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            data: results.report.length === 0 ? [1] : results.report.map((res) => res.count),
                            backgroundColor: results.report.length === 0 ? ['grey'] : colors,
                            borderWidth: 0
                        }],
                        labels: results.report.length === 0 ? ['No hay respuestas'] : results.report.map((res) => parseUserType(res.type))
                    },
                    options: { responsive: true }
                };
                if (chartEncuestas) {
                    chartEncuestas.data = configEncuesta.data;
                    chartEncuestas.update();
                } else {
                    var ctxEncuesta = document.getElementById('chart_encuestas').getContext('2d');
                    chartEncuestas = new Chart(ctxEncuesta, configEncuesta);
                }
                $('#panel_encuestas .poll-container').html(results.content);
            }).catch(function(error) {
                console.log(error);
            });
    }

    function updateUsers() {
        callWS('/admin-api/reports/users', 'GET', {})
            .then(function(results) {
                $('#panel_usuarios .panel-loading').addClass('hide');
                const userTypes = results.userTypes;
                var configUsers = {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            data: userTypes.map((res) => res.count),
                            backgroundColor: colors,
                            borderWidth: 0
                        }],
                        labels: userTypes.map((res) => parseUserType(res.type))
                    },
                    options: { responsive: true }
                };
                if (chartUsers) {
                    chartUsers.data = configUsers.data;
                    chartUsers.update();
                } else {
                    var ctxUsers = document.getElementById('chart_users').getContext('2d');
                    chartUsers = new Chart(ctxUsers, configUsers);
                }
                if (results.userAdded && results.userAdded.average) {
                    $('#count_users_added').numerator({ toValue: parseFloat(results.userAdded.average), rounding: 2 });
                }
            }).catch(function(error) {
                console.log(error);
            });
    }

    function updateReservas(params) {
        callWS('/admin-api/reports/reservas', 'POST', params)
            .then(function(results) {
                $('#panel_reservas .panel-loading').addClass('hide');
                const resultsDeporte = results.deporte;
                const resultsUsers = results.usuarios;
                const resultsTiempo = results.tiempo;
                
                var configReservasDeporte = {
                    type: 'bar',
                    data: {
                        datasets: [{
                            data: resultsDeporte.map((res) => res.count),
                            backgroundColor: colors
                        }],
                        labels: resultsDeporte.map((res) => res.name)
                    },
                    options: { responsive: true, legend: { display: false } }
                };
                if (chartReservasDeporte) {
                    chartReservasDeporte.data = configReservasDeporte.data;
                    chartReservasDeporte.update();
                } else {
                    var ctxUsers = document.getElementById('chart_reservas_deporte').getContext('2d');
                    chartReservasDeporte = new Chart(ctxUsers, configReservasDeporte);
                }

                var configReservasUsuarios = {
                    type: 'bar',
                    data: {
                        datasets: [{
                            data: resultsUsers.map((res) => res.count),
                            backgroundColor: colors
                        }],
                        labels: resultsUsers.map((res) => parseUserType(res.type))
                    },
                    options: { responsive: true, legend: { display: false } }
                };
                if (chartReservasUsuario) {
                    chartReservasUsuario.data = configReservasUsuarios.data;
                    chartReservasUsuario.update();
                } else {
                    var ctxUsers = document.getElementById('chart_reservas_usuario').getContext('2d');
                    chartReservasUsuario = new Chart(ctxUsers, configReservasUsuarios);
                }

                var configReservasTiempo = {
                    type: 'bar',
                    data: {
                        datasets: [{
                            data: resultsTiempo.map((res) => res.count),
                            backgroundColor: colors
                        }],
                        labels: resultsTiempo.map((res) => res.hora)
                    },
                    options: { responsive: true, legend: { display: false } }
                };
                if (chartReservasTiempo) {
                    chartReservasTiempo.data = configReservasTiempo.data;
                    chartReservasTiempo.update();
                } else {
                    var ctxUsers = document.getElementById('chart_reservas_tiempo').getContext('2d');
                    chartReservasTiempo = new Chart(ctxUsers, configReservasTiempo);
                }
            }).catch(function(error) {
                console.log(error);
            });
    }

    $(document).ready(function() {
        updateUsers();
        updateTicketsTotals({});
        updateTicketsByCategory({});
        updatePollAnswers(0);
        updateReservas({});

        $('#encuesta_id').change(function(e) {
            $('#panel_encuestas .panel-loading').removeClass('hide');
            updatePollAnswers($(this).val());
        });

        $('#daterange_gestiones').daterangepicker({
            locale: { applyLabel: 'Aceptar', cancelLabel: 'Cancelar' }
        }, function(start, end) {
            $('#panel_gestiones .panel-loading').removeClass('hide');
            const dateRange = { dateRange: {
                start: start.format('YYYY-MM-DD'),
                end: end.format('YYYY-MM-DD')
            }};
            updateTicketsTotals(dateRange);
            updateTicketsByCategory(dateRange);
        });

        $('#daterange_reservas').daterangepicker({
            locale: { applyLabel: 'Aceptar', cancelLabel: 'Cancelar' }
        }, function(start, end) {
            $('#panel_reservas .panel-loading').removeClass('hide');
            updateReservas({
                dateRange: {
                    start: start.format('YYYY-MM-DD'),
                    end: end.format('YYYY-MM-DD')
                }
            });
        });
    });
})();