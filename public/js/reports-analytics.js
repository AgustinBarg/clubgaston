(function() {
    var chartUsers,
        chartViews,
        colors = ['#F66D44', '#FEAE65', '#AADEA7', '#64C2A6', '#2D87BB', '#476088', '#FFC562', '#FF6D74', '#4FDDC3', '#61A8E8', '#559821', '#8EAF2D', '#CFD861', '#EB5F59', '#2B425A', '#192938'];

    function callWS(url, method, data) {
        return new Promise(function(resolve, reject) {
            $.ajax({
                url: url,
                method: method,
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                }
            }).done(function(resp) {
                resolve(resp);
                })
                .fail(function(err) {
                    resolve(err)
                });
        });
    }

    function parseUserType(type) {
        switch (type) {
            case 'familiar': return 'Familiar';
            case 'inquilino': return 'Inquilino';
            case 'owner': return 'Propietario';
            default: return type;
        }
    }

    function updateViewsTotal(params) {
        callWS('/admin-api/analytics/all-views', 'POST', params)
            .then(function(table) {
                $('#panel_views .panel-loading').addClass('hide');
                $('#table_views').html(table);
            }).catch(function(error) {
            });
    }

    function updateTopViews(params) {
        callWS('/admin-api/analytics/top-views', 'POST', params)
            .then(function(results) {
                $('#panel_views .panel-loading').addClass('hide');
                var configTopViews = {
                    type: 'doughnut',
                    data: {
                        datasets: [{
                            data: results.length === 0 ? [1] : results.map((res) => res.count),
                            backgroundColor: results.length === 0 ? ['grey'] : colors,
                            borderWidth: 0
                        }],
                        labels: results.length === 0 ? ['No hay Vistas'] : results.map((res) => res.name)
                    },
                    options: { responsive: true, legend: { display: false } }
                };
                if (chartViews) {
                    chartViews.data = configTopViews.data;
                    chartViews.update();
                } else {
                    var ctxEncuesta = document.getElementById('chart_views').getContext('2d');
                    chartViews = new Chart(ctxEncuesta, configTopViews);
                }
            }).catch(function(error) {

            });
    }

    function updateUsers(params) {
        callWS('/admin-api/analytics/active-users', 'POST', params)
            .then(function(results) {
                $('#panel_usuarios_active .panel-loading').addClass('hide');
                var usersTotals = 0,
                    configUsers = {
                        type: 'doughnut',
                        data: {
                            datasets: [{
                                data: results.length === 0 ? [1] : results.map((res) => res.count),
                                backgroundColor: results.length === 0 ? ['grey'] : colors,
                                borderWidth: 0
                            }],
                            labels: results.length === 0 ? ['No hay Vistas'] : results.map((res) => res.os)
                        },
                        options: { responsive: true }
                    };
                if (chartUsers) {
                    chartUsers.data = configUsers.data;
                    chartUsers.update();
                } else {
                    var ctxUsers = document.getElementById('chart_users_active').getContext('2d');
                    chartUsers = new Chart(ctxUsers, configUsers);
                }
                usersTotals = results.reduce((total, res) => total + res.count, 0); 
                $('#count_users_active').numerator({ toValue: parseFloat(usersTotals), rounding: 0 });
            }).catch(function(error) {
                console.log(error);
            });
    }

    $(document).ready(function() {
        updateUsers({});
        updateTopViews({});
        updateViewsTotal({});

        $('#daterange_views').daterangepicker({
            locale: { applyLabel: 'Aceptar', cancelLabel: 'Cancelar' }
        }, function(start, end) {
            $('#panel_views .panel-loading').removeClass('hide');
            const dateRange = { dateRange: {
                start: start.format('YYYY-MM-DD'),
                end: end.format('YYYY-MM-DD')
            }};
            updateTopViews(dateRange);
            updateViewsTotal(dateRange);
        });

        $('#daterange_users').daterangepicker({
            locale: { applyLabel: 'Aceptar', cancelLabel: 'Cancelar' }
        }, function(start, end) {
            $('#panel_usuarios_active .panel-loading').removeClass('hide');
            const dateRange = { dateRange: {
                start: start.format('YYYY-MM-DD'),
                end: end.format('YYYY-MM-DD')
            }};
            updateUsers(dateRange);
        });
    });
})();