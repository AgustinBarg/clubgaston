<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Costa Esmeralda</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="font-family: Arial, Verdana !important;color:#4d4d4d">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody><tr>
			<td style="padding: 0 0 30px 0;">
				<!-- Table1 -->
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody><tr>
						<td align="center" style="padding-top: 10px;">
							<img width="300" src="https://costa-esmeralda.covecinos.com/images/logo-costa.png?" alt="Costa Esmeralda" style="display: block;">
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 0px 0px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td style="text-align: center;font-size: 20px; color: #4d4d4d;">
											¡Bienvenido a la App de Costa Esmeralda!
										</td>
									</tr>
									<tr>
										<td>
											<table border="0" cellpadding="3" cellspacing="0" width="100%" style="text-align:center; margin-top: 20px;">
												<tr>
													<td style="font-size: 16px; text-align:center; padding: 5px;">
														Tu código para ingresar es:<br/>
														<span style="font-weight: bold;font-size:22px,color:#37b34a">{{$user_found->codigo}}</span>
													</td>
												</tr>
											</table>										
										</td>
									</tr>
									<tr>
										<td style="text-align: center;font-size: 12px; color: #4d4d4d;padding: 40px 0px 10px 0px;">
											Cualquier duda podés comunicarte a<br/>
											<a href="mailto:soporte@costa-esmeralda.com.ar">soporte@costa-esmeralda.com.ar</a>
										</td>
									</tr>
								</tbody></table>
								<br><br><br>
							</td>
						</tr>
						<tr>
							<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; text-align:center;">
								<img width="300" src="http://costa-esmeralda.com.ar/wp-content/uploads/2019/01/mail-footer.png"/>
							</td>
						</tr>
					</tbody></table>
					<!-- Table1 -->
				</td>
			</tr>
		</tbody></table>

	</body></html>