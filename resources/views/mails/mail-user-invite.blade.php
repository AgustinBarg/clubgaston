<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Costa Esmeralda</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="font-family: Arial, Verdana !important;color:#4d4d4d">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody><tr>
			<td style="padding: 0 0 30px 0;">
				<!-- Table1 -->
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody><tr>
						<td align="center" style="padding-top: 10px;">
							<img width="300" src="https://costa-esmeralda.covecinos.com/images/logo-costa.png?" alt="Costa Esmeralda" style="display: block;">
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 0px 0px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td style="text-align: center;font-size: 20px; color: #4d4d4d;">
											Hola {{$new_user->name}}
										</td>
									</tr>
									<tr>
										<td>
											<table border="0" cellpadding="3" cellspacing="0" width="100%" style="text-align:left; margin-top: 20px;">
												<tr>
													<td>
														El propietario <b>{{$owner->name}}</b> de Costa Esmeralda te autorizó para a usar la App de la comunidad. Ahora vas a poder realizar reservas, iniciar gestiones y mucho más.
													</td>
												</tr>
												<tr>
													<td>
														<br>
														<strong>- DNI para ingreso:</strong> {{$new_user->dni}} <br>
														<strong>- Fecha Validez de la Invitación:</strong> <span style="font-size: 11px; color: grey;">(Año-Mes-Día)</span> . Desde: <strong>{{$new_user->invited_valid_from}}</strong>. @if ($new_user->type == 'inquilino') Hasta: <strong>{{$new_user->invited_valid_to}}</strong> @endif<br> <br>
														*si tu DNI no es correcto, no podrás ingresar a la App. Contactate con el propietario para que elimine tu usuario y lo vuelva a crear con los datos correspondientes. <br><br>
													</td>
												</tr>
												<tr>
													<td>
														- Bajate la app para Android <a href="https://play.google.com/store/apps/details?id=com.mcdg.costaesmeralda">aqui</a> / iPhone <a href="https://itunes.apple.com/ar/app/costa-esmeralda/id1165364737?mt=8">aqui</a>
													</td>
												</tr>
												<tr>
													<td>
														- Seleccioná la opción ingresar como inquilino/propietario
													</td>
												</tr>
												<tr>
													<td>
														- Ingresá tu DNI
													</td>
												</tr>
												<tr>
													<td>
														- Recibirás un código de acceso en tu email: <b>{{$new_user->email}}</b>, asegurate de chequear el correo no deseado.
													</td>
												</tr>
												<tr>
													<td>
														- Ingresa el código en la app y listo!
													</td>
												</tr>
											</table>										
										</td>
									</tr>
									<tr>
										<td style="text-align: center;font-size: 12px; color: #4d4d4d;padding: 40px 0px 10px 0px;">
											Cualquier duda podés comunicarte a<br/>
											<a href="mailto:soporte@costa-esmeralda.com.ar">soporte@costa-esmeralda.com.ar</a>
										</td>
									</tr>
								</tbody></table>
								<br><br><br>
							</td>
						</tr>
						<tr>
							<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; text-align:center;">
								<img width="300" src="http://costa-esmeralda.com.ar/wp-content/uploads/2019/01/mail-footer.png"/>
							</td>
						</tr>
					</tbody></table>
					<!-- Table1 -->
				</td>
			</tr>
		</tbody></table>

	</body></html>