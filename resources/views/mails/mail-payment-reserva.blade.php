<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Costa Esmeralda</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="font-family: Arial, Verdana !important;color:#4d4d4d">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody><tr>
			<td style="padding: 0 0 30px 0;">
				<!-- Table1 -->
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody><tr>
						<td align="center" style="padding-top: 10px;">
							<img width="300" src="https://costa-esmeralda.covecinos.com/images/logo-costa.png?" alt="Costa Esmeralda" style="display: block;">
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 0px 0px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td>
											<table border="0" cellpadding="3" cellspacing="0" width="100%" style="text-align:left; margin-top: 20px;">
												<tr>
													<td>
														Hola <b>{{$user->name}}</b>
													</td>
												</tr>
												<tr>
													<td style="height: 15px"></td>
												</tr>
												<tr>
													<td>
                                                        Si ya realizaste tu pago mediante MercadoPago, podes ignorar este mensaje.
													</td>
												</tr>
												<tr>
													<td>
                                                        Si por algún motivo no pudiste completar el pago en la app de Costa, podés usar <a href="{{$paymentLink}}">este link</a> para pagar.
													</td>
												</tr>
												<tr>
													<td>
                                                        Recordá que tenés 15 minutos para realizar el pago, sino lo hacés en ese tiempo, la reserva se cancela y deberás volver a cargarla.
													</td>
												</tr>
												<tr>
													<td style="height: 15px"></td>
												</tr>
												<tr>
													<td>
                                                        <b>¡Muchas Gracias!</b>
                                                    </td>
												</tr>
											</table>										
										</td>
									</tr>
								</tbody></table>
								<br><br><br>
							</td>
						</tr>
						<tr>
							<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; text-align:center;">
								<img width="300" src="http://costa-esmeralda.com.ar/wp-content/uploads/2019/01/mail-footer.png"/>
							</td>
						</tr>
					</tbody></table>
					<!-- Table1 -->
				</td>
			</tr>
		</tbody></table>

	</body></html>