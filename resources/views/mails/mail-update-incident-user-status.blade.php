<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Costa Esmeralda</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body style="font-family: Arial, Verdana !important;color:#4d4d4d">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody><tr>
			<td style="padding: 0 0 30px 0;">
				<!-- Table1 -->
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody><tr>
						<td align="center" style="padding-top: 10px;">
							<img width="300" src="https://costa-esmeralda.covecinos.com/images/logo-costa.png?" alt="Costa Esmeralda" style="display: block;">
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 0px 0px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td>
											<table border="0" cellpadding="3" cellspacing="0" width="100%" style="text-align:left; margin-top: 20px;">
												<tr>
													<td>
														La gestión iniciada en Costa Esmeralda cambio su estado a: <b>{{$incident->state->name}}</b>
													</td>
												</tr>
												<tr>
													<td style="height: 15px"></td>
												</tr>
												<tr>
													<td>
														- <b>ID de gestión:</b> {{$incident->id}}
													</td>
												</tr>
												<tr>
													<td>
														- <b>Categoría:</b> {{$incident->tema->responsableArea->name}} -> {{$incident->tema->name}} 
													</td>
												</tr>
												<tr>
													<td>
														- <b>Asunto:</b> {{$incident->title}}
													</td>
												</tr>
												<tr>
													<td>
														- <b>Mensaje:</b> {{$incident->detalle}}
													</td>
												</tr>
												<tr>
													<td style="height: 15px"></td>
												</tr>
												<tr>
													<td>
														@if ($incident->auth_code)
															Si querés responder a esta gestión para agregar información o cerrar el ticket ingresá a <a href="{{$incident->getLink()}}">este link</a>.
														@endif
														<br>
														Si sos propietario o usuario registrado, también podés utilizar la app móvil de Costa Esmeralda para ver la gestión actualizada o responder.
													</td>
												</tr>
												<tr>
													<td><br><br></td>
												</tr>
												<tr>
													<td>
													<b>Importante:</b> En caso de presentarse una urgencia o bien el reclamo/incidente requiera de una atención inmediata, se puede comunicar con la guardia del predio, al 011 3221-7101 opción 1 (Int 1911 para emergencias). El horario de atención de la Administración es de Lunes a Sábados de 08:00 a 16:00 horas. <b>(Todos los días hasta las 20:00 horas a partir del 02 de Enero).</b>
													</td>
												</tr>
											</table>										
										</td>
									</tr>
								</tbody></table>
								<br><br><br>
							</td>
						</tr>
						<tr>
							<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px; text-align:center;">
								<img width="300" src="http://costa-esmeralda.com.ar/wp-content/uploads/2019/01/mail-footer.png"/>
							</td>
						</tr>
					</tbody></table>
					<!-- Table1 -->
				</td>
			</tr>
		</tbody></table>

	</body></html>