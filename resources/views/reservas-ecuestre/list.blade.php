@extends('layouts.app')

@section('content')

    <div class="container">
      <a class="btn btn-primary pull-right exportar" href="{{route('reservas.export')}}">Exportar Excel Reservas Ecuestres {{date('d-m-Y')}}</a>

        <div class="panel panel-default">
            Listado de Reservas Ecuestres
        </div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="panel-body">
            <form method="GET" class="form-inline" action="{{ route('reservas-ecuestre.list', [
                    'order_by' => $orderBy, 
                    'order_direction' => $orderDirection,
                    'espacio_id' => $espacioId,
                    'data' => $date
                ]) }}">
                <div class="form-group">
                    <input type="text" class="form-control" name="query" id="query" placeholder="Buscar por usuario...">
                    <select name="espacio_id" id="espacio_id" class="form-control">
                        <option value="" selected="selected">No filtrar por espacio</option>
                        @foreach($espacios as $espacio)
                            <option value="{{ $espacio->id }}">{{ $espacio->nombre }}</option>
                        @endforeach
                    </select>
                    <input type="date" class="form-control" name="date" id="date">
                </div>
                <button type="submit" class="btn btn-default">Buscar</button>
            </form>
        </div>

        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        @if ($reservas)
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>id
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'id', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'id', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Usuario
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'user_id', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'user_id', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Espacio
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'espacio_id', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'espacio_id', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Fecha Ingreso
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'fechaIngreso', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'fechaIngreso', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Periodo
                    </th>
                    <th>
                        Estado
                    </th>
                    <th>
                        Notas
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'notas', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-ecuestre.list', ['order_by' => 'notas', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>eliminar</th>
                </tr>
                </thead>

                @foreach($reservas as $reserva)
                    <tbody>
                    <td>{{ $reserva->id }}</td>
                    <td>
                        {{ $reserva->user->name }}
                        <div>({{ $reserva->user->email }})</div>
                    </td>
                    <td>
                        {{ $reserva->espacio->nombre }}
                        @if ($reserva->places > 0)
                            <div><strong>{{$reserva->places}} caballo(s)</strong></div>
                        @endif
                    </td>
                    <td>{{ $reserva->fechaIngreso->format('d-m-Y') }}</td>
                    <td>
                        {{ $reserva->fechaInicio->format('d-m-Y') }} / {{ $reserva->fechaFin->format('d-m-Y') }}
                    </td>
                    <td>{{ $reserva->getStatus() }}</td>
                    <td>{{ $reserva->notas }}</td>
                    <td>
                        <form method="POST" class="delete" action="{{ route('reservas.delete') }}">
                            <input type="hidden" name="id" class="delete" value="{{ $reserva->id }}">
                            {!! csrf_field() !!}
                            <button type="submit" class="delete" value="delete">eliminar
                            </button>
                        </form>
                    </td>
                    </tbody>

                @endforeach
            </table>
        @else
            <h4>No se encontraron reservas</h4>
        @endif

        @if($orderBy && $orderDirection)
            {{ $reservas->appends(['order_by' => $orderBy, 'order_direction' => $orderDirection, 'espacio_id' => $espacioId, 'date' => $date ])->links() }}
        @else
            {{ $reservas->appends([ 'espacio_id' => $espacioId, 'date' => $date ])->links() }}
        @endif
    </div>
@stop
