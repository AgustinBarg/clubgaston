<hr>
<div class="row">
    <div class="col-md-6">
        <label class="m-0">Desde:</label>
        <div>{{$period['from']}}</div>
    </div>
    <div class="col-md-6">
        <label class="m-0">Hasta:</label>
        <div>{{$period['to']}}</div>
    </div>
</div>

@foreach ($places as $place)
    @if ($place->id === $espacio->id)
        <div class="row margin-top">
            <div class="col-md-6">
                <label class="m-0">Precio Dueño:</label>
                <div>${{number_format($place->price_owner * $multiplier, 2)}}</div>
            </div>
            <div class="col-md-6">
                <label class="m-0">Precio Inquilino:</label>
                <div>${{number_format($place->price_inquilino * $multiplier, 2)}}</div>
            </div>
        </div>
        <div class="margin-top">
            @if ($place->vacantes > 0)
                <div class="alert alert-success m-0">
                    <strong>{{$place->nombre}}</strong>
                    <div class="text-right">{{$place->vacantes}}/{{$place->vacants}} disponibles</div>
                </div>
            @else
                <div class="alert alert-danger text-center m-0">
                    No hay vacantes disponibles para este espacio en el periodo seleccionado.
                </div>
            @endif
        </div>
    @endif
@endforeach