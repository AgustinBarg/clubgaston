@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Crear Nueva Reserva Ecuestre</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('reservas-ecuestre.new') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <label for="user_id" class="col-md-4 control-label">Usuario</label>
                            <div class="col-md-6">
                                <autocomplete></autocomplete>
                                @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('period') ? 'has-error' : '' }}">
                            <label for="period" class="col-md-4 control-label">Periodo de reserva</label>

                            <div class="col-md-6">
                                <select class="form-control" id="period" name="period">
                                    <option value="semana">Semana</option>
                                    <option value="quincena">Quincena</option>
                                    <option value="mes">Mes</option>
                                </select>

                                @if ($errors->has('period'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('period') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                            <label for="date" class="col-md-4 control-label">Fecha de Ingreso</label>

                            <div class="col-md-6">
                                <input id="date" type="date" class="form-control" name="date">

                                @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                                @endif
                                <span class="help-block">
                                    <small style="display: block; line-height: 12px">Fecha Inicio y Fecha Fin se calcularán en base al Periodo y Fecha de Ingreso</small>
                                </span>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('espacio_id') ? 'has-error' : '' }}">
                            <label for="espacio_id" class="col-md-4 control-label">Espacio</label>

                            <div class="col-md-6">
                                <select class="form-control" id="espacio_id" name="espacio_id">
                                    @foreach ($espacios as $espacio)
                                        <option value="{{ $espacio->id }}">{{ $espacio->nombre }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('espacio_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('espacio_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div id="div_places" style="display: none;" class="form-group {{ $errors->has('places') ? 'has-error' : '' }}">
                            <label for="places" class="col-md-4 control-label">Cantidad de Caballos</label>

                            <div class="col-md-6">
                                <input type="number" name="places" id="places">
                                @if ($errors->has('places'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('places') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Monto a Pagar</label>

                            <div class="col-md-6">
                                <input id="amount" type="number" step="0.1" class="form-control" name="amount">

                                @if ($errors->has('amount'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('payment_method') ? 'has-error' : '' }}">
                            <label for="payment_method" class="col-md-4 control-label">Forma de Pago</label>

                            <div class="col-md-6">
                                <select class="form-control" id="payment_method" name="payment_method">
                                    <option value="efectivo">Efectivo</option>
                                    <option value="mercadopago">Mercadopago</option>
                                </select>

                                @if ($errors->has('payment_method'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('payment_method') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('notas') ? 'has-error' : '' }}">
                            <label for="notas" class="col-md-4 control-label">Notas</label>

                            <div class="col-md-6">
                                <textarea id="notas" class="form-control" name="notas"></textarea>

                                @if ($errors->has('notas'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('notas') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div id="checkPlaces_container"></div>
                            </div>
                        </div>

                        @if (isset($error))
                            <div class="alert alert-danger">
                                <?php echo print_r($error); ?>
                            </div>
                        @endif

                        @if ($errors->has('validation'))
                            <div class="alert alert-danger">
                                {{ $errors->first('validation') }}
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Crear Reserva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        function checkPlaces() {
            var data = {
                period: $('#period').val(),
                date: $('#date').val(),
                espacio_id: $('#espacio_id').val()
            };
            if (!data.period || !data.date) { return; }

            $.ajax({
                url: '/reservas-ecuestre/check-places',
                method: 'POST',
                data: data,
                beforeSend: function (request) {
                    $('#checkPlaces_container').html('<div class="text-center">...Cargando Disponibilidad...</div>');
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                }
            }).done(function(resp) {
                $('#checkPlaces_container').html(resp);
            })
            .fail(function(err) {
                console.log(err);
                $('#checkPlaces_container').html('<div class="alert alert-danger">' + + '</div>')
            });
        }
        $(document).ready(function() {
            $('#period').change(checkPlaces);
            $('#date').change(checkPlaces);
            $('#espacio_id').change(function() {
                if ($(this).val() >= 4 && $(this).val() <= 7) {
                    $('#div_places').show();
                } else {
                    $('#div_places').hide();
                    $('#div_places input').val('');
                }
                checkPlaces();
            });
        })
    </script>
@endsection