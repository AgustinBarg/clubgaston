@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Modificar Usuario</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('postedit') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="col-md-4 control-label">User ID</label>

                                <div class="col-md-6">
                                    <input readonly="true" id="id" type="number" min="0" class="form-control" name="id" value="{{$users[0]['id']}}" required autofocus>

                                    @if ($errors->has('id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">name</label>

                                <div class="col-md-6">
                                    <input id="name" type="" class="form-control" name="name" value="{{$users[0]['name']}}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">e-mail</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{$users[0]['email']}}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                <label for="pagoinformado" class="col-md-4 control-label">ROL</label>

                                <div class="col-md-6">
                                    <input id="pagoinformado" type="number" min="0" max="4" class="form-control" name="role" value="{{$users[0]['role']}}">

                                    @if ($errors->has('pagoinformado'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pagoinformado') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('sede_id') ? ' has-error' : '' }}">
                                <label for="sede_id" class="col-md-4 control-label">Sede Id</label>

                                <div class="col-md-6">
                                    <input id="sede_id" type="number" min="0" max="4" class="form-control" name="sede_id" value="{{$users[0]['sede_id']}}">

                                    @if ($errors->has('sede_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sede_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Editar Usuario
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
