@extends('layouts.app')

@push('scripts')
    <link href="{{ asset('css/reports.css') }}?date=20201202" rel="stylesheet">
    <style>
        .panel > .panel-body {
            min-height: auto !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        @if (isset($user))
            <div class="panel panel-default">
                <div class="panel-heading">{{$user->name}} ({{$user->type === 'owner' ? 'Propietario' : $user->type}})</div>
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">PERFIL</div>
                        <div class="panel-body">
                            <div>
                                <label class="text-bold">ID: </label> {{$user->id}}
                            </div>
                            <div>
                                <label class="text-bold">Email: </label> {{$user->email}}

                                <button id="btn_edit_email" class="btn btn-outline text-costa ml-10">
                                    <span class="glyphicon glyphicon-edit"></span> Editar
                                </button>
                            </div>
                            <div class="padding-vertical">
                                <label class="text-bold">Clave de Ingreso: </label>
                                <div class="claveContainer">
                                    <input class="form-control" id="password_field" readonly="readonly" type="password" value="{{strtoupper($user->codigo)}}">
                                    <span id="password_field_icon" class="glyphicon glyphicon-eye-open"></span>
                                </div>
                            </div>
                            <div><label class="text-bold">Tel: </label> {{$user->telefono_movil ?? '-'}}</div>
                            <div><label class="text-bold">CCDocumento (FOX): </label> {{$user->ccdocumento}}</div>
                            <div><label class="text-bold">DNI: </label> {{$user->dni}}</div>
                            <div><label class="text-bold">ID Cliente (FOX): </label> {{$user->id_cliente}}</div>
                            <div class="{{$user->moroso ? 'text-danger' : ''}}"><label class="text-bold">Moroso (FOX): </label> {{$user->moroso ? 'SI' : 'NO'}}</div>
                            <div class="{{$user->borrado ? 'text-danger' : ''}}"><label class="text-bold">Desactivado: </label> {{$user->borrado ? 'SI' : 'NO'}}</div>
                            @if ($user->type === 'owner' && count($user->lotes) > 0)
                                <hr>
                                @foreach($user->lotes as $lote)
                                    <div><label class="text-bold">Lote: </label> {{$lote->id_lote ?? '-'}} / {{$lote->name ?? '-'}} / {{$lote->emprendimiento_id ?? '-'}}</div>
                                @endforeach
                                <hr>
                            @endif
                            @if ($user->type !== 'owner')
                                <hr>
                                <div><label class="text-bold">Lote: </label> {{$user->lote ?? '-'}} / {{$user->lote_ubicacion ?? '-'}} / {{$user->emprendimiento_id ?? '-'}}</div>
                                <hr>
                                <div><label class="text-bold">Invitado Por: </label> {{$user->invited_by ? $user->owner->name . ' (ID: ' . $user->invited_by . ')' : '-'}} </div>
                                <div><label class="text-bold">Validez Invitación: </label> {{$user->invited_valid_from ?? '-'}} <span class="text-bold">-></span> {{$user->invited_valid_to ?? '-'}}</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">INFO OPEN KEY</div>
                        <div class="panel-body">
                            @if ($openKey['ReturnVal'] == 0)
                                <div><label class="text-bold">Nombre Completo: </label> {{$openKey['Apellido']}}, {{$openKey['Nombre']}}</div>    
                                <div><label class="text-bold">ID Persona: </label> {{$openKey['IdPersona']}}</div>
                                <div><label class="text-bold">Grupo Primario: </label> {{$openKey['GrupoPrimario']}}</div>
                                <div class="{{$openKey['Moroso'] ? 'text-success' : 'text-danger'}}"><label class="text-bold">Moroso? </label> {{$openKey['Moroso'] ? 'SI' : 'NO'}}</div>
                                <div class="{{$openKey['PuedeAutorizar'] ? 'text-success' : 'text-danger'}}"><label class="text-bold">Puede Autorizar? </label> {{$openKey['PuedeAutorizar'] ? 'SI' : 'NO'}}</div>
                                <div><label class="text-bold">Documento: </label> {{$openKey['TipoDocumentoDescripcion']}} - {{$openKey['DocumentoNro']}}</div>
                                <div><label class="text-bold">Tarea: </label> {{$openKey['TareaDesc']}}</div>
                                <div><label class="text-bold">Req. Seguro? </label> {{$openKey['ReqSeguro'] ? 'SI' : 'NO'}}</div>
                                <div><label class="text-bold">Req. Antec. Penales? </label> {{$openKey['ReqAntecPenales'] ? 'SI' : 'NO'}}</div>
                            @else
                                <div class="text-center">No se pudo obtener info con el DNI del usuario.</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">USUARIOS APP</div>
                        <div class="panel-body">
                            @if (count($relatedUsers) > 0)
                                @foreach($relatedUsers as $relatedUser)
                                    @if ($loop->index > 0)
                                        <hr>
                                    @endif
                                    <div><label class="text-bold">Nombre: </label> {{$relatedUser->name}} <span class="text-uppercase">({{$relatedUser->type}})</span></div>
                                    <div><label class="text-bold">Email: </label> {{$relatedUser->email}}</div>
                                    <div><label class="text-bold">DNI: </label> {{$relatedUser->dni}}</div>
                                    <div><label class="text-bold">Validez Invitación: </label> {{$relatedUser->invited_valid_from ?? '-'}} <span class="text-bold">-></span> {{$relatedUser->invited_valid_to ?? '-'}}</div>
                                @endforeach
                            @else
                                <div class="text-center">No hay usuarios invitados por este Usuario.</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">AUTORIZACIONES OPEN KEY</div>
                        <div class="panel-body">
                            @if (count($openKeyUsers) > 0)
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr><th>Nombre</th><th>Validez</th></tr>
                                        </thead>
                                        <tbody>
                                            @foreach($openKeyUsers as $openKeyUser)
                                                <tr>
                                                    <td>{{$openKeyUser['Autorizado']}}</td>
                                                    <td>{{substr($openKeyUser['Desde'], 0, 10)}} <span class="text-bold">-></span> {{substr($openKeyUser['Hasta'], 0, 10)}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="text-center">No hay Autorizaciones para este DNI.</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
        
    </div>

    <div class="modal fade" id="modal_edit_email" tabindex="-1" role="dialog" 
        aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-notify modal-danger" role="document">
            <form action="{{ route('user.profile-edit-email', ['userId' => $user->id]) }}" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="heading lead m-0">Editar Email</p>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" name="email" class="form-control" value="{{$user->email}}" required>
                        </div>
                        <div class="error-container"></div>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary-modal btn-success">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#btn_edit_email').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('#modal_edit_email').modal('show');
                return false;
            });

            $('#password_field_icon').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                if ($(this).hasClass('glyphicon-eye-open')) {
                    $(this).removeClass('glyphicon-eye-open');
                    $(this).addClass('glyphicon-eye-close');
                    $('#password_field').attr('type', 'text');
                } else {
                    $(this).removeClass('glyphicon-eye-close');
                    $(this).addClass('glyphicon-eye-open');
                    $('#password_field').attr('type', 'password');
                }
                return false;
            });
        });
    </script>
@endsection

