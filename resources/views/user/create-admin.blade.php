@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear Usuario Admin</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                              {{ session('status') }}
                            </div>
                        @endif

                        @if($errors)
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger bg-danger">{{ $error }}</div>
                            @endforeach
                        @endif

                        {{ Form::open(['route' => 'user.save-admin']) }}
                            <div>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">Nombre Completo</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" required name="name" value="{{old('name')}}"></input>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">Email</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" required name="email" value="{{old('email')}}"></input>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">Contraseña</label>
                                    <div class="col-md-6">
                                        <input id="password" type="text" class="form-control" required minlength="6" name="password" value="{{old('password')}}"></input>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">DNI</label>
                                    <div class="col-md-6">
                                        <input id="document" type="number" class="form-control" required name="document" value="{{old('document')}}"></input>
                                        @if ($errors->has('document'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('document') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group{{ $errors->has('responsable') ? ' has-error' : '' }}">
                                <label for="role_id" class="col-md-4 control-label">Rol</label>
                                <div class="col-md-6">
                                    <select name="role_id" id="role_id" class="form-control" required>
                                        <option value="">Selecciona...</option>
                                        <option value="2">Deportivo / Reservas</option>
                                        <option value="3">Incidentes / Tickets</option>
                                    </select>

                                    @if ($errors->has('responsable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('responsable') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group">
                                <label for="responsible_area" class="control-label">Area Responsabilidad</label>
                                <div><small class="text-muted">Recibirá mail sobre los incidentes/tickets creados en las areas seleccionadas</small></div>
                                <div>
                                    @foreach($areas as $area)
                                        <div>
                                            <input type="checkbox" name="responsible_area[]" value="{{$area->id}}">
                                            <label>{{$area->name}}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Crear Usuario
                                    </button>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#image').change(function() {
                if ($(this).val() !== '') {
                    $('#btn_delete_file').show();
                }
            });
            $('#btn_delete_file').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('#image').val('');
                $(this).hide();
                return false;
            });
            $("[name='user_type']").change(function() {
                if ($(this).val() == 'external_user') {
                    $('#existing_user_container').hide();
                    $('#external_user_container').show();
                    $('#external_user_container input').attr('required', 'required');
                } else {
                    $('#external_user_container').hide();
                    $('#external_user_container input').removeAttr('required');
                    $('#existing_user_container').show();
                }
            });
        });
    </script>
@endsection