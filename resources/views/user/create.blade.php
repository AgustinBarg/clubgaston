@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear Usuario</div>
                    <div class="panel-body">
                        {{ Form::open(array('url' => '/user/new')) }}
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                {{ Form::label('name', 'Nombre') }}
                                {{ Form::input('text', 'name', '', ['class' => 'form-control', 'placeholder' => 'nombre']) }}
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::input('text', 'email', '', ['class' => 'form-control', 'placeholder' => 'email']) }}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                {{ Form::label('password', 'Password') }}
                                {{ Form::input('text', 'password', '', ['class' => 'form-control', 'placeholder' => 'password']) }}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{ Form::label('sedes', 'Sede') }}
                            <select name="sede_id" id="sede_id" class="form-control {{ $errors->has('sede_id') ? 'has-error' : '' }}">
                                @foreach($sedes as $sede)
                                    <option value="{{ $sede->id }}">{{ $sede->name }}</option>
                                @endforeach
                                @if ($errors->has('sede_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sede_id') }}</strong>
                                    </span>
                                @endif
                            </select>

                            {{ Form::label('role', 'Seccion') }}
                            <select name="role_id" id="role_id" class="form-control {{ $errors->has('password') ? 'has-error' : '' }}">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                                @if ($errors->has('role_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role_id') }}</strong>
                                    </span>
                                @endif
                            </select>

                            {{ Form::submit('Crear usuario', array('class' => 'btn btn-primary', 'style' => 'margin-top:20px')) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection