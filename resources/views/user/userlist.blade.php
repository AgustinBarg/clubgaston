@extends('layouts.app')

@section('content')
<div class="container">

    <div class="panel panel-default">
        <div class="panel-heading">Listado de usuarios</div>
        <div class="panel-body">
            <form method="GET" class="form-inline" action="{{ route('user.list') }}">
             <div class="form-group">
                 <input type="text" class="form-control" name="searchText" id="searchText" placeholder="Buscar..." value="{{isset($searchText) ? $searchText : ''}}">
             </div>
             <button type="submit" class="btn btn-default">Buscar</button>
         </form>
     </div>
 </div>
 @if (Session::has('message'))
 <div class="alert alert-info">{{ Session::get('message') }}</div>
 @endif


 <table class="table table-bordered" id="users-table">
    <thead>
        <tr>
            <th>
                id
                <a href="{{ route('user.list', ['order_by' => 'id', 'order_direction' => 'asc']) }}">
                    <span class="glyphicon glyphicon-triangle-top"></span>
                </a>
                <a href="{{ route('user.list', ['order_by' => 'id', 'order_direction' => 'desc']) }}">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </a>
            </th>
            <th>
                nombre
                <a href="{{ route('user.list', ['order_by' => 'name', 'order_direction' => 'asc']) }}">
                    <span class="glyphicon glyphicon-triangle-top"></span>
                </a>
                <a href="{{ route('user.list', ['order_by' => 'name', 'order_direction' => 'desc']) }}">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </a>
            </th>
            <th>
                email
                <a href="{{ route('user.list', ['order_by' => 'email', 'order_direction' => 'asc']) }}">
                    <span class="glyphicon glyphicon-triangle-top"></span>
                </a>
                <a href="{{ route('user.list', ['order_by' => 'email', 'order_direction' => 'desc']) }}">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </a>
            </th>
            <th>
                rol
                <a href="{{ route('user.list', ['order_by' => 'role_id', 'order_direction' => 'asc']) }}">
                    <span class="glyphicon glyphicon-triangle-top"></span>
                </a>
                <a href="{{ route('user.list', ['order_by' => 'role_id', 'order_direction' => 'desc']) }}">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </a>
            </th>
            <th></th>
        </tr>
    </thead>
    @foreach($users as $user)
    <tbody>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->role->name }}</td>
        <td>
            @if (Auth::user()->role->id == 5)
                <a href="{{ route('user.profile', ['userId' => $user->id]) }}">Ver Perfil</a>
            @endif
        </td>
    </tbody>
    @endforeach
</table>
{{ $users->links() }}
</div>

@stop

