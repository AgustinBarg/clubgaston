@extends('layouts.app')

@section('content')
<div class="container">
        <a id="btn_add_interest" class="btn btn-primary pull-right exportar" href="#">Agregar Interés</a>
        <div class="panel panel-default">
            <div class="panel-heading">
                Listado Intereses
            </div>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if(count($interests) > 0)
                <table class="table table-bordered" id="incidentes-table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Titulo</th>
                            <th>Usuarios</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($interests as $interest)
                        <tr>
                            <td>{{ $interest->id }}</td>
                            <td>{{ $interest->title }}</td>
                            <td>{{ $interest->usuarios}}</td>
                            <td>
                                <a class="btn-edit-interest" 
                                    style="display: inline-block;"
                                    data-interest-id="{{$interest->id}}"
                                    data-interest-title="{{$interest->title}}" href="#">
                                    <span class="glyphicon glyphicon-pencil"></span> Editar
                                </a>
                                <button style="display: inline-block; margin: 0; margin-left: 15px;" data-id="{{$interest->id}}" class="delete" value="delete">Eliminar</button>
                                <form id="delete_form_{{$interest->id}}" style="display:none;" method="POST" action="{{ route('interests.delete', ['interestId' => $interest->id]) }}">
                                    {!! csrf_field() !!}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>No se encontraron Intereses</p>
            @endif
        </div>
    </div>

    <div class="modal fade" id="modal_edit_interest" tabindex="-1" role="dialog" 
        aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-notify modal-danger" role="document">
            <form action="{{route('interests.save')}}" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="heading lead m-0">Editar/Crear Interés</p>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" id="interest_id" name="interest_id">
                        <div class="form-group">
                            <label for="interest_title" class="control-label">Titulo</label>
                            <input type="text" id="interest_title" name="interest_title" class="form-control" required>
                        </div>
                        <div class="error-container"></div>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary-modal btn-success">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#btn_add_interest').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('#interest_id').val('');
                $('#interest_title').val('');
                $('#modal_edit_interest').modal('show');
                return false;
            });

            $('.btn-edit-interest').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('#interest_id').val($(this).attr('data-interest-id'));
                $('#interest_title').val($(this).attr('data-interest-title'));
                $('#modal_edit_interest').modal('show');
                return false;
            });

            $('.delete').click(function() {
                const interestId = $(this).attr('data-id');
                swal({
                    title: "Eliminar Interés",
                    text: "Estás seguro de eliminar ests Interés?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $('#delete_form_' + interestId).submit();
                    }
                });
            });
        });
    </script>
@endsection
