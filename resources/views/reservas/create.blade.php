@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Crear nueva reserva</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('reservas.new') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <label for="user_id" class="col-md-4 control-label">Usuario</label>
                            <div class="col-md-6">
                                <autocomplete></autocomplete>
                                @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('fecha_reserva') ? 'has-error' : '' }}">
                            <label for="fecha_reserva" class="col-md-4 control-label">Fecha de reserva</label>

                            <div class="col-md-6">
                                <input id="fecha_reserva" type="date" class="form-control" name="fecha_reserva">

                                @if ($errors->has('fecha_reserva'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fecha_reserva') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('horario_reserva') ? ' has-error' : '' }}">
                            <label for="horario_reserva" class="col-md-4 control-label">Horario</label>
                            <div class="col-md-6">
                                <select class="form-control" id="horario_reserva" name="horario_reserva">
                                    @foreach ($freeHours as $hour)
                                    <option value="{{ $hour }}">{{ $hour }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('horario_reserva'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('horario_reserva') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('espacio_id') ? 'has-error' : '' }}">
                            <label for="espacio_id" class="col-md-4 control-label">Espacio</label>

                            <div class="col-md-6">
                                <select class="form-control" id="espacio_id" name="espacio_id">
                                    @foreach ($espacios as $espacio)
                                        <option 
                                            data-priceowner="{{$espacio->price_owner}}"
                                            data-priceinquilino="{{$espacio->price_inquilino}}"
                                            value="{{ $espacio->id }}">{{ $espacio->nombre . " - " . $espacio->categoria->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('espacio_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('espacio_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="m-0">Precio Dueño:</label>
                                        <div><span id="price_owner"></span></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="m-0">Precio Inquilino:</label>
                                        <div><span id="price_inquilino"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Monto a Pagar</label>

                            <div class="col-md-6">
                                <input id="amount" type="number" step="0.1" class="form-control" name="amount">

                                @if ($errors->has('amount'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('payment_method') ? 'has-error' : '' }}">
                            <label for="payment_method" class="col-md-4 control-label">Forma de Pago</label>

                            <div class="col-md-6">
                                <select class="form-control" id="payment_method" name="payment_method">
                                    <option value="efectivo">Efectivo</option>
                                    <option value="mercadopago">Mercadopago</option>
                                </select>

                                @if ($errors->has('payment_method'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('payment_method') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('notas') ? 'has-error' : '' }}">
                            <label for="notas" class="col-md-4 control-label">Notas</label>

                            <div class="col-md-6">
                                <textarea id="notas" class="form-control" name="notas"></textarea>

                                @if ($errors->has('notas'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('notas') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Crear Reserva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        function showPrices() {
            var selectedValue = $('#espacio_id').val();
            var selectedOption = $('#espacio_id option[value="' + selectedValue + '"]');
            $('#price_owner').html('$' + selectedOption.attr('data-priceowner'));
            $('#price_inquilino').html('$' + selectedOption.attr('data-priceinquilino'));
        }
        $(document).ready(function() {
            $('#espacio_id').change(showPrices);
            showPrices();
        })
    </script>
@endsection