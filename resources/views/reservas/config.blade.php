@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Configuración Reservas</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('reservas.config-save') }}">
                        {{ csrf_field() }}

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
                        <div class="form-group {{ $errors->has('reservas_payment_allow') ? 'has-error' : '' }}">
                            <label for="reservas_payment_allow" class="control-label">Reservas con Pago</label>
                            <div>
                                <select class="form-control" id="reservas_payment_allow" name="reservas_payment_allow" required>
                                    <option value="1" @if($config->reservas_payment_allow) selected @endif>Habilitadas</option>
                                    <option value="0" @if(!$config->reservas_payment_allow) selected @endif>Deshabilitadas</option>
                                </select>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group {{ $errors->has('reservas_validation') ? 'has-error' : '' }}">
                            <label for="reservas_validation" class="control-label">Reservas con Validación</label>
                            <div>
                                <select class="form-control" id="reservas_validation" name="reservas_validation" required>
                                    <option value="1" @if($config->reservas_validation) selected @endif>Habilitadas</option>
                                    <option value="0" @if(!$config->reservas_validation) selected @endif>Deshabilitadas</option>
                                </select>
                            </div>
                            <div class="text-muted">Si están habilitadas, aplicarán las reglas de validacion via QR y penalización.</div>
                        </div>
                        <hr>

                        <div class="form-group {{ $errors->has('ecuestre_allow') ? 'has-error' : '' }}">
                            <label for="ecuestre_allow" class="control-label">Reservas Ecuestres</label>
                            <div>
                                <select class="form-control" id="ecuestre_allow" name="ecuestre_allow" required>
                                    <option value="1" @if($config->ecuestre_allow) selected @endif>Habilitadas</option>
                                    <option value="0" @if(!$config->ecuestre_allow) selected @endif>Deshabilitadas</option>
                                </select>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group {{ $errors->has('gym_allow') ? 'has-error' : '' }}">
                            <label for="gym_allow" class="control-label">Reservas Gym</label>
                            <div>
                                <select class="form-control" id="gym_allow" name="gym_allow" required>
                                    <option value="1" @if($config->gym_allow) selected @endif>Habilitadas</option>
                                    <option value="0" @if(!$config->gym_allow) selected @endif>Deshabilitadas</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('gym_allow_credits') ? 'has-error' : '' }}">
                            <label for="gym_allow_credits" class="control-label">Sistema Creditos Gym Inquilinos</label>
                            <div>
                                <select class="form-control" id="gym_allow_credits" name="gym_allow_credits" required>
                                    <option value="1" @if($config->gym_allow_credits) selected @endif>Habilitado</option>
                                    <option value="0" @if(!$config->gym_allow_credits) selected @endif>Deshabilitado</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gym_max_hour" class="control-label">Reservas Máximas por hora - Gym</label>
                            <div>
                                <input value="{{$config->gym_max_hour}}" id="gym_max_hour" type="number" step="1" class="form-control" name="gym_max_hour" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gym_max_hour" class="control-label">Horario Propietarios - Gym</label>
                            <div>
                                Desde
                                <select class="form-control" id="gym_owner_inicio" name="gym_owner_inicio" required>
                                    @foreach($times as $time)
                                        <option value="{{$time->time}}"
                                                @if($time->time === $config->gym_owner_inicio) selected @endif>{{substr($time->time, 0, 5)}}</option>
                                    @endforeach
                                </select>
                                Hasta
                                <select class="form-control" id="gym_owner_fin" name="gym_owner_fin" required>
                                    @foreach($times as $time)
                                        <option value="{{$time->time}}"
                                                @if($time->time === $config->gym_owner_fin) selected @endif>{{substr($time->time, 0, 5)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="gym_max_hour" class="control-label">Horario Inquilinos - Gym</label>
                            <div>
                                Desde
                                <select class="form-control" id="gym_inquilino_inicio" name="gym_inquilino_inicio" required>
                                    @foreach($times as $time)
                                        <option value="{{$time->time}}"
                                                @if($time->time === $config->gym_inquilino_inicio) selected @endif>{{substr($time->time, 0, 5)}}</option>
                                    @endforeach
                                </select>
                                Hasta
                                <select class="form-control" id="gym_inquilino_fin" name="gym_inquilino_fin" required>
                                    @foreach($times as $time)
                                        <option value="{{$time->time}}"
                                                @if($time->time === $config->gym_inquilino_fin) selected @endif>{{substr($time->time, 0, 5)}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Guardar Configuración
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection