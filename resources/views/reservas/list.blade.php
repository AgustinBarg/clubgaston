@extends('layouts.app')

@section('content')

    <div class="container">
      <a class="btn btn-primary pull-right exportar" href="{{route('reservas.export')}}">Exportar Excel Reservas {{date('d-m-Y')}}</a>

        <div class="panel panel-default">
            Listado de reservas
        </div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="panel-body">
            <form method="GET" class="form-inline" action="{{ route('reservas.list', [
                    'order_by' => $orderBy, 
                    'order_direction' => $orderDirection,
                    'espacio_id' => $espacioId,
                    'data' => $date
                ]) }}">
                <div class="form-group">
                    <input type="text" class="form-control" name="query" id="query" placeholder="Buscar por usuario...">
                    <select name="espacio_id" id="espacio_id" class="form-control">
                        <option value="" @if(!$espacioId) selected="selected" @endif>No filtrar por espacio</option>
                        @foreach($espacios as $espacio)
                            <option @if($espacioId == $espacio->id) selected="selected" @endif 
                                    value="{{ $espacio->id }}">{{ $espacio->nombre }}</option>
                        @endforeach
                    </select>
                    <input type="date" class="form-control" name="date" value="{{$date}}" id="date">
                    <select name="validated" id="validated" class="form-control">
                        <option value="" @if($validated == null) selected="selected" @endif>Todas</option>
                        <option @if($validated === "1") selected="selected" @endif 
                                    value="1">Reservas Validadas</option>
                        <option @if($validated === "0") selected="selected" @endif 
                                    value="0">Reservas NO Validadas</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Buscar</button>
            </form>
        </div>

        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        @if ($reservas)
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>id
                        <a href="{{ route('reservas.list', ['order_by' => 'id', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas.list', ['order_by' => 'id', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Usuario
                        <a href="{{ route('reservas.list', ['order_by' => 'user_id', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas.list', ['order_by' => 'user_id', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Espacio
                        <a href="{{ route('reservas.list', ['order_by' => 'espacio_id', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas.list', ['order_by' => 'espacio_id', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Hora Inicio
                        <a href="{{ route('reservas.list', ['order_by' => 'horaInicio', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas.list', ['order_by' => 'horaInicio', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th class="text-center">
                        Estado
                    </th>
                    <th class="text-center">
                        Validación
                    </th>
                    <th>
                        Notas
                        <a href="{{ route('reservas.list', ['order_by' => 'notas', 'order_direction' => 'asc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas.list', ['order_by' => 'notas', 'order_direction' => 'desc', 'espacio_id' => $espacioId, 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th></th>
                </tr>
                </thead>

                @foreach($reservas as $reserva)
                    <tbody>
                    <td>{{ $reserva->id }}</td>
                    <td>{{ $reserva->user->name }}</td>
                    <td>{{ $reserva->espacio->nombre }}</td>
                    <td>{{ $reserva->horaInicio }}</td>
                    <td class="text-center">
                        <span title="{{ $reserva->getStatus() }}" 
                                alt="{{ $reserva->getStatus() }}" 
                                class="glyphicon glyphicon-{{$reserva->getStatusIcon()}} {{$reserva->getStatusClass()}}"></span>  
                    </td>
                    <td class="text-center">
                        @if ($reserva->validated)
                            <span title="Validada" alt="Validada" class="text-success glyphicon glyphicon-check"></span>
                            @if ($reserva->validated_by)
                                <div>{{ $reserva->validator->name}}</div>
                            @endif
                        @else
                            <span title="No Validada" alt="No Validada" class="text-danger glyphicon glyphicon-remove"></span>
                            <form method="POST" class="success" action="{{ route('reservas.validate') }}">
                                <input type="hidden" name="id" class="success" value="{{ $reserva->id }}">
                                {!! csrf_field() !!}
                                <button type="submit" class="success btn-validate m-0" value="validate">validar
                                </button>
                            </form>
                        @endif
                    </td>
                    <td>{{ $reserva->notas }}</td>
                    <td>
                        <form method="POST" class="delete" action="{{ route('reservas.delete') }}">
                            <input type="hidden" name="id" class="delete" value="{{ $reserva->id }}">
                            {!! csrf_field() !!}
                            <button type="submit" class="delete" value="delete">eliminar
                            </button>
                        </form>
                    </td>
                    </tbody>

                @endforeach
            </table>
        @else
            <h4>No se encontraron reservas</h4>
        @endif

        @if($orderBy && $orderDirection)
            {{ $reservas->appends(['order_by' => $orderBy, 'order_direction' => $orderDirection, 'espacio_id' => $espacioId, 'date' => $date ])->links() }}
        @else
            {{ $reservas->appends([ 'espacio_id' => $espacioId, 'date' => $date ])->links() }}
        @endif
    </div>
@stop
