@extends('layouts.tv')

@section('content')
<div class="container">

    <!-- <div class="panel panel-default">
        Listado de reservas
    </div> -->

    @if ($espaciosCategoriasGridView)
    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <td>Espacio</td>
                @foreach($totalHours as $hour)
                <th>{{ $hour }}:00</th>
                @endforeach
            </tr>
        </thead>

        @foreach($espaciosCategoriasGridView->categorias as $categoria)

        <!-- <tbody>
            <td><strong>{{ $categoria->name }}</strong></td>
        </tbody> -->

        @foreach($categoria->espacios as $espacio)
        <tbody>
            <td class="espacio-color-{{ $categoria->name }}">{{ $espacio->name }}</td>
            @foreach($espacio->reservas as $reserva)
            @if ($reserva == "free")
            <td class="space-reserva-free">
                L
            </td>
            @elseif ($reserva == "close")
            <td class="<?php if ($loop->first) { echo 'space-name'; } ?> space-reserva-close">
                -
            </td>
            @else
            <td class="<?php if ($loop->first) { echo 'space-name'; } ?>  space-reserva-occupied"
                style="cursor: pointer;" 
                    <?php if ($reserva && is_object($reserva)) { ?>
                        alt="{{$reserva->userName}}" title="{{$reserva->userName}}">
                    <?php } else { ?>
                        alt="Espacio Bloqueado" title="Espacio Bloqueado">
                    <?php } ?>
                X
            </td>
            @endif
            @endforeach
        </tbody>
        @endforeach

        @endforeach
    </table>
    @else
    <h4>No hay espacios disponibles</h4>
    @endif
    <form method="GET" class="form-inline btn-hoy" action="{{ route('reservas.tv_list') }}">
        <div class="form-group">
            <button type="submit" class="btn btn-default">Reservas de hoy</button>
        </div>
    </form>
    <form method="GET" class="form-inline" action="{{ route('reservas.tv_list') }}">
        <div class="form-group">
            Seleccionar otra fecha <input type="date" value="{{ isset($reservas_date) ? $reservas_date : ''}}" class="form-control" name="fecha_reservas" id="fecha_reservas">
            <button type="submit" class="btn btn-default">Actualizar</button>
        </div>
        <p class="referencia"><span class="verde">Verde = Disponible</span><span class="rojo">Rojo = Reservado</span></p>
    </form>
</div>
@stop