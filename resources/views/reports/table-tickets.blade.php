<div class="costa-table">
    <div class="costa-table__header bg-costa text-white">
        <div class="row">
            <div class="col-xs-9">
                <span class="text-bold small">CATEGORIA</span>
            </div>
            <div class="col-xs-3 text-right">
                <span class="text-bold small">CANT.</span>
            </div>
        </div>
    </div>
    <div class="scrollable-div">
    @if (count($results) > 0)
        @foreach($results as $result)
            <div class="costa-table__row">
                <div class="row">
                    <div class="col-xs-9">
                        {{$result->name}}
                    </div>
                    <div class="col-xs-3 text-right">
                        {{$result->count}}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="costa-table__row">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="text-bold">No se encontraron gestiones para el rango seleccionado.</p>
                </div>
            </div>
        </div>
    @endif
    </div>
</div>