@extends('layouts.app')

@push('scripts')
    <link href="{{ asset('css/reports.css') }}?date=20201202" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"  />
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div id="panel_usuarios" class="panel panel-default">
                    <div class="panel-heading">USUARIOS ACTIVOS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="daterange-container">
                                    <div class="text-bold">Rango Fechas</div>
                                    <input type="text" id="daterange_users" name="daterange_users" value="" />
                                </div>
                            </div>
                        </div>

                        <canvas id="chart_users" width="300" height="300"></canvas>
                        <hr>
                        <p class="text-uppercase text-bold small m-0">Total Usuarios Activos</p>
                        <div id="count_users_active" class="text-center h1 m-0 text-costa text-bold">0</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div id="panel_views" class="panel panel-default">
                    <div class="panel-heading">VISTAS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="daterange-container">
                                    <div class="text-bold">Rango Fechas</div>
                                    <input type="text" id="daterange_views" name="daterange_views" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <canvas id="chart_views" width="300" height="300"></canvas>
                            </div>
                            <div class="col-md-6">
                                <p class="text-uppercase text-bold small m-0">VISTAS TOTALES</p>
                                <div id="table_views"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('js/reports-libs.js') }}"></script>
    <script src="{{ asset('js/reports-analytics.js') }}"></script>
@endsection
