@extends('layouts.app')

@push('scripts')
    <link href="{{ asset('css/reports.css') }}?date=20201202" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"  />
@endpush

@section('content')
    <div class="container" id="printAll">
        <div class="row">
            <div class="col-sm-12 text-right">
                <button type="button" onclick="printJS({
                    printable: 'printAll',
                    type: 'html',
                    css: 'css/for-print.css?date=09112021',
                    documentTitle: 'Reportes'
                })" class="btn-print d-print-none">
                    Imprimir Todos los Reportes PDF
                </button>
            </div>
        </div>
        <div class="row">
            <div class="full-print col-sm-4">
                <div id="panel_usuarios_active" class="panel panel-default">
                    <div class="panel-heading">USUARIOS ACTIVOS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="daterange-container">
                                    <div class="text-bold">Rango Fechas</div>
                                    <input type="text" id="daterange_users" name="daterange_users" value="" />
                                </div>
                            </div>
                        </div>

                        <canvas id="chart_users_active" width="300" height="300"></canvas>
                        <hr>
                        <p class="text-uppercase text-bold small m-0">Total Usuarios Activos</p>
                        <div id="count_users_active" class="text-center h1 m-0 text-costa text-bold">0</div>

                        <div class="row text-right margin-top">
                            <div class="col-md-12 text-right">
                                <button type="button" onclick="printJS({
                                    printable: 'panel_usuarios_active',
                                    type: 'html',
                                    css: 'css/for-print.css',
                                    documentTitle: 'Reporte Usuarios Activos'
                                })" class="btn-print d-print-none">
                                    Imprimir PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="full-print col-sm-8">
                <div id="panel_views" class="panel panel-default">
                    <div class="panel-heading">VISTAS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="daterange-container">
                                    <div class="text-bold">Rango Fechas</div>
                                    <input type="text" id="daterange_views" name="daterange_views" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <canvas id="chart_views" width="300" height="300"></canvas>
                            </div>
                            <div class="col-md-6">
                                <p class="text-uppercase text-bold small m-0">VISTAS TOTALES</p>
                                <div id="table_views"></div>
                            </div>
                        </div>
                        <div class="row text-right margin-top">
                            <div class="col-md-12 text-right">
                                <button type="button" onclick="printJS({
                                    printable: 'panel_views',
                                    type: 'html',
                                    css: 'css/for-print.css',
                                    documentTitle: 'Reporte Vistas Totales'
                                })" class="btn-print d-print-none">
                                    Imprimir PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="full-print col-sm-4">
                <div id="panel_usuarios" class="panel panel-default">
                    <div class="panel-heading">USUARIOS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        
                        <canvas id="chart_users" width="300" height="300"></canvas>
                        <hr>
                        <p class="text-uppercase text-bold small m-0">Promedio Usuarios Agregados por Propietario</p>
                        <div id="count_users_added" class="text-center h1 m-0 text-costa text-bold">0</div>

                        <div class="row text-right margin-top">
                            <div class="col-md-12 text-right">
                                <button type="button" onclick="printJS({
                                    printable: 'panel_usuarios',
                                    type: 'html',
                                    css: 'css/for-print.css',
                                    documentTitle: 'Reporte Usuarios'
                                })" class="btn-print d-print-none">
                                    Imprimir PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_encuestas" class="panel panel-default margin-top">
                    <div class="panel-heading">ENCUESTAS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        
                        <select name="encuesta_id" id="encuesta_id" class="form-control">
                            <option value="0" selected="selected">Todas</option>
                            @foreach ($encuestas as $encuesta)
                                <option value="{{$encuesta->id}}">{{$encuesta->question}}</option>
                            @endforeach
                        </select>
                        <canvas id="chart_encuestas" width="300" height="300"></canvas>

                        <div class="poll-container">
                        </div>

                        <div class="row text-right margin-top">
                            <div class="col-md-12 text-right">
                                <button type="button" onclick="printJS({
                                    printable: 'panel_encuestas',
                                    type: 'html',
                                    css: 'css/for-print.css',
                                    documentTitle: 'Reporte Encuestas'
                                })" class="btn-print d-print-none">
                                    Imprimir PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="full-print col-sm-8">
                <div id="panel_gestiones" class="panel panel-default">
                    <div class="panel-heading">GESTIONES</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="daterange-container">
                                    <div class="text-bold">Rango Fechas</div>
                                    <input type="text" id="daterange_gestiones" name="daterange_gestiones" value="" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <p class="text-uppercase text-bold">Promedio Tiempo Respuesta</p>
                                <div class="text-center text-costa text-bold">
                                    <span id="avg_ticket_answer" class="h1 m-0 text-bold">0</span> <span>hs.</span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <p class="text-uppercase text-bold m-0">GESTIONES POR TIPO DE USUARIO</p>
                                <div id="count_tickets_user"></div>
                            </div>
                        </div>
                        <hr>
                        <p class="text-uppercase text-bold small m-0">GESTIONES Por Categoría</p>
                        <div id="table_tickets"></div>

                        <div class="row text-right margin-top">
                            <div class="col-md-12 text-right">
                                <button type="button" onclick="printJS({
                                    printable: 'panel_gestiones',
                                    type: 'html',
                                    css: 'css/for-print.css',
                                    documentTitle: 'Reporte Gestiones'
                                })" class="btn-print d-print-none">
                                    Imprimir PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="panel_reservas" class="panel panel-default margin-top">
                    <div class="panel-heading">RESERVAS</div>
                    <div class="panel-body">
                        <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="daterange-container">
                                    <div class="text-bold">Rango Fechas</div>
                                    <input type="text" id="daterange_reservas" name="daterange_reservas" value="" />
                                </div>
                            </div>
                        </div>

                        <div class="row margin-top">
                            <div class="col-sm-6">
                                <p class="text-uppercase text-bold m-0">RESERVAS POR DEPORTE</p>
                                <canvas id="chart_reservas_deporte"></canvas>
                            </div>
                            <div class="col-sm-6">
                                <p class="text-uppercase text-bold m-0">RESERVAS POR TIPO USUARIO</p>
                                <canvas id="chart_reservas_usuario"></canvas>
                            </div>
                        </div>
                        <div class="row margin-top">
                            <div class="col-sm-12">
                                <p class="text-uppercase text-bold m-0">RESERVAS POR HORARIO</p>
                                <canvas id="chart_reservas_tiempo"></canvas>
                            </div>
                        </div>

                        <div class="row text-right margin-top">
                            <div class="col-md-12 text-right">
                                <button type="button" onclick="printJS({
                                    printable: 'panel_reservas',
                                    type: 'html',
                                    css: 'css/for-print.css',
                                    documentTitle: 'Reporte Reservas'
                                })" class="btn-print d-print-none">
                                    Imprimir PDF
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('js/reports-libs.js') }}"></script>
    <script src="{{ asset('js/reports.js') }}?date=20200818"></script>
    <script src="{{ asset('js/reports-analytics.js') }}?date=20200130"></script>
@endsection
