<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Costa Esmeralda - Gestiones</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <style>
        .btn-primary {
            border-color: #008E69;
            background: #008E69;
            width: 100%;
            padding: 15px 0;
        }

        .btn-primary:hover, .btn-primary:active {
            border-color: #007657;
            background: #007657;
        }

        .imgTicket {
            width: 100%;
            max-width: 500px;
            box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .6);
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            var $target = $('html,body'); 
            $target.animate({scrollTop: $target.height()}, 500);
        });
    </script>
</head>
<body>
    <div class="container">
        <div class="row mt-4">
            <div class="col-md" align="center">
                <img class="img-fluid mb-4"" src="{{ asset('images/logo-costa.png') }}" />
            </div>
            <div class="col-md"></div>
            <div class="col-md" align="center">
                <a target="_blank" href="https://apps.apple.com/ar/app/costa-esmeralda/id1165364737">
                    <img width="130px" src="{{ asset('images/logo-appstore.png') }}"/>
                </a>
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.mcdg.costaesmeralda&hl=es&gl=AR">
                    <img width="150px" src="{{ asset('images/logo-playstore.png') }}"/>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md">
                @if ($incidente)
                    @if (session('error'))
                        <div class="my-2 alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="card bg-default mt-4 mb-4">
                        <h5 class="card-header">{{$incidente->user->name}} ~ Gestión N {{$incidente->id}}</h5>
                        <div class="card-body">
                            <p class="card-text">{{$incidente->title}}</p>
                            <p class="card-text">
                                <small>
                                    {{$incidente->tema->responsableArea->name}}</span> > <span>{{$incidente->tema->name}}
                                </small>
                            </p>
                        </div>
                        <div class="card-footer">
                            Estado: {{$incidente->state->name}}
                        </div>
                    </div>
                    <div>
                        <div class="alert alert-info" role="alert">
                            <p class="mb-0"><small class="text-uppercase">{{ date('j M G:i', strtotime($incidente->created_at)) }}<span class="text-lowercase">hs</span></small></p>
                            <p class="mb-0">
                                <b>{{$incidente->user->name}}:</b> {{$incidente->detalle}}
                            </p>
                        </div>
                    </div>
                    @foreach($incidente->messages as $message)
                        @if ($message->user_id != $incidente->user_id)
                            <div class="alert alert-success" role="alert">
                                <p class="mb-0"><small class="text-uppercase">{{ date('j M G:i', strtotime($message->created_at)) }}<span class="text-lowercase">hs</span></small></p>
                                <p class="mb-0">
                                    <b>Costa Esmeralda:</b> {{$message->message}}
                                </p>
                                @if (count($message->images) > 0)
                                    @foreach($message->images as $image)
                                        <img src="{{ url('/') . '/images/' . $image->image_path}}" class="img-fluid imgTicket mt-2">
                                    @endforeach
                                @endif
                            </div>
                        @else
                            <div class="alert alert-info" role="alert">
                                <p class="mb-0"><small class="text-uppercase">{{ date('j M G:i', strtotime($message->created_at)) }}<span class="text-lowercase">hs</span></small></p>
                                <p class="mb-0">
                                    <b>{{$incidente->user->name}}:</b> {{$message->message}}
                                </p>
                                @if (count($message->images) > 0)
                                    @foreach($message->images as $image)
                                        <img src="{{ url('/') . '/images/' . $image->image_path}}" class="img-fluid imgTicket mt-2">
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    @endforeach
                    @if ($incidente->state_id != 3)
                        <form role="form" action="{{route('gestion-send-message')}}" method="POST" enctype="multipart/form-data" class="pb-5">
                            {{ csrf_field() }}
                            <input type="hidden" name="incident_id" value="{{$incidente->id}}">
                            <input type="hidden" name="incident_code" value="{{$incidente->auth_code}}">
                            <div class="form-group">
                                <label for="comment">Responder:</label>
                                <textarea class="form-control" rows="5" id="comment" name="comment" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="image"> Adjuntar una imagen </label>
                                <input type="file" name="image" class="form-control-file" id="image" />
                                <p class="help-block text-muted"><small>Sólo usar JPEG, JPG, GIF, PNG.</small></p>
                            </div>
                            @if (session('success'))
                                <div class="my-2 alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if (session('error'))
                                <div class="my-2 alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">
                                    Enviar Respuesta
                                </button>
                            </div>
                        </form>
                    @endif
                @else
                    <div class="alert alert-danger m-0">
                        <div class="text-center">
                            <span class="glyphicon glyphicon-alert" style="font-size: 40px;"></span>
                        </div>
                        <p class="margin-top">Ups! Lamentablemente la página que estás intentando acceder no existe o no se encuentra disponible.</p>
                        <p>Si recibiste el link via mail, por favor contactate con administración.</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</body>
</html>