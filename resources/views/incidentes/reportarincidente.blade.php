@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Reportar nuevo incidente</div>
                    <div class="panel-body">


                        @if($errors)
                            @foreach ($errors->all() as $error)
                                <div class="bg-danger">{{ $error }}</div>
                            @endforeach
                        @endif


                        {{ Form::open(['route' => 'incidente.new', 'files' => true]) }}
                            <div class="row form-group">
                                <label for="id" class="col-md-4 control-label">Tipo de Usuario</label>
                                <div class="col-md-6">
                                    <div>
                                        <input type="radio" id="existing_user" name="user_type" value="existing_user"
                                                @if (old('user_type') != 'external_user') checked @endif>
                                        <label for="existing_user">Usuario Existente</label>
                                    </div>
                                    <div>
                                        <input type="radio" id="external_user" name="user_type" value="external_user"
                                                @if (old('user_type') == 'external_user') checked @endif>
                                        <label for="external_user">Usuario Externo</label>
                                    </div>
                                </div>
                            </div>
                            

                            <div id="existing_user_container" @if (old('user_type') == 'external_user') style="display: none;" @endif
                                    class="row form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="col-md-4 control-label">Usuario</label>
                                <div class="col-md-6">
                                    <autocomplete></autocomplete>
                                    @if ($errors->has('user_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div id="external_user_container" @if (old('user_type') != 'external_user') style="display: none;" @endif>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">Nombre Completo</label>
                                    <div class="col-md-6">
                                        <input id="user_name" type="text" class="form-control" name="user_name" value="{{old('user_name')}}"></input>
                                        @if ($errors->has('user_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('user_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">Email</label>
                                    <div class="col-md-6">
                                        <input id="user_email" type="email" class="form-control" name="user_email" value="{{old('user_email')}}"></input>
                                        @if ($errors->has('user_email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('user_email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="user_id" class="col-md-4 control-label">DNI</label>
                                    <div class="col-md-6">
                                        <input id="user_dni" type="number" class="form-control" name="user_dni" value="{{old('user_dni')}}"></input>
                                        @if ($errors->has('user_dni'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('user_dni') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row form-group{{ $errors->has('responsable') ? ' has-error' : '' }}">
                                <label for="responsable" class="col-md-4 control-label">Categoria/Responsable</label>

                                <div class="col-md-6">

                                    <select name="tema_responsable_area_id" id="tema_responsable_area_id" class="form-control">
                                        @foreach($subAreas as $subArea)
                                            <option value="{{ $subArea->id }}">{{ $subArea->responsableArea->name }} - {{ $subArea->name }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('responsable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('responsable') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="detalle" class="col-md-4 control-label">Titulo</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{old('title')}}" required></input>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group{{ $errors->has('detalle') ? ' has-error' : '' }}">
                                <label for="detalle" class="col-md-4 control-label">Detalle</label>

                                <div class="col-md-6">
                                    <textarea id="detalle" class="form-control" rows="5" name="detalle" required>{{old('detalle')}}</textarea>
                                    @if ($errors->has('detalle'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('detalle') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                <label for="location" class="col-md-4 control-label">Ubicación (Lote, sector, lugar)</label>

                                <div class="col-md-6">
                                    <input id="location" class="form-control" name="location" value="{{old('location')}}" />
                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                {{ Form::label('image', 'Imagen') }}
                                {{ Form::file('image', null) }}
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif

                                <button id="btn_delete_file" class="btn btn-outline text-danger" style="display: none;">
                                    <span class="glyphicon glyphicon-remove"></span> quitar archivo
                                </button>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Reportar Incidente
                                    </button>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#image').change(function() {
                if ($(this).val() !== '') {
                    $('#btn_delete_file').show();
                }
            });
            $('#btn_delete_file').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('#image').val('');
                $(this).hide();
                return false;
            });
            $("[name='user_type']").change(function() {
                if ($(this).val() == 'external_user') {
                    $('#existing_user_container').hide();
                    $('#external_user_container').show();
                    $('#external_user_container input').attr('required', 'required');
                } else {
                    $('#external_user_container').hide();
                    $('#external_user_container input').removeAttr('required');
                    $('#existing_user_container').show();
                }
            });
        });
    </script>
@endsection