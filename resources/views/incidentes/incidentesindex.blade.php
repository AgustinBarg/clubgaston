@extends('var.www.overview.resources.views.layouts.app')

@section('content')
    <div class="container">
        <ul class="nav nav-pills nav-stacked" style="text-align: center; margin-bottom: 10px;">


            @if(Auth::user())
                <li role="presentation" class="active"><a href="{{route('indexcrear')}}">Reportar Incidente</a></li>
                <li role="presentation" class="active"><a href="{{route('listadoincidentes')}}">Listado Incidentes</a></li>
            @endif
        </ul>


    </div>
@endsection
