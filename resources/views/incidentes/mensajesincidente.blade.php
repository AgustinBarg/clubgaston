@extends('layouts.app')
@section('content')

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default panel-incidente-messages">
            <div class="panel-heading">Incidente n {{ $incidente->id }}  </div>
            
            <?php if ($incidente->user->type != 'owner') { ?>
                <div class="panel-heading">
                    <span style="font-weight: bold;">Usuario Invitado</span><br> 
                    {{ $incidente->user->name }} ({{$incidente->user->lote}} / {{$incidente->user->lote_ubicacion}}) / ({{$incidente->user->type}})
                </div>
                <div class="panel-heading">
                    <span style="font-weight: bold;">Usuario Dueño</span><br> 
                    @if ($incidente->user->owner)
                        {{ $incidente->user->owner->name }} ({{$incidente->user->owner->lote}} / {{$incidente->user->owner->lote_ubicacion}}) 
                    @else
                        --
                    @endif
                </div>
            <?php } else { ?>
                <div class="panel-heading">
                    <span style="font-weight: bold;">Usuario</span><br> 
                    {{ $incidente->user->name }} ({{$incidente->user->lote}} / {{$incidente->user->lote_ubicacion}}) 
                </div>
            <?php } ?>

            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <form role="form" method="POST" action="{{ route('incidente.edit') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="noredirect" value="true">
                            <input id="id" class="form-control d-none" name="id" value="{{ $incidente->id }}" readonly>
                            <label for="">Responsable</label>
                            <select name="tema_responsable_area_id" id="tema_responsable_area_id" class="form-control">
                                @foreach($temasResponableArea as $temaResponableArea)
                                    <option @if($temaResponableArea->id == $incidente->tema->id) selected="selected" @endif value="{{ $temaResponableArea->id }}">{{ $temaResponableArea->responsableArea->name }} - {{ $temaResponableArea->name }}</option>
                                @endforeach
                            </select>
                            <button value="{{ $incidente->id }}" type="submit">Reasignar</button>
                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <form role="form" method="POST" action="{{ route('incidente.edit') }}">
                            {{csrf_field()}}
                            <input type="hidden" name="noredirect" value="true">
                            <input id="id" class="form-control d-none" name="id" value="{{ $incidente->id }}" readonly>
                            <label>Estado</label>
                            <select name="state_id" id="state_id" class="form-control">
                                @foreach($states as $state)
                                    <option @if($state->id == $incidente->state_id) selected="selected" @endif value="{{ $state->id }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                            <button value="{{ $incidente->id }}" type="submit">Modificar</button>
                        </form>
                    </div>
                </div>
                
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>

            <div class="panel-heading"><span style="font-weight: bold;">Titulo</span><br> {{ $incidente->title }} </div>
            <div class="panel-heading"><span style="font-weight: bold;">Detalle</span><br> {{ $incidente->detalle  }} </div>
            <div class="panel-heading"><span style="font-weight: bold;">Fecha de creacion</span><br> {{ $incidente->created_at->format('d m Y') }} </div>
            <div class="panel-heading"><span style="font-weight: bold;">Ubicacion</span><br> {{ $incidente->location  }} </div>
            <div class="panel-heading">
                <span style="font-weight: bold;">Lat Long</span><br> {{ $incidente->latlong_location ? $incidente->latlong_location : '-' }} 
                @if ($incidente->latlong_location)
                    <a href="https://www.google.com.ar/maps/place/{{ $incidente->latlong_location  }}" target="_blank">
                        Ver en mapa
                    </a>
                @endif
            </div>
            <div class="panel-heading panel-messages"><span>Mensajes</span></div>
            <div class="panel-heading">
                @if($incidente->image_path != null)
                    <img src="{{ url('/') . '/images/' . $incidente->image_path }}" style="max-width: 80%;">
                @endif
            </div>
            <?php $previousUser = 0; ?>
            <div class="scrollable-messages-container">
            @foreach ($incidente->messages as $message)
                <div class="panel-heading panel-single-message
                            @if($message->user->id !== $incidente->user->id) panel-right @endif
                            @if ($previousUser != $message->user->id) panel-different-user @endif">
                    <?php if ($previousUser != $message->user->id) { ?>
                        <?php $previousUser = $message->user->id; ?>
                        <div class="user-data"><b>{{ $message->user->name }}</b></div>
                    <?php } ?>
                    <time>{{ $message->created_at }}</time>
                    {{ $message->message }} <br>
                    @if (count($message->images) > 0)
                        @foreach ($message->images as $image)
                            <img src="{{ url('/') . '/images/' . $image->image_path }}" alt="" class="image-message" style="max-width: 100%;max-height: 300px;"> <br>
                        @endforeach
                    @endif
                    
                </div>
            @endforeach
            </div>
            <div class="panel-body">
                {{ Form::open(['route' => 'sendmessage', 'files' => true]) }}
                    {{csrf_field()}}
                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        <label for="message" class="col-md-4 control-label">Nuevo Mensaje:</label>

                        <div class="col-md-6">
                            <input hidden="true" readonly="true" id="id" type="number" min="0" class="form-control" name="id" value="{{ $incidente->id }}" required>
                            <textarea id="message" class="form-control" rows="5" name="message"  required></textarea>
                            {{ Form::label('image', 'Imagen') }}
                            {{ Form::file('image', null) }}
                            <button type="submit">Enviar</button>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                            @if ($errors->has('image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>
@endsection

@section('footer')
    <script>
        jQuery(document).ready(function() {
            setTimeout(function() {
                jQuery('.scrollable-messages-container').scrollTop(jQuery('.scrollable-messages-container')[0].scrollHeight);
            }, 500);
        });
    </script>
@endsection