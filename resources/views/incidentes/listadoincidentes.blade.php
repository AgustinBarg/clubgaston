@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="panel panel-default">
            <div class="panel-heading">
                Listado incidentes
            </div>
            <div class="panel-body">
                <form method="GET" action="{{ route('incidente.index', [
                        'order_by' => $orderBy, 
                        'order_direction' => $orderDirection,
                        'tema_id' => $temaId,
                        'area_id' => $areaId,
                        'state_id' => $stateId,
                        'incidente_id' => $incidenteId,
                        'user_name' => $userName,
                        'parentuser_name' => $parentuserName

                    ]) }}">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <input type="text" class="form-control" name="query" id="query" placeholder="Buscar por titulo..." value="{{$query}}">        
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <input type="number" class="form-control" name="incidente_id" id="incidente_id" placeholder="Buscar por numero de gestion" value="{{$incidenteId}}">       
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <select name="tema_id" id="tema_id" class="form-control">
                                    <option value="">No filtrar por tema</option>
                                    @foreach($temasResponableArea as $temaResponableArea)
                                        <option value="{{ $temaResponableArea->id }}"
                                            <?php if($temaId == $temaResponableArea->id) { echo 'selected="selected"'; } ?>>{{ $temaResponableArea->responsableArea->name }} - {{ $temaResponableArea->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <select name="area_id" id="area_id" class="form-control">
                                    <option value="">No filtrar por area</option>
                                    @foreach($responsableAreas as $responsableArea)
                                        <option value="{{ $responsableArea->id }}"
                                            <?php if($areaId == $responsableArea->id) { echo 'selected="selected"'; } ?>>{{ $responsableArea->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <select name="state_id" id="state_id" class="form-control">
                                    <option value="">No filtrar por estado</option>
                                    @foreach($states as $state)
                                        <option value="{{ $state->id }}"
                                            <?php if($stateId == $state->id) { echo 'selected="selected"'; } ?>>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Buscar por nombre de usuario" value="{{$userName}}">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <input type="text" class="form-control" name="parentuser_name" id="parentuser_name" placeholder="Buscar por nombre de usuario padre" value="{{$parentuserName}}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">Buscar</button>
                </form>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Totales
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <span>Total Realizadas:</span> {{$totals['open'] + $totals['closed']}}
                    </div>
                    <div class="col-sm-4">
                        <span>Abiertas:</span> {{$totals['open']}}
                    </div>
                    <div class="col-sm-4">
                        <span>Cerradas:</span> {{$totals['closed']}}
                    </div>
                </div>
            </div>
        </div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if(count($incidentes) > 0)
            <table class="table table-bordered" id="incidentes-table">
                <thead>
                <tr>
                    <th>
                        id
                        <a href="{{ route('incidente.index', ['order_by' => 'id', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'id', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Nombre de usuario de reporte
                        <a href="{{ route('incidente.index', ['order_by' => 'user_id', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'user_id', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Categoria
                        <a href="{{ route('incidente.index', ['order_by' => 'tema_id', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'tema_id', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Titulo
                        <a href="{{ route('incidente.index', ['order_by' => 'title', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'title', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Fecha de creacion
                        <a href="{{ route('incidente.index', ['order_by' => 'created_at', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'created_at', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Ultima modificacion
                        <a href="{{ route('incidente.index', ['order_by' => 'updated_at', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'updated_at', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>Ver detalle</th>
                    <th>
                        Estado
                        <a href="{{ route('incidente.index', ['order_by' => 'state_id', 'order_direction' => 'asc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('incidente.index', ['order_by' => 'state_id', 'order_direction' => 'desc', 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                </tr>

                @foreach($incidentes as $incidente)
                    <tbody>
                    <td>{{ $incidente->id }}</td>
                    <td>{{ $incidente->user->name }}</td>
                    <td>
                        <form role="form" method="POST" action="{{ route('incidente.edit') }}">
                            {{csrf_field()}}
                            <input id="id" class="form-control d-none" name="id" value="{{ $incidente->id }}" readonly>
                            <select name="tema_responsable_area_id" id="tema_responsable_area_id" class="form-control">
                                @foreach($temasResponableArea as $temaResponableArea)
                                    <option <?php if($temaResponableArea->id == $incidente->tema_id) { echo 'selected="selected"'; } ?> value="{{ $temaResponableArea->id }}">{{ $temaResponableArea->responsableArea->name }} - {{ $temaResponableArea->name }}</option>
                                @endforeach
                            </select>
                            <button value="{{ $incidente->id }}" type="submit">Reasignar</button>
                        </form>
                    </td>
                    <td>{{ $incidente->title }}</td>
                    <td>{{ $incidente->created_at->format('d/m/Y H:i') }}</td>
                    <td>{{ $incidente->updated_at->format('d/m/Y H:i') }}</td>
                    <td>
                        <a class="glyphicon-envelope" target="_blank" href="{{ route('incidente.id', ['id' => $incidente->id]) }}"> Abrir</a>
                    </td>
                    <td>
                        <form role="form" method="POST" action="{{ route('incidente.edit') }}">
                            {{csrf_field()}}
                            <input id="id" class="form-control d-none" name="id" value="{{ $incidente->id }}" readonly>
                            <select name="state_id" id="state_id" class="form-control">
                                @foreach($states as $state)
                                    <option <?php if($state->id == $incidente->state_id) { echo 'selected="selected"'; } ?> value="{{ $state->id }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                            <button value="{{ $incidente->id }}" type="submit">Modificar</button>
                        </form>
                    </td>
                    </tbody>
                    </form>
                    </thead>

                @endforeach
            </table>
        @else
            <p>No se encontraron incidentes</p>
        @endif
        @if($orderBy && $orderDirection)
            {{ $incidentes->appends(['order_by' => $orderBy, 'order_direction' => $orderDirection, 'tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName  ])->links() }}
        @else
            {{ $incidentes->appends(['tema_id' => $temaId, 'area_id' => $areaId, 'state_id' => $stateId, 'incidente_id' => $incidenteId, 'user_name' => $userName, 'parentuser_name' => $parentuserName ])->links() }}
        @endif
    </div>
@stop

