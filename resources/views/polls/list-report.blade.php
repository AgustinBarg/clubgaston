@extends('layouts.app')

@push('scripts')
    <link href="{{ asset('css/reports.css') }}?date=20201202" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
@endpush

@section('content')
    <div class="container" id="printAll">
        <div class="panel panel-default">
            Graficos Encuestas
        </div>
        <div class="row">
            <div class="col-sm-12 text-right">
                <button type="button" onclick="printJS({
                    printable: 'printAll',
                    type: 'html',
                    css: '/css/for-print.css?date=09122021',
                    documentTitle: 'Reportes'
                })" class="btn-print d-print-none">
                    Imprimir Todos los Reportes PDF
                </button>
            </div>
        </div>
        <div class="row wrap-before-print" style="display: flex; flex-wrap: wrap;">
            @foreach($polls as $poll)
                <div class="full-print col-sm-4">
                    <div id="panel_encuestas_{{$poll->id}}" data-id="{{$poll->id}}" class="panel-encuesta panel panel-default margin-top">
                        <div class="panel-heading text-uppercase">{{$poll->question}}</div>
                        <div class="panel-body">
                            <div class="panel-loading"><span class="glyphicon glyphicon-repeat fa-spin"></span></div>
                            <canvas id="chart_encuestas_{{$poll->id}}" width="300" height="300"></canvas>
                            <div class="poll-container">
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ asset('js/reports-libs.js') }}"></script>
    <script>
        (function() {
            var colors = ['#F66D44', '#FEAE65', '#AADEA7', '#64C2A6', '#2D87BB', '#476088', '#FFC562', '#FF6D74', '#4FDDC3', '#61A8E8', '#559821', '#8EAF2D', '#CFD861', '#EB5F59', '#2B425A', '#192938'];

            function parseUserType(type) {
                switch (type) {
                    case 'familiar': return 'Familiar';
                    case 'inquilino': return 'Inquilino';
                    case 'owner': return 'Propietario';
                    default: return type;
                }
            }
            function callWS(url, method, data) {
                return new Promise(function(resolve, reject) {
                    $.ajax({
                        url: url,
                        method: method,
                        data: data,
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        }
                    }).done(function(resp) {
                        resolve(resp);
                        })
                        .fail(function(err) {
                            resolve(err)
                        });
                });
            }

            function updatePollAnswers( pollId ) {
                callWS('/admin-api/reports/poll-answers/' + pollId, 'GET', {})
                    .then(function(results) {
                        $('#panel_encuestas_' + pollId + ' .panel-loading').addClass('hide');
                        var configEncuesta = {
                            type: 'doughnut',
                            data: {
                                datasets: [{
                                    data: results.report.length === 0 ? [1] : results.report.map((res) => res.count),
                                    backgroundColor: results.report.length === 0 ? ['grey'] : colors,
                                    borderWidth: 0
                                }],
                                labels: results.report.length === 0 ? ['No hay respuestas'] : results.report.map((res) => parseUserType(res.type))
                            },
                            options: { responsive: true }
                        };
                        var ctxEncuesta = document.getElementById('chart_encuestas_' + pollId).getContext('2d');
                        chartEncuestas = new Chart(ctxEncuesta, configEncuesta);
                        $('#panel_encuestas_' + pollId + ' .poll-container').html(results.content);
                    }).catch(function(error) {
                        console.log(error);
                    });
            }

            $(document).ready(function() {
                $('.panel-encuesta').each(function() {
                    updatePollAnswers($(this).attr('data-id'));
                });
            });
        })();
    </script>
@endsection