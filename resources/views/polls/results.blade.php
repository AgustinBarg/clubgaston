
<div class="poll-options options-answered">
    @foreach ($poll->options as $option)
        <div class="poll-option">
            <div class="poll-option-details">
                <div class="poll-answer">
                    {{$option->text}} <span style="font-size: 15px; color: grey;">({{$option->votes}} votos)</span>
                </div>
                <div class="poll-bar">
                    <div class="poll-bar-inside" 
                        style="width: @if($poll->totalVotes > 0) {{round($option->votes / $poll->totalVotes * 100)}}% @endif"></div>
                </div>
            </div>
            <div class="poll-option-number">@if($poll->totalVotes > 0) {{round($option->votes / $poll->totalVotes * 100)}}% @endif</div>
        </div>
    @endforeach
</div>
<div class="total-votes"><strong>Total Votos:</strong> {{$poll->totalVotes}}</div>