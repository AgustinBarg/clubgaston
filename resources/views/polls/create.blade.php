@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear Encuesta</div>
                    <div class="panel-body">
                        {{ Form::open(array('url' => '/encuestas/create')) }}

                            <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                                {{ Form::label('question', 'Pregunta') }}
                                {{ Form::input('text', 'question', '', ['id' => 'poll_question', 'class' => 'form-control', 'placeholder' => 'Pregunta', 'maxlength' => 191]) }}
                                <div id="poll_question_characters" class="remaining-characters">0/191</div>
                                @if ($errors->has('question'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('question') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('valid_from') ? 'has-error' : '' }}">
                                <label for="valid_from" class="control-label">Disponible Desde:</label>
                                <input id="valid_from" type="date" class="form-control" name="valid_from">

                                @if ($errors->has('valid_from'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('valid_from') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('valid_from') ? 'has-error' : '' }}">
                                <label for="valid_to" class="control-label">Disponible Hasta:</label>
                                <input id="valid_to" type="date" class="form-control" name="valid_to">

                                @if ($errors->has('valid_to'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('valid_to') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div id="answers_container">
                                <div class="form-group">
                                    <label for="answers[]">Respuesta 1</label> 
                                    <input placeholder="Respuesta 1" name="answers[]" type="text" value="" class="form-control"> 
                                </div>
                            </div>

                            <div class="form-group">
                                <button id="btn_add_answer" class="btn btn-primary">Agregar Respuesta</button>
                                <button id="btn_delete_answer" class="btn btn-primary">Eliminar Respuesta</button>
                            </div>



                            {{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function() {
            var indexRespuestas = 1;

            $('#poll_question').keyup(function() {
                const count_chars = $('#poll_question').val().length + '/191';
                $('#poll_question_characters').html(count_chars);
            });

            $('#btn_add_answer').click(function($e) {
                $e.preventDefault();
                $e.stopPropagation();

                indexRespuestas++;

                var newForm =    '<div class="form-group">';
                newForm +=          '<label for="answers[]">Respuesta ' + indexRespuestas + '</label> ';
                newForm +=          '<input placeholder="Respuesta ' + indexRespuestas + '" name="answers[]" type="text" value="" class="form-control"> ';
                newForm +=        '</div>';
                                    
                $('#answers_container').append(newForm);
            });

            $('#btn_delete_answer').click(function($e) {
                $e.preventDefault();
                $e.stopPropagation();

                indexRespuestas--;
                $('#answers_container .form-group:last-child').remove();
            });
        });
    </script>
@endsection