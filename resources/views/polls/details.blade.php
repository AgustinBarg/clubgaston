@extends('layouts.app')
@section('content')

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default panel-incidente-messages">
            <div class="panel-heading">
                <span style="font-weight: bold;">Pregunta</span><br> 
                {{ $poll->question}}
            </div>
            <div class="panel-heading">
                <span style="font-weight: bold;">Fecha Creacion</span><br> 
                {{ $poll->created_at->format('d/m/Y - G:i\h\s') }} 
            </div>
            <div class="panel-heading">
                <span style="font-weight: bold;">Válida Desde</span><br> 
                {{ $poll->valid_from->format('d/m/Y') }} 
            </div>
            <div class="panel-heading">
                <span style="font-weight: bold;">Válida Hasta</span><br> 
                {{ $poll->valid_to->format('d/m/Y') }} 
            </div>
            <div class="panel-heading panel-messages"><span>Resultados</span></div>

            <div class="panel-body">
                <form id="form_filter" method="GET" class="form-inline form-encuesta text-center" action="">
                    <input type="hidden" id="type" name="type" value="<?php if(!isset($params['type'])) { echo 'all'; } else { echo $params['type']; } ?>">
                    <div class="form-group">
                        <select id="select_month" name="month" class="form-control">
                            <option value="" selected="selected">Mes</option>
                            <option value="1" <?php if(isset($params['month']) && $params['month'] == '1') { echo 'selected'; } ?>>Enero</option>
                            <option value="2" <?php if(isset($params['month']) && $params['month'] == '2') { echo 'selected'; } ?>>Febrero</option>
                            <option value="3" <?php if(isset($params['month']) && $params['month'] == '3') { echo 'selected'; } ?>>Marzo</option>
                            <option value="4" <?php if(isset($params['month']) && $params['month'] == '4') { echo 'selected'; } ?>>Abril</option>
                            <option value="5" <?php if(isset($params['month']) && $params['month'] == '5') { echo 'selected'; } ?>>Mayo</option>
                            <option value="6" <?php if(isset($params['month']) && $params['month'] == '6') { echo 'selected'; } ?>>Junio</option>
                            <option value="7" <?php if(isset($params['month']) && $params['month'] == '7') { echo 'selected'; } ?>>Julio</option>
                            <option value="8" <?php if(isset($params['month']) && $params['month'] == '8') { echo 'selected'; } ?>>Agosto</option>
                            <option value="9" <?php if(isset($params['month']) && $params['month'] == '9') { echo 'selected'; } ?>>Septiembre</option>
                            <option value="10" <?php if(isset($params['month']) && $params['month'] == '10') { echo 'selected'; } ?>>Octubre</option>
                            <option value="11" <?php if(isset($params['month']) && $params['month'] == '11') { echo 'selected'; } ?>>Noviembre</option>
                            <option value="12" <?php if(isset($params['month']) && $params['month'] == '12') { echo 'selected'; } ?>>Diciembre</option>
                        </select>
                        <select id="select_year" name="year" class="form-control">
                            <option value="" selected="selected">Año</option>
                            <?php
                                $year = date('Y');
                                for ($i = 0; $i < 3; $i++) { 
                            ?>
                                <option value="{{($year-$i)}}" <?php if(isset($params['year']) && $params['year'] == ($year-$i)) { echo 'selected'; } ?>>{{($year-$i)}}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <div id="select_type" class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" data-type="all" 
                                class="btn btn-secondary <?php if(!isset($params['type']) || $params['type'] == 'all') { echo 'active'; } ?>">Todos</button>
                        <button type="button" data-type="propietarios" 
                                class="btn btn-secondary <?php if(isset($params['type']) && $params['type'] == 'propietarios') { echo 'active'; } ?>">Propietarios</button>
                        <button type="button" data-type="inquilinos" 
                                class="btn btn-secondary <?php if(isset($params['type']) && $params['type'] == 'inquilinos') { echo 'active'; } ?>">Inquilinos</button>
                    </div>
                </form>
            </div>

            <div class="poll-container">
                @include('polls.results', ['poll' => $poll])
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        function checkMonthAndYearSubmit() {
            $('#form_filter').submit();
        }
        $(document).ready(function() {
            $('#select_year').change(checkMonthAndYearSubmit);
            $('#select_month').change(checkMonthAndYearSubmit);
            $('#select_type button').click(function() {
                const type = $(this).attr('data-type');
                $('#type').val(type);
                $('#form_filter').submit();
            })
        });
    </script>
@endsection