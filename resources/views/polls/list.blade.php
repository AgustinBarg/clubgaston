@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="panel panel-default">
            Encuestas
        </div>

        @if($polls != null)
            <table class="table table-bordered" id="incidentes-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Encuesta</th>
                        <th>Respuestas</th>
                        <th>Fecha</th>
                        <th>Detalle</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($polls as $poll)
                    <tr>
                        <td>{{ $poll->id }}</td>
                        <td>{{ $poll->question }}</td>
                        <td>{{ $poll->getVotes()}}</td>
                        <td>{{ $poll->created_at->format('d/m/Y - G:i\h\s') }}</td>
                        <td>
                            <a href="{{ route('polls.details', ['pollId' => $poll->id]) }}"> Ver</a>
                            <button data-id="{{$poll->id}}" class="delete" value="delete">Eliminar</button>

                            <form id="delete_form_{{$poll->id}}" style="display:none;" method="POST" action="{{ route('polls.delete', ['pollId' => $poll->id]) }}">
                                {!! csrf_field() !!}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay Encuestas.
        @endif
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function() {
            $('.delete').click(function() {
                const pollId = $(this).attr('data-id');
                swal({
                    title: "Eliminar Encuesta",
                    text: "Estás seguro de eliminar esta encuesta? (Los usuarios no podrán ver los resultados ni votar) ",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $('#delete_form_' + pollId).submit();
                    }
                });
            });
        })
    </script>
@endsection