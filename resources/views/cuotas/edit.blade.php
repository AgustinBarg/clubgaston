@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Modificar Cuota</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('cuotas.update') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('cuota_id') ? ' has-error' : '' }}">
                                <label for="id" class="col-md-4 control-label">Cuota ID</label>

                                <div class="col-md-6">
                                    <input readonly="true" id="id" type="number" min="0" class="form-control" name="id" value="{{ $cuota->id }}" required autofocus>

                                    @if ($errors->has('id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('pagoinformado') ? ' has-error' : '' }}">
                                <label for="pagoinformado" class="col-md-4 control-label">Pago Informado* (aaaa-mm-dd)</label>

                                <div class="col-md-6">
                                    <input id="pagoinformado" type="date" class="form-control" name="pagoinformado" value="{{ $cuota->pagoinformado }}">

                                    @if ($errors->has('pagoinformado'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pagoinformado') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group" {{ $errors->has('vencimiento') ? ' has-error' : '' }}>
                                <label for="vencimiento" class="col-md-4 control-label">Vencimiento (aaaa-mm-dd)</label>

                                <div class="col-md-6">
                                    <input id="vencimiento" type="date" class="form-control" name="vencimiento" value="{{ $cuota->vencimiento }}" required>
                                    @if ($errors->has('vencimiento'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('vencimiento') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('vencimiento') ? ' has-error' : '' }}">
                                <label for="saldo" class="col-md-4 control-label">Saldo*</label>

                                <div class="col-md-6">
                                    <input id="saldo" type="number" value="{{ $cuota->saldo }}" class="form-control" name="saldo">
                                    @if ($errors->has('saldo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('saldo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="detalle" class="col-md-4 control-label">Detalle*</label>

                                <div class="col-md-6">
                                    <input id="detalle" type="text" class="form-control" name="detalle" value="{{ $cuota->detalle }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Editar Cuota
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
