@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear nueva cuota</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('cuotas.post') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="col-md-4 control-label">Cuota para usuario @if($user != null){{ $user->name }}@endif:</label>
                                <div class="col-md-6">
                                    @if($users != null)
                                        <select class="form-control" id="user_id" name="user_id">
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    @elseif($user != null)

                                        <input id="user_id" class="form-control" name="user_id" value="{{ $user->id }}" readonly="true">
                                    @endif

                                    @if ($errors->has('user_id'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('pagoinformado') ? 'has-error' : '' }}">
                                <label for="pagoinformado" class="col-md-4 control-label">Pago Informado (aaaa-mm-dd)</label>

                                <div class="col-md-6">
                                    <input id="pagoinformado" type="date" class="form-control" name="pagoinformado">

                                    @if ($errors->has('pagoinformado'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pagoinformado') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('vencimiento') ? 'has-error' : '' }}">
                                <label for="vencimiento" class="col-md-4 control-label">Vencimiento (aaaa-mm-dd)</label>

                                <div class="col-md-6">
                                    <input id="vencimiento" type="date" class="form-control" name="vencimiento" required>
                                    @if ($errors->has('vencimiento'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('vencimiento') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('saldo') ? 'has-error' : '' }}">
                                <label for="saldo" class="col-md-4 control-label">Saldo</label>

                                <div class="col-md-6">
                                    <input id="saldo" type="number" step="0.01" class="form-control" name="saldo">
                                    @if ($errors->has('saldo'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('saldo') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('detalle') ? 'has-error' : '' }}">
                                <label for="detalle" class="col-md-4 control-label">Detalle</label>

                                <div class="col-md-6">
                                    <input id="detalle" type="text" class="form-control" name="detalle">
                                    @if ($errors->has('detalle'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('detalle') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Crear Cuota
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
