@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel-heading">Cuotas del ultimo mes</div>
        <div class="form-group">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <table class="table table-bordered" id="cuotas-table">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>usuario</th>
                        <th>pagoinformado</th>
                        <th>vencimiento</th>
                        <th>saldo</th>
                        <th>detalle</th>
                        <th>accion</th>
                    </tr>
                </thead>
                @foreach($cuotas as $cuota)
                    <tbody>
                        <td>{{ $cuota->id }}</td>
                        <td>{{ $cuota->userName }}</td>
                        <td>{{ $cuota->paymentStatusMessage }}</td>
                        <td>{{ $cuota->vencimiento }}</td>
                        <td>{{ $cuota->saldo }}</td>
                        <td>{{ $cuota->detalle }}</td>
                        <td>
                            <form method="POST" action="{{ route('cuotas.informarpago') }}">
                                <input type="hidden" name="id" value="{{ $cuota->id }}">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn">
                                    <span>Marcar Pago Informado</span>
                                </button>
                            </form>
                            <a href="{{ route('cuotas.edit', ['id' => $cuota->id])  }}" class=" btn btn-default">Editar cuota</a>
                            <a href="{{ route('cuotas.payment_data', ['id' => $cuota->id])  }}" class="btn btn-default">Ver pago</a>
                        </td>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection