@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel-heading">Cuotas de usuario {{ $user->name  }}</div>
        <div class="form-group">
            @if (count($cuotas) > 0)
                <table class="table table-bordered" id="cuotas-table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>pagoinformado</th>
                            <th>vencimiento</th>
                            <th>saldo</th>
                            <th>detalle</th>
                            <th>accion</th>
                        </tr>
                    </thead>
                        @foreach($cuotas as $cuota)
                            <tbody>
                                <td>{{ $cuota->id }}</td>
                                <td>{{ $cuota->pagoinformado }}</td>
                                <td>{{ $cuota->vencimiento }}</td>
                                <td>{{ $cuota->saldo }}</td>
                                <td>{{ $cuota->detalle }}</td>
                                <td>
                                    <form method="POST" action="{{ route('informarpago') }}">
                                        <input type="hidden" name="id" value="{{ $cuota->id }}">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="btn">
                                            <span>Marcar Pago Informado</span>
                                        </button>
                                    </form>
                                    <form method="POST" action="{{ route('cuotas.edit', ['id' => $cuota->id]) }}">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="btn">
                                            <span>Modificar cuota</span>
                                        </button>
                                    </form>
                                </td>
                            </tbody>
                        @endforeach
                </table>
            @else
                <div>Al usuario {{ $user->name }} no se le ha asignado aun ninguna cuota.</div>
                <li role="presentation" class="active"><a href="{{ route('cuotas.crear', ['id' => $user->id]) }}">Crear Cuota</a></li>
            @endif
        </div>
    </div>
@endsection


<script>
    $(".btn").on("submit", function(){
        return confirm("Esta seguro que desea marcar el pago informado?");
    });
</script>
