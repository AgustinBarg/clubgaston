@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Revisar pago</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            Fecha de pago:  {{ $cuota->pagoinformado }}
                        </div>
                        <div class="col-md-12">
                            Vencimiento:  {{ $cuota->vencimiento }}
                        </div>
                        <div class="col-md-12">
                            Saldo:  {{ $cuota->saldo }}
                        </div>
                        @if($cuota->detalle != null)
                            <div class="col-md-12">
                                Detalles:  {{ $cuota->detalle }}
                            </div>
                        @endif

                        @if($cuota->payment_ticket != null)
                            <div class="col-md-12">
                                <img src="{{ url('/') . '/images/' . $cuota->paymentTicket }}">
                            </div>
                        @endif

                        <a href="{{ route('cuotas.payment_approve', ['cuota_id' => $cuota->id])  }}" class="btn btn-default">Aprobar pago</a>
                        <a href="{{ route('cuotas.payment_reject', ['cuota_id' => $cuota->id])  }}" class="btn btn-default">Rechazar pago</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
