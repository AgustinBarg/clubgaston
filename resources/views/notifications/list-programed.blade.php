@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="panel panel-default">
            Notificaciones Programadas
        </div>

        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
        @if(session()->has('status'))
            <div class="alert alert-success">
                {{ session()->get('status') }}
            </div>
        @endif
        @if($notifications != null)
            <table class="table table-bordered" id="incidentes-table">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Mensaje</th>
                        <th>Tipo</th>
                        <th>Filtro</th>
                        <th>Fecha a Enviar</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($notifications as $notification)
                    <tr>
                        <td>{{ $notification->title }}</td>
                        <td>{{ $notification->message }}</td>
                        <td>{{ $notification->getType()}}</td>
                        <td>
                            @if ($notification->filter_type == 'single_user' and $notification->user)
                                {{$notification->user->name}} ({{$notification->user->email}})
                            @elseif ($notification->filter_type == 'interes' and $notification->interes)
                                Tema Interés: {{$notification->interes->name}}
                            @elseif ($notification->filter_type == 'sector')
                                Sector Lote: {{$notification->lote_sector}}
                            @else
                                {{ $notification->getFilterType()}}
                            @endif
                        </td>
                        <td>{{ $notification->schedule_for->format('d/m/Y - G:i\h\s') }}</td>
                        <td>
                            <form method="POST" class="delete" class="m-0" action="{{ route('notifications.programed.delete') }}">
                                <input type="hidden" name="id" class="delete" value="{{ $notification->id }}">
                                {!! csrf_field() !!}
                                <button type="submit" class="delete m-0" value="delete">eliminar
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            No hay notificaciones.
        @endif
    </div>
@stop