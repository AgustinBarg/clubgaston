@extends('layouts.app')

@section('content')
    {{ Form::open(array('url' => '/notifications/send')) }}
    <div class="container">
        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear Notificacion</div>
                    <div class="panel-body">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            {{ Form::label('title', 'Titulo') }}
                            {{ Form::input('text', 'title', '', ['id' => 'messasge_title', 'class' => 'form-control', 'placeholder' => 'Titulo', 'maxlength' => 40]) }}
                            <div id="message_title_characters" class="remaining-characters">0/40</div>
                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                            {{ Form::label('body', 'Mensaje') }}
                            {{ Form::textarea('body', '', ['id' => 'messasge_body', 'class' => 'form-control', 'placeholder' => 'Mensaje', 'maxlength' => 180]) }}
                            <div id="message_body_characters" class="remaining-characters">0/180</div>

                            @if ($errors->has('body'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('body') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                            {{ Form::label('type', 'Tipo') }}
                            <select class="form-control" id="type" name="type">
                                <option value="general">Texto Simple</option>
                                <option value="news">Noticias</option>
                                <option value="news-single">Noticia Individual</option>
                                <option value="polls">Encuestas</option>
                                <option value="polls-single">Encuesta Individual</option>
                                <option value="informes">Costa Informes</option>
                                <option value="lanzamientos">Lanzamiento</option>
                                <option value="lanzamiento-single">Lanzamiento Individual</option>
                                <option value="servicios">Guia de Servicios</option>
                                <option value="eventos">Eventos</option>
                                <option value="events-single">Evento Individual</option>
                                <option value="galerias">Galerias</option>
                                <option value="add-users">Añadir Usuarios</option>
                                <option value="camera">Camara en Vivo</option>
                                <option value="reservations">Reservas</option>
                                <option value="expenses">Expensas</option>
                                <option value="utilities">Datos Utiles</option>
                                <option value="map">Mapa</option>
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div id="form_type_id" class="form-group {{ $errors->has('type_id') ? 'has-error' : '' }}">
                            {{ Form::label('type_id', 'Id Individual') }}
                            {{ Form::input('number', 'type_id', '', ['class' => 'form-control', 'placeholder' => 'Id Individual']) }}
                            @if ($errors->has('type_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('type_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        {{ Form::submit('Enviar notificacion', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Fecha Envio</div>
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <div>
                                    <input type="radio" id="now" name="notification_time" value="now"
                                            @if (old('notification_time') != 'schedule') checked @endif>
                                    <label for="now">Enviar Ahora</label>
                                </div>
                                <div>
                                    <input type="radio" id="schedule" name="notification_time" value="schedule"
                                            @if (old('notification_time') == 'schedule') checked @endif>
                                    <label for="schedule">Programar Envio</label>
                                </div>
                            </div>
                        </div>
                        <div id="parameters_schedule_container" @if (old('notification_time') != 'schedule') style="display: none;" @endif>
                            <div class="form-group">
                                <label for="schedule_date" class="control-label">Día</label>
                                <input id="schedule_date" type="date" class="form-control" name="schedule_date" value="old('schedule_date')">
                            </div>
                            <div class="form-group">
                                <label for="schedule_horario" class="control-label">Hora</label>
                                <select class="form-control" id="schedule_horario" name="schedule_horario" value="old('schedule_horario')">
                                    @foreach ($times as $time)
                                        <option value="{{ $time }}">{{ $time }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Filtrar Usuarios</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="filter_type" class="control-label">Tipo de Filtro</label>
                            <select class="form-control" id="filter_type" name="filter_type">
                                <option value="all_users">Todos los Usuarios</option>
                                <option value="single_user">Solo un Usuario</option>
                                <option value="owner">Solo los Propietarios</option>
                                <option value="inquilino">Solo los Inquilinos</option>
                                <option value="morosos">Solo los Morosos</option>
                                <option value="sector">Por Sector</option>
                                <option value="interes">Por Tema Interés</option>
                            </select>
                        </div>

                        <div id="filter_sector" class="form-group filter-container d-none">
                            <label for="lote_sector">Sector</label>
                            <select class="form-control" id="lote_sector" name="lote_sector">
                                @foreach ($sectores as $sector)
                                    <option value="{{ $sector }}">{{ $sector }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="filter_interes" class="form-group filter-container d-none">
                            <label for="tema_interes_id">Interes</label>
                            <select class="form-control" id="tema_interes_id" name="tema_interes_id">
                                @foreach ($intereses as $interes)
                                    <option value="{{ $interes->id }}">{{ $interes->title }} ({{$interes->usuarios}})</option>
                                @endforeach
                            </select>
                        </div>

                        <div id="filter_single_user" class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }} filter-container d-none">
                            <label for="user_id">Usuario Individual</label>
                            <div>
                                <autocomplete></autocomplete>
                                @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
@endsection

@section('footer')
    <script>
        $(document).ready(function() {
            $('#messasge_title').keyup(function() {
                const count_chars = $('#messasge_title').val().length + '/40';
                $('#message_title_characters').html(count_chars);
            });

            $('#messasge_body').keyup(function() {
                const count_chars = $('#messasge_body').val().length + '/180';
                $('#message_body_characters').html(count_chars);
            });

            $('#filter_type').change(function() {
                $('.filter-container').addClass('d-none');
                $('#filter_' + $(this).val()).removeClass('d-none');
            })

            $("[name='notification_time']").change(function() {
                if ($(this).val() == 'schedule') {
                    $('#parameters_schedule_container').show();
                    $('#schedule_date').attr('required', 'required');
                    $('#schedule_horario').attr('required', 'required');
                } else {
                    $('#parameters_schedule_container').hide();
                    $('#schedule_date').removeAttr('required');
                    $('#schedule_horario').removeAttr('required');
                }
            });
        });
    </script>
@endsection