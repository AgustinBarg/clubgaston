@extends('layouts.app')

@push('scripts')
    <link href="{{ asset('css/reports.css') }}?date=20201202" rel="stylesheet">
    <style>
        .panel > .panel-body {
            min-height: auto !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Estado de Cuenta Usuarios</div>
           <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6">
                        <form method="POST" action="{{ route('reservas-gym.users-status') }}">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="control-label">Usuario</label>
                                <div>
                                    <autocomplete></autocomplete>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-default mt-2">Buscar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @if (isset($user))
            <div class="panel panel-default">
                <div class="panel-heading">{{$user->name}} ({{$user->type}})</div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">PAGOS</div>
                        <div class="panel-body">
                            @if (count($payments) > 0)
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Fecha</th>
                                            <th>Monto</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($payments as $payment)
                                            <tr>
                                                <td>{{$payment->id}}</td>
                                                <td>{{$payment->created_at}}</td>
                                                <td>${{$payment->amount}}</td>
                                                <td>{{$payment->getStatusAdmin()}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="text-center padding">No se encontraron pagos de Créditos para Gimnasio para este usuario</div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">PAQUETES</div>
                        <div class="panel-body">
                            @if (count($packages) > 0)
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Pago Id</th>
                                            <th>Fecha</th>
                                            <th>Creditos</th>
                                            <th>Vencimiento</th>
                                            <th>Final./Usado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($packages as $package)
                                            <tr>
                                                <td>{{$package->payment_id}}</td>
                                                <td>{{$package->created_at}}</td>
                                                <td>{{$package->amount}}</td>
                                                <td>{{$package->valid_to}}</td>
                                                <td>{{$package->finished ? 'SI' : 'NO'}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="text-center padding">No se encontraron Paquetes de Créditos para Gimnasio para este usuario</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">MOVIMIENTOS</div>
                        <div class="panel-body">
                            @if (count($movements) > 0)
                                <table class="table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Reserva ID?</th>
                                            <th>Tipo</th>
                                            <th>Monto</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($movements as $movement)
                                            <tr>
                                                <td>{{$movement->created_at}}</td>
                                                <td>{{$movement->reserva_id}}</td>
                                                <td>{{$movement->type}}</td>
                                                <td>{{$movement->amount > 0 ? '+' : ''}}{{$movement->amount}}</td>
                                                <td>{{$movement->subtotal}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="text-center padding">No se encontraron Movimientos de Créditos para Gimnasio para este usuario</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
        
    </div>
@stop
