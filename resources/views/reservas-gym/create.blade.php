@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Crear Nueva Reserva Gym</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('reservas-gym.new') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            <label for="user_id" class="col-md-4 control-label">Usuario</label>
                            <div class="col-md-6">
                                <autocomplete></autocomplete>
                                @if ($errors->has('user_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('fecha_reserva') ? 'has-error' : '' }}">
                            <label for="fecha_reserva" class="col-md-4 control-label">Fecha de reserva</label>

                            <div class="col-md-6">
                                <input id="fecha_reserva" required type="date" class="form-control" name="fecha_reserva"  value="{{ old('fecha_reserva') }}">

                                @if ($errors->has('fecha_reserva'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fecha_reserva') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('horario_reserva') ? ' has-error' : '' }}">
                            <label for="horario_reserva" class="col-md-4 control-label">Hora</label>
                            <div class="col-md-6">
                                <select class="form-control" required id="horario_reserva" name="horario_reserva">
                                    @foreach ($times as $time)
                                        <option @if(old('horario_reserva') == $time->time) selected @endif value="{{ $time->time }}">{{ substr($time->time, 0, 5) }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('horario_reserva'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('horario_reserva') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('notas') ? 'has-error' : '' }}">
                            <label for="notas" class="col-md-4 control-label">Notas</label>

                            <div class="col-md-6">
                                <textarea id="notas" class="form-control" name="notas">{{ old('notas') }}</textarea>

                                @if ($errors->has('notas'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('notas') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div id="checkPlaces_container"></div>
                            </div>
                        </div>

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{session('error')}}
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Crear Reserva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
@endsection