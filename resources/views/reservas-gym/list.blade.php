@extends('layouts.app')

@section('content')

    <div class="container">
      <a class="btn btn-primary pull-right exportar" href="{{route('reservas-gym.export')}}">Exportar Excel Reservas Gimnasio {{date('d-m-Y')}}</a>

        <div class="panel panel-default">
            Listado de Reservas Gimnasio
        </div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="panel-body">
            <form method="GET" class="form-inline" action="{{ route('reservas-gym.list', [
                    'order_by' => $orderBy, 
                    'order_direction' => $orderDirection,
                    'data' => $date
                ]) }}">
                <div class="form-group">
                    <input type="text" class="form-control" name="query" id="query" placeholder="Buscar por usuario...">
                    <input type="date" class="form-control" name="date" id="date">
                    <select name="validated" id="validated" class="form-control">
                        <option value="" @if($validated == null) selected="selected" @endif>Todas</option>
                        <option @if($validated === "1") selected="selected" @endif 
                                    value="1">Reservas Validadas</option>
                        <option @if($validated === "0") selected="selected" @endif 
                                    value="0">Reservas NO Validadas</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Buscar</button>
            </form>
        </div>

        @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        @if ($reservas)
            <table class="table table-bordered" id="users-table">
                <thead>
                <tr>
                    <th>id
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'id', 'order_direction' => 'asc', 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'id', 'order_direction' => 'desc', 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Usuario
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'user_id', 'order_direction' => 'asc', 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'user_id', 'order_direction' => 'desc', 'data' => $date ]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Fecha Inicio
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'fechaIngreso', 'order_direction' => 'asc', 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'fechaIngreso', 'order_direction' => 'desc', 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th>
                        Validada
                    </th>
                    <th>
                        Notas
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'notas', 'order_direction' => 'asc', 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-top"></span>
                        </a>
                        <a href="{{ route('reservas-gym.list', ['order_by' => 'notas', 'order_direction' => 'desc', 'data' => $date]) }}">
                            <span class="glyphicon glyphicon-triangle-bottom"></span>
                        </a>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($reservas as $reserva)
                        <tr>
                            <td>{{ $reserva->id }}</td>
                            <td>
                                {{ $reserva->user->name }}
                                <div>({{ $reserva->user->email }})</div>
                            </td>
                            <td>{{ $reserva->fechaInicio }}</td>
                            <td class="text-center">
                                @if ($reserva->validated)
                                    <span title="Validada" alt="Validada" class="text-success glyphicon glyphicon-check"></span>
                                    @if ($reserva->validated_by)
                                        <div>{{ $reserva->validator->name}}</div>
                                    @endif
                                @else
                                    <span title="No Validada" alt="No Validada" class="text-danger glyphicon glyphicon-remove"></span>
                                    <form method="POST" class="success" action="{{ route('reservas-gym.validate') }}">
                                        <input type="hidden" name="id" class="success" value="{{ $reserva->id }}">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="success btn-validate m-0" value="validate">validar
                                        </button>
                                    </form>
                                @endif
                            </td>
                            <td>{{ $reserva->notas }}</td>
                            <td>
                                <form method="POST" class="delete" action="{{ route('reservas-gym.delete') }}">
                                    <input type="hidden" name="id" class="delete" value="{{ $reserva->id }}">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="delete" value="delete">eliminar
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h4>No se encontraron reservas</h4>
        @endif

        @if($orderBy && $orderDirection)
            {{ $reservas->appends(['order_by' => $orderBy, 'order_direction' => $orderDirection, 'date' => $date ])->links() }}
        @else
            {{ $reservas->appends([ 'date' => $date ])->links() }}
        @endif
    </div>
@stop
