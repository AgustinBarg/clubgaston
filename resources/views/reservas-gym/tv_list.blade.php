@extends('layouts.tv')

@section('content')
<div class="container">
    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <td>DIA / HORA</td>
                @foreach ($times as $time)
                    <th>{{ substr($time->time, 0, 5)  }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($dates as $date)
                <tr>
                    <td>
                        {{ $date }}
                    </td>
                    @foreach($availables as $available)
                        @if($available['date'] == $date)
                            @foreach($available['places'] as $item)
                                @if ($item->vacantes > 0) 
                                    <td class="text-center space-reserva-free">
                                        <div class="text-white">
                                            {{ $item->vacantes }} / {{ $item->vacants}}
                                        </div>
                                    </td>
                                @else
                                    <td class="text-center space-reserva-occupied">
                                        <div class="text-white">
                                            {{ $item->vacantes }} / {{ $item->vacants}}
                                        </div>
                                    </td>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>

    <form method="GET" class="form-inline btn-hoy" action="{{ route('reservas-gym.tv_list') }}">
        <div class="form-group">
            <button type="submit" class="btn btn-default">Reservas de hoy</button>
        </div>
    </form>
    <form method="GET" class="form-inline" action="{{ route('reservas-gym.tv_list') }}">
        <div class="form-group">
            Seleccionar otra fecha <input type="date" value="{{ isset($reservas_date) ? $reservas_date : ''}}" class="form-control" name="fecha_reservas" id="fecha_reservas">
            <button type="submit" class="btn btn-default">Actualizar</button>
        </div>
        <p class="referencia"><span class="verde">Verde = Disponible</span><span class="rojo">Rojo = Reservado</span></p>
    </form>
</div>
@stop