@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  @if(Auth::user())  <div class="panel-heading">Hola {{Auth::user()->name}}</div>

                 <div class="panel-body">
                        Iniciaste sesion
                     <br>
                     <passport-clients></passport-clients>
                     <passport-authorized-clients></passport-authorized-clients>
                     <passport-personal-access-tokens></passport-personal-access-tokens>
                 </div>
                    @endif

                      @if(Auth::guest())   <div class="panel-body">
                          Por favor Inicia Sesion para continuar
                      </div>

                      @endif

                      @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                      @endif
                </div>
            </div>
        </div>
    </div>
@endsection
