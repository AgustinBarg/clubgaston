<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Costa Esmeralda') }}</title>

	<!-- Styles -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	@stack('scripts')
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-sm">
				<div class="text-center">
					<img class="d-block mx-auto img-fluid" style="max-height: 250px; padding-top:20px;" src="{{ asset('images/autorizaciones-cover-2.png') }}" alt="...">
					<p class="px-3 text-secondary">Ingresá tus datos y enviá el siguiente formulario para poder ingresar a Costa Esmeralda</p>
				</div>

				@if (session('error'))
					<div class="my-4 alert alert-danger p2">
						{{session('error')}}
					</div>
				@endif

				<form class="px-2 px-sm-4 pb-5 text-info" action="{{ route('authorizations-save', ['code' => $code]) }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="first_name">Nombre(s)</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-address-card-o"></i>
								</div>
							</div>
							<input value="{{old('first_name')}}" id="first_name" name="first_name" type="text" required="required" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="last_name">Apellido</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-address-card-o"></i>
								</div>
							</div>
							<input value="{{old('last_name')}}" id="last_name" name="last_name" type="text" required="required" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="document">DNI</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-address-card-o"></i>
								</div>
							</div>
							<input value="{{old('document')}}" id="document" name="document" type="number" required="required" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-envelope-o"></i>
								</div>
							</div>
							<input value="{{old('email')}}" id="email" name="email" type="text" required="required" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="patent">Patente</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text">
									<i class="fa fa-car"></i>
								</div>
							</div>
							<input value="{{old('patent')}}" id="patent" name="patent" type="text" required="required" class="form-control text-uppercase">
						</div>
					</div>
					<div class="form-group">
						<button style="background-color:#008e69;" name="submit" type="submit" class="btn btn-block text-white">
							<span class="ounded-pill">Enviar</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>