<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Costa Esmeralda') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    @stack('scripts')
</head>
<body>
	<div class="container">
        <div class="row">
          	<div class="col-sm">
			  	<img class="d-block mx-auto img-fluid" style="max-height: 250px; padding-top:20px;" src="{{ asset('images/autorizaciones-cover-2.png') }}" alt="...">
				<h4 class="px-3 text-success text-center">
					<div class="my-2">✓</div>
					¡Tus datos fueron enviados correctamente!
				</h4>
				<div class="px-3 text-dark font-weight-bold my-3">
					Fecha de validez:
					<div class="d-block">
						{{$authorization->valid_from->format('d/m/Y')}} - {{$authorization->valid_to->format('d/m/Y')}}
					</div>
				</div>
    			<p class="px-3 text-secondary">Dirigite a la entrada de visitas/proveedores, mostrale tu DNI al personal de vigilancia e indicale que el propietario ya te autorizó a ingresar desde la app de Costa</p>
    			<p class="px-3 text-dark font-weight-bold">¿Cómo llegar?</p>
				<p class="px-3 text-secondary">
					Acceso Norte <small>(viniendo desde Mar de Ajó)</small>
					<a class="text-primary d-block" target="_blank" href="https://www.google.com/maps/dir//-37.0028173,-56.8069189/@-37.0031793,-56.8076539,18z/data=!4m2!4m1!3e0">Ver mapa →</a>
				</p>
    			<p class="px-3 text-secondary">
					Acceso Sur <small>(viniendo dede Pinamar)</small>
					<a class="text-primary d-block" target="_blank" href="https://www.google.com/maps/dir//-37.0200472,-56.8184267/@-37.0209916,-56.8193118,18z/data=!4m2!4m1!3e0">Ver mapa →</a>
				</p>
        	</div>
		</div>
	</div>
</body>
</html>