<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Costa Esmeralda') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    @stack('scripts')
</head>
<body>
	<div class="container">
        <div class="row text-center">
          	<div class="col-sm">
			  	<img class="d-block mx-auto img-fluid" style="max-height: 250px; padding-top:20px;" src="{{ asset('images/autorizaciones-cover-2.png') }}" alt="...">
				<h4 class="px-3 text-danger">
					<div class="my-2">❌</div>
					¡Tus datos no pueden ser enviados!
				</h4>
          		<p class="px-3 mb-0 text-secondary">
					Es posible que el link o fecha de tu invitación haya expirado. 
				</p>
				<p class="px-3 my-0 text-secondary">
					Por favor comunicate con el propietario o inquilino que generó la autorización para que vuelva a intentarlo y te envíe un nuevo link.
				</p>
        	</div>
		</div>
	</div>
</body>
</html>