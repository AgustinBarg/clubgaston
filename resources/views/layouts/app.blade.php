<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Costa Esmeralda') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/print.css') }}?date=20201202" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?date=20211112" rel="stylesheet">
    <link href="{{ asset('css/hardcoded.css') }}?date=20211112" rel="stylesheet">

    @stack('scripts')
</head>
<body>
<div id="app">
    @if(Auth::user())
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                @if(Auth::user())
                    <a style="margin-bottom: 10px;margin-top: -10px;" class="navbar-brand" href="{{ url('/home') }}">
                        <img src="{{asset('images/logo-costa-white.png')}}"/>
                    </a>
                @endif
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;@if(Auth::user() && Auth::user()->role->id > 4)
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Usuarios
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('user.list') }}">Listado Usuarios</a></li>
                                <li><a href="{{ route('interests.list') }}">Listado Intereses</a></li>
                                @if(Auth::user() && Auth::user()->role->id == 5)
                                    <li><a href="{{ route('user.create-admin') }}">Crear Usuario Admin</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif
                </ul>

                @if(Auth::user() && (Auth::user()->role->id == 4 || Auth::user()->role->id == 5))
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Expensas
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a href="{{ route('cuotas.crear') }}">Crear Expensa</a></li>
                                <li role="presentation"><a href="{{ route ('cuotas.list') }}">Ver ultimas Expensas</a></li>
                            </ul>
                        </li>

                    </ul>
                @endif

                <ul class="nav navbar-nav">
                    &nbsp;@if(Auth::user() && (Auth::user()->role->id == 3 || Auth::user()->role->id == 5))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Incidentes
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('incidente.index') }}">Incidentes</a></li>
                                <li><a href="{{ route('incidente.create') }}">Crear incidente</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>

                <ul class="nav navbar-nav">
                    @if(Auth::user() && Auth::user()->role->id == 5)
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Notificaciones
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('notifications.index') }}">Notificaciones</a></li>
                                <li><a href="{{ route('notifications.programed') }}">Notificaciones Programadas</a></li>
                                <li><a href="{{ route('notifications.create') }}">Crear notificacion</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>

                <ul class="nav navbar-nav">
                    @if(Auth::user() && (Auth::user()->role->id == 2 || Auth::user()->role->id == 5))
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reservas
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('reservas.list') }}">Reservas</a></li>
                                <li><a href="{{ route('reservas.tv_list') }}" target="_blank">Reservas TV</a></li>
                                <li><a href="{{ route('reservas.create') }}">Crear reservas</a></li>
                                <hr>
                                <li><a href="{{ route('reservas-gym.list') }}">Reservas Gym</a></li>
                                <li><a href="{{ route('reservas-gym.tv_list') }}" target="_blank">Reservas Gym TV</a></li>
                                <li><a href="{{ route('reservas-gym.create') }}">Crear Reserva Gym</a></li>
                                <li><a href="{{ route('reservas-gym.users') }}">Estados Cuenta Gym</a></li>
                                <hr>
                                <li><a href="{{ route('reservas-ecuestre.list') }}">Reservas Ecuestre</a></li>
                                <li><a href="{{ route('reservas-ecuestre.create') }}">Crear Reserva Ecuestre</a></li>
                                <hr>
                                <li><a href="{{ route('reservas.config') }}">Configuración</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>

                <ul class="nav navbar-nav">
                    @if(Auth::user() && Auth::user()->role->id == 5)
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Encuestas
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('polls.index') }}">Encuestas</a></li>
                                <li><a href="{{ route('polls.reports') }}">Reportes Graficos</a></li>
                                <li><a href="{{ route('polls.create') }}">Crear Encuesta</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="">
                            <a href="#" class="" aria-expanded="false">
                                <span>  {{ Auth::user()->name }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Cerrar Sesion
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @endif

    @yield('content')
</div>

    <!-- Scripts -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/print.js') }}?date=20201202"></script>
    <script src="{{ asset('js/app.js') }}?date=20191123"></script>

    @yield('footer')
</body>
</html>
