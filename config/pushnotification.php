<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'AIzaSyA-4eU1shVtfMx3O-nKQEx7XrEO81bFhJY',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAvyizaNE:APA91bG8QacFahelx-nmHanoGX7o_Pf3oT2ZkV0j2CUHxFmneR-t_ODqX2GErb1pmzHA6wX2S6X_MifRuC-Af2MQ97O-T3bVBOeYfzeXk-MaSZ_yEnS2J4vd8Z0xGkNPkYKx5j44_V-Q',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/costa22.pem',
      'passPhrase' => 'Costa2022', //Optional
      'dry_run' => false
  ],
  'apns' => [
    'topic' => env('PUSH_CUSTOM_TOPIC'),
    'key' => env('PUSH_CUSTOM_KEY'),
    'team' => env('PUSH_CUSTOM_TEAM'),
    'file' => env('PUSH_CUSTOM_FILE')
  ]
];