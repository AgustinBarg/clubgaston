<?php

use Illuminate\Database\Seeder;

class SubAreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\SubArea::create([
            'id' => 1,
            'responsable_area_id' => 1,
            'name' => 'Horarios erroneos'
        ]);

        \App\SubArea::create([
            'id' => 2,
            'responsable_area_id' => 1,
            'name' => 'Faltante de material'
        ]);

        \App\SubArea::create([
            'id' => 3,
            'responsable_area_id' => 1,
            'name' => 'Profesores'
        ]);

        \App\SubArea::create([
            'id' => 4,
            'responsable_area_id' => 1,
            'name' => 'Requisito para clase'
        ]);

        \App\SubArea::create([
            'id' => 5,
            'responsable_area_id' => 2,
            'name' => 'Cables'
        ]);

        \App\SubArea::create([
            'id' => 6,
            'responsable_area_id' => 2,
            'name' => 'Postes'
        ]);
    }
}
