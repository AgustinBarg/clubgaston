<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'name' => 'frontend_user',
            'id' => 1
        ]);

        \App\Role::create([
            'name' => 'deportes',
            'id' => 2
        ]);

        \App\Role::create([
            'name' => 'mantenimiento',
            'id' => 3
        ]);

        \App\Role::create([
            'name' => 'pagos',
            'id' => 4
        ]);

        \App\Role::create([
            'name' => 'admin',
            'id' => 5
        ]);
    }
}
