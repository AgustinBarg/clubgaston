<?php

use Illuminate\Database\Seeder;

class SedesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Sede::create([
            'id' => 1,
            'name' => 'Sede principal'
        ]);
    }
}
