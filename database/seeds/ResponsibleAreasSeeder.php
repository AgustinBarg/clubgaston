<?php

use Illuminate\Database\Seeder;

class ResponsibleAreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ResponsibleArea::create([
            'id' => 1,
            'name' => 'Deportes',
        ]);

        \App\ResponsibleArea::create([
            'id' => 2,
            'name' => 'Mantenimiento',
        ]);

        \App\ResponsibleArea::create([
            'id' => 3,
            'name' => 'Administracion',
        ]);
    }
}
