<?php

use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Categorias::create([
            'id' => 1,
            'name' => 'Tenis',
            'icon' => 'random',
        ]);

        \App\Categorias::create([
            'id' => 2,
            'name' => 'Futbol',
            'icon' => 'random',
        ]);

        \App\Categorias::create([
            'id' =>3,
            'name' => 'Paddle',
            'icon' => 'random',
        ]);
    }
}
