<?php

use Illuminate\Database\Seeder;

class NewUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Matias Rodriguez',
            'email' => 'm.gaston.rodriguez@gmail.com',
            'codido' => '4XM1AB008',
            'password' => bcrypt('4XM1AB008'),
            'sede_id' => 1,
            'role_id' => 1,
            'ccdocumento' => '32891212',
            'dni' => '32891212'
        ]);
    }
}


