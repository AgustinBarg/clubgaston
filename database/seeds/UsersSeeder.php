<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'id' => 4,
            'name' => 'Jeronimo',
            'email' => 'jeronimocoello34@gmail.com',
            'password' => bcrypt('momo'),
            'sede_id' => 1,
            'role_id' => 5
        ]);
    }
}
