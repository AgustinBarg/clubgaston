<?php

use Illuminate\Database\Seeder;

class EspaciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Espacio::create([
            'id' => 1,
            'nombre' => 'cancha futbol 1',
            'descripcion' => 'Cancha de futbol',
            'horaInicio' => 8,
            'horaFin' => 20,
            'maxHoras' => 3,
            'categoria_id' => 2,
            'sede_id' => 1
        ]);

        \App\Espacio::create([
            'id' => 2,
            'nombre' => 'cancha futbol 2',
            'descripcion' => 'Cancha de futbol',
            'horaInicio' => 8,
            'horaFin' => 21,
            'maxHoras' => 3,
            'categoria_id' => 2,
            'sede_id' => 1
        ]);

        \App\Espacio::create([
            'id' => 3,
            'nombre' => 'cancha tenis 1',
            'descripcion' => 'Cancha de tenis',
            'horaInicio' => 8,
            'horaFin' => 20,
            'maxHoras' => 3,
            'categoria_id' => 1,
            'sede_id' => 1
        ]);

        \App\Espacio::create([
            'id' => 4,
            'nombre' => 'cancha paddle 1',
            'descripcion' => 'Cancha de paddle',
            'horaInicio' => 8,
            'horaFin' => 20,
            'maxHoras' => 3,
            'categoria_id' => 3,
            'sede_id' => 1
        ]);
    }
}
