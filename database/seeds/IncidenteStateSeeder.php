<?php

use Illuminate\Database\Seeder;

class IncidenteStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\IncidenteState::create([
            'id' => 1,
            'name' => 'Abierto'
        ]);

        \App\IncidenteState::create([
            'id' => 2,
            'name' => 'Cerrado'
        ]);

        \App\IncidenteState::create([
            'id' => 3,
            'name' => 'Cerrado por Admin'
        ]);
    }
}
