<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(SedesSeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(UsersSeeder::class);
         $this->call(CategoriasSeeder::class);
         $this->call(ResponsibleAreasSeeder::class);
         $this->call(EspaciosSeeder::class);
         $this->call(SubAreasSeeder::class);
         $this->call(IncidenteStateSeeder::class);
    }
}
