<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('facebook_id')->nullable();
                $table->string('name');
                $table->string('email');
                $table->string('password')->nullable();
                $table->integer('role_id')->unsigned();
                $table->foreign('role_id')->references('id')->on('roles');
                $table->rememberToken();
                $table->integer('sede_id')->unsigned()->nullable();
                $table->foreign('sede_id')->references('id')->on('sedes');
                $table->string('ccdocumento')->nullable();
                $table->string('dni')->nullable();
                $table->string('id_cliente')->nullable();
                $table->string('lote')->nullable();
                $table->string('lote_ubicacion')->nullable();
                $table->string('telefono_movil')->nullable();
                $table->string('codigo')->nullable();
                $table->string('activado')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
