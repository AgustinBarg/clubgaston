<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldValidatedToReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservas', function (Blueprint $table) {
            $table->boolean('validated')->default(true);
            $table->integer('validated_by')->unsigned()->nullable();
            $table->foreign('validated_by')->references('id')->on('users');
            $table->dateTimeTz('validated_at')->nullable();
        });

        Schema::table('reservas_gym', function (Blueprint $table) {
            $table->boolean('validated')->default(true);
            $table->integer('validated_by')->unsigned()->nullable();
            $table->foreign('validated_by')->references('id')->on('users');
            $table->dateTimeTz('validated_at')->nullable();
        });

        Schema::table('espacios', function (Blueprint $table) {
            $table->string('qr_validation')->nullable();
        });

        Schema::table('reservas_config', function (Blueprint $table) {
            $table->integer('reservas_max_days')->default(7);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
