<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncidentImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tickets_incidentes_images')) {
            Schema::create('tickets_incidentes_images', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('ticket_incidente_id')->unsigned();
                $table->foreign('ticket_incidente_id')->references('id')->on('tickets_incidentes');
                $table->string('image_path')->nullable();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('tickets_messages_images')) {
            Schema::create('tickets_messages_images', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('ticket_message_id')->unsigned();
                $table->foreign('ticket_message_id')->references('id')->on('tickets_messages');
                $table->string('image_path')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
