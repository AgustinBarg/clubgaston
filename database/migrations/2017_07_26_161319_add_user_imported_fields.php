<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserImportedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('users')) {
            Schema::table('users', function($table)
            {
                if (!Schema::hasColumn('users', 'ccdocumento')) {
                    $table->string('ccdocumento');
                }

                if (!Schema::hasColumn('users', 'dni')) {
                    $table->string('dni');
                }

                if (!Schema::hasColumn('users', 'id_cliente')) {
                    $table->integer('id_cliente')->nullable();
                }

                if (!Schema::hasColumn('users', 'lote')) {
                    $table->string('lote');
                }

                if (!Schema::hasColumn('users', 'lote_ubicacion')) {
                    $table->string('lote_ubicacion');
                }

                if (!Schema::hasColumn('users', 'telefono_movil')) {
                    $table->string('telefono_movil');
                }

                if (!Schema::hasColumn('users', 'codigo')) {
                    $table->string('codigo');
                }

                if (!Schema::hasColumn('users', 'activado')) {
                    $table->boolean('activado')->default(false);
                } 
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
