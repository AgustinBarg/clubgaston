<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservasEcuestre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reservas')) {
            Schema::table('reservas', function($table) {
                $table->decimal('amount')->nullable();
            });
        }

        if (!Schema::hasTable('reservas_ecuestre')) {
            Schema::create('reservas_ecuestre', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer('espacio_id')->unsigned();
                $table->foreign('espacio_id')->references('id')->on('espacios_ecuestre');

                $table->decimal('amount')->nullable();
                $table->integer('places')->default(1);

                $table->date('fechaInicio');
                $table->date('fechaFin');

                $table->string('notas')->nullable();
                $table->boolean('deleted')->default(false);
                $table->integer('updated_by')->nullable();

                $table->integer('payment_id')->unsigned()->nullable();
                $table->foreign('payment_id')->references('id')->on('payments');

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
