<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReservaStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reservas')) {
            Schema::table('reservas', function($table) {
                $table->string('status')->default('pending');
            });
        }

        if (Schema::hasTable('reservas_ecuestre')) {
            Schema::table('reservas_ecuestre', function($table) {
                $table->string('status')->default('pending');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
