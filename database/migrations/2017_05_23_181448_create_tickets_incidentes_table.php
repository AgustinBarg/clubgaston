<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsIncidentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tickets_incidentes')) {
            Schema::create('tickets_incidentes', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->string('title');
                $table->string('detalle');
                $table->integer('state_id')->unsigned();
                $table->foreign('state_id')->references('id')->on('incidente_states');
                $table->integer('tema_id')->unsigned();
                $table->foreign('tema_id')->references('id')->on('sub_areas');
                $table->string('image_path')->nullable();
                $table->string('location')->nullable();
                $table->string('latlong_location')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_incidentes');
    }
}
