<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsEcuestre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('espacios')) {
            Schema::table('espacios', function($table) {
                $table->decimal('price_owner')->default(0);
                $table->decimal('price_inquilino')->default(0);
            });
        }

        if (!Schema::hasTable('payments')) {
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');

                $table->string('description');
                $table->decimal('amount');
                $table->string('status');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');

                $table->timestamps();
            });
        }

        if (Schema::hasTable('reservas')) {
            Schema::table('reservas', function($table) {
                $table->integer('payment_id')->unsigned()->nullable();
                $table->foreign('payment_id')->references('id')->on('payments');
            });
        }

        if (!Schema::hasTable('espacios_ecuestre')) {
            Schema::create('espacios_ecuestre', function (Blueprint $table) {
                $table->increments('id');

                $table->string('nombre');
                $table->string('descripcion');

                $table->integer('vacants');
                $table->integer('places')->nullable();

                $table->boolean('shared')->default(false);

                $table->decimal('price_owner');
                $table->decimal('price_inquilino');

                $table->string('imageFile')->nullable();

                $table->integer('category_id')->nullable();

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
