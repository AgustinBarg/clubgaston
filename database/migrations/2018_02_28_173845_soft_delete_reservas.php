<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeleteReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        if (Schema::hasTable('reservas')) {
            Schema::table('reservas', function($table)
            {
                if (!Schema::hasColumn('reservas', 'deleted')) {
                    $table->boolean('deleted')->default(false);
                }

                if (!Schema::hasColumn('reservas', 'updated_by')) {
                    $table->integer('updated_by')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
