<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubareas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sub_areas')) {
            Schema::create('sub_areas', function(Blueprint $table){
               $table->increments('id');
                $table->integer('responsable_area_id')->unsigned();
                $table->foreign('responsable_area_id')->references('id')->on('responsible_areas');
                $table->string('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_areas');
    }
}
