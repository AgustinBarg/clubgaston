<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasTable('payments')) {
            Schema::table('payments', function($table) {
                if (!Schema::hasColumn('payments', 'type')) {
                    $table->string('type')->default('reserva');
                }
                if (!Schema::hasColumn('payments', 'method')) {
                    $table->string('method')->default('mercadopago');
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
