<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserLotesAndRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        if (Schema::hasTable('users')) {
            Schema::table('users', function($table)
            {
                if (!Schema::hasColumn('users', 'invited_by')) {
                    $table->integer('invited_by')->nullable();
                }

                if (!Schema::hasColumn('users', 'type')) {
                    $table->string('type')->default('owner');
                }
            });
        }

        if (!Schema::hasTable('user_lotes')) {
            Schema::create('user_lotes', function(Blueprint $table){
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer('id_lote');
                $table->integer('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
