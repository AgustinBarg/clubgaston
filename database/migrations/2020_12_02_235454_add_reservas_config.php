<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReservasConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reservas_config')) {
            Schema::create('reservas_config', function (Blueprint $table) {
                $table->increments('id');

                $table->boolean('ecuestre_allow')->default(true);

                $table->boolean('gym_allow')->default(true);
                $table->boolean('gym_allow_credits')->default(true);
                $table->integer('gym_max_hour')->unsigned()->default(10);
                $table->time('gym_inquilino_inicio', 0);	
                $table->time('gym_inquilino_fin', 0);	
                
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
