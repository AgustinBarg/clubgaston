<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEncuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('polls')) {
            Schema::create('polls', function (Blueprint $table) {
                $table->increments('id');
                $table->string('question');
                $table->date('valid_from');
                $table->date('valid_to');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('poll_options')) {
            Schema::create('poll_options', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('poll_id')->unsigned();
                $table->foreign('poll_id')->references('id')->on('polls');
                $table->string('text');
                $table->boolean('allow_text')->default(false);
                $table->integer('votes')->default(0);
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('poll_answers')) {
            Schema::create('poll_answers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('poll_option_id')->unsigned();
                $table->foreign('poll_option_id')->references('id')->on('poll_options');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->string('text')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
