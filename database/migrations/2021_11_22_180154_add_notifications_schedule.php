<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications_scheduled', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('message');

            $table->string('type')->nullable();
            $table->string('type_id')->nullable();
            $table->string('filter_type')->nullable();

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('sent')->nullable();
            $table->dateTimeTz('schedule_for');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
