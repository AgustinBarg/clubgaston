<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsInteresAndSectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notifications_scheduled', function (Blueprint $table) {
            $table->integer('tema_interes_id')->unsigned()->nullable();
            $table->foreign('tema_interes_id')->references('id')->on('interests');

            $table->string('lote_sector')->nullable();
        });

        Schema::table('notifications', function (Blueprint $table) {
            $table->integer('tema_interes_id')->unsigned()->nullable();
            $table->foreign('tema_interes_id')->references('id')->on('interests');

            $table->string('lote_sector')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
