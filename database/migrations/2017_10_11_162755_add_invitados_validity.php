<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvitadosValidity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        if (Schema::hasTable('users')) {
            Schema::table('users', function($table)
            {
                if (!Schema::hasColumn('users', 'invited_valid_from')) {
                    $table->date('invited_valid_from')->nullable();
                }

                if (!Schema::hasColumn('users', 'invited_valid_to')) {
                    $table->date('invited_valid_to')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
