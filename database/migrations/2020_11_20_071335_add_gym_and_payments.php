<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGymAndPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('reservas_gym')) {
            Schema::create('reservas_gym', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');

                $table->dateTimeTz('fechaInicio');
                $table->dateTimeTz('fechaFin');

                $table->string('notas')->nullable();

                $table->boolean('deleted')->default(false);
                $table->integer('updated_by')->nullable();
                
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('user_credits_packages')) {
            Schema::create('user_credits_packages', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');

                $table->integer('payment_id')->unsigned();
                $table->foreign('payment_id')->references('id')->on('payments');

                $table->decimal('amount')->nullable();
                $table->date('valid_to');

                $table->boolean('finished')->default(false);
                
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('user_credits')) {
            Schema::create('user_credits', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');

                $table->integer('reserva_id')->unsigned()->nullable();
                $table->foreign('reserva_id')->references('id')->on('reservas_gym');

                $table->integer('credits_package')->unsigned()->nullable();
                $table->foreign('credits_package')->references('id')->on('user_credits_packages');

                $table->string('type')->default('compra');  // compra / reserva / vencimiento
                
                $table->integer('amount')->default(0);
                $table->integer('subtotal')->default(0);
                
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
