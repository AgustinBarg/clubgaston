<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspaciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('espacios')) {
            Schema::create('espacios', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre');
                $table->string('descripcion');
                $table->integer('horaInicio');
                $table->integer('horaFin');
                $table->integer('maxHoras');
                $table->string('imageFile')->nullable();
                $table->integer('categoria_id')->unsigned();
                $table->foreign('categoria_id')->references('id')->on('categorias');
                $table->integer('sede_id')->unsigned();
                $table->foreign('sede_id')->references('id')->on('sedes');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espacios');
    }
}
